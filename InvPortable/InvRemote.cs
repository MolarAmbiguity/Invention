﻿/*! 10 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class WebHost
  {
    public WebHost(string Host, int Port, Inv.Persist.Governor Governor)
    {
      this.Protocol = new WebProtocol(Governor);

      this.SocketServer = new Inv.Tcp.Server(Host, Port);
      SocketServer.ConnectEvent += (Channel) =>
      {
        var Session = new WebSession(this, Channel);

        lock (SessionDictionary)
          SessionDictionary.Add(Channel, Session);

        if (ConnectEvent != null)
          ConnectEvent(Session);
      };
      SocketServer.DisconnectEvent += (Channel) =>
      {
        WebSession Session = null;

        lock (SessionDictionary)
        {
          Session = SessionDictionary.GetValueOrDefault(Channel);
          SessionDictionary.Remove(Channel);
        }

        if (Session != null)
        {
          if (DisconnectEvent != null)
            DisconnectEvent(Session);
        }
      };

      this.TypeSwitch = new Inv.TypeSwitch<WebSession>();
      this.SessionDictionary = new Dictionary<Inv.Tcp.Channel, WebSession>();
    }

    public Inv.WebProtocol Protocol { get; private set; }
    public event Action<Inv.WebSession> ConnectEvent;
    public event Action<Inv.WebSession> DisconnectEvent;

    public void Start()
    {
      SocketServer.Start();
    }
    public void Stop()
    {
      SocketServer.Stop();
    }
    public void Compile<TMessage>(object Owner)
    {
      TypeSwitch.Compile<TMessage>(Owner);
    }
    public void Compile<TMessage>(Action<Inv.WebSession, TMessage> Action)
    {
      TypeSwitch.Compile<TMessage>(Action);
    }
    public void SendAll<TData>(TData DataValue)
    {
      var DataBuffer = Protocol.Encode(DataValue);

      lock (SessionDictionary)
      {
        SessionDictionary.RemoveAll(D => !D.Value.IsActive);

        foreach (var Session in SessionDictionary.Values)
          Session.SendResponse(DataBuffer);
      }
    }

    internal void Receive(WebSession Session, object DataValue)
    {
      TypeSwitch.Execute(Session, DataValue);
    }
    internal void RemoveSession(WebSession Session)
    {
      lock (SessionDictionary)
        SessionDictionary.Remove(Session.TcpChannel);
    }

    private Inv.Tcp.Server SocketServer;
    private Dictionary<Inv.Tcp.Channel, WebSession> SessionDictionary;
    private Inv.TypeSwitch<WebSession> TypeSwitch;
  }

  public sealed class WebSession
  {
    internal WebSession(Inv.WebHost WebHost, Inv.Tcp.Channel TcpChannel)
    {
      this.WebHost = WebHost;
      this.TcpChannel = TcpChannel;
      this.IsActive = true;

      this.InputStream = new WebInputStream(TcpChannel.InputStream);
      this.OutputStream = new WebOutputStream(TcpChannel.OutputStream);

      this.ReceiveTask = System.Threading.Tasks.Task.Run(() =>
      {
        try
        {
          while (IsActive)
          {
            byte[] DataBuffer = null;

            var Marker = InputStream.PeekMarker();

            if (Marker == WebMarker.LinkRequest)
            {
              InputStream.ReadMarker(WebMarker.LinkRequest);

              var DataLength = InputStream.ReadInt32();

              if (DataLength > 0)
              {
                DataBuffer = new byte[DataLength];
                InputStream.ReadBuffer(DataBuffer);
              }
            }
            else if (Marker == WebMarker.Terminal)
            {
              this.IsActive = false;
            }

            if (DataBuffer != null)
            {
              Task.Run(() =>
              {
                var DataValue = WebHost.Protocol.Decode<object>(DataBuffer);
                WebHost.Receive(this, DataValue);
              });
            }
          }
        }
        catch
        {
          this.IsActive = false;
        }
      });
    }

    public bool IsActive { get; private set; }

    public void Call<TRequest, TResponse>(TRequest Request, TResponse Response)
    {
      SendRequest(Request);
      ReceiveResponse(Response);
    }
    public void SendResponse<TData>(TData DataValue)
    {
      var DataBuffer = WebHost.Protocol.Encode(DataValue);

      OutputStream.WriteMarker(WebMarker.LinkResponse);
      OutputStream.WriteInt32(DataBuffer.Length);
      OutputStream.WriteBuffer(DataBuffer);
    }
    public void ReceiveRequest<TData>(TData DataValue)
    {
      InputStream.ReadMarker(WebMarker.LinkRequest);

      var DataLength = InputStream.ReadInt32();

      var DataBuffer = new byte[DataLength];
      InputStream.ReadBuffer(DataBuffer);

      WebHost.Protocol.Decode(DataBuffer, DataValue);
    }
    public void SendRequest<TData>(TData DataValue)
    {
      var DataBuffer = WebHost.Protocol.Encode(DataValue);

      OutputStream.WriteMarker(WebMarker.HostRequest);
      OutputStream.WriteInt32(DataBuffer.Length);
      OutputStream.WriteBuffer(DataBuffer);
    }
    public void ReceiveResponse<TData>(TData DataValue)
    {
      InputStream.ReadMarker(WebMarker.HostResponse);

      var DataLength = InputStream.ReadInt32();

      var DataBuffer = new byte[DataLength];
      InputStream.ReadBuffer(DataBuffer);

      WebHost.Protocol.Decode(DataBuffer, DataValue);
    }

    internal Inv.Tcp.Channel TcpChannel { get; private set; }

    private Inv.WebHost WebHost;
    private System.Threading.Tasks.Task ReceiveTask;
    private WebInputStream InputStream;
    private WebOutputStream OutputStream;
  }
}