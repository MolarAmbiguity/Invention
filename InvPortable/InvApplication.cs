﻿/*! 122 !*/
#if DEBUG
//#define TRADE_PANEL_CHANGE
//#define TRADE_PANEL_RENDER
#endif
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.Android, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.iOS, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.Web, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.Wpf, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.Wua, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Inv.Uwa, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]

namespace Inv
{
  internal interface Platform
  {
    int ThreadAffinity();
    void CalendarShowPicker(CalendarPicker CalendarPicker);
    bool EmailSendMessage(EmailMessage EmailMessage);
    bool PhoneIsSupported { get; }
    bool PhoneDial(string PhoneNumber);
    bool PhoneSMS(string PhoneNumber);
    long DirectoryGetLengthFile(File File);
    DateTime DirectoryGetLastWriteTimeUtcFile(File File);
    void DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp);
    System.IO.Stream DirectoryCreateFile(File File);
    System.IO.Stream DirectoryAppendFile(File File);
    System.IO.Stream DirectoryOpenFile(File File);
    bool DirectoryExistsFile(File File);
    void DirectoryDeleteFile(File File);
    void DirectoryCopyFile(File SourceFile, File TargetFile);
    void DirectoryMoveFile(File SourceFile, File TargetFile);
    IEnumerable<File> DirectoryGetFolderFiles(Folder Folder, string FileMask);
    System.IO.Stream DirectoryOpenAsset(Asset Asset);
    bool LocationIsSupported { get; }
    void LocationLookup(LocationLookup LocationLookup);
    void AudioPlaySound(Inv.Sound Sound, float Volume);
    void WindowAsynchronise(Action Action);
    Modifier WindowGetModifier();
    long ProcessMemoryBytes();
    void WebSocketConnect(WebSocket Socket);
    void WebSocketDisconnect(WebSocket Socket);
    void WebLaunchUri(Uri Uri);
    void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
  }

  public sealed class Application
  {
    public Application()
    {
      this.Device = new Device(this);
      this.Window = new Window(this);
      this.Directory = new Directory(this);
      this.Audio = new Audio(this);
      this.Location = new Location(this);
      this.Calendar = new Calendar(this);
      this.Market = new Market(this);
      this.Web = new Web(this);
      this.Email = new Email(this);
      this.Phone = new Phone(this);
    }

    public string Title { get; set; }
    public bool IsExit { get; private set; }
    public Device Device { get; private set; }
    public Window Window { get; private set; }
    public Directory Directory { get; private set; }
    public Audio Audio { get; private set; }
    public Location Location { get; private set; }
    public Calendar Calendar { get; private set; }
    public Market Market { get; private set; }
    public Web Web { get; private set; }
    public Email Email { get; private set; }
    public Phone Phone { get; private set; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action SuspendEvent;
    public event Action ResumeEvent;
    public event Func<bool> ExitQuery;
    public event Action<Exception> HandleExceptionEvent;

    public void Exit()
    {
      CheckThreadAffinity();

      this.IsExit = true;
    }
    public float GetProcessMemoryMB()
    {
      return Platform.ProcessMemoryBytes() / 1024.0F / 1024.0F;
    }

    internal Platform Platform { get; set; }
#if DEBUG
    internal int ThreadAffinity { get; private set; }
#endif

    internal void StartInvoke()
    {
      CheckThreadAffinity();

      if (StartEvent != null)
        StartEvent();
    }
    internal void StopInvoke()
    {
      CheckThreadAffinity();

      if (StopEvent != null)
        StopEvent();
    }
    internal void SuspendInvoke()
    {
      CheckThreadAffinity();

      if (SuspendEvent != null)
        SuspendEvent();
    }
    internal void ResumeInvoke()
    {
      CheckThreadAffinity();

      if (ResumeEvent != null)
        ResumeEvent();
    }
    internal bool ExitQueryInvoke()
    {
      CheckThreadAffinity();

      if (ExitQuery != null)
        return ExitQuery();
      else
        return true;
    }
    internal void HandleExceptionInvoke(Exception Exception)
    {
      CheckThreadAffinity();

      if (HandleExceptionEvent != null)
        HandleExceptionEvent(Exception);
    }

    internal void Begin()
    {
      this.IsExit = false;
#if DEBUG
      this.ThreadAffinity = Platform.ThreadAffinity();
#endif
    }
    [Conditional("DEBUG")]
    internal void CheckThreadAffinity()
    {
#if DEBUG
      if (ThreadAffinity != Platform.ThreadAffinity())
        throw new Exception("Thread affinity mismatch.");
#endif
    }
  }

  public sealed class Device
  {
    internal Device(Application Application)
    {
      this.Application = Application;
    }

    public string Name { get; internal set; }
    public string Model { get; internal set; }
    public string System { get; internal set; }

    internal Application Application { get; private set; }
  }

  public sealed class Calendar
  {
    internal Calendar(Application Application)
    {
      this.Application = Application;
    }

    public CalendarPicker NewDatePicker()
    {
      return new CalendarPicker(this, true, false);
    }
    public CalendarPicker NewTimePicker()
    {
      return new CalendarPicker(this, false, true);
    }

    internal Application Application { get; private set; }
  }

  public sealed class CalendarPicker
  {
    internal CalendarPicker(Calendar Calendar, bool SetDate, bool SetTime)
    {
      this.Calendar = Calendar;
      this.SetDate = SetDate;
      this.SetTime = SetTime;
    }

    public bool SetDate { get; private set; }
    public bool SetTime { get; private set; }
    public DateTime Value { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;

    public void Show()
    {
      Calendar.Application.Platform.CalendarShowPicker(this);
    }

    internal void SelectInvoke()
    {
      if (SelectEvent != null)
        SelectEvent();
    }
    internal void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    private Calendar Calendar;
  }

  public sealed class Market
  {
    internal Market(Application Application)
    {
      this.Application = Application;
    }

    public void Browse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Application.Platform.MarketBrowse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }

    internal Application Application { get; private set; }
  }

  public sealed class Email
  {
    internal Email(Application Application)
    {
      this.Application = Application;
    }

    public EmailMessage NewMessage()
    {
      return new EmailMessage(this);
    }

    internal Application Application { get; private set; }
  }

  public sealed class Phone
  {
    internal Phone(Application Application)
    {
      this.Application = Application;
    }

    public bool IsSupported
    {
      get { return Application.Platform.PhoneIsSupported; }
    }

    public bool Dial(string Number)
    {
      return Application.Platform.PhoneDial(Number);
    }
    public bool SMS(string Number)
    {
      return Application.Platform.PhoneSMS(Number);
    }

    private Application Application;
  }

  public sealed class EmailMessage
  {
    internal EmailMessage(Email Email)
    {
      this.Email = Email;
      this.ToList = new Inv.DistinctList<EmailRecipient>();
      this.AttachmentList = new Inv.DistinctList<EmailAttachment>();
    }

    public string Subject { get; set; }
    public string Body { get; set; }

    public void To(string RecipientName, string RecipientAddress)
    {
      ToList.Add(new EmailRecipient(RecipientName, RecipientAddress));
    }
    public void Attach(string DisplayName, File File)
    {
      AttachmentList.Add(new EmailAttachment(DisplayName, File));
    }

    public bool Send()
    {
      AttachmentList.RemoveAll(A => !A.File.Exists());

      return Email.Application.Platform.EmailSendMessage(this);
    }

    internal IEnumerable<EmailRecipient> GetTos()
    {
      return ToList;
    }
    internal IEnumerable<EmailAttachment> GetAttachments()
    {
      return AttachmentList;
    }
    internal bool HasAttachments()
    {
      return AttachmentList.Count > 0;
    }

    private Email Email;
    private Inv.DistinctList<EmailRecipient> ToList;
    private Inv.DistinctList<EmailAttachment> AttachmentList;
  }

  public sealed class EmailRecipient
  {
    internal EmailRecipient(string Name, string Address)
    {
      this.Name = Name;
      this.Address = Address;
    }

    public string Name { get; private set; }
    public string Address { get; private set; }
  }

  public sealed class EmailAttachment
  {
    internal EmailAttachment(string Name, File File)
    {
      this.Name = Name;
      this.File = File;
    }

    public string Name { get; private set; }
    public File File { get; private set; }
  }

  public sealed class Audio
  {
    internal Audio(Application Application)
    {
      this.Application = Application;
    }

    public void SetMuted(bool IsMuted)
    {
      this.IsMuted = IsMuted;
    }
    public void Play(Inv.Sound Sound, float Volume = 1.0F)
    {
      Debug.Assert(Volume >= 0.0F && Volume <= 1.0F, "Play volume must be between 0.0 and 1.0.");

      if (!IsMuted)
        Application.Platform.AudioPlaySound(Sound, Volume);
    }

    private Application Application;
    private bool IsMuted;
  }

  public sealed class Directory
  {
    internal Directory(Application Application)
    {
      this.Application = Application;
      this.RootFolder = new Folder(this, null);
    }

    public Folder RootFolder { get; private set; }

    public Folder NewFolder(string Name)
    {
      return new Folder(this, Name);
    }
    public Asset NewAsset(string Name)
    {
      return new Asset(this, Name);
    }

    internal Application Application { get; private set; }
  }

  public sealed class Folder
  {
    internal Folder(Directory Directory, string Name)
    {
      this.Directory = Directory;
      this.Name = Name;
    }

    public string Name { get; private set; }

    public File NewFile(string Name)
    {
      return new File(this, Name);
    }
    public IEnumerable<File> GetFiles(string Mask)
    {
      return Directory.Application.Platform.DirectoryGetFolderFiles(this, Mask);
    }

    internal Directory Directory { get; private set; }
  }

  public sealed class Asset
  {
    internal Asset(Directory Directory, string Name)
    {
      this.Directory = Directory;
      this.Name = Name;
    }

    public Directory Directory { get; private set; }
    public string Name { get; private set; }
    public string Title
    {
#if NET40
      get 
      { 
        var LastIndex = Name.LastIndexOf('.');

        if (LastIndex < 0)
          return Name;
        else
          return Name.Substring(0, LastIndex);
      }
#else
      get { return System.IO.Path.GetFileNameWithoutExtension(Name); }
#endif
    }
    public string Extension
    {
#if NET40
      get 
      { 
        var LastIndex = Name.LastIndexOf('.');

        if (LastIndex < 0)
          return "";
        else
          return Name.Substring(LastIndex);
      }
#else
      get { return System.IO.Path.GetExtension(Name); }
#endif
    }

    public System.IO.Stream Open()
    {
      return Directory.Application.Platform.DirectoryOpenAsset(this);
    }
  }

  public sealed class File
  {
    internal File(Folder Folder, string Name)
    {
      this.Folder = Folder;
      this.Name = Name;
    }

    public Folder Folder { get; private set; }
    public string Name { get; private set; }
    public string Title
    {
#if NET40
      get 
      { 
        var LastIndex = Name.LastIndexOf('.');

        if (LastIndex < 0)
          return Name;
        else
          return Name.Substring(0, LastIndex);
      }
#else
      get { return System.IO.Path.GetFileNameWithoutExtension(Name); }
#endif
    }
    public string Extension
    {
#if NET40
      get 
      { 
        var LastIndex = Name.LastIndexOf('.');

        if (LastIndex < 0)
          return "";
        else
          return Name.Substring(LastIndex);
      }
#else
      get { return System.IO.Path.GetExtension(Name); }
#endif
    }

    public long GetLength()
    {
      return Folder.Directory.Application.Platform.DirectoryGetLengthFile(this);
    }
    public DateTime GetLastWriteTimeUtc()
    {
      return Folder.Directory.Application.Platform.DirectoryGetLastWriteTimeUtcFile(this);
    }
    public void SetLastWriteTimeUtc(DateTime Timestamp)
    {
      Folder.Directory.Application.Platform.DirectorySetLastWriteTimeUtcFile(this, Timestamp);
    }
    public System.IO.Stream Create()
    {
      return Folder.Directory.Application.Platform.DirectoryCreateFile(this);
    }
    public System.IO.Stream Open()
    {
      return Folder.Directory.Application.Platform.DirectoryOpenFile(this);
    }
    public System.IO.Stream Append()
    {
      return Folder.Directory.Application.Platform.DirectoryAppendFile(this);
    }
    public bool Exists()
    {
      return Folder.Directory.Application.Platform.DirectoryExistsFile(this);
    }
    public void Delete()
    {
      Folder.Directory.Application.Platform.DirectoryDeleteFile(this);
    }
    public void Copy(Inv.File CopyFile)
    {
      Folder.Directory.Application.Platform.DirectoryCopyFile(this, CopyFile);
    }
    public void CopyReplace(Inv.File CopyFile)
    {
      if (CopyFile.Exists())
        CopyFile.Delete();

      Folder.Directory.Application.Platform.DirectoryCopyFile(this, CopyFile);
    }
    public void Move(Inv.File MoveFile)
    {
      Folder.Directory.Application.Platform.DirectoryMoveFile(this, MoveFile);
    }
    public void MoveReplace(Inv.File MoveFile)
    {
      if (MoveFile.Exists())
        MoveFile.Delete();

      Folder.Directory.Application.Platform.DirectoryMoveFile(this, MoveFile);
    }
    public byte[] ReadAllBytes()
    {
      var Result = new byte[GetLength()];

      using (var Stream = Open())
        Stream.Read(Result, 0, Result.Length);

      return Result;
    }
    public void WriteAllBytes(byte[] Buffer)
    {
      using (var Stream = Create())
        Stream.Write(Buffer, 0, Buffer.Length);
    }

    public CsvFile AsCsv()
    {
      return new CsvFile(this);
    }
    public IniFile AsIni()
    {
      return new IniFile(this);
    }
    public TextFile AsText()
    {
      return new TextFile(this);
    }
  }

  public sealed class CsvFile
  {
    internal CsvFile(File Base)
    {
      this.Base = Base;
    }

    public Inv.CsvWriter Create()
    {
      return new Inv.CsvWriter(Base.Create());
    }
    public Inv.CsvReader Open()
    {
      return new Inv.CsvReader(Base.Open());
    }
    public Inv.CsvWriter Append()
    {
      return new Inv.CsvWriter(Base.Append());
    }

    private File Base;
  }

  public sealed class IniFile
  {
    internal IniFile(File Base)
    {
      this.Base = Base;
    }

    public Inv.IniWriter Create()
    {
      return new Inv.IniWriter(Base.Create());
    }
    public Inv.IniReader Open()
    {
      return new Inv.IniReader(Base.Open());
    }
    public Inv.IniWriter Append()
    {
      return new Inv.IniWriter(Base.Append());
    }

    private File Base;
  }

  public sealed class TextFile
  {
    internal TextFile(File Base)
    {
      this.Base = Base;
    }

    public System.IO.StreamWriter Create()
    {
      return new System.IO.StreamWriter(Base.Create());
    }
    public System.IO.StreamReader Open()
    {
      return new System.IO.StreamReader(Base.Open());
    }
    public System.IO.StreamWriter Append()
    {
      return new System.IO.StreamWriter(Base.Append());
    }
    public IEnumerable<string> ReadLines()
    {
      using (var TextReader = Open())
      {
        var FileLine = TextReader.ReadLine();

        while (FileLine != null)
        {
          yield return FileLine;

          FileLine = TextReader.ReadLine();
        }
      }
    }
    public string ReadAll()
    {
      if (Base.Exists())
      {
        using (var StreamReader = Open())
          return StreamReader.ReadToEnd();
      }
      else
      {
        return null;
      }
    }
    public void WriteAll(string Text)
    {
      using (var StreamWriter = Create())
        StreamWriter.Write(Text);
    }

    private File Base;
  }

  public enum TransitionAnimation
  {
    None,
    Fade,
    CarouselBack,
    CarouselNext
  }

  public sealed class Transition
  {
    internal Transition(Window Window)
    {
      this.Window = Window;
      this.Animation = TransitionAnimation.None;
      this.Duration = Window.DefaultTransitionDuration;
    }

    public TransitionAnimation Animation { get; private set; }
    public TimeSpan Duration { get; set; }

    public void SetAnimation(TransitionAnimation Animation)
    {
      CheckThreadAffinity();

      this.Animation = Animation;
    }
    public void None()
    {
      SetAnimation(TransitionAnimation.None);
    }
    public void Fade()
    {
      SetAnimation(TransitionAnimation.Fade);
    }
    public void CarouselBack()
    {
      SetAnimation(TransitionAnimation.CarouselBack);
    }
    public void CarouselNext()
    {
      SetAnimation(TransitionAnimation.CarouselNext);
    }

    [Conditional("DEBUG")]
    private void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }

    private Window Window;
  }

  public sealed class Window
  {
    internal Window(Application Application)
    {
      this.Application = Application;
      this.ActiveTimerSet = new HashSet<Timer>();
      this.DefaultFont = new Font(this);
      this.Background = new Background(this);
      this.DisplayRate = new WindowDisplayRate(this);
      this.DefaultTransitionDuration = TimeSpan.FromMilliseconds(300);
      this.IsChanged = true;
    }

    public Application Application { get; private set; }
    public Background Background { get; private set; }
    public Font DefaultFont { get; private set; }
    public TimeSpan DefaultTransitionDuration { get; set; }
    public int Width { get; internal set; }
    public int Height { get; internal set; }
    public WindowDisplayRate DisplayRate { get; private set; }
    public Surface ActiveSurface { get; private set; }

    public bool IsActiveSurface(Inv.Surface Surface)
    {
      CheckThreadAffinity();

      return ActiveSurface == Surface;
    }
    public Surface NewSurface()
    {
      CheckThreadAffinity();

      return new Surface(this);
    }
    public Transition Transition(Surface Surface)
    {
      CheckThreadAffinity();

      Debug.Assert(Surface.Window == this, "Surface must belong to this window.");

      this.ActiveSurface = Surface;
      this.ActiveTransition = new Transition(this); 
 
      return ActiveTransition;
    }
    public Timer NewTimer()
    {
      CheckThreadAffinity();

      return new Timer(this);
    }
    public WindowTask NewTask(Action<WindowThread> Action)
    {
      CheckThreadAffinity();

      return new WindowTask(this, Action);
    }
    public WindowTask RunTask(Action<WindowThread> Action)
    {
      CheckThreadAffinity();

      var Result = NewTask(Action);
      
      Result.Run();

      return Result;
    }
    public Modifier GetModifier()
    {
      CheckThreadAffinity();

      return Application.Platform.WindowGetModifier();
    }

    internal Transition ActiveTransition { get; set; }
    internal HashSet<Timer> ActiveTimerSet { get; set; }

    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      CheckThreadAffinity();

      this.IsChanged = true;
    }

    [Conditional("DEBUG")]
    internal void CheckThreadAffinity()
    {
      Application.CheckThreadAffinity();
    }

    private bool IsChanged;
  }

  public sealed class WindowDisplayRate
  {
    internal WindowDisplayRate(Window Window)
    {
      this.Window = Window;
    }

    public int PerSecond
    {
      get { return LastCount; }
    }

    internal void Calculate()
    {
      Window.CheckThreadAffinity();

      var CurrentTick = System.Environment.TickCount;
      var Difference = CurrentTick - LastTick;

      if (Difference < 0 || Difference >= 1000)
      {
        this.LastCount = CurrentCount;
        this.CurrentCount = 0;
        this.LastTick = System.Environment.TickCount;
      }

      CurrentCount++;
    }

    private Window Window;
    private int LastTick;
    private int LastCount;
    private int CurrentCount;
  }

  public sealed class WindowTask
  {
    internal WindowTask(Window Window, Action<WindowThread> Action)
    {
      this.Window = Window;
      this.Base = new System.Threading.Tasks.Task(() =>
      {
        try
        {
          this.SW = Stopwatch.StartNew();
          try
          {
            Action(new WindowThread(this));
          }
          finally
          {
            SW.Stop();
          }
        }
        catch (Exception Exception)
        {
          Window.Application.HandleExceptionInvoke(Exception);

          // TODO: keep this exception to throw on Wait() ?
        }
      });
    }

    public TimeSpan Elapsed
    {
      get { return SW.Elapsed; }
    }
    public bool IsCompleted
    {
      get { return Base.IsCompleted; }
    }

    public void Run()
    {
      Window.CheckThreadAffinity();

      Base.Start();
    }
    public void Wait()
    {
      Window.CheckThreadAffinity();

      if (Base.Status == TaskStatus.Running)
        Base.Wait();
    }

    internal Window Window { get; private set; }
    internal Stopwatch SW { get; private set; }

    private Task Base;
  }

  public sealed class WindowThread
  {
    internal WindowThread(WindowTask Task)
    {
      this.Task = Task;
    }

    /// <summary>
    /// Sleep the thread so the task will run to the minimum duration.
    /// If the task has exceeded the minimum duration then no sleep is performed.
    /// </summary>
    /// <param name="MinimumDuration"></param>
    public void Yield(TimeSpan MinimumDuration)
    {
      // TODO: check the task's thread affinity.

      var Result = MinimumDuration - Task.SW.Elapsed;

      if (Result > TimeSpan.Zero)
        Sleep(Result);
    }
    public void Sleep(TimeSpan Duration)
    {
      using (var WaitHandle = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset))
        WaitHandle.WaitOne(Duration);
      //using (var WaitHandle = new System.Threading.ManualResetEventSlim(false))
      //  WaitHandle.Wait(Duration);
    }
    /// <summary>
    /// Post a delegate back to the main window thread.
    /// </summary>
    /// <param name="Action"></param>
    public void Post(Action Action)
    {
      Task.Window.Application.Platform.WindowAsynchronise(Action);
    }

    private WindowTask Task;
  }

  public enum Key
  {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    n0,
    n1,
    n2,
    n3,
    n4,
    n5,
    n6,
    n7,
    n8,
    n9,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    Period,
    Tilde,
    BackQuote,
    Asterisk,
    Comma,
    Escape,
    Enter,
    Tab,
    Space,
    Insert,
    Delete,
    Up,
    Down,
    Left,
    Right,
    Home,
    PageUp,
    End,
    PageDown,
    Clear,
    Slash,
    Backslash,
    Plus,
    Minus
  }

  public sealed class Modifier
  {
    internal Modifier()
    {
    }

    public bool IsLeftShift { get; internal set; }
    public bool IsRightShift { get; internal set; }
    public bool IsShift
    {
      get { return IsLeftShift || IsRightShift; }
    }
    public bool IsLeftAlt { get; internal set; }
    public bool IsRightAlt { get; internal set; }
    public bool IsAlt
    {
      get { return IsLeftAlt || IsRightAlt; }
    }
    public bool IsLeftCtrl { get; internal set; }
    public bool IsRightCtrl { get; internal set; }
    public bool IsCtrl
    {
      get { return IsLeftCtrl || IsRightCtrl; }
    }
  }

  public sealed class Keystroke
  {
    internal Keystroke()
    {
    }

    public Key Key { get; internal set; }
    public Modifier Modifier { get; internal set; }

    public override string ToString()
    {
      var Result = Key.ToString();

      if (Modifier.IsCtrl)
        Result = "ctrl + " + Result;

      if (Modifier.IsAlt)
        Result = "alt + " + Result;

      if (Modifier.IsShift)
        Result = "shift + " + Result;

      return Result;
    }
  }

  public sealed class Surface 
  {
    internal Surface(Window Window)
    {
      this.Window = Window;
      this.PanelSet = new HashSet<Panel>();
      this.ChangedPanelList = new DistinctList<Panel>();
      this.StartAnimationSet = new HashSet<Animation>();
      this.StopAnimationSet = new HashSet<Animation>();
      this.Background = new Background(this);
      this.IsChanged = true;
    }

    public Window Window { get; private set; }
    public Background Background { get; private set; }
    public Panel Content
    {
      get { return ContentField; }
      set
      {
        if (ContentField != value)
        {
          if (ContentField != null)
            Detach(null, ContentField);

          this.ContentField = value;

          if (ContentField != null)
            Attach(null, ContentField);

          Change();
        }
      }
    }
    public event Action GestureBackwardEvent;
    public event Action GestureForwardEvent;
    public event Action<Keystroke> KeystrokeEvent;
    public event Action ArrangeEvent;
    public event Action ComposeEvent;

    public Animation NewAnimation()
    {
      CheckThreadAffinity();

      return new Animation(this);
    }
    public Button NewButton()
    {
      CheckThreadAffinity();

      return new Button(this);
    }
    public Canvas NewCanvas()
    {
      CheckThreadAffinity();

      return new Canvas(this);
    }
    public Dock NewVerticalDock()
    {
      CheckThreadAffinity();

      return new Dock(this, DockOrientation.Vertical);
    }
    public Dock NewHorizontalDock()
    {
      CheckThreadAffinity();

      return new Dock(this, DockOrientation.Horizontal);
    }
    public Edit NewEdit()
    {
      CheckThreadAffinity();

      return new Edit(this);
    }
    public Graphic NewGraphic()
    {
      CheckThreadAffinity();

      return new Graphic(this);
    }
    public Label NewLabel()
    {
      CheckThreadAffinity();

      return new Label(this);
    }
    public Memo NewMemo()
    {
      CheckThreadAffinity();

      return new Memo(this);
    }
    public Overlay NewOverlay()
    {
      CheckThreadAffinity();

      return new Overlay(this);
    }
    public Render NewRender()
    {
      CheckThreadAffinity();

      return new Render(this);
    }
    public Scroll NewVerticalScroll()
    {
      CheckThreadAffinity();

      return new Scroll(this, ScrollOrientation.Vertical);
    }
    public Scroll NewHorizontalScroll()
    {
      CheckThreadAffinity();

      return new Scroll(this, ScrollOrientation.Horizontal);
    }
    public Frame NewFrame()
    {
      CheckThreadAffinity();

      return new Frame(this);
    }
    public Stack NewHorizontalStack()
    {
      CheckThreadAffinity();
      
      return new Stack(this, StackOrientation.Horizontal);
    }
    public Stack NewVerticalStack()
    {
      CheckThreadAffinity();

      return new Stack(this, StackOrientation.Vertical);
    }
    public Table NewTable()
    {
      CheckThreadAffinity();

      return new Table(this);
    }
    public void SetFocus(Panel Panel)
    {
      CheckThreadAffinity();

      this.Focus = Panel;
    }
    public void GestureBackward()
    {
      CheckThreadAffinity();

      GestureBackwardInvoke();
    }
    public void GestureForward()
    {
      CheckThreadAffinity();

      GestureForwardInvoke();
    }
    public int GetPanelCount()
    {
      CheckThreadAffinity();

      return PanelSet.Count;
    }
    public int GetPanelDepth()
    {
      CheckThreadAffinity();

      if (Content == null)
        return 0;
      else
        return MaxVisibleDepth(Content, 0);
    }
    public string GetPanelDisplay()
    {
      CheckThreadAffinity();

      var StringBuilder = new StringBuilder();

      StringBuilder.AppendLine("total panel count: " + GetPanelCount());
      StringBuilder.AppendLine("maximum visible panel depth: " + GetPanelDepth());
      StringBuilder.AppendLine();

      if (Content != null && Content.Visibility.Get())
        PanelDisplay(Content, StringBuilder, 0);
      else
        StringBuilder.AppendLine("empty");
      
      return StringBuilder.ToString();
    }
    public void Rearrange()
    {
      CheckThreadAffinity();

      ArrangeInvoke();
    }

    internal Panel Focus { get; set; }
    internal object Node { get; set; }
    internal HashSet<Panel> PanelSet { get; private set; }
    internal HashSet<Animation> StartAnimationSet { get; private set; }
    internal HashSet<Animation> StopAnimationSet { get; private set; }

    internal void KeystrokeInvoke(Keystroke Keystroke)
    {
      CheckThreadAffinity();

      if (KeystrokeEvent != null)
        KeystrokeEvent(Keystroke);
    }
    internal void GestureBackwardInvoke()
    {
      CheckThreadAffinity();

      if (GestureBackwardEvent != null)
        GestureBackwardEvent();
    }
    internal bool GestureBackwardQuery()
    {
      CheckThreadAffinity();

      return GestureBackwardEvent != null;
    }
    internal void GestureForwardInvoke()
    {
      CheckThreadAffinity();

      if (GestureForwardEvent != null)
        GestureForwardEvent();
    }
    internal bool GestureForwardQuery()
    {
      CheckThreadAffinity();

      return GestureForwardEvent != null;
    }
    internal void ComposeInvoke()
    {
      CheckThreadAffinity();

      if (ComposeEvent != null)
        ComposeEvent();
    }
    internal void ArrangeInvoke()
    {
      CheckThreadAffinity();

      if (ArrangeEvent != null)
        ArrangeEvent();
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      CheckThreadAffinity();

      this.IsChanged = true;
    }
    internal void Attach(Inv.Panel Parent, Inv.Panel Child)
    {
      CheckThreadAffinity();

      Debug.Assert(Parent == null || Parent.Surface == this, "Parent does not belong to this surface");
      Debug.Assert(Child.Surface == this, "Child does not belong to this surface");

      if (Parent == null || PanelSet.Contains(Parent))
        Attach(Child);
    }
    internal void Detach(Inv.Panel Parent, Inv.Panel Child)
    {
      CheckThreadAffinity();

      Debug.Assert(Parent == null || Parent.Surface == this, "Parent does not belong to this surface");
      Debug.Assert(Child.Surface == this, "Child does not belong to this surface");

      if (Parent == null || PanelSet.Contains(Parent))
        Detach(Child);
    }
    internal void StartAnimation(Animation Animation)
    {
      CheckThreadAffinity();

      StopAnimationSet.Remove(Animation);
      StartAnimationSet.Add(Animation);
    }
    internal void StopAnimation(Animation Animation)
    {
      CheckThreadAffinity();

      StartAnimationSet.Remove(Animation);
      StopAnimationSet.Add(Animation);
    }
    internal void ProcessChanges(Action<Inv.Panel> Action)
    {
      CheckThreadAffinity();

      foreach (var ChangedPanel in ChangedPanelList)
      {
        // some panels will exercise sub-panels which means we don't need to check them again.
        if (ChangedPanel.IsChanged)
          Action(ChangedPanel);
      }
      ChangedPanelList.Clear();
    }
    internal void ChangePanel(Panel Panel)
    {
      CheckThreadAffinity();

      ChangedPanelList.Add(Panel);
    }
    internal void RenderPanel(Panel Panel)
    {
      CheckThreadAffinity();

      ChangedPanelList.Remove(Panel);
    }

    private void Attach(Panel Panel)
    {
      PanelSet.Add(Panel);
      foreach (var Child in Panel.GetChilds())
        Attach(Child);
    }
    private void Detach(Panel Panel)
    {
      PanelSet.Remove(Panel);
      foreach (var Child in Panel.GetChilds())
        Detach(Child);
    }
    private void PanelDisplay(Panel Panel, StringBuilder Builder, int Level)
    {
      var Indent = new string(' ', Level * 2);

      var VisibleChildrenArray = Panel.GetChilds().Where(C => C.Visibility.Get()).ToArray();

      if (VisibleChildrenArray.Length > 0)
      {
        Builder.AppendLine(Indent + Panel.DisplayType);

        foreach (var Child in VisibleChildrenArray)
          PanelDisplay(Child, Builder, Level + 1);
      }
      else
      {
        Builder.AppendLine(Indent + Panel.DisplayType + ";");
      }
    }
    private int MaxVisibleDepth(Panel Panel, int Depth)
    {
      var Result = Depth;

      if (Panel.Visibility.Get())
      {
        foreach (var Child in Panel.GetChilds())
        {
          var ChildDepth = MaxVisibleDepth(Child, Depth + 1);

          if (Result < ChildDepth)
            Result = ChildDepth;
        }
      }

      return Result;
    }

    [Conditional("DEBUG")]
    internal void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }
    
    private Panel ContentField;
    private Inv.DistinctList<Panel> ChangedPanelList;
    private bool IsChanged;
  }

  public sealed class Animation
  {
    internal Animation(Surface Surface)
    {
      this.Surface = Surface;
      this.TargetList = new DistinctList<AnimationTarget>();
    }

    public bool IsActive { get; private set; }
    public event Action CompleteEvent;

    public void RemoveTargets()
    {
      CheckThreadAffinity();

      Debug.Assert(!IsActive, "Animation must not be active.");

      TargetList.Clear();
    }
    public AnimationTarget AddTarget(Panel Panel)
    {
      CheckThreadAffinity();

      var Result = new AnimationTarget(Panel);

      TargetList.Add(Result);

      return Result;
    }
    public void Start()
    {
      CheckThreadAffinity();

      if (!IsActive)
      {
        this.IsActive = true;
        Surface.StartAnimation(this);
      }
    }
    public void Stop()
    {
      CheckThreadAffinity();

      if (IsActive)
      {
        this.IsActive = false;
        Surface.StopAnimation(this);
      }
    }

    internal object Node { get; set; }

    internal IEnumerable<AnimationTarget> GetTargets()
    {
      CheckThreadAffinity();

      return TargetList;
    }
    internal void Complete()
    {
      CheckThreadAffinity();

      this.IsActive = false;

      if (CompleteEvent != null)
        CompleteEvent();
    }

    [Conditional("DEBUG")]
    private void CheckThreadAffinity()
    {
      Surface.CheckThreadAffinity();
    }

    private Inv.DistinctList<AnimationTarget> TargetList;
    private Surface Surface;
  }

  public sealed class AnimationTarget
  {
    internal AnimationTarget(Panel Panel)
    {
      this.Panel = Panel;
      this.CommandList = new Inv.DistinctList<AnimationCommand>();
    }

    internal Panel Panel { get; private set; }

    public AnimationCommand FadeOpacityOut(TimeSpan Duration, TimeSpan? Offset = null)
    {
      return FadeOpacity(1.0F, 0.0F, Duration, Offset);
    }
    public AnimationCommand FadeOpacityIn(TimeSpan Duration, TimeSpan? Offset = null)
    {
      return FadeOpacity(0.0F, 1.0F, Duration, Offset);
    }
    public AnimationCommand FadeOpacity(float From, float To, TimeSpan Duration, TimeSpan? Offset = null)
    {
      var Result = new AnimationOpacityCommand(this)
      {
        Duration = Duration,
        Offset = Offset,
        From = From,
        To = To
      };

      CommandList.Add(Result);

      return Result;
    }

    internal IEnumerable<AnimationCommand> GetCommands()
    {
      return CommandList;
    }

    private Inv.DistinctList<AnimationCommand> CommandList;
  }

  public abstract class AnimationCommand
  {
    internal AnimationCommand(AnimationTarget Target)
    {
      this.Target = Target;
    }

    public event Action CompleteEvent;

    internal AnimationTarget Target { get; private set; }

    internal void Complete()
    {
      if (CompleteEvent != null)
        CompleteEvent();
    }
  }

  internal sealed class AnimationOpacityCommand : AnimationCommand
  {
    public AnimationOpacityCommand(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public float From { get; internal set; }
    public float To { get; internal set; }
  }

  public abstract class Panel
  {
    internal Panel(Surface Surface)
    {
      this.Surface = Surface;
      this.Opacity = new Opacity(this);
      this.Background = new Background(this);
      this.CornerRadius = new CornerRadius(this);
      this.Border = new Border(this);
      this.Alignment = new Alignment(this);
      this.Visibility = new Visibility(this);
      this.Size = new Size(this);
      this.Margin = new Edge(this);
      this.Padding = new Edge(this);
      this.Elevation = new Elevation(this);
      this.Parent = null;
      this.ChildSet = null;
      this.IsChanged = true;
    }

    public Surface Surface { get; private set; }
    public Opacity Opacity { get; private set; }
    public Background Background { get; private set; }
    public CornerRadius CornerRadius { get; private set; }
    public Border Border { get; private set; }
    public Alignment Alignment { get; private set; }
    public Visibility Visibility { get; private set; }
    public Size Size { get; private set; }
    public Edge Margin { get; private set; }
    public Edge Padding { get; private set; }
    public Elevation Elevation { get; private set; }

    internal object Node { get; set; }
    internal bool IsChanged { get; set; }
    internal virtual string DisplayType
    {
      get { return GetType().Name.ToLower(); }
    }

    internal void Change()
    {
      CheckThreadAffinity();

      if (!IsChanged)
      {
        this.IsChanged = true;
        Surface.ChangePanel(this);

#if TRADE_PANEL_CHANGE
        if (GetType() != typeof(Render))
          Debug.WriteLine("CHANGE " + DisplayType);
#endif
      }
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        Surface.RenderPanel(this);

#if TRADE_PANEL_RENDER
        if (GetType() != typeof(Render))
          Debug.WriteLine("RENDER " + DisplayType);
#endif

        return true;
      }

      return false;
    }
    internal void AddChild(Panel Panel)
    {
      Debug.Assert(Panel != this, "Parent must not add itself as a child.");
      Debug.Assert(Panel.Surface == Surface, "Child must belong to the owning surface.");
      Debug.Assert(Panel.Parent == null, "Child must not already have a parent.");

      Panel.Parent = this;

      if (ChildSet == null)
        ChildSet = new HashSet<Inv.Panel>();

      ChildSet.Add(Panel);

      Surface.Attach(this, Panel);
    }
    internal void RemoveChild(Panel Panel)
    {
      Debug.Assert(Panel != null, "Panel must be specified.");
      Debug.Assert(Panel != this, "Parent must not remove itself as a child.");
      Debug.Assert(Panel.Surface == Surface, "Child must belong to the owning surface.");
      Debug.Assert(Panel.Parent == this, "Child must belong to this parent.");

      Panel.Parent = null;

      if (ChildSet != null)
        ChildSet.Remove(Panel);

      Surface.Detach(this, Panel);
    }
    internal bool HasChilds()
    {
      return ChildSet != null && ChildSet.Count > 0;
    }
    internal IEnumerable<Panel> GetChilds()
    {
      if (ChildSet != null)
        return ChildSet;
      else
        return new Panel[] { };
    }

    [Conditional("DEBUG")]
    protected void CheckThreadAffinity()
    {
      Surface.CheckThreadAffinity();
    }

    private Panel Parent;
    private HashSet<Panel> ChildSet;
  }

  public sealed class Overlay : Panel
  {
    internal Overlay(Surface Surface)
      : base(Surface)
    {
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    public void AddPanel(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    public void InsertPanel(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();
      
      RemoveChild(Panel);
      PanelCollection.Remove(Panel);
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Element in PanelCollection)
          RemoveChild(Element);
        PanelCollection.Clear();
      }
    }
    public IEnumerable<Panel> GetPanels()
    {
      CheckThreadAffinity();

      return PanelCollection;
    }

    internal Collection<Panel> PanelCollection { get; private set; }
  }

  public sealed class Render : Panel
  {
    internal Render(Surface Surface)
      : base(Surface)
    {
      this.ContextSingleton = new Singleton<RenderContext>(this);
      ContextSingleton.Data = new RenderContext(Surface);
    }

    public event Action<Point> SingleTapEvent;
    public event Action<Point> DoubleTapEvent;
    public event Action<Point> ContextTapEvent;
    public event Action<Point> PressEvent;
    public event Action<Point> ReleaseEvent;
    public event Action<Point> MoveEvent;
    public event Action<int> ZoomEvent;

    public void Draw(Action<RenderContext> Action)
    {
      CheckThreadAffinity();

      var Context = ContextSingleton.Data;

      Context.Start();

      Action(Context);

      Context.Stop();

      ContextSingleton.Change();
    }

    internal Singleton<RenderContext> ContextSingleton { get; private set; }

    internal IEnumerable<RenderCommand> GetCommands()
    {
      return ContextSingleton.Data.GetCommands();
    }
    internal void PressInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (PressEvent != null)
        PressEvent(Point);
    }
    internal void ReleaseInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (ReleaseEvent != null)
        ReleaseEvent(Point);
    }
    internal void MoveInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (MoveEvent != null)
        MoveEvent(Point);
    }
    internal void SingleTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (SingleTapEvent != null)
        SingleTapEvent(Point);
    }
    internal void DoubleTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (DoubleTapEvent != null)
        DoubleTapEvent(Point);
    }
    internal void ContextTapInvoke(Point Point)
    {
      CheckThreadAffinity();

      if (ContextTapEvent != null)
        ContextTapEvent(Point);
    }
    internal void ZoomInvoke(int Delta)
    {
      CheckThreadAffinity();

      if (ZoomEvent != null)
        ZoomEvent(Delta);
    }
  }

  internal enum RenderType
  {
    Text,
    Rectangle,
    Ellipse,
    Image
  }

  internal sealed class RenderCommand
  {
    internal RenderType Type;

    // DrawText.
    internal string TextFragment;
    internal string TextFontName;
    internal int TextFontSize;
    internal Colour? TextFontColour;
    internal FontWeight TextFontWeight;
    internal Point TextPoint;
    internal HorizontalPosition TextHorizontal;
    internal VerticalPosition TextVertical;

    // DrawRectangle
    internal Colour? RectangleFillColour;
    internal Colour? RectangleStrokeColour;
    internal int RectangleStrokeThickness;
    internal Rect RectangleRect;

    // DrawEllipse.
    internal Colour? EllipseFillColour;
    internal Colour? EllipseStrokeColour;
    internal int EllipseStrokeThickness;
    internal Point EllipseCenter;
    internal Point EllipseRadius;

    // DrawImage.
    internal Inv.Image? ImageSource;
    internal Rect ImageRect;
    internal float ImageOpacity;
    internal Colour? ImageTint;
    internal Mirror? ImageMirror;
  }

  public enum VerticalPosition
  {
    Top,
    Center,
    Bottom
  }

  public enum HorizontalPosition
  {
    Left,
    Center,
    Right
  }

  public enum Mirror
  {
    Vertical,
    Horizontal
  }

  public sealed class RenderContext
  {
    internal RenderContext(Surface Surface)
    {
      this.Surface = Surface;
      this.CommandList = new Inv.DistinctList<RenderCommand>(256);
    }

    public int Width { get; internal set; }
    public int Height { get; internal set; }

    public void DrawText(string Fragment, string FontName, int FontSize, Inv.FontWeight FontWeight, Colour FontColour, Point Point, HorizontalPosition Horizontal, VerticalPosition Vertical)
    {
      var Command = AddCommand();
      Command.Type = RenderType.Text;
      Command.TextFragment = Fragment;
      Command.TextFontName = FontName;
      Command.TextFontSize = FontSize;
      Command.TextFontWeight = FontWeight;
      Command.TextFontColour = FontColour;
      Command.TextPoint = Point;
      Command.TextHorizontal = Horizontal;
      Command.TextVertical = Vertical;
    }
    public void DrawRectangle(Colour? FillColour, Colour? StrokeColour, int StrokeThickness, Rect Rect)
    {
      Debug.Assert(StrokeThickness >= 0);

      var Command = AddCommand();
      Command.Type = RenderType.Rectangle;
      Command.RectangleFillColour = FillColour;
      Command.RectangleStrokeColour = StrokeColour;
      Command.RectangleStrokeThickness = StrokeThickness;
      Command.RectangleRect = Rect;
    }
    public void DrawEllipse(Colour? FillColour, Colour? StrokeColour, int StrokeThickness, Point Center, Point Radius)
    {
      Debug.Assert(StrokeThickness >= 0);

      var Command = AddCommand();
      Command.Type = RenderType.Ellipse;
      Command.EllipseFillColour = FillColour;
      Command.EllipseStrokeColour = StrokeColour;
      Command.EllipseStrokeThickness = StrokeThickness;
      Command.EllipseCenter = Center;
      Command.EllipseRadius = Radius;
    }
    public void DrawImage(Inv.Image? Image, Rect Rect, float Opacity = 1.0F, Colour? Tint = null, Mirror? Mirror = null)
    {
      Debug.Assert(Rect.Width >= 0 && Rect.Height >= 0 && Opacity >= 0.0F && Opacity <= 1.0F);

      var Command = AddCommand();
      Command.Type = RenderType.Image;
      Command.ImageSource = Image;
      Command.ImageRect = Rect;
      Command.ImageOpacity = Opacity;
      Command.ImageTint = Tint;
      Command.ImageMirror = Mirror;
    }

    internal void Start()
    {
      CheckThreadAffinity();

      this.CommandCount = 0;
    }
    internal void Stop()
    {
      CheckThreadAffinity();
    }
    internal IEnumerable<RenderCommand> GetCommands()
    {
      return CommandList.Take(CommandCount);
    }

    private RenderCommand AddCommand()
    {
      if (CommandCount < CommandList.Count)
      {
        return CommandList[CommandCount++];
      }
      else
      {
        var Element = new RenderCommand();
        CommandList.Add(Element);
        CommandCount++;
        return Element;
      }
    }

    [Conditional("DEBUG")]
    private void CheckThreadAffinity()
    {
      Surface.CheckThreadAffinity();
    }

    private Surface Surface;
    private Inv.DistinctList<RenderCommand> CommandList;
    private int CommandCount;
  }

  public sealed class Canvas : Panel
  {
    internal Canvas(Surface Surface)
      : base(Surface)
    {
      this.PieceCollection = new Collection<CanvasPiece>(this);
    }

    public void AddPanel(Panel Panel, Rect Rect)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PieceCollection.Add(new CanvasPiece(Rect, Panel));
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      PieceCollection.RemoveWhere(E => E.Panel == Panel);
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PieceCollection.Count > 0)
      {
        foreach (var Element in PieceCollection)
          RemoveChild(Element.Panel);
        PieceCollection.Clear();
      }
    }

    internal Collection<CanvasPiece> PieceCollection { get; private set; }
  }

  internal sealed class Singleton<T> : Element
  {
    internal Singleton(Panel Panel)
      : base(Panel)
    {
    }

    public T Data
    {
      get
      {
        CheckThreadAffinity(); 
        
        return DataField; 
      }
      set
      {
        CheckThreadAffinity();

        if (!object.Equals(DataField, value))
        {
          this.DataField = value;
          Change();
        }
      }
    }

    private T DataField;
  }

  internal sealed class Collection<T> : Element, IEnumerable<T>
  {
    internal Collection(Panel Panel)
      : base(Panel)
    {
      this.List = new Inv.DistinctList<T>();
    }

    public T this[int Index]
    {
      get 
      {
        CheckThreadAffinity();

        return List[Index]; 
      }
    }
    public int Count
    {
      get 
      {
        CheckThreadAffinity();

        return List.Count; 
      }
    }
    public void Clear()
    {
      CheckThreadAffinity();

      if (List.Count > 0)
      {
        List.Clear();
        Change();
      }
    }
    public void Add(T Item)
    {
      CheckThreadAffinity();

      List.Add(Item);
      Change();
    }
    public void Insert(int Index, T Item)
    {
      CheckThreadAffinity();

      List.Insert(Index, Item);
      Change();
    }
    public void Remove(T Item)
    {
      CheckThreadAffinity();

      if (List.Remove(Item))
        Change();
    }
    public void RemoveWhere(Predicate<T> ItemPredicate)
    {
      CheckThreadAffinity();

      if (List.RemoveAll(ItemPredicate) > 0)
        Change();
    }
    public bool Contains(T Item)
    {
      CheckThreadAffinity();

      return List.Contains(Item);
    }
    public int IndexOf(T Item)
    {
      return List.IndexOf(Item);
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      CheckThreadAffinity();

      return List.GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      CheckThreadAffinity();

      return List.GetEnumerator();
    }

    private Inv.DistinctList<T> List;
  }

  internal sealed class CanvasPiece
  {
    public CanvasPiece(Rect Rect, Panel Panel)
    {
      this.Rect = Rect;
      this.Panel = Panel;
    }

    public readonly Rect Rect;
    public readonly Panel Panel;
  }

  public enum DockOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Dock : Panel
  {
    internal Dock(Surface Surface, DockOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.HeaderCollection = new Inv.Collection<Panel>(this);
      this.ClientCollection = new Inv.Collection<Panel>(this);
      this.FooterCollection = new Inv.Collection<Panel>(this);
    }

    public DockOrientation Orientation { get; private set; }
    public bool HasPanels()
    {
      CheckThreadAffinity();

      return HeaderCollection.Count > 0 || ClientCollection.Count > 0 || FooterCollection.Count > 0;
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      RemoveHeaders();
      RemoveClients();
      RemoveFooters();
    }
    public void AddHeader(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Add(Panel);
    }
    public void InsertHeader(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Insert(Index, Panel);
    }
    public void RemoveHeader(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      HeaderCollection.Remove(Panel);
    }
    public Inv.Panel HeaderAt(int Index)
    {
      CheckThreadAffinity();

      return HeaderCollection[Index];
    }
    public bool HasHeader(Panel Panel)
    {
      CheckThreadAffinity();

      return HeaderCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetHeaders()
    {
      CheckThreadAffinity();

      return HeaderCollection;
    }
    public bool HasHeaders()
    {
      CheckThreadAffinity();

      return HeaderCollection.Count > 0;
    }
    public void RemoveHeaders()
    {
      CheckThreadAffinity();

      if (HeaderCollection.Count > 0)
      {
        foreach (var Header in HeaderCollection)
          RemoveChild(Header);
        HeaderCollection.Clear();
      }
    }
    public void AddClient(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      ClientCollection.Add(Panel);
    }
    public void InsertClient(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      ClientCollection.Insert(Index, Panel);
    }
    public void RemoveClient(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      ClientCollection.Remove(Panel);
    }
    public Inv.Panel ClientAt(int Index)
    {
      CheckThreadAffinity();

      return ClientCollection[Index];
    }
    public bool HasClient(Panel Panel)
    {
      CheckThreadAffinity();

      return ClientCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetClients()
    {
      CheckThreadAffinity();

      return ClientCollection;
    }
    public bool HasClients()
    {
      CheckThreadAffinity();

      return ClientCollection.Count > 0;
    }
    public void RemoveClients()
    {
      CheckThreadAffinity();

      if (ClientCollection.Count > 0)
      {
        foreach (var Client in ClientCollection)
          RemoveChild(Client);
        ClientCollection.Clear();
      }
    }
    public void AddFooter(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      FooterCollection.Add(Panel);
    }
    public void InsertFooter(int Index, Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      FooterCollection.Insert(Index, Panel);
    }
    public void RemoveFooter(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      FooterCollection.Remove(Panel);
    }
    public Inv.Panel FooterAt(int Index)
    {
      CheckThreadAffinity();

      return FooterCollection[Index];
    }
    public bool HasFooter(Panel Panel)
    {
      CheckThreadAffinity();

      return FooterCollection.Contains(Panel);
    }
    public IEnumerable<Panel> GetFooters()
    {
      CheckThreadAffinity();

      return FooterCollection;
    }
    public bool HasFooters()
    {
      CheckThreadAffinity();

      return FooterCollection.Count > 0;
    }
    public void RemoveFooters()
    {
      CheckThreadAffinity();

      if (FooterCollection.Count > 0)
      {
        foreach (var Footer in FooterCollection)
          RemoveChild(Footer);
        FooterCollection.Clear();
      }
    }

    internal override string DisplayType
    {
      get { return Orientation == DockOrientation.Vertical ? "v-dock" : "h-dock"; }
    }
    internal Collection<Panel> HeaderCollection { get; private set; }
    internal Collection<Panel> ClientCollection { get; private set; }
    internal Collection<Panel> FooterCollection { get; private set; }

    internal bool CollectionRender()
    {
      CheckThreadAffinity();

      var Result = false;
      
      if (HeaderCollection.Render())
        Result = true;

      if (ClientCollection.Render())
        Result = true;

      if (FooterCollection.Render())
        Result = true;

      return Result;
    }
  }

  public enum StackOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Stack : Panel
  {
    internal Stack(Surface Surface, StackOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    public StackOrientation Orientation { get; private set; }

    public void AddPanel(Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    public void InsertPanel(int Index, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    public void RemovePanels()
    {
      CheckThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Panel in PanelCollection)
          RemoveChild(Panel);
        PanelCollection.Clear();
      }
    }
    public IEnumerable<Panel> GetPanels()
    {
      CheckThreadAffinity();

      return PanelCollection;
    }
    public void RemovePanel(Panel Panel)
    {
      CheckThreadAffinity();

      RemoveChild(Panel);
      PanelCollection.Remove(Panel);
    }
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);

      InsertPanel(PanelIndex, Panel);
    }
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      CheckThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;

      InsertPanel(PanelIndex, Panel);
    }
    public bool HasPanel(Panel Panel)
    {
      CheckThreadAffinity();

      return PanelCollection.Contains(Panel);
    }
    public Inv.Panel PanelAt(int Index)
    {
      CheckThreadAffinity();

      return PanelCollection[Index];
    }
    public bool HasPanels()
    {
      CheckThreadAffinity();

      return PanelCollection.Count > 0;
    }

    internal override string DisplayType
    {
      get { return Orientation == StackOrientation.Vertical ? "v-stack" : "h-stack"; }
    }
    internal Inv.Collection<Panel> PanelCollection { get; private set; }
  }

  public sealed class Frame : Panel
  {
    internal Frame(Surface Surface)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<Panel>(this);
    }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }

    internal Singleton<Panel> ContentSingleton { get; private set; }
  }

  public enum ScrollOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class Scroll : Panel
  {
    internal Scroll(Surface Surface, ScrollOrientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.ContentSingleton = new Singleton<Panel>(this);
    }

    public ScrollOrientation Orientation { get; private set; }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }

    internal override string DisplayType
    {
      get { return Orientation == ScrollOrientation.Vertical ? "v-scroll" : "h-scroll"; }
    }
    internal Singleton<Panel> ContentSingleton { get; private set; }
  }

  public sealed class Table : Panel
  {
    internal Table(Surface Surface)
      : base(Surface)
    {
      this.RowCollection = new Collection<TableRow>(this);
      this.ColumnCollection = new Collection<TableColumn>(this);
      this.CellCollection = new Collection<TableCell>(this);
      this.CellGrid = new Grid<TableCell>();
    }

    public void RemovePanels()
    {
      CheckThreadAffinity();

      RowCollection.Clear();
      ColumnCollection.Clear();
      CellCollection.Clear();

      CellGrid.Width = 0;
      CellGrid.Height = 0;
    }
    public TableRow AddRow()
    {
      CheckThreadAffinity();

      var Result = new TableRow(this, RowCollection.Count);

      RowCollection.Add(Result);

      CellGrid.Height++;
      foreach (var Column in ColumnCollection)
        CellGrid[Column.Index, Result.Index] = NewCell(Column, Result);

      return Result;
    }
    public TableRow GetRow(int Index)
    {
      CheckThreadAffinity();

      return RowCollection[Index];
    }
    public IEnumerable<TableRow> GetRows()
    {
      CheckThreadAffinity();

      return RowCollection;
    }
    public TableColumn AddColumn()
    {
      CheckThreadAffinity();

      var Result = new TableColumn(this, ColumnCollection.Count);

      ColumnCollection.Add(Result);

      CellGrid.Width++;
      foreach (var Row in RowCollection)
        CellGrid[Result.Index, Row.Index] = NewCell(Result, Row);

      return Result;
    }
    public TableColumn GetColumn(int Index)
    {
      CheckThreadAffinity();

      return ColumnCollection[Index];
    }
    public IEnumerable<TableColumn> GetColumns()
    {
      CheckThreadAffinity();

      return ColumnCollection;
    }
    public TableCell GetCell(int ColumnIndex, int RowIndex)
    {
      CheckThreadAffinity();

      return GetCell(GetColumn(ColumnIndex), GetRow(RowIndex));
    }
    public TableCell GetCell(TableColumn Column, TableRow Row)
    {
      CheckThreadAffinity();

      return CellGrid[Column.Index, Row.Index];
    }
    public void SetCell(Func<int, int, Inv.Panel> PanelFunction)
    {
      CheckThreadAffinity();

      foreach (var Cell in CellCollection)
        Cell.Content = PanelFunction(Cell.Column.Index, Cell.Row.Index);
    }
    public IEnumerable<TableCell> GetCells()
    {
      CheckThreadAffinity();

      return CellGrid;
    }
    public void Compose(int ColumnCount, int RowCount)
    {
      CheckThreadAffinity();

      ColumnCollection.Clear();
      foreach (var Column in ColumnCount.NumberSeries())
        AddColumn();

      RowCollection.Clear();
      foreach (var Row in RowCount.NumberSeries())
        AddRow();

      CellGrid.Width = ColumnCount;
      CellGrid.Height = RowCount;

      CellCollection.Clear();
      CellGrid.Fill((ColumnIndex, RowIndex) => NewCell(ColumnCollection[ColumnIndex], RowCollection[RowIndex]));
    }
    public void Compose(Inv.Panel[,] PanelArray)
    {
      CheckThreadAffinity();

      Compose(PanelArray.GetLength(0), PanelArray.GetLength(1));

      foreach (var Cell in CellCollection)
        Cell.Content = PanelArray[Cell.Column.Index, Cell.Row.Index];
    }

    internal Inv.Collection<TableRow> RowCollection { get; private set; }
    internal Inv.Collection<TableColumn> ColumnCollection { get; private set; }
    internal Inv.Collection<TableCell> CellCollection { get; private set; }

    internal bool CollectionRender()
    {
      CheckThreadAffinity();

      var Result = false;

      if (RowCollection.Render())
        Result = true;

      if (ColumnCollection.Render())
        Result = true;

      if (CellCollection.Render())
        Result = true;

      return Result;
    }

    private TableCell NewCell(TableColumn Column, TableRow Row)
    {
      var Result = new TableCell(this, Column, Row);

      CellCollection.Add(Result);

      return Result;
    }

    private Inv.Grid<TableCell> CellGrid;
  }

  public sealed class TableRow : Element
  {
    internal TableRow(Panel Panel, int Index)
      : base(Panel)
    {
      this.Index = Index;
      this.Length = new TableLength(Panel);
      this.ContentSingleton = new Singleton<Inv.Panel>(Panel);
    }

    public int Index { get; private set; }
    public TableLength Length { get; private set; }
    public Inv.Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            Panel.RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            Panel.AddChild(ContentSingleton.Data);
        }
      }
    }

    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }
  }

  public sealed class TableColumn : Element
  {
    internal TableColumn(Panel Panel, int Index)
      : base(Panel)
    {
      this.Index = Index;
      this.Length = new TableLength(Panel);
      this.ContentSingleton = new Singleton<Inv.Panel>(Panel);
    }

    public int Index { get; private set; }
    public TableLength Length { get; private set; }
    public Inv.Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            Panel.RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            Panel.AddChild(ContentSingleton.Data);
        }
      }
    }

    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }
  }

  public sealed class TableCell : Element
  {
    internal TableCell(Panel Panel, TableColumn Column, TableRow Row)
      : base(Panel)
    {
      this.Column = Column;
      this.Row = Row;
      this.ContentSingleton = new Singleton<Inv.Panel>(Panel);
    }

    public TableColumn Column { get; private set; }
    public TableRow Row { get; private set; }
    public Inv.Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            Panel.RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            Panel.AddChild(ContentSingleton.Data);
        }
      }
    }

    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }
  }

  internal enum TableLengthType
  {
    Auto,
    Fixed,
    Star
  }

  public sealed class TableLength : Element
  {
    internal TableLength(Panel Panel)
      : base(Panel)
    {
      this.Type = TableLengthType.Auto;
      this.Value = 0;
    }

    public void Auto()
    {
      Set(TableLengthType.Auto, 0);
    }
    public void Fixed(int Points)
    {
      Set(TableLengthType.Fixed, Points);
    }
    public void Star(int Units = 1)
    {
      Set(TableLengthType.Star, Units);
    }

    internal TableLengthType Type { get; private set; }
    internal int Value { get; private set; }

    private void Set(TableLengthType Type, int Value)
    {
      CheckThreadAffinity();

      if (Type != this.Type || Value != this.Value)
      {
        this.Type = Type;
        this.Value = Value;
        Change();
      }
    }
  }

  public sealed class Button : Panel
  {
    internal Button(Surface Surface)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<Panel>(this);
      this.IsEnabledField = true;
      this.IsFocusableField = false;
    }

    public Panel Content
    {
      get { return ContentSingleton.Data; }
      set
      {
        CheckThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
            RemoveChild(ContentSingleton.Data);

          ContentSingleton.Data = value;

          if (ContentSingleton.Data != null)
            AddChild(ContentSingleton.Data);
        }
      }
    }
    public bool IsEnabled
    {
      get
      {
        CheckThreadAffinity();

        return IsEnabledField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsEnabledField != value)
        {          
          this.IsEnabledField = value;
          Change();
        }
      }
    }
    public bool IsFocusable
    {
      get
      {
        CheckThreadAffinity();

        return IsFocusableField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsFocusableField != value)
        {
          this.IsFocusableField = value;
          Change();
        }
      }
    }
    public event Action SingleTapEvent;
    public event Action ContextTapEvent;

    public void SingleTap()
    {
      SingleTapInvoke();
    }
    public void ContextTap()
    {
      ContextTapInvoke();
    }

    internal Singleton<Panel> ContentSingleton { get; private set; }

    internal void SingleTapInvoke()
    {
      CheckThreadAffinity();

      if (SingleTapEvent != null)
        SingleTapEvent();
    }
    internal void ContextTapInvoke()
    {
      CheckThreadAffinity();

      if (ContextTapEvent != null)
        ContextTapEvent();
    }

    private bool IsEnabledField;
    private bool IsFocusableField;
  }

  public enum Justification
  {
    Left,
    Center,
    Right
  }

  public sealed class Timer
  {
    internal Timer(Window Window)
    {
      this.Window = Window;
    }

    public event Action IntervalEvent;
    public TimeSpan IntervalTime { get; set; }
    public bool IsEnabled { get; private set; }

    public void Start()
    {
      CheckThreadAffinity();

      if (!IsEnabled)
      {
        this.IsEnabled = true;
        Window.ActiveTimerSet.Add(this);
      }
    }
    public void Stop()
    {
      CheckThreadAffinity();

      if (IsEnabled)
      {
        this.IsEnabled = false;
        // NOTE: the platform engine must remove the timer from the active set once it has been stopped.
      }
    }

    internal object Node { get; set; }

    internal void IntervalInvoke()
    {
      if (IntervalEvent != null)
        IntervalEvent();
    }

    [Conditional("DEBUG")]
    private void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }

    private Window Window;
  }

  public sealed class Label : Panel
  {
    internal Label(Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
      this.LineWrappingField = true;
    }

    public Font Font { get; private set; }
    public Justification? Justification { get; private set; }
    public string Text
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }
    public bool LineWrapping
    {
      get
      {
        CheckThreadAffinity(); 
        
        return LineWrappingField; 
      }
      set
      {
        CheckThreadAffinity();

        if (LineWrappingField != value)
        {
          this.LineWrappingField = value;
          Change();
        }
      }
    }

    public void JustifyNone()
    {
      Justify(null);
    }
    public void JustifyLeft()
    {
      Justify(Inv.Justification.Left);
    }
    public void JustifyCenter()
    {
      Justify(Inv.Justification.Center);
    }
    public void JustifyRight()
    {
      Justify(Inv.Justification.Right);
    }

    internal override string DisplayType
    {
      get { return "label:" + Text.ConvertToCSharpString(); }
    }

    private void Justify(Justification? Justification)
    {
      CheckThreadAffinity();

      if (this.Justification != Justification)
      {
        this.Justification = Justification;
        Change();
      }
    }

    private string TextField;
    private bool LineWrappingField;
  }

  public sealed class Graphic : Panel
  {
    internal Graphic(Surface Surface)
      : base(Surface)
    {
      this.ImageSingleton = new Singleton<Image?>(this);
    }

    public Inv.Image? Image
    {
      get { return ImageSingleton.Data;  }
      set { this.ImageSingleton.Data = value; }
    }

    internal override string DisplayType
    {
      get { return "graphic" + (Image != null ? ":" + Image.Value.GetBuffer().Length : ""); }
    }
    internal Singleton<Inv.Image?> ImageSingleton { get; private set; }
  }

  public sealed class Edit : Panel
  {
    internal Edit(Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
    }

    public Font Font { get; private set; }
    public bool IsReadOnly
    {
      get
      {
        CheckThreadAffinity();

        return IsReadOnlyField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    public string Text
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }
    public event Action ChangeEvent;

    internal void ChangeInvoke(string Text)
    {
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;

        if (ChangeEvent != null)
          ChangeEvent();
      }
    }

    internal override string DisplayType
    {
      get { return "edit:" + Text.ConvertToCSharpString(); }
    }

    private string TextField;
    private bool IsReadOnlyField;
  }

  public sealed class Memo : Panel
  {
    internal Memo(Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
    }

    public Font Font { get; private set; }
    public string Text
    {
      get 
      {
        CheckThreadAffinity(); 
        
        return TextField; 
      }
      set
      {
        CheckThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }
    public bool IsReadOnly
    {
      get 
      {
        CheckThreadAffinity(); 
        
        return IsReadOnlyField; 
      }
      set
      {
        CheckThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    public event Action ChangeEvent;

    internal override string DisplayType
    {
      get { return "memo:" + Text.ConvertToCSharpString(); }
    }

    internal void ChangeInvoke(string Text)
    {
      CheckThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;

        if (ChangeEvent != null)
          ChangeEvent();
      }
    }

    private string TextField;
    private bool IsReadOnlyField;
  }

  public abstract class Element
  {
    internal Element(Window Window)
      : this(Window.Change)
    {
      this.Window = Window;
      this.Surface = null;
      this.Panel = null;
    }
    internal Element(Surface Surface)
      : this(Surface.Change)
    {
      this.Window = Surface.Window;
      this.Surface = Surface;
      this.Panel = null;
    }
    internal Element(Panel Panel)
      : this(Panel.Change)
    {
      this.Window = Panel.Surface.Window;
      this.Surface = Panel.Surface;
      this.Panel = Panel;
    }
    private Element(Action ChangeAction)
    {
      this.ChangeAction = ChangeAction;
      this.IsChanged = true;
    }

    internal Window Window { get; private set; }
    internal Surface Surface { get; private set; }
    internal Panel Panel { get; private set; }
    internal bool IsChanged { get; private set; }

    internal void Change()
    {
      CheckThreadAffinity();

      if (!IsChanged)
        this.IsChanged = true;

      // TODO: this ensures the parent IsChanged is set (to workaround platform engine bugs where Element.Render() is not called).
      if (ChangeAction != null)
        ChangeAction();
    }
    internal bool Render()
    {
      CheckThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }
      else
      {
        return false;
      }
    }

    [Conditional("DEBUG")]
    protected void CheckThreadAffinity()
    {
      Window.CheckThreadAffinity();
    }

    private Action ChangeAction;
  }

  public sealed class Border : Element
  {
    internal Border(Panel Panel)
      : base(Panel)
    {
      this.Thickness = new Edge(Panel);
    }

    public Edge Thickness { get; private set; }
    public Colour? Colour
    {
      get 
      {
        CheckThreadAffinity();

        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    private Colour? ColourField;
  }

  public struct Colour
  {
    public Colour(uint Argb)
      : this()
    {
      RawValue = (int)Argb;
    }
    public Colour(int Argb)
      : this()
    {
      RawValue = Argb;
    }

    public string Name
    {
      get { return NameDictionary.GetByLeftOrDefault(this, Hex); }
    }
    public string Hex
    {
      get { return "#" + RawValue.ToString("X"); }
    }
    public int RawValue { get; private set; }
    public bool IsOpaque
    {
      get { return GetARGBRecord().A == 0xFF; }
    }

    public Inv.Colour Opacity(float Percent)
    {
      Debug.Assert(Percent >= 0.0F && Percent <= 1.0F);

      if (Percent == 1.0F)
      {
        return this;
      }
      else
      {
        var Record = GetARGBRecord();

        Record.A = (byte)(Record.A * Percent);

        return new Inv.Colour(Record.Argb);
      }
    }
    public Inv.Colour Lighten(float Percent)
    {
      Debug.Assert(Percent >= 0.0F && Percent <= 1.0F);

      var Input = GetHSLRecord();

      var L = Input.L == 0.0 ? Percent : Input.L + (Input.L * Percent);

      if (L > 1.0)
        L = 1.0F;
      else if (L <= 0.0)
        L = 0.0F;

      return ConvertHSLToColor(GetARGBRecord().A, Input.H, Input.S, L);
    }
    public Inv.Colour Darken(float Percent)
    {
      Debug.Assert(Percent >= 0.0F && Percent <= 1.0F);

      var Input = GetHSLRecord();

      // NOTE: asymmetrical with lighten because black can't get any darker.
      var L = Input.L - (Input.L * Percent);

      if (L > 1.0)
        L = 1.0F;
      else if (L <= 0.0)
        L = 0.0F;

      return ConvertHSLToColor(GetARGBRecord().A, Input.H, Input.S, L);
    }
    public Inv.Colour BackgroundToForeground()
    {
      var Input = GetHSLRecord();

      return ConvertHSLToColor(GetARGBRecord().A, Input.H, Input.S, Input.L <= 0.5F ? 1.0F : 0.0F);
    }
    public ColourARGBRecord GetARGBRecord()
    {
      var Result = new ColourARGBRecord();

      Result.Argb = RawValue;

      return Result;
    }
    public ColourHSLRecord GetHSLRecord()
    {
      var Record = GetARGBRecord();

      var Result = new ColourHSLRecord();

      // NOTE: lifted from System.Drawing.Color methods GetHue(), GetSaturation(), GetBrightness()

      // HUE.
      {
        if ((int)Record.R == (int)Record.G && (int)Record.G == (int)Record.B)
        {
          Result.H = 0.0f;
        }
        else
        {
          var num1 = (float)Record.R / (float)byte.MaxValue;
          var num2 = (float)Record.G / (float)byte.MaxValue;
          var num3 = (float)Record.B / (float)byte.MaxValue;
          var num4 = 0.0f;
          var num5 = num1;
          var num6 = num1;
          if ((double)num2 > (double)num5)
            num5 = num2;
          if ((double)num3 > (double)num5)
            num5 = num3;
          if ((double)num2 < (double)num6)
            num6 = num2;
          if ((double)num3 < (double)num6)
            num6 = num3;
          float num7 = num5 - num6;
          if ((double)num1 == (double)num5)
            num4 = (num2 - num3) / num7;
          else if ((double)num2 == (double)num5)
            num4 = (float)(2.0 + ((double)num3 - (double)num1) / (double)num7);
          else if ((double)num3 == (double)num5)
            num4 = (float)(4.0 + ((double)num1 - (double)num2) / (double)num7);
          var num8 = num4 * 60f;
          if ((double)num8 < 0.0)
            num8 += 360f;
          Result.H = num8;
        }
      }

      // SATURATION.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = 0.0f;
        var num5 = (float)num1;
        var num6 = (float)num1;
        if ((double)num2 > (double)num5)
          num5 = num2;
        if ((double)num3 > (double)num5)
          num5 = num3;
        if ((double)num2 < (double)num6)
          num6 = num2;
        if ((double)num3 < (double)num6)
          num6 = num3;
        if ((double)num5 != (double)num6)
          num4 = ((double)num5 + (double)num6) / 2.0 > 0.5 ? (float)(((double)num5 - (double)num6) / (2.0 - (double)num5 - (double)num6)) : (float)(((double)num5 - (double)num6) / ((double)num5 + (double)num6));
        Result.S = num4;
      }

      // BRIGHTNESS.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = (float)num1;
        var num5 = (float)num1;
        if ((double)num2 > (double)num4)
          num4 = num2;
        if ((double)num3 > (double)num4)
          num4 = num3;
        if ((double)num2 < (double)num5)
          num5 = num2;
        if ((double)num3 < (double)num5)
          num5 = num3;

        Result.L = (float)(((double)num4 + (double)num5) / 2.0);
      }

      return Result;
    }
    public int GetArgb()
    {
      return RawValue;
    }

    public static bool operator ==(Colour t1, Colour t2)
    {
      if (object.ReferenceEquals(t1, t2))
        return true;

      if (object.ReferenceEquals(t1, null) || object.ReferenceEquals(t2, null))
        return false;

      return t1.RawValue == t2.RawValue;
    }
    public static bool operator !=(Colour t1, Colour t2)
    {
      if (object.ReferenceEquals(t1, t2))
        return false;

      if (object.ReferenceEquals(t1, null) || object.ReferenceEquals(t2, null))
        return true;

      return t1.RawValue != t2.RawValue;
    }

    public int CompareTo(Colour Colour)
    {
      return this.RawValue.CompareTo(Colour.RawValue);
    }
    public bool EqualTo(Colour Colour)
    {
      return this.RawValue == Colour.RawValue;
    }
    public override bool Equals(object obj)
    {
      var Target = obj as Colour?;

      if (object.ReferenceEquals(Target, null))
        return base.Equals(obj);
      else
        return RawValue == Target.Value.RawValue;
    }
    public override int GetHashCode()
    {
      return RawValue.GetHashCode();
    }

    static Colour()
    {
      List = new Inv.DistinctList<Inv.Colour>();
      NameDictionary = new Bidictionary<Inv.Colour, string>();

      var ColourType = typeof(Inv.Colour);
      var ColourInfo = ColourType.GetReflectionInfo();

      foreach (var Field in ColourInfo.GetReflectionFields().Where(F => F.IsStatic && F.IsPublic && F.FieldType == ColourType))
      {
        var Colour = (Inv.Colour)Field.GetValue(null);
        List.Add(Colour);

        NameDictionary.Add(Colour, Field.Name);
      }
    }

    public static Inv.Colour FromArgb(byte Alpha, byte Red, byte Green, byte Blue)
    {
      var Result = new ColourARGBRecord()
      {
        A = Alpha,
        R = Red,
        G = Green,
        B = Blue
      };

      return new Inv.Colour(Result.Argb);
    }
    public static Inv.Colour FromName(string Name)
    {
      return NameDictionary.GetByRightOrDefault(Name, Inv.Colour.Transparent);
    }

    private static Inv.Colour ConvertHSLToColor(byte alpha, float hue, float saturation, float lighting)
    {
#if DEBUG
      if (0 > alpha || 255 < alpha)
        throw new ArgumentOutOfRangeException("alpha");
      if (0f > hue || 360f < hue)
        throw new ArgumentOutOfRangeException("hue");
      if (0f > saturation || 1f < saturation)
        throw new ArgumentOutOfRangeException("saturation");
      if (0f > lighting || 1f < lighting)
        throw new ArgumentOutOfRangeException("lighting");
#endif
      if (saturation == 0)
        return Inv.Colour.FromArgb(alpha, Convert.ToByte(lighting * 255), Convert.ToByte(lighting * 255), Convert.ToByte(lighting * 255));

      float fMax, fMid, fMin;

      if (0.5 < lighting)
      {
        fMax = lighting - (lighting * saturation) + saturation;
        fMin = lighting + (lighting * saturation) - saturation;
      }
      else
      {
        fMax = lighting + (lighting * saturation);
        fMin = lighting - (lighting * saturation);
      }

      var iSextant = (int)Math.Floor(hue / 60f);

      if (300f <= hue)
        hue -= 360f;

      hue /= 60f;
      hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);

      if (iSextant % 2 == 0)
        fMid = hue * (fMax - fMin) + fMin;
      else
        fMid = fMin - hue * (fMax - fMin);

      var iMax = Convert.ToByte(fMax * 255);
      var iMid = Convert.ToByte(fMid * 255);
      var iMin = Convert.ToByte(fMin * 255);

      switch (iSextant)
      {
        case 1:
          return Inv.Colour.FromArgb(alpha, iMid, iMax, iMin);
        case 2:
          return Inv.Colour.FromArgb(alpha, iMin, iMax, iMid);
        case 3:
          return Inv.Colour.FromArgb(alpha, iMin, iMid, iMax);
        case 4:
          return Inv.Colour.FromArgb(alpha, iMid, iMin, iMax);
        case 5:
          return Inv.Colour.FromArgb(alpha, iMax, iMin, iMid);
        default:
          return Inv.Colour.FromArgb(alpha, iMax, iMid, iMin);
      }
    }

    public static readonly Inv.Colour AliceBlue = new Inv.Colour(0xFFF0F8FF);
    public static readonly Inv.Colour AntiqueWhite = new Inv.Colour(0xFFFAEBD7);
    //public static readonly Inv.Colour Aqua = new Inv.Colour(0xFF00FFFF); // NOTE: duplicate of Cyan
    public static readonly Inv.Colour Aquamarine = new Inv.Colour(0xFF7FFFD4);
    public static readonly Inv.Colour Azure = new Inv.Colour(0xFFF0FFFF);
    public static readonly Inv.Colour Beige = new Inv.Colour(0xFFF5F5DC);
    public static readonly Inv.Colour Bisque = new Inv.Colour(0xFFFFE4C4);
    public static readonly Inv.Colour Black = new Inv.Colour(0xFF000000);
    public static readonly Inv.Colour BlanchedAlmond = new Inv.Colour(0xFFFFEBCD);
    public static readonly Inv.Colour Blue = new Inv.Colour(0xFF0000FF);
    public static readonly Inv.Colour BlueViolet = new Inv.Colour(0xFF8A2BE2);
    public static readonly Inv.Colour Brown = new Inv.Colour(0xFFA52A2A);
    public static readonly Inv.Colour BurlyWood = new Inv.Colour(0xFFDEB887);
    public static readonly Inv.Colour CadetBlue = new Inv.Colour(0xFF5F9EA0);
    public static readonly Inv.Colour Chartreuse = new Inv.Colour(0xFF7FFF00);
    public static readonly Inv.Colour Chocolate = new Inv.Colour(0xFFD2691E);
    public static readonly Inv.Colour Coral = new Inv.Colour(0xFFFF7F50);
    public static readonly Inv.Colour CornflowerBlue = new Inv.Colour(0xFF6495ED);
    public static readonly Inv.Colour Cornsilk = new Inv.Colour(0xFFFFF8DC);
    public static readonly Inv.Colour Crimson = new Inv.Colour(0xFFDC143C);
    public static readonly Inv.Colour Cyan = new Inv.Colour(0xFF00FFFF);
    public static readonly Inv.Colour DarkBlue = new Inv.Colour(0xFF00008B);
    public static readonly Inv.Colour DarkCyan = new Inv.Colour(0xFF008B8B);
    public static readonly Inv.Colour DarkGoldenrod = new Inv.Colour(0xFFB8860B);
    public static readonly Inv.Colour DarkGray = new Inv.Colour(0xFFA9A9A9);
    public static readonly Inv.Colour DarkGreen = new Inv.Colour(0xFF006400);
    public static readonly Inv.Colour DarkKhaki = new Inv.Colour(0xFFBDB76B);
    public static readonly Inv.Colour DarkMagenta = new Inv.Colour(0xFF8B008B);
    public static readonly Inv.Colour DarkOliveGreen = new Inv.Colour(0xFF556B2F);
    public static readonly Inv.Colour DarkOrange = new Inv.Colour(0xFFFF8C00);
    public static readonly Inv.Colour DarkOrchid = new Inv.Colour(0xFF9932CC);
    public static readonly Inv.Colour DarkRed = new Inv.Colour(0xFF8B0000);
    public static readonly Inv.Colour DarkSalmon = new Inv.Colour(0xFFE9967A);
    public static readonly Inv.Colour DarkSeaGreen = new Inv.Colour(0xFF8FBC8F);
    public static readonly Inv.Colour DarkSlateBlue = new Inv.Colour(0xFF483D8B);
    public static readonly Inv.Colour DarkSlateGray = new Inv.Colour(0xFF2F4F4F);
    public static readonly Inv.Colour DarkTurquoise = new Inv.Colour(0xFF00CED1);
    public static readonly Inv.Colour DarkViolet = new Inv.Colour(0xFF9400D3);
    public static readonly Inv.Colour DarkYellow = new Inv.Colour(0xFFCCCC00);
    public static readonly Inv.Colour DeepPink = new Inv.Colour(0xFFFF1493);
    public static readonly Inv.Colour DeepSkyBlue = new Inv.Colour(0xFF00BFFF);
    public static readonly Inv.Colour DimGray = new Inv.Colour(0xFF696969);
    public static readonly Inv.Colour DodgerBlue = new Inv.Colour(0xFF1E90FF);
    public static readonly Inv.Colour Firebrick = new Inv.Colour(0xFFB22222);
    public static readonly Inv.Colour FloralWhite = new Inv.Colour(0xFFFFFAF0);
    public static readonly Inv.Colour ForestGreen = new Inv.Colour(0xFF228B22);
    //public static readonly Inv.Colour Fuchsia = new Inv.Colour(0xFFFF00FF); // NOTE: duplicate of Magenta
    public static readonly Inv.Colour Gainsboro = new Inv.Colour(0xFFDCDCDC);
    public static readonly Inv.Colour GhostWhite = new Inv.Colour(0xFFF8F8FF);
    public static readonly Inv.Colour Gold = new Inv.Colour(0xFFFFD700);
    public static readonly Inv.Colour Goldenrod = new Inv.Colour(0xFFDAA520);
    public static readonly Inv.Colour Gray = new Inv.Colour(0xFF808080);
    public static readonly Inv.Colour Green = new Inv.Colour(0xFF008000);
    public static readonly Inv.Colour GreenYellow = new Inv.Colour(0xFFADFF2F);
    public static readonly Inv.Colour Honeydew = new Inv.Colour(0xFFF0FFF0);
    public static readonly Inv.Colour HotPink = new Inv.Colour(0xFFFF69B4);
    public static readonly Inv.Colour IndianRed = new Inv.Colour(0xFFCD5C5C);
    public static readonly Inv.Colour Indigo = new Inv.Colour(0xFF4B0082);
    public static readonly Inv.Colour Ivory = new Inv.Colour(0xFFFFFFF0);
    public static readonly Inv.Colour Khaki = new Inv.Colour(0xFFF0E68C);
    public static readonly Inv.Colour Lavender = new Inv.Colour(0xFFE6E6FA);
    public static readonly Inv.Colour LavenderBlush = new Inv.Colour(0xFFFFF0F5);
    public static readonly Inv.Colour LawnGreen = new Inv.Colour(0xFF7CFC00);
    public static readonly Inv.Colour LemonChiffon = new Inv.Colour(0xFFFFFACD);
    public static readonly Inv.Colour LightBlue = new Inv.Colour(0xFFADD8E6);
    public static readonly Inv.Colour LightCoral = new Inv.Colour(0xFFF08080);
    public static readonly Inv.Colour LightCyan = new Inv.Colour(0xFFE0FFFF);
    public static readonly Inv.Colour LightGoldenrodYellow = new Inv.Colour(0xFFFAFAD2);
    public static readonly Inv.Colour LightGray = new Inv.Colour(0xFFD3D3D3);
    public static readonly Inv.Colour LightGreen = new Inv.Colour(0xFF90EE90);
    public static readonly Inv.Colour LightPink = new Inv.Colour(0xFFFFB6C1);
    public static readonly Inv.Colour LightSalmon = new Inv.Colour(0xFFFFA07A);
    public static readonly Inv.Colour LightSeaGreen = new Inv.Colour(0xFF20B2AA);
    public static readonly Inv.Colour LightSkyBlue = new Inv.Colour(0xFF87CEFA);
    public static readonly Inv.Colour LightSlateGray = new Inv.Colour(0xFF778899);
    public static readonly Inv.Colour LightSteelBlue = new Inv.Colour(0xFFB0C4DE);
    public static readonly Inv.Colour LightYellow = new Inv.Colour(0xFFFFFFE0);
    public static readonly Inv.Colour Lime = new Inv.Colour(0xFF00FF00);
    public static readonly Inv.Colour LimeGreen = new Inv.Colour(0xFF32CD32);
    public static readonly Inv.Colour Linen = new Inv.Colour(0xFFFAF0E6);
    public static readonly Inv.Colour Magenta = new Inv.Colour(0xFFFF00FF);
    public static readonly Inv.Colour Maroon = new Inv.Colour(0xFF800000);
    public static readonly Inv.Colour MediumAquamarine = new Inv.Colour(0xFF66CDAA);
    public static readonly Inv.Colour MediumBlue = new Inv.Colour(0xFF0000CD);
    public static readonly Inv.Colour MediumOrchid = new Inv.Colour(0xFFBA55D3);
    public static readonly Inv.Colour MediumPurple = new Inv.Colour(0xFF9370DB);
    public static readonly Inv.Colour MediumSeaGreen = new Inv.Colour(0xFF3CB371);
    public static readonly Inv.Colour MediumSlateBlue = new Inv.Colour(0xFF7B68EE);
    public static readonly Inv.Colour MediumSpringGreen = new Inv.Colour(0xFF00FA9A);
    public static readonly Inv.Colour MediumTurquoise = new Inv.Colour(0xFF48D1CC);
    public static readonly Inv.Colour MediumVioletRed = new Inv.Colour(0xFFC71585);
    public static readonly Inv.Colour MidnightBlue = new Inv.Colour(0xFF191970);
    public static readonly Inv.Colour MintCream = new Inv.Colour(0xFFF5FFFA);
    public static readonly Inv.Colour MistyRose = new Inv.Colour(0xFFFFE4E1);
    public static readonly Inv.Colour Moccasin = new Inv.Colour(0xFFFFE4B5);
    public static readonly Inv.Colour NavajoWhite = new Inv.Colour(0xFFFFDEAD);
    public static readonly Inv.Colour Navy = new Inv.Colour(0xFF000080);
    public static readonly Inv.Colour OldLace = new Inv.Colour(0xFFFDF5E6);
    public static readonly Inv.Colour Olive = new Inv.Colour(0xFF808000);
    public static readonly Inv.Colour OliveDrab = new Inv.Colour(0xFF6B8E23);
    public static readonly Inv.Colour Orange = new Inv.Colour(0xFFFFA500);
    public static readonly Inv.Colour OrangeRed = new Inv.Colour(0xFFFF4500);
    public static readonly Inv.Colour Orchid = new Inv.Colour(0xFFDA70D6);
    public static readonly Inv.Colour PaleGoldenrod = new Inv.Colour(0xFFEEE8AA);
    public static readonly Inv.Colour PaleGreen = new Inv.Colour(0xFF98FB98);
    public static readonly Inv.Colour PaleTurquoise = new Inv.Colour(0xFFAFEEEE);
    public static readonly Inv.Colour PaleVioletRed = new Inv.Colour(0xFFDB7093);
    public static readonly Inv.Colour PapayaWhip = new Inv.Colour(0xFFFFEFD5);
    public static readonly Inv.Colour PeachPuff = new Inv.Colour(0xFFFFDAB9);
    public static readonly Inv.Colour Peru = new Inv.Colour(0xFFCD853F);
    public static readonly Inv.Colour Pink = new Inv.Colour(0xFFFFC0CB);
    public static readonly Inv.Colour Plum = new Inv.Colour(0xFFDDA0DD);
    public static readonly Inv.Colour PowderBlue = new Inv.Colour(0xFFB0E0E6);
    public static readonly Inv.Colour Purple = new Inv.Colour(0xFF800080);
    public static readonly Inv.Colour Red = new Inv.Colour(0xFFFF0000);
    public static readonly Inv.Colour RosyBrown = new Inv.Colour(0xFFBC8F8F);
    public static readonly Inv.Colour RoyalBlue = new Inv.Colour(0xFF4169E1);
    public static readonly Inv.Colour SaddleBrown = new Inv.Colour(0xFF8B4513);
    public static readonly Inv.Colour Salmon = new Inv.Colour(0xFFFA8072);
    public static readonly Inv.Colour SandyBrown = new Inv.Colour(0xFFF4A460);
    public static readonly Inv.Colour SeaGreen = new Inv.Colour(0xFF2E8B57);
    public static readonly Inv.Colour SeaShell = new Inv.Colour(0xFFFFF5EE);
    public static readonly Inv.Colour Sienna = new Inv.Colour(0xFFA0522D);
    public static readonly Inv.Colour Silver = new Inv.Colour(0xFFC0C0C0);
    public static readonly Inv.Colour SkyBlue = new Inv.Colour(0xFF87CEEB);
    public static readonly Inv.Colour SlateBlue = new Inv.Colour(0xFF6A5ACD);
    public static readonly Inv.Colour SlateGray = new Inv.Colour(0xFF708090);
    public static readonly Inv.Colour Snow = new Inv.Colour(0xFFFFFAFA);
    public static readonly Inv.Colour SpringGreen = new Inv.Colour(0xFF00FF7F);
    public static readonly Inv.Colour SteelBlue = new Inv.Colour(0xFF4682B4);
    public static readonly Inv.Colour Tan = new Inv.Colour(0xFFD2B48C);
    public static readonly Inv.Colour Teal = new Inv.Colour(0xFF008080);
    public static readonly Inv.Colour Thistle = new Inv.Colour(0xFFD8BFD8);
    public static readonly Inv.Colour Tomato = new Inv.Colour(0xFFFF6347);
    public static readonly Inv.Colour Transparent = new Inv.Colour(0x00FFFFFF);
    public static readonly Inv.Colour Turquoise = new Inv.Colour(0xFF40E0D0);
    public static readonly Inv.Colour Violet = new Inv.Colour(0xFFEE82EE);
    public static readonly Inv.Colour Wheat = new Inv.Colour(0xFFF5DEB3);
    public static readonly Inv.Colour White = new Inv.Colour(0xFFFFFFFF);
    public static readonly Inv.Colour WhiteSmoke = new Inv.Colour(0xFFF5F5F5);
    public static readonly Inv.Colour Yellow = new Inv.Colour(0xFFFFFF00);
    public static readonly Inv.Colour YellowGreen = new Inv.Colour(0xFF9ACD32);
    public static readonly Inv.DistinctList<Inv.Colour> List;

    private static Bidictionary<Inv.Colour, string> NameDictionary;
  }

  public struct ColourHSLRecord
  {
    public float H; // Hue.
    public float S; // Saturation.
    public float L; // Lightness.
  }

  [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Explicit)]
  public struct ColourARGBRecord
  {
    [System.Runtime.InteropServices.FieldOffset(0)]
    public Int32 Argb;
    [System.Runtime.InteropServices.FieldOffset(3)]
    public byte A;
    [System.Runtime.InteropServices.FieldOffset(2)]
    public byte R;
    [System.Runtime.InteropServices.FieldOffset(1)]
    public byte G;
    [System.Runtime.InteropServices.FieldOffset(0)]
    public byte B;
  }

  public sealed class Point
  {
    public Point(int X, int Y)
    {
      this.X = X;
      this.Y = Y;
    }

    public int X { get; private set; }
    public int Y { get; private set; }

    public static Inv.Point operator -(Inv.Point Left, Inv.Point Right)
    {
      return new Inv.Point(Left.X - Right.X, Left.Y - Right.Y);
    }
  }

  public sealed class Rect
  {
    public Rect(int Left, int Top, int Width, int Height)
    {
      Debug.Assert(Width >= 0, "Width must not be negative: " + Width);
      Debug.Assert(Height >= 0, "Height must not be negative: " + Height);

      this.Left = Left;
      this.Top = Top;
      this.Right = Left + Width - 1; // denormalised for use by Android.
      this.Bottom = Top + Height - 1; // denormalised for use by Android.
      this.Width = Width;
      this.Height = Height;
    }

    public int Left { get; private set; }
    public int Top { get; private set; }
    public int Right { get; private set; }
    public int Bottom { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public Inv.Point TopLeft()
    {
      return new Point(Left, Top);
    }
    public Inv.Point TopRight()
    {
      return new Point(Right, Top);
    }
    public Inv.Point BottomLeft()
    {
      return new Point(Left, Bottom);
    }
    public Inv.Point BottomRight()
    {
      return new Point(Right, Bottom);
    }
    public Inv.Point TopCenter()
    {
      return new Point(Left + (Width / 2), Top);
    }
    public Inv.Point BottomCenter()
    {
      return new Point(Left + (Width / 2), Bottom);
    }
    public Inv.Point Center()
    {
      return new Point(Left + (Width / 2), Top + (Height / 2));
    }
    public bool Contains(Inv.Point Point)
    {
      return Point.X >= Left && Point.X <= Right && Point.Y >= Top && Point.Y <= Bottom;
    }
    public Inv.Rect Expand(int Size)
    {
      return new Inv.Rect(Left - Size, Top - Size, Width + (Size * 2), Height + (Size * 2));
    }
  }

  public sealed class Size : Element
  {
    internal Size(Panel Panel)
      : base(Panel)
    {
    }

    public int? Width { get; private set; }
    public int? Height { get; private set; }
    public int? MinimumWidth { get; private set; }
    public int? MinimumHeight { get; private set; }
    public int? MaximumWidth { get; private set; }
    public int? MaximumHeight { get; private set; }

    public void Set(Size Size)
    {
      CheckThreadAffinity();

      if (this.Width != Size.Width || this.Height != Size.Height)
      {
        this.Width = Width;
        this.Height = Height;
        Change();
      }
    }
    public void Set(int Width, int Height)
    {
      CheckThreadAffinity();

      if (this.Width != Width || this.Height != Height)
      {
        Debug.Assert(Width >= 0 && Height >= 0);

        this.Width = Width;
        this.Height = Height;
        Change();
      }
    }
    public void SetWidth(int Width)
    {
      CheckThreadAffinity();

      if (this.Width != Width)
      {
        Debug.Assert(Width >= 0);

        this.Width = Width;
        Change();
      }
    }
    public void SetHeight(int Height)
    {
      CheckThreadAffinity();

      if (this.Height != Height)
      {
        Debug.Assert(Height >= 0);

        this.Height = Height;
        Change();
      }
    }
    public void SetMinimum(int MinimumWidth, int MinimumHeight)
    {
      CheckThreadAffinity();

      if (this.MinimumWidth != MinimumWidth || this.MinimumHeight != MinimumHeight)
      {
        this.MinimumWidth = MinimumWidth;
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    public void SetMinimumWidth(int MinimumWidth)
    {
      CheckThreadAffinity();

      if (this.MinimumWidth != MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;
        Change();
      }
    }
    public void SetMinimumHeight(int MinimumHeight)
    {
      CheckThreadAffinity();

      if (this.MinimumHeight != MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    public void SetMaximum(int MaximumWidth, int MaximumHeight)
    {
      CheckThreadAffinity();

      if (this.MaximumWidth != MaximumWidth || this.MaximumHeight != MaximumHeight)
      {
        this.MaximumWidth = MaximumWidth;
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }
    public void SetMaximumWidth(int MaximumWidth)
    {
      CheckThreadAffinity();

      if (this.MaximumWidth != MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;
        Change();
      }
    }
    public void SetMaximumHeight(int MaximumHeight)
    {
      CheckThreadAffinity();

      if (this.MaximumHeight != MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }

    public void Auto()
    {
      CheckThreadAffinity();

      if (this.Width != null || this.Height != null)
      {
        this.Width = null;
        this.Height = null;
        Change();
      }
    }
    public void AutoWidth()
    {
      CheckThreadAffinity();

      if (Width != null)
      {
        this.Width = null;
        Change();
      }
    }
    public void AutoHeight()
    {
      CheckThreadAffinity();

      if (Height != null)
      {
        this.Height = null;
        Change();
      }
    }
    public void AutoMinimum()
    {
      CheckThreadAffinity();

      if (MinimumWidth != null || MinimumHeight != null)
      {
        this.MinimumWidth = null;
        this.MinimumHeight = null;
        Change();
      }
    }
    public void AutoMinimumWidth()
    {
      CheckThreadAffinity();

      if (MinimumWidth != null)
      {
        this.MinimumWidth = null;
        Change();
      }
    }
    public void AutoMinimumHeight()
    {
      CheckThreadAffinity();

      if (MinimumHeight != null)
      {
        this.MinimumHeight = null;
        Change();
      }
    }
    public void AutoMaximum()
    {
      CheckThreadAffinity();

      if (MaximumWidth != null || MaximumHeight != null)
      {
        this.MaximumWidth = null;
        this.MaximumHeight = null;
        Change();
      }
    }
    public void AutoMaximumWidth()
    {
      CheckThreadAffinity();

      if (MaximumWidth != null)
      {
        this.MaximumWidth = null;
        Change();
      }
    }
    public void AutoMaximumHeight()
    {
      CheckThreadAffinity();

      if (MaximumHeight != null)
      {
        this.MaximumHeight = null;
        Change();
      }
    }
  }

  public sealed class Visibility : Element
  {
    internal Visibility(Panel Panel)
      : base(Panel)
    {
      this.IsVisible = true;
    }

    public event Action ChangeEvent;

    public bool Get()
    {
      CheckThreadAffinity();

      return IsVisible;
    }
    public void Set(bool IsVisible)
    {
      CheckThreadAffinity(); 

      if (this.IsVisible != IsVisible)
      {
        this.IsVisible = IsVisible;

        Change();

        ChangeInvoke();
      }
    }
    public void Show()
    {
      Set(true);
    }
    public void Collapse()
    {
      Set(false);
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private bool IsVisible;
  }

  public sealed class Background : Element
  {
    internal Background(Window Window)
      : base(Window)
    {
    }
    internal Background(Surface Surface)
      : base(Surface)
    {
    }
    internal Background(Panel Panel)
      : base(Panel)
    {
    }

    public Colour? Colour
    {
      get
      {
        CheckThreadAffinity(); 
        
        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    private Colour? ColourField;
  }

  public sealed class Opacity : Element
  {
    internal Opacity(Panel Panel)
      : base(Panel)
    {
      this.Value = 1.00F;
    }

    public float Get()
    {
      CheckThreadAffinity();

      return this.Value;
    }
    public void Set(float Value)
    {
      CheckThreadAffinity();

      if (this.Value != Value)
      {
        this.Value = Value;
        Change();
      }
    }

    internal void BypassSet(float Opacity)
    {
      // set the opacity without notifying the change, useful for animations.
      this.Value = Opacity;
    }

    private float Value;
  }

  public sealed class Alignment : Element
  {
    internal Alignment(Panel Panel)
      : base(Panel)
    {
      this.Placement = Placement.Stretch;
    }

    public Placement Get()
    {
      CheckThreadAffinity();

      return Placement;
    }
    public void Set(Placement Placement)
    {
      CheckThreadAffinity();

      if (this.Placement != Placement)
      {
        this.Placement = Placement;
        Change();
      }
    }
    public void TopLeft()
    {
      Set(Placement.TopLeft);
    }
    public void TopCenter()
    {
      Set(Placement.TopCenter);
    }
    public void TopRight()
    {
      Set(Placement.TopRight);
    }
    public void TopStretch()
    {
      Set(Inv.Placement.TopStretch);
    }
    public void CenterLeft()
    {
      Set(Placement.CenterLeft);
    }
    public void Center()
    {
      Set(Placement.Center);
    }
    public void CenterRight()
    {
      Set(Placement.CenterRight);
    }
    public void CenterStretch()
    {
      Set(Placement.CenterStretch);
    }
    public void BottomLeft()
    {
      Set(Placement.BottomLeft);
    }
    public void BottomCenter()
    {
      Set(Placement.BottomCenter);
    }
    public void BottomRight()
    {
      Set(Placement.BottomRight);
    }
    public void BottomStretch()
    {
      Set(Placement.BottomStretch);
    }
    public void Stretch()
    {
      Set(Placement.Stretch);
    }
    public void StretchLeft()
    {
      Set(Placement.StretchLeft);
    }
    public void StretchRight()
    {
      Set(Placement.StretchRight);
    }
    public void StretchCenter()
    {
      Set(Placement.StretchCenter);
    }

    private Placement Placement;
  }

  public enum Placement
  {
    Stretch,
    StretchLeft,
    StretchCenter,
    StretchRight,
    TopStretch,
    TopLeft,
    TopCenter,
    TopRight,
    CenterStretch,
    CenterLeft,
    Center,
    CenterRight,
    BottomStretch,
    BottomLeft,
    BottomCenter,
    BottomRight
  }

  public sealed class Font : Element
  {
    internal Font(Window Window)
      : base(Window)
    {
      this.WeightField = FontWeight.Regular;
    }
    internal Font(Panel Panel)
      : base(Panel)
    {
      this.WeightField = FontWeight.Regular;
    }

    public string Name
    {
      get
      {
        CheckThreadAffinity(); 
        
        return NameField; 
      }
      set
      {
        CheckThreadAffinity();

        if (NameField != value)
        {
          this.NameField = value;
          Change();
        }
      }
    }
    public int? Size
    {
      get
      {
        CheckThreadAffinity(); 
        
        return SizeField; 
      }
      set
      {
        CheckThreadAffinity();

        Debug.Assert(value != 0, "Font size of zero is not supported.");

        if (SizeField != value)
        {
          this.SizeField = value;
          Change();
        }
      }
    }
    public Colour? Colour
    {
      get
      {
        CheckThreadAffinity(); 
        
        return ColourField; 
      }
      set
      {
        CheckThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }
    public FontWeight Weight
    {
      get
      {
        CheckThreadAffinity();

        return WeightField;
      }
      set
      {
        CheckThreadAffinity();

        if (WeightField != value)
        {
          this.WeightField = value;
          Change();
        }
      }
    }

    public void Thin()
    {
      Weight = FontWeight.Thin;
    }
    public void Light()
    {
      Weight = FontWeight.Light;
    }
    public void Regular()
    {
      Weight = FontWeight.Regular;
    }
    public void Medium()
    {
      Weight = FontWeight.Medium;
    }
    public void Bold()
    {
      Weight = FontWeight.Bold;
    }
    public void Black()
    {
      Weight = FontWeight.Black;
    }

    private string NameField;
    private int? SizeField;
    private Colour? ColourField;
    private FontWeight WeightField;
  }

  public enum FontWeight
  {
    Thin,
    Light,
    Regular,
    Medium,
    Bold,
    Black
  }

  public sealed class CornerRadius : Element
  {
    internal CornerRadius(Panel Panel)
      : base(Panel)
    {
    }

    public int TopLeft { get; private set; }
    public int TopRight { get; private set; }
    public int BottomRight { get; private set; }
    public int BottomLeft { get; private set; }
    public bool IsSet
    {
      get
      {
        CheckThreadAffinity(); 
        
        return TopLeft != 0 || TopRight != 0 || BottomRight != 0 || BottomLeft != 0; 
      }
    }

    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    public void Set(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      CheckThreadAffinity();

      if (this.TopLeft != TopLeft || this.TopRight != TopRight || this.BottomRight != BottomRight || this.BottomLeft != BottomLeft)
      {
        Debug.Assert(TopLeft >= 0 && TopRight >= 0 && BottomRight >= 0 && BottomLeft >= 0);

        this.TopLeft = TopLeft;
        this.TopRight = TopRight;
        this.BottomRight = BottomRight;
        this.BottomLeft = BottomLeft;

        Change();
      }
    }
  }

  public sealed class Edge : Element // Margin & Padding.
  {
    internal Edge(Panel Panel)
      : base(Panel)
    {
    }

    public int Left { get; private set; }
    public int Top { get; private set; }
    public int Right { get; private set; }
    public int Bottom { get; private set; }
    public bool IsSet
    {
      get
      {
        CheckThreadAffinity();

        return Left != 0 || Top != 0 || Right != 0 || Bottom != 0; 
      }
    }

    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    public void Set(int Left, int Top, int Right, int Bottom)
    {
      CheckThreadAffinity();

      if (this.Left != Left || this.Top != Top || this.Right != Right || this.Bottom != Bottom)
      {
        Debug.Assert(Left >= 0 && Top >= 0 && Right >= 0 && Bottom >= 0);

        this.Left = Left;
        this.Top = Top;
        this.Right = Right;
        this.Bottom = Bottom;
        Change();
      }
    }
  }

  public sealed class Elevation : Element
  {
    internal Elevation(Panel Panel)
      : base(Panel)
    {
    }

    public int Depth { get; private set; }
    public Inv.Colour? Colour { get; private set; }

    public void Clear()
    {
      Set(0, null);
    }
    public void Set(int Depth, Inv.Colour? Colour)
    {
      CheckThreadAffinity();

      if (this.Depth != Depth || this.Colour != Colour)
      {
        this.Depth = Depth;
        this.Colour = Colour;
        Change();
      }
    }
  }

  public sealed class Location
  {
    internal Location(Application Application)
    {
      this.Application = Application;
    }

    public bool IsSupported
    {
      get { return Application.Platform.LocationIsSupported; }
    }
    public event Action<Coordinate> ChangeEvent;

    public void Lookup(Inv.Coordinate Coordinate, Action<LocationLookup> ResultAction)
    {
      var Result = new LocationLookup(Coordinate, ResultAction);

      Application.Platform.LocationLookup(Result);
    }

    internal bool IsRequired
    {
      get { return ChangeEvent != null; }
    }

    internal void ChangeInvoke(Coordinate Coordinate)
    {
      if (ChangeEvent != null)
        ChangeEvent(Coordinate);
    }

    private Application Application;
  }

  public sealed class LocationLookup
  {
    internal LocationLookup(Coordinate Coordinate, Action<LocationLookup> ResultAction)
    {
      this.Coordinate = Coordinate;
      this.ResultAction = ResultAction;
    }

    public Coordinate Coordinate { get; private set; }

    public IEnumerable<Placemark> GetPlacemarks()
    {
      return PlacemarkArray;
    }
    public void SetPlacemarks(IEnumerable<Placemark> Placemarks)
    {
      this.PlacemarkArray = Placemarks.ToArray();
      ResultAction(this);
    }

    private Action<LocationLookup> ResultAction;
    private Placemark[] PlacemarkArray;
  }

  public sealed class Placemark
  {
    internal Placemark(string Name, string Locality, string SubLocality, string PostalCode, string AdministrativeArea, string SubAdministrativeArea, string CountryName, string CountryCode, double Latitude, double Longitude)
    {
      this.Name = Name;
      this.Locality = Locality;
      this.SubLocality = SubLocality;
      this.PostalCode = PostalCode;
      this.AdministrativeArea = AdministrativeArea;
      this.SubAdministrativeArea = SubAdministrativeArea;
      this.CountryName = CountryName;
      this.CountryCode = CountryCode;
      this.Latitude = Latitude;
      this.Longitude = Longitude;
    }

    public string Name { get; private set; } // street
    public string Locality { get; private set; } // suburb.
    public string SubLocality { get; private set; } // city.
    public string PostalCode { get; private set; } // postal code
    public string AdministrativeArea { get; private set; } // state
    public string SubAdministrativeArea { get; private set; } // ?
    public string CountryName { get; private set; }
    public string CountryCode { get; private set; }
    public double Latitude { get; private set; }
    public double Longitude { get; private set; }

    public string ToCanonical()
    {
      using (var StringWriter = new System.IO.StringWriter())
      {
        using (var CsvWriter = new Inv.CsvWriter(StringWriter))
          CsvWriter.WriteRecord(Name, Locality, SubLocality, PostalCode, AdministrativeArea, SubAdministrativeArea, CountryName, CountryCode, Latitude.ToString(), Longitude.ToString());

        return StringWriter.ToString();
      }
    }
    public static Placemark FromCanonical(string Text)
    {
      if (Text == null)
        return null;

      using (var StringReader = new System.IO.StringReader(Text))
      {
        using (var CsvReader = new Inv.CsvReader(StringReader))
        {
          var CsvRecord = CsvReader.ReadRecord().ToArray();

          return new Placemark
          (
            Name: CsvRecord.Length > 0 ? CsvRecord[0] : null,
            Locality: CsvRecord.Length > 1 ? CsvRecord[1] : null,
            SubLocality: CsvRecord.Length > 2 ? CsvRecord[2] : null,
            PostalCode: CsvRecord.Length > 3 ? CsvRecord[3] : null,
            AdministrativeArea: CsvRecord.Length > 4 ? CsvRecord[4] : null,
            SubAdministrativeArea: CsvRecord.Length > 5 ? CsvRecord[5] : null,
            CountryName: CsvRecord.Length > 6 ? CsvRecord[6] : null,
            CountryCode : CsvRecord.Length > 7 ? CsvRecord[7] : null,
            Latitude: CsvRecord.Length > 8 ? double.Parse(CsvRecord[8]) : 0.0,
            Longitude: CsvRecord.Length > 9 ? double.Parse(CsvRecord[9]) : 0.0
          );
        }
      }
    }
  }

  public sealed class Coordinate
  {
    internal Coordinate(double Latitude, double Longitude, double Altitude)
    {
      this.Latitude = Latitude;
      this.Longitude = Longitude;
      this.Altitude = Altitude;
    }

    public double Latitude { get; private set; }
    public double Longitude { get; private set; }
    public double Altitude { get; private set; }
  }
}