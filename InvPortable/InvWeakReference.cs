﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class WeakReferenceGovernor
  {
    static WeakReferenceGovernor()
    {
      ManagedCompaction = new WeakReferenceCollection<IWeakReferenceCompaction>();

      CompactionInterval = TimeSpan.FromMinutes(1);
      CompactEventWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);

      CompactionCriticalSection = new ExclusiveCriticalSection("WeakReferenceGovernor-Compaction");

      // NOTE: can't use the ThreadGovernor here as this would create a cycle (ThreadGovernor uses a WeakReference collection).
      CompactionThread = new System.Threading.Tasks.Task(CompactionMethod);
      //CompactionThread.Name = "WeakReferenceGovernor-Compaction";
      //CompactionThread.IsBackground = true;
      CompactionThread.Start();
    }

    public static TimeSpan CompactionInterval { get; set; }
    public static WeakReferenceCollection<T> NewCollection<T>()
    {
      var Result = new WeakReferenceCollection<T>();

      ManagedCompaction.Add(Result);

      return Result;
    }
    public static WeakReferenceAugmentation<TKey, TValue> NewAugmentation<TKey, TValue>()
    {
      var Result = new WeakReferenceAugmentation<TKey, TValue>();

      ManagedCompaction.Add(Result);

      return Result;
    }
    public static void RequestCompact()
    {
      CompactEventWaitHandle.Set();
    }
    public static void Compact()
    {
      // calls to compact are serialised.
      using (CompactionCriticalSection.Lock())
      {
        ManagedCompaction.Compact();

        foreach (var CompactCollection in ManagedCompaction)
          CompactCollection.Compact();
      }
    }

    private static void CompactionMethod()
    {
      // NOTE: this is the only place a 'System.Threading.Thread' should be used directly - use ThreadContext otherwise.

      // NOTE: background threads are terminated with the process.
      try
      {
        do
        {
          CompactEventWaitHandle.WaitOne(CompactionInterval);

          Compact();
        }
        while (true);
      }
      catch (Exception Exception)
      {
        // NOTE: a failed compaction thread means the collections will accumulate 'dead' entries and may impact on performance.
        if (Debugger.IsAttached)
          Debugger.Break();
        else
          Debug.Assert(false, Exception.Message);
      }
    }

    private static EventWaitHandle CompactEventWaitHandle;
    private static WeakReferenceCollection<IWeakReferenceCompaction> ManagedCompaction;
    private static ExclusiveCriticalSection CompactionCriticalSection;
    private static System.Threading.Tasks.Task CompactionThread;
  }

  internal interface IWeakReferenceCompaction
  {
    void Compact();
  }

  public sealed class WeakReferenceCollection<TItem> : IWeakReferenceCompaction, IEnumerable<TItem>
  {
    internal WeakReferenceCollection()
    {
      this.CriticalSection = new ExclusiveCriticalSection("WeakReferenceCollection");
      this.ReferenceList = new DistinctList<WeakReference>();
    }

    public void Clear()
    {
      using (CriticalSection.Lock())
      {
        ReferenceList.Clear();
      }
    }
    public void Add(TItem Item)
    {
      Debug.Assert(Item != null, "Item must be specified.");

      var WeakReference = new WeakReference(Item);

      using (CriticalSection.Lock())
      {
        ReferenceList.Add(WeakReference);
      }
    }
    public void Remove(TItem Item)
    {
      Debug.Assert(Item != null, "Item must be specified.");

      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => Item.Equals(Index.Target));
      }
    }
    public void RemoveRange(IEnumerable<TItem> Items)
    {
      Debug.Assert(Items != null, "Items must be specified.");

      var ItemSet = Items.ToHashSet();

      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => ItemSet.Contains((TItem)Index.Target));
      }
    }
    public TItem RemoveFirstOrDefault()
    {
      using (CriticalSection.Lock())
      {
        for (var WeakReferenceIndex = 0; WeakReferenceIndex < ReferenceList.Count; WeakReferenceIndex++)
        {
          var WeakReference = ReferenceList[WeakReferenceIndex];
          var WeakReferenceTarget = WeakReference.Target;

          if (WeakReferenceTarget != null)
          {
            ReferenceList.RemoveRange(0, WeakReferenceIndex + 1);

            return (TItem)WeakReferenceTarget;
          }
        }

        return default(TItem);
      }
    }
    public int Count
    {
      get
      {
        using (CriticalSection.Lock())
        {
          return ReferenceList.Count(Index => Index.Target != null);
        }
      }
    }

    internal void Compact()
    {
      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => !Index.IsAlive);
      }
    }

    private DistinctList<TItem> Retrieve()
    {
      var Result = new DistinctList<TItem>();

      using (CriticalSection.Lock())
      {
        foreach (var WeakReference in ReferenceList)
        {
          var Item = WeakReference.Target;

          if (Item != null)
            Result.Add((TItem)Item);
        }
      }

      return Result;
    }

    void IWeakReferenceCompaction.Compact()
    {
      Compact();
    }
    IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }

    private ExclusiveCriticalSection CriticalSection;
    private DistinctList<WeakReference> ReferenceList;
  }

  public sealed class WeakReferenceAugmentation<TKey, TValue> : IWeakReferenceCompaction, IEnumerable<KeyValuePair<TKey, TValue>>
  {
    internal WeakReferenceAugmentation()
    {
      this.CriticalSection = new ExclusiveCriticalSection("WeakReferenceAugmentation");
      this.Dictionary = new Dictionary<WeakKey, TValue>(new WeakKeyEqualityComparer());
    }

    public int Count
    {
      get
      {
        using (CriticalSection.Lock())
        {
          return Dictionary.Count(Index => Index.Key.Reference.IsAlive);
        }
      }
    }

    public void Add(TKey Key, TValue Value)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        Dictionary.Add(WeakKey, Value);
      }
    }
    public bool Remove(TKey Key)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.Remove(WeakKey);
      }
    }
    public void Update(TKey Key, Func<TValue, TValue> ModifyFunction)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        var CurrentValue = Dictionary.GetValueOrDefault(WeakKey);

        var ModifyValue = ModifyFunction(CurrentValue);

        if (!object.Equals(ModifyValue, CurrentValue))
          Dictionary[WeakKey] = CurrentValue;
      }
    }
    public bool HasValue(TKey Key)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.ContainsKey(WeakKey);
      }
    }
    public bool TryGetValue(TKey Key, out TValue Value)
    {
      Debug.Assert(Key != null, "Key must not be null.");

      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.TryGetValue(WeakKey, out Value);
      }
    }
    public bool TrySetValue(TKey Key, TValue Value)
    {
      Debug.Assert(Key != null, "Key must not be null.");

      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        if (Dictionary.ContainsKey(WeakKey))
        {
          Dictionary[WeakKey] = Value;

          return true;
        }
        else
        {
          return false;
        }
      }
    }
    public TValue GetValueOrDefault(TKey Key, TValue DefaultValue = default(TValue))
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        TValue Value;
        if (Dictionary.TryGetValue(WeakKey, out Value))
          return Value;
        else
          return DefaultValue;
      }
    }
    public TValue GetValueOrAdd(TKey Key, Func<TKey, TValue> AddFunction)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        TValue Value;
        if (!Dictionary.TryGetValue(WeakKey, out Value))
        {
          Value = AddFunction(Key);

          Dictionary.Add(WeakKey, Value);
        }

        return Value;
      }
    }
    public TValue this[TKey Key]
    {
      get
      {
        Debug.Assert(Key != null, "Key must not be null.");

        var WeakKey = new WeakKey(Key);

        using (CriticalSection.Lock())
        {
          return Dictionary[WeakKey];
        }
      }
      set
      {
        var WeakKey = new WeakKey(Key);

        using (CriticalSection.Lock())
        {
          Dictionary[WeakKey] = value;
        }
      }
    }

    internal void Compact()
    {
      using (CriticalSection.Lock())
        Dictionary.RemoveAll(Index => !Index.Key.Reference.IsAlive);
    }
    internal void Compact(Func<TValue, bool> RemoveWhereFunc)
    {
      using (CriticalSection.Lock())
        Dictionary.RemoveAll(Index => !Index.Key.Reference.IsAlive || RemoveWhereFunc(Index.Value));
    }

    private DistinctList<KeyValuePair<TKey, TValue>> ToDistinctList()
    {
      using (CriticalSection.Lock())
      {
        var Result = new DistinctList<KeyValuePair<TKey, TValue>>(Dictionary.Count);

        foreach (var WeakReference in Dictionary)
        {
          var ReferenceTarget = WeakReference.Key.Reference.Target;

          if (ReferenceTarget != null)
            Result.Add(new KeyValuePair<TKey, TValue>((TKey)ReferenceTarget, WeakReference.Value));
        }

        return Result;
      }
    }

    void IWeakReferenceCompaction.Compact()
    {
      Compact();
    }
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
    {
      return ToDistinctList().GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return ToDistinctList().GetEnumerator();
    }

    private ExclusiveCriticalSection CriticalSection;
    private Dictionary<WeakKey, TValue> Dictionary;

    private sealed class WeakKey
    {
      public WeakKey(TKey Key)
      {
        this.Reference = new WeakReference(Key);
        this.HashCode = Key.GetHashCode();
      }

      public WeakReference Reference { get; private set; }
      public int HashCode { get; private set; }
    }

    private sealed class WeakKeyEqualityComparer : IEqualityComparer<WeakKey>
    {
      bool IEqualityComparer<WeakKey>.Equals(WeakKey x, WeakKey y)
      {
        if (x == y)
        {
          return true;
        }
        else
        {
          var xTarget = x.Reference.Target;
          var yTarget = y.Reference.Target;

          return xTarget != null && yTarget != null && xTarget.Equals(yTarget);
        }
      }
      int IEqualityComparer<WeakKey>.GetHashCode(WeakKey obj)
      {
        return obj.HashCode;
      }
    }
  }
}