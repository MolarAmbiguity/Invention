﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class IniReader : IDisposable
  {
    internal IniReader(string Text)
      : this(new StringReader(Text))
    {
    }
    internal IniReader(Stream Stream)
      : this (new StreamReader(Stream, true))
    {
    }
    internal IniReader(TextReader TextReader)
    {
      this.TextReader = TextReader;
    }
    public void Dispose()
    {
      TextReader.Dispose();
    }

    public string ReadSection()
    {
      var Result = ReadLine();

      if (Result.StartsWith("[") && Result.EndsWith("]"))
        Result = Result.Substring(1, Result.Length - 2);

      return Result;
    }
    public string ReadTuple(string Key)
    {
      var NextLine = ReadLine();

      if (NextLine == null)
        return null;
      
      var TupleArray = NextLine.Split(new char[] { '=' }, 2);

      if (TupleArray.Length == 0)
        throw new Exception(string.Format("Tuple '{0}' was not found.", Key));

      if (TupleArray[0] != Key)
        throw new Exception(string.Format("Tuple key '{0}' was expected but '{1}' was found.", Key, TupleArray[0]));

      if (TupleArray.Length == 2)
        return TupleArray[1];
      else
        return null;
    }

    private string ReadLine()
    {
      var Result = TextReader.ReadLine();
      while (Result != null)
      {
        Result = Result.Trim();

        if (!Result.StartsWith(";")) // semicolon is a single line INI comment.
          break;

        Result = TextReader.ReadLine();
      }
      return Result;
    }

    private TextReader TextReader;
  }

  public sealed class IniWriter : IDisposable
  {
    internal IniWriter(Stream Stream)
      : this(new StreamWriter(Stream, Encoding.UTF8))
    {
    }
    internal IniWriter(TextWriter TextWriter)
    {
      this.TextWriter = TextWriter;
    }
    public void Dispose()
    {
      TextWriter.Dispose();
    }

    public void WriteSection(string Header)
    {
      TextWriter.WriteLine("[" + Header + "]");
    }
    public void WriteTuple(string Key, string Value)
    {
      if (Value == null)
        TextWriter.WriteLine(Key);
      else
        TextWriter.WriteLine(Key + "=" + Value);
    }

    private TextWriter TextWriter;
  }
}
