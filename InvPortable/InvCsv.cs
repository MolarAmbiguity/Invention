﻿/*! 7 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace Inv
{
  public sealed class CsvReader : IDisposable
  {
    internal CsvReader(string Text)
      : this(new StringReader(Text))
    {
    }
    public CsvReader(Stream Stream)
      : this (new StreamReader(Stream, true))
    {
    }
    internal CsvReader(TextReader TextReader)
    {
      this.TextExtractor = new TextExtractor(TextReader);
    }

    public bool More()
    {
      return TextExtractor.More();
    }
    public void ReadRecordFieldArray(params string[] FieldArray)
    {
      var Record = ReadRecord().ToArray();

      if (Record.Length != FieldArray.Length)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected {0} fields but found {1} fields.", Record.Length, FieldArray.Length));

      var FieldEnumerator = FieldArray.GetEnumerator();

      foreach (var Field in Record)
      {
        FieldEnumerator.MoveNext();

        if (Field != (string)FieldEnumerator.Current)
          throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected field {0} but found field {1}", Field, FieldEnumerator.Current));
      }
    }
    public List<string> ReadRecord()
    {
      var Result = new List<string>();

      ReadRecordBegin();

      while (MoreRecordFields())
        Result.Add(ReadRecordField());

      ReadRecordEnd();

      return Result;
    }
    public void ReadRecordFieldValue(string ExpectedValue)
    {
      var FoundValue = ReadRecordField();

      if (FoundValue != ExpectedValue)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected field {0} but found field {1}", ExpectedValue, FoundValue));
    }
    public string ReadRecordField()
    {
      var FieldResult = new StringBuilder();
      var FieldContinue = TextExtractor.More() && !FieldTerminalSet.Contains(TextExtractor.NextCharacter());

      while (FieldContinue)
      {
        if (TextExtractor.NextCharacter() != '"')
        {
          // If it doesn't start with a quote then the entry is ended by a new line or the separator character.
          FieldResult.Append(TextExtractor.ConsumeCharacter());
        }
        else
        {
          // Otherwise, if it is double quoted, the entry is ended only by a closing double quote.
          // Two double quotes within the entry are resolved to one double quote and is not considered a closing quote.

          TextExtractor.ConsumeCharacter('"');

          var ResultStringBuilder = new StringBuilder();
          var QuoteContinue = TextExtractor.More();

          while (QuoteContinue && TextExtractor.More())
          {
            if (TextExtractor.NextCharacter() == '"')
            {
              TextExtractor.ConsumeCharacter('"');

              QuoteContinue = TextExtractor.More() && (TextExtractor.NextCharacter() == '"');

              if (QuoteContinue)
                ResultStringBuilder.Append(TextExtractor.ConsumeCharacter());
            }
            else
            {
              ResultStringBuilder.Append(TextExtractor.ConsumeCharacter());
            }
          }

          if (QuoteContinue)
            throw new UnterminatedFieldException(ResultStringBuilder.ToString());

          FieldResult.Append(ResultStringBuilder);
        }

        FieldContinue = TextExtractor.More() && !FieldTerminalSet.Contains(TextExtractor.NextCharacter());
      }

      HasNextField = false;

      if (TextExtractor.More() && TextExtractor.NextCharacter() == ',')
      {
        TextExtractor.ConsumeCharacter(',');

        HasNextField = true;
      }

      return FieldResult.ToString();
    }
    public void ReadRecordBegin()
    {
      TextExtractor.ConsumeWhileCharacterSet(WhitespaceSet);
    }
    public void ReadRecordEnd()
    {
      TextExtractor.ConsumeWhileCharacterSet(WhitespaceSet);
    }
    public bool MoreRecordFields()
    {
      return HasNextField || (TextExtractor.More() && !VerticalWhitespaceSet.ContainsValue(TextExtractor.NextCharacter()));
    }
    public void SkipRecordField(int Count)
    {
      for (var Index = 0; Index < Count; Index++)
        ReadRecordField();
    }
    public void Dispose()
    {
      TextExtractor.Dispose();
    }

    private TextExtractor TextExtractor;
    private bool HasNextField;

    static CsvReader()
    {
      FieldTerminalSet = new CharacterSet()
      {
        '\0',
        '\n',
        '\r',
        ','
      };

      VerticalWhitespaceSet = new CharacterSet()
      {
        '\n',
        '\r'
      };

      HorizontalWhitespaceSet = new CharacterSet()
      {
        '\0',
        ' ',
        '\t'
      };

      WhitespaceSet = new CharacterSet();
      WhitespaceSet.AddSet(VerticalWhitespaceSet);
      WhitespaceSet.AddSet(HorizontalWhitespaceSet);
    }

    private static CharacterSet FieldTerminalSet;
    private static CharacterSet WhitespaceSet;
    private static CharacterSet HorizontalWhitespaceSet;
    private static CharacterSet VerticalWhitespaceSet;
  }

  public sealed class CsvWriter : IDisposable
  {
    public CsvWriter(Stream Stream)
      : this (new StreamWriter(Stream, Encoding.UTF8))
    {
    }
    internal CsvWriter(TextWriter TextWriter)
    {
      this.TextWriter = TextWriter;
    }

    public void WriteRecordBegin()
    {
      RecordNew = true;
    }
    public void WriteRecordField(string FieldValue)
    {
      if (RecordNew)
      {
        RecordNew = false;
      }
      else
      {
        TextWriter.Write(",");
      }

      if (FieldValue != null)
      {
        if (FieldValue.IndexOfAny("\",\r\n".ToCharArray()) >= 0)
          TextWriter.Write('"' + FieldValue.Replace("\"", "\"\"") + '"');
        else
          TextWriter.Write(FieldValue);
      }
    }
    public void WriteRecordEnd()
    {
      TextWriter.WriteLine();
    }
    public void WriteRecord(params string[] FieldValueArray)
    {
      WriteRecordBegin();

      foreach (var FieldValue in FieldValueArray)
        WriteRecordField(FieldValue);

      WriteRecordEnd();
    }
    public void Flush()
    {
      TextWriter.Flush();
    }
    public void Dispose()
    {
      TextWriter.Dispose();
    }

    private bool RecordNew;
    private TextWriter TextWriter;
  }

  public class UnterminatedFieldException : System.Exception
  {
    public UnterminatedFieldException(string message) : base(message) { }
  }
}
