﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Globalization;

namespace Inv
{
  public sealed class ExclusiveCriticalSection
  {
    public ExclusiveCriticalSection(string Name)
    {
      this.Name = Name;
      this.Handle = new ExclusiveCriticalSectionHandle(this);
    }

    public string Name { get; private set; }
    public bool IsLockHeld
    {
      get { return Monitor.IsEntered(Handle); }
    }

    public IDisposable Lock()
    {
      Enter();
      return Handle;
    }
    public bool TryEnter()
    {
      var Result = Monitor.TryEnter(Handle);
      return Result;
    }
    public void Enter()
    {
      Monitor.Enter(Handle);
    }
    public void Exit()
    {
      Monitor.Exit(Handle);
    }
    public ExclusiveCriticalAccess<T> NewAccess<T>(T Value)
    {
      return new ExclusiveCriticalAccess<T>(this, Value);
    }

    private ExclusiveCriticalSectionHandle Handle;

    private sealed class ExclusiveCriticalSectionHandle : IDisposable
    {
      public ExclusiveCriticalSectionHandle(ExclusiveCriticalSection Section)
      {
        this.Section = Section;
      }

      void IDisposable.Dispose()
      {
        Section.Exit();
      }

      private ExclusiveCriticalSection Section;
    }
  }

  public sealed class ExclusiveCriticalAccess<T>
  {
    internal ExclusiveCriticalAccess(ExclusiveCriticalSection Section, T Value)
    {
      this.Section = Section;
      Backing_Value = Value;
    }

    public T Value
    {
      get
      {
        Debug.Assert(!Section.IsLockHeld, string.Format(CultureInfo.InvariantCulture, "Cannot read this member as the owning critical section '{0}' does not have the lock held.", Section.Name));

        return Backing_Value;
      }
      set
      {
        Debug.Assert(!Section.IsLockHeld, string.Format(CultureInfo.InvariantCulture, "Cannot write this member as the owning critical section '{0}' does not have the lock held.", Section.Name));

        this.Backing_Value = value;
      }
    }

    private ExclusiveCriticalSection Section;
    private T Backing_Value;
  }

  public sealed class ReaderWriterCriticalSection : IDisposable
  {
    public ReaderWriterCriticalSection(string Name)
    {
      this.Name = Name;
      this.Handle = new ReaderWriterLockSlim();
      this.Timeout = TimeSpan.FromSeconds(60);
    }
    public void Dispose()
    {
      // NOTE: IsDisposed is used to deal with a behaviour difference between .NET 4.5.0 and .NET 4.5.1.
      //       in 4.5.0: a double dispose will raise an ObjectDisposedException
      //       in 4.5.1: a double dispose is ignored (as per the .NET specification).
      if (!IsDisposed)
      {
        IsDisposed = true;

        Handle.Dispose();
      }
    }

    public string Name { get; private set; }
    public bool IsWriteLockHeld
    {
      get { return Handle.IsWriteLockHeld; }
    }
    public bool IsReadLockHeld
    {
      get { return Handle.IsReadLockHeld; }
    }
    public bool IsUpgradeableReadLockHeld
    {
      get { return Handle.IsUpgradeableReadLockHeld; }
    }
    public bool IsAnyLockHeld
    {
      get { return Handle.IsReadLockHeld || Handle.IsUpgradeableReadLockHeld || Handle.IsWriteLockHeld; }
    }
    public TimeSpan Timeout { get; set; }

    public ReaderWriterCriticalAccess<T> NewAccess<T>(T Value)
    {
      return new ReaderWriterCriticalAccess<T>(this, Value);
    }
    public ReaderWriterCriticalReadLock EnterReadLock()
    {
      if (Handle.TryEnterReadLock(Timeout))
        return new ReaderWriterCriticalReadLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
    }
    public void ExitReadLock()
    {
      Handle.ExitReadLock();
    }
    public ReaderWriterCriticalWriteLock EnterWriteLock()
    {
      if (Handle.TryEnterWriteLock(Timeout))
        return new ReaderWriterCriticalWriteLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
    }
    public void ExitWriteLock()
    {
      Handle.ExitWriteLock();
    }
    public ReaderWriterCriticalReadLock RequireAnyLock()
    {
      return IsAnyLockHeld ? null : EnterReadLock();
    }
    public ReaderWriterCriticalReadLock RequireReadLock()
    {
      return IsReadLockHeld ? null : EnterReadLock();
    }
    public void InsideReadLock(Action Action)
    {
      using (EnterReadLock())
        Action();
    }
    public T InsideReadLock<T>(Func<T> Action)
    {
      using (EnterReadLock())
        return Action();
    }
    public ReaderWriterCriticalWriteLock RequireWriteLock()
    {
      return IsWriteLockHeld ? null : EnterWriteLock();
    }
    public ReaderWriterCriticalFinalLock EnterFinalLock()
    {
      if (Handle.TryEnterWriteLock(Timeout))
        return new ReaderWriterCriticalFinalLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
    }
    public void InsideWriteLock(Action Action)
    {
      using (EnterWriteLock())
        Action();
    }
    public T InsideWriteLock<T>(Func<T> Action)
    {
      using (EnterWriteLock())
        return Action();
    }
    public void NoTimeout()
    {
      this.Timeout = TimeSpan.FromMilliseconds(-1);
    }

    internal ReaderWriterLockSlim Handle { get; private set; }

    private bool IsDisposed;
  }

  public sealed class ReaderWriterCriticalSectionTimeoutException : Exception
  {
    public ReaderWriterCriticalSectionTimeoutException(ReaderWriterCriticalSection Section)
      : base("ReaderWriterCriticalSection timeout for '" + Section.Name + "'")
    {
    }
  }

  public sealed class ReaderWriterCriticalWriteLock : IDisposable
  {
    internal ReaderWriterCriticalWriteLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitWriteLock();
        Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalFinalLock : IDisposable
  {
    internal ReaderWriterCriticalFinalLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitWriteLock();
        Section.Dispose();
        Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalReadLock : IDisposable
  {
    internal ReaderWriterCriticalReadLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitReadLock();
        Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalUpgradeableReadLock : IDisposable
  {
    internal ReaderWriterCriticalUpgradeableReadLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitUpgradeableReadLock();
        Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalAccess<T>
  {
    internal ReaderWriterCriticalAccess(ReaderWriterCriticalSection Section, T Value)
    {
      this.Section = Section;
      Backing_Value = Value;
    }

    public T Value
    {
      get
      {
        Debug.Assert(Section.IsAnyLockHeld, string.Format(CultureInfo.InvariantCulture, "Cannot read this member as the owning critical section '{0}' does not have at least a read lock held.", Section.Name));

        return Backing_Value;
      }
      set
      {
        Debug.Assert(Section.IsWriteLockHeld, string.Format(CultureInfo.InvariantCulture, "Cannot write this member as the owning critical section '{0}' does not have a write lock held.", Section.Name));

        Backing_Value = value;
      }
    }

    private ReaderWriterCriticalSection Section;
    private T Backing_Value;
  }
}