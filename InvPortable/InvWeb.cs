﻿/*! 10 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public sealed class Web
  {
    internal Web(Application Application)
    {
      this.Application = Application;
    }

    public void Launch(Uri Uri)
    {
      Application.Platform.WebLaunchUri(Uri);
    }
    public WebDownload GetDownload(Uri Uri)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Uri);
      RemoteFileRequest.Method = "GET";

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public WebLink NewLink(string Host, int Port, Inv.Persist.Governor Governor)
    {
      return new WebLink(this, Host, Port, Governor);
    }
    public WebSocket NewSocket(string Host, int Port)
    {
      return new WebSocket(this, Host, Port);
    }
    public WebBroker NewBroker(string BaseUri)
    {
      return new WebBroker(BaseUri);
    }

    internal Application Application { get; private set; }
  }

  public sealed class WebDownload : IDisposable
  {
    internal WebDownload(long Length, System.IO.Stream Stream)
    {
      this.Length = Length;
      this.Stream = Stream;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public long Length { get; private set; }
    public System.IO.Stream Stream { get; private set; }
  }

  public sealed class WebProtocol
  {
    internal WebProtocol(Inv.Persist.Governor Governor)
    {
      this.Governor = Governor;
    }

    public byte[] Encode<TData>(TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        Governor.Save(Data, MemoryStream);

        MemoryStream.Flush();

        return MemoryStream.ToArray();
      }
    }
    public TData Decode<TData>(byte[] DataBuffer)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        return (TData)Governor.Load(MemoryStream);
    }
    public void Decode<TData>(byte[] DataBuffer, TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        Governor.Load(Data, MemoryStream);
    }

    private Inv.Persist.Governor Governor;
  }

  internal enum WebContent
  {
    TextPlain,
    TextCsv,
    TextJson
  }

  public sealed class WebMessage
  {
    public WebMessage(WebMarker Marker, byte[] Buffer)
    {
      this.Marker = Marker;
      this.Buffer = Buffer;
    }

    public WebMarker Marker { get; private set; }
    public byte[] Buffer { get; private set; }
  }

  public enum WebMarker
  {
    Terminal,
    HostMessage,
    LinkMessage,
    HostRequest,
    HostResponse,
    LinkRequest,
    LinkResponse
  }

  public sealed class WebInputStream
  {
    public WebInputStream(System.IO.Stream Base)
    {
      this.Base = Base;
      this.MarkerBuffer = new byte[1];
      this.Int32Buffer = new byte[4];
    }

    public void ReadMarker(WebMarker Marker)
    {
      var Result = PeekMarker();

      if (Result != Marker)
        throw new Exception("Protocol marker mismatch.");

      MarkerBuffer[0] = (byte)WebMarker.Terminal;
    }
    public WebMarker PeekMarker()
    {
      var Result = (WebMarker)MarkerBuffer[0];

      if (Result == WebMarker.Terminal)
      {
        var MarkerLength = Base.Read(MarkerBuffer, 0, 1);
        if (MarkerLength == 0)
          return WebMarker.Terminal;
      }

      return (WebMarker)MarkerBuffer[0];
    }
    public int ReadInt32()
    {
      ReadByteArray(Int32Buffer, 4);

      return BitConverter.ToInt32(Int32Buffer, 0); 
    }
    public void ReadBuffer(byte[] Buffer)
    {
      ReadByteArray(Buffer, Buffer.Length);
    }
       
    private void ReadByteArray(byte[] Buffer, int Length)
    {
      var Index = 0;
      while (Index < Length)
      {
        var Actual = Base.Read(Buffer, Index, Length - Index);

        if (Actual == 0)
          throw new Exception("Disconnection detected.");

        Index += Actual;
      }
    }
    
    private System.IO.Stream Base;
    private byte[] Int32Buffer;
    private byte[] MarkerBuffer;
  }

  public sealed class WebOutputStream
  {
    public WebOutputStream(System.IO.Stream Base)
    {
      this.Base = Base;
      this.Cache = new System.IO.MemoryStream();
      this.MarkerBuffer = new byte[1];
    }

    public void WriteMarker(WebMarker Marker)
    {
      MarkerBuffer[0] = (byte)Marker;
      Cache.Write(MarkerBuffer, 0, 1);
    }
    public void WriteInt32(int Value)
    {
      var LengthBuffer = BitConverter.GetBytes(Value);

      Cache.Write(LengthBuffer, 0, LengthBuffer.Length);
    }
    public void WriteBuffer(byte[] Buffer)
    {
      WriteByteArray(Buffer, Buffer.Length);
    }
    public void Prepare()
    {
      Cache.Position = 0;
      Cache.SetLength(0);
    }
    public void Commit()
    {
      Cache.Flush();

      var CacheArray = Cache.ToArray();
      Base.Write(CacheArray, 0, CacheArray.Length);
    }

    private void WriteByteArray(byte[] Buffer, int Length)
    {
      Cache.Write(Buffer, 0, Length);
    }

    private System.IO.Stream Base;
    private System.IO.MemoryStream Cache;
    private byte[] MarkerBuffer;
  }

  public sealed class WebChannel
  {
    internal WebChannel(WebProtocol WebProtocol, System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.WebProtocol = WebProtocol;
      this.Input = new WebInputStream(InputStream);
      this.Output = new WebOutputStream(OutputStream);

      this.ReceiveCriticalSection = new Inv.ExclusiveCriticalSection("InvWebConnection");

      this.ConsumeWaitHandle = new System.Threading.EventWaitHandle(true, System.Threading.EventResetMode.ManualReset);
      this.ProduceWaitHandle = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.AutoReset);

      this.IsActive = true;

      this.ReceiveTask = System.Threading.Tasks.Task.Run(() =>
      {
        try
        {
          while (IsActive)
          {
            if (ConsumeWaitHandle.WaitOne() && IsActive)
            {
              var Marker = Input.PeekMarker();
              Input.ReadMarker(Marker);

              var DataLength = Input.ReadInt32();

              var DataBuffer = new byte[DataLength];
              Input.ReadBuffer(DataBuffer);

              if (Marker == WebMarker.HostRequest || Marker == WebMarker.LinkRequest)
              {
                Task.Run(() =>
                {
                  var DataValue = WebProtocol.Decode<object>(DataBuffer);
                  ReceiveEvent(DataValue);
                });
              }
              else
              {
                using (ReceiveCriticalSection.Lock())
                {
                  Debug.Assert(this.ReceiveMessage == null);

                  this.ReceiveMessage = new WebMessage(Marker, DataBuffer);

                  ProduceWaitHandle.Set();
                  ConsumeWaitHandle.Reset();
                }
              }
            }
          }
        }
        catch
        {
          this.IsActive = false;
        }
      });

    }
    public void Dispose()
    {
      if (IsActive)
      {
        this.IsActive = false;

        ConsumeWaitHandle.Set();
        ConsumeWaitHandle.Dispose();

        ProduceWaitHandle.Set();
        ProduceWaitHandle.Dispose();

        ReceiveTask.Wait();
      }
    }

    public bool IsActive { get; private set; }
    public WebInputStream Input { get; private set; }
    public WebOutputStream Output { get; private set; }
    public event Action<object> ReceiveEvent;

    public void SendLinkRequest<TData>(TData DataValue)
    {
      Output.WriteMarker(WebMarker.LinkRequest);

      var DataBuffer = WebProtocol.Encode(DataValue);
      Output.WriteInt32(DataBuffer.Length);
      Output.WriteBuffer(DataBuffer);
    }
    public void ReceiveLinkResponse<TData>(TData DataValue)
    {
      ProduceWaitHandle.WaitOne();

      using (ReceiveCriticalSection.Lock())
      {
        if (ReceiveMessage == null || ReceiveMessage.Marker != WebMarker.LinkResponse)
          throw new Exception("Receive message mismatch.");

        WebProtocol.Decode(ReceiveMessage.Buffer, DataValue);

        this.ReceiveMessage = null;
        ConsumeWaitHandle.Set();
      }
    }
    public void SendHostResponse<TData>(TData DataValue)
    {
      Output.WriteMarker(WebMarker.HostResponse);

      var DataBuffer = WebProtocol.Encode(DataValue);
      Output.WriteInt32(DataBuffer.Length);
      Output.WriteBuffer(DataBuffer);
    }
    public void ReceiveHostResponse<TData>(TData DataValue)
    {
      ProduceWaitHandle.WaitOne();

      using (ReceiveCriticalSection.Lock())
      {
        if (ReceiveMessage == null || ReceiveMessage.Marker != WebMarker.HostResponse)
          throw new Exception("Receive message mismatch.");

        WebProtocol.Decode(ReceiveMessage.Buffer, DataValue);

        this.ReceiveMessage = null;
        ConsumeWaitHandle.Set();
      }
    }

    private WebProtocol WebProtocol;
    private System.Threading.Tasks.Task ReceiveTask;
    private System.Threading.EventWaitHandle ProduceWaitHandle;
    private System.Threading.EventWaitHandle ConsumeWaitHandle;
    private ExclusiveCriticalSection ReceiveCriticalSection;
    private WebMessage ReceiveMessage;
  }

  public sealed class WebSocket
  {
    internal WebSocket(Web Web, string Host, int Port)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
    }

    public void Connect()
    {
      if (!IsConnected)
      {
        Web.Application.Platform.WebSocketConnect(this);
        this.IsConnected = true;
      }
    }
    public void Disconnect()
    {
      if (IsConnected)
      {
        this.IsConnected = false;
        Web.Application.Platform.WebSocketDisconnect(this);
      }
    }

    public Web Web { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public WebChannel Channel { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      if (InputStream == null || OutputStream == null)
        this.Channel = null;
      else
        this.Channel = new WebChannel(null, InputStream, OutputStream);
    }

    private bool IsConnected;
  }

  public sealed class WebBroker
  {
    public WebBroker(string BaseUri)
    {
      this.BaseUri = BaseUri;

      //System.Net.WebRequest.DefaultWebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    public WebDownload GetDownload(string QueryText)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      RemoteFileRequest.Method = "GET";

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public string GetPlainText(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";

      using (var WebResponse = WebRequest.GetResponse())
      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
        return StreamReader.ReadToEnd().Trim();
    }
    public IEnumerable<string> GetPlainTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      {
        while (!ResponseReader.EndOfStream)
          yield return ResponseReader.ReadLine();
      }
    }
    public IEnumerable<string[]> GetCsvTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "GET";

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      using (var CsvReader = new Inv.CsvReader(ResponseReader))
      {
        while (!ResponseReader.EndOfStream)
          yield return CsvReader.ReadRecord().ToArray();
      }
    }
    public void PostCsvTextLine(string QueryText, string[] Text)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "POST";

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
      using (var CsvWriter = new Inv.CsvWriter(RequestWriter))
        CsvWriter.WriteRecord(Text);

      WebRequest.GetResponse().Dispose();
    }
    public void PostPlainTextLine(string QueryText, string Line)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "POST";

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.WriteLine(Line);

      WebRequest.GetResponse().Dispose();
    }
    public T GetTextJsonObject<T>(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.ContentType = WebContentType(WebContent.TextJson);
      WebRequest.Method = "GET";

      return ReadObject<T>(WebRequest);
    }
    public void PostTextJsonObject(string QueryText, object Request)
    {
      PostTextJsonObject<object>(QueryText, Request);
    }
    public TResponse PostTextJsonObject<TResponse>(string QueryText, object Request)
    {
      var RequestText = Newtonsoft.Json.JsonConvert.SerializeObject(Request);
      //Debug.WriteLine(RequestText);

      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.Method = "POST";
      WebRequest.ContentType = WebContentType(WebContent.TextJson);
      // TODO: ContentLength?

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.Write(RequestText);

      return ReadObject<TResponse>(WebRequest);
    }

    private T ReadObject<T>(System.Net.HttpWebRequest WebRequest)
    {
      System.Net.HttpWebResponse WebResponse;
      bool WebFailed;
      try
      {
        WebResponse = (System.Net.HttpWebResponse)WebRequest.GetResponse();
        WebFailed = false;
      }
      catch (System.Net.WebException WebException)
      {
        WebResponse = (System.Net.HttpWebResponse)WebException.Response;
        WebFailed = true;
      }
      catch
      {
        throw;
      }

      if (WebResponse == null)
        return default(T);

      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
      {
        var ResponseText = StreamReader.ReadToEnd();

        if (WebFailed)
          throw new Exception(ResponseText);

        var ResponseValue = Deserialize<T>(ResponseText);

        return ResponseValue;
      }
    }
    private T Deserialize<T>(string Text)
    {
      //Debug.WriteLine(Text);

      try
      {
        var Result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Text);

        //Debug.WriteLine(Result);

        return Result;
      }
      catch (Newtonsoft.Json.JsonReaderException)
      {
#if DEBUG
        throw;
#else

        return default(T);
#endif
      }
    }
    private string WebContentType(WebContent Content)
    {
      switch (Content)
      {
        case WebContent.TextCsv:
          return "text/csv";

        case WebContent.TextPlain:
          return "text/plain";

        case WebContent.TextJson:
          return "text/json";

        default:
          throw new Exception("HttpContent not handled: " + Content);
      }
    }

    private string BaseUri;
  }

  public sealed class WebLink
  {
    internal WebLink(Web Web, string Host, int Port, Inv.Persist.Governor Governor)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.Protocol = new Inv.WebProtocol(Governor);
      this.ConnectionList = new DistinctList<WebConnection>();
      this.TypeSwitch = new TypeSwitch<WebConnection>();
    }

    public Inv.WebProtocol Protocol { get; private set; }
    public bool IsActive { get; private set; }

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        lock (ConnectionList)
        {
          ConnectionList.Clear();
          ConnectionList.Add(new WebConnection(this));
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        lock (ConnectionList)
        {
          foreach (var Connection in ConnectionList)
            Connection.Dispose();

          ConnectionList.Clear();
        }
      }
    }
    public void Call<TRequest, TResponse>(TRequest Request, TResponse Response)
    {
      Send(Request);
      Receive(Response);
    }
    public void Send<TData>(TData Data)
    {
      var Connection = TakeConnection();

      Connection.Channel.SendLinkRequest(Data);

      PoolConnection(Connection);
    }
    public void Receive<TMessage>(TMessage Message)
    {
      var Connection = TakeConnection();

      Connection.Channel.ReceiveLinkResponse(Message);

      PoolConnection(Connection);
    }
    public void Compile<TMessage>(object Owner)
    {
      TypeSwitch.Compile<TMessage>(Owner);
    }
    public void Compile<TMessage>(Action<WebConnection, TMessage> Action)
    {
      TypeSwitch.Compile<TMessage>(Action);
    }
    internal Inv.WebSocket NewSocket()
    {
      return Web.NewSocket(Host, Port);
    }
    internal void Receive(WebConnection Connection, object DataValue)
    {
      Web.Application.Platform.WindowAsynchronise(() => TypeSwitch.Execute(Connection, DataValue));
    }

    private WebConnection TakeConnection()
    {
      lock (ConnectionList)
      {
        ConnectionList.RemoveAll(C => !C.IsActive);

        if (ConnectionList.Count > 0)
        {
          var Result = ConnectionList[ConnectionList.Count - 1];
          ConnectionList.RemoveAt(ConnectionList.Count - 1);
          return Result;
        }
        else
        {
          return new WebConnection(this);
        }
      }
    }
    private void PoolConnection(WebConnection Connection)
    {
      lock (ConnectionList)
        ConnectionList.Add(Connection);
    }

    private Inv.Web Web;
    private string Host;
    private int Port;
    private Inv.TypeSwitch<WebConnection> TypeSwitch;
    private Inv.DistinctList<WebConnection> ConnectionList;
  }

  public sealed class WebConnection
  {
    internal WebConnection(WebLink WebLink)
    {
      this.WebProtocol = WebLink.Protocol;
      this.WebSocket = WebLink.NewSocket();
      WebSocket.Connect();

      this.IsActive = true;
    }
    public void Dispose()
    {
      if (IsActive)
      {
        this.IsActive = false;

        WebSocket.Disconnect();
      }
    }

    public bool IsActive { get; private set; }
    public WebChannel Channel
    {
      get { return WebSocket.Channel; }
    }

    private WebSocket WebSocket;
    private WebProtocol WebProtocol;
  }
}
