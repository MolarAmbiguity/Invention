﻿/*! 31 !*/
using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Collections.ObjectModel;
using System.Net;

/// <summary>
/// Namespace containing extension methods for framework classes.
/// </summary>
namespace Inv.Support
{
  public static class ReflectionHelper
  {
    public static MethodInfo GetReflectionMethod(this Type type, string name)
    {
      return type.GetReflectionInfo().GetReflectionMethods().Find(M => M.Name == name);
    }
    public static object GetReflectionValue(this FieldInfo Field, object Self)
    {
      return Field.GetValue(Self);
    }
    public static void SetReflectionValue(this FieldInfo Field, object Self, object Value)
    {
      Field.SetValue(Self, Value);
    }
    public static object ReflectionCreate(this Type Type)
    {
      return Activator.CreateInstance(Type);
    }
    public static ConstructorInfo ReflectionDefaultConstructor(this Type Type)
    {
      return Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 0);
    }
    public static object ReflectionCreate(this Type Type, Type ParamType, object ParamValue)
    {
      // TODO: may be necessary for JSIL.
      var Constructor = Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 1 && C.GetParameters()[0].ParameterType == ParamType);

      if (Constructor == null)
        return Activator.CreateInstance(Type, ParamValue);
      //throw new Exception(Type.FullName + " does not have a default constructor");

      return Constructor.Invoke(new object[] { ParamValue });
    }
    public static object ReflectionCreate(this Type Type, Type ParamType1, object ParamValue1, Type ParamType2, object ParamValue2)
    {
      // TODO: may be necessary for JSIL.
      var Constructor = Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 2 && C.GetParameters()[0].ParameterType == ParamType1 && C.GetParameters()[1].ParameterType == ParamType2);

      if (Constructor == null)
        return Activator.CreateInstance(Type, ParamValue1, ParamValue2);
      //throw new Exception(Type.FullName + " does not have a default constructor");

      return Constructor.Invoke(new object[] { ParamValue1, ParamValue2 });
    }
    public static Type[] GetReflectionTypes(this Assembly Assembly)
    {
      return Assembly.DefinedTypes.Select(T => T.AsType()).ToArray();
    }
    public static TypeInfo GetReflectionInfo(this Type type)
    {
      return type.GetTypeInfo();
    }
    public static FieldInfo[] GetReflectionFields(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredFields.ToArray();
    }
    public static FieldInfo[] GetReflectionFields(this Type Type)
    {
      return Type.GetRuntimeFields().ToArray();
    }
    public static PropertyInfo GetReflectionProperty(this Type Type, string Name)
    {
      return Type.GetTypeInfo().DeclaredProperties.Find(P => P.Name == Name);
    }
    public static FieldInfo GetReflectionField(this Type Type, string Name)
    {
      return Type.GetRuntimeField(Name);
    }
    public static PropertyInfo[] GetReflectionProperties(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredProperties.ToArray();
    }
    public static MethodInfo[] GetReflectionMethods(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredMethods.ToArray();
    }
    public static MethodInfo GetReflectionGetMethod(this PropertyInfo Property)
    {
      return Property.GetMethod;
    }
    public static MethodInfo GetReflectionSetMethod(this PropertyInfo Property)
    {
      return Property.SetMethod;
    }
    public static object GetReflectionValue(this PropertyInfo Property, object Self)
    {
      return Property.GetValue(Self);
    }
    public static void SetReflectionValue(this PropertyInfo Property, object Self, object Value)
    {
      Property.SetValue(Self, Value);
    }
    public static Type[] GetReflectionGenericTypeArguments(this Type Type)
    {
      return Type.GenericTypeArguments;
    }
    public static MethodInfo GetReflectionInfo(this Delegate del)
    {
      return del.GetMethodInfo();
    }
    public static Assembly GetReflectionAssembly(this Type type)
    {
      return type.GetTypeInfo().Assembly;
    }
    public static Type GetReflectionNestedType(this Type Type, string Name)
    {
      var Result = Type.GetTypeInfo().DeclaredNestedTypes.Find(T => T.Name == Name);

      return Result != null ? Result.AsType() : null;
    }
    public static Array GetReflectionEnumValues(this Type enumType)
    {
      Debug.Assert(enumType != null, "Type 'enumType' cannot be null");
      Debug.Assert(enumType.GetTypeInfo().IsEnum, "Type '" + enumType.Name + "' is not an enum");

      var Result = new List<string>();

      var i = 0;
      while (Enum.IsDefined(enumType, i))
      {
        Result.Add(Enum.GetName(enumType, i));
        i++;
      }

      return Result.ToArray();
    }
    public static bool IsInstanceOfType(this Type type, object obj)
    {
      return obj != null && type.GetTypeInfo().IsAssignableFrom(obj.GetType().GetTypeInfo());
    }
  }

  /// <summary>
  /// Static class containing helper functions for <see cref="IEnumerable{T}"/>.
  /// </summary>
  public static class EnumHelper
  {
    /// <summary>
    /// Get a list of values for an Enum.
    /// </summary>
    /// <typeparam name="T">The type to retrieve values for. Must be an Enum.</typeparam>
    /// <returns>An <see cref="IEnumerable{T}"/> of the values of the specified Enum.</returns>
    public static IEnumerable<T> GetEnumerable<T>()
    {
      var EnumType = typeof(T);

      //Debug.Assert(EnumType.IsEnum, "Type must be an enum: " + EnumType.FullName);

      return (IEnumerable<T>)Enum.GetValues(EnumType);
    }

    public static T Parse<T>(string Value, bool IgnoreCase = false)
    {
      return (T)Enum.Parse(typeof(T), Value, IgnoreCase);
    }
    public static T? ParseOrDefault<T>(string Value, T? Default = null, bool IgnoreCase = false) where T : struct
    {
      T Result;

      if (Enum.TryParse(Value, IgnoreCase, out Result))
        return Result;
      else
        return Default;
    }
    public static T? ParseOrDefaultByName<T>(string Value, T? Default = null, bool IgnoreCase = false) where T : struct
    {
      var NameSet = GetEnumerable<T>().ToDictionary(E => E.ToString(), IgnoreCase ? StringComparer.CurrentCultureIgnoreCase : StringComparer.CurrentCulture);

      if (NameSet.ContainsKey(Value))
        return NameSet[Value];
      else
        return Default;
    }

    public static T Next<T>(T Value)
    {
      var List = GetEnumerable<T>().ToList();

      var Index = List.IndexOf(Value) + 1;
      if (Index >= List.Count)
        Index = 0;

      return List[Index];
    }
    public static T Previous<T>(T Value)
    {
      var List = GetEnumerable<T>().ToList();

      var Index = List.IndexOf(Value) - 1;
      if (Index < 0)
        Index = List.Count - 1;

      return List[Index];
    }
  }

  public static class ExceptionHelper
  {
    static ExceptionHelper()
    {
      PreserveMethodInfo = typeof(Exception).GetReflectionMethod("InternalPreserveStackTrace");
    }

    public static Exception Aggregate(this Inv.DistinctList<Exception> ExceptionList, string Message)
    {
      return Aggregate((IEnumerable<Exception>)ExceptionList, Message);
    }
    public static Exception Aggregate(this IEnumerable<Exception> Exceptions, string Message)
    {
      var ExceptionArray = Exceptions.ToArray();

      if (ExceptionArray.Length > 0)
      {
        ExceptionArray.ForEach(Exception => Exception.PreserveStackTrace());

        if (ExceptionArray.Length == 1)
          return ExceptionArray[0];
        else
          return new AggregateException(Message, ExceptionArray);
      }
      else
      {
        return null;
      }
    }
    public static T Preserve<T>(this T Exception)
      where T : Exception
    {
      Exception.PreserveStackTrace();

      return Exception;
    }

    public static string AsReport(this Exception Exception)
    {
      var ExceptionTextStringBuilder = new StringBuilder();

      if (Exception != null)
        BuildExceptionReport(ExceptionTextStringBuilder, Exception);

      return ExceptionTextStringBuilder.ToString();
    }

    private static void BuildExceptionReport(StringBuilder StringBuilder, Exception Exception)
    {
      StringBuilder.AppendLine(Exception.GetType().FullName);
      StringBuilder.AppendLine(Exception.Message);
      StringBuilder.AppendLine();
      StringBuilder.Append(Exception.StackTrace ?? "");

      var ReflectionTypeLoadException = Exception as ReflectionTypeLoadException;

      if (ReflectionTypeLoadException != null)
      {
        foreach (var LoaderSystemException in ReflectionTypeLoadException.LoaderExceptions)
        {
          StringBuilder.AppendLine();
          BuildExceptionReport(StringBuilder, LoaderSystemException);
        }
      }

      var AggregateException = Exception as AggregateException;

      if (AggregateException != null)
      {
        foreach (var InnerAggregateException in AggregateException.InnerExceptions)
        {
          StringBuilder.AppendLine();
          BuildExceptionReport(StringBuilder, InnerAggregateException);
        }
      }

      if (Exception.InnerException != null)
      {
        StringBuilder.AppendLine();
        StringBuilder.AppendLine();
        BuildExceptionReport(StringBuilder, Exception.InnerException);
      }
    }
    private static void PreserveStackTrace(this Exception Exception)
    {
      PreserveMethodInfo.Invoke(Exception, null);
    }

    private static MethodInfo PreserveMethodInfo;
  }

  /// <summary>
  /// Static class containing extension methods for <see cref="Stream"/>.
  /// </summary>
  public static class StreamHelper
  {
    /// <summary>
    /// Compare two <see cref="Stream"/>s.
    /// </summary>
    /// <remarks>
    /// This method will leave both <see cref="Stream"/>s seeked as far forward as required to either discern a difference
    /// between them or to compare every byte in the stream.
    /// </remarks>
    /// <param name="LeftStream">First stream to compare.</param>
    /// <param name="RightStream">Second stream to compare.</param>
    /// <param name="BufferSize">The comparison chunk size, in bytes.</param>
    /// <returns>True if the two streams contain the same data; false otherwise.</returns>
    public static bool EqualTo(this Stream LeftStream, Stream RightStream, int BufferSize)
    {
      Debug.Assert(LeftStream.Position == 0);
      Debug.Assert(RightStream.Position == 0);

      var LeftLength = LeftStream.Length;
      var RightLength = RightStream.Length;

      if (LeftLength != RightLength)
        return false;

      var LeftBuffer = new byte[BufferSize];
      var RightBuffer = new byte[BufferSize];

      var RemainderLength = LeftLength;

      while (RemainderLength > 0)
      {
        var ReadLength = LeftBuffer.Length;

        if (ReadLength > RemainderLength)
          ReadLength = (int)RemainderLength;

        RemainderLength -= ReadLength;

        LeftStream.Read(LeftBuffer, 0, ReadLength);
        RightStream.Read(RightBuffer, 0, ReadLength);

        for (var BufferIndex = 0; BufferIndex < ReadLength; BufferIndex++)
        {
          if (LeftBuffer[BufferIndex] != RightBuffer[BufferIndex])
            return false;
        }
      }

      return true;
    }
  }

  /// <summary>
  /// Static class containing extension methods for memory.
  /// </summary>
  public static class MemoryHelper
  {
    /// <summary>
    /// Extract a slice from the beginning of a byte array.
    /// </summary>
    /// <param name="ByteArray">The source data.</param>
    /// <param name="Length">The number of bytes to copy.</param>
    /// <returns><paramref name="Length"/> bytes from the start of <paramref name="ByteArray"/>.</returns>
    public static byte[] Slice(this byte[] ByteArray, int Length)
    {
      var ResultBuffer = new byte[Length];
      Array.Copy(ByteArray, ResultBuffer, Length);

      return ResultBuffer;
    }

    /// <summary>
    /// Compare this byte array to another byte array.
    /// </summary>
    /// <param name="LeftByteArray">This byte array.</param>
    /// <param name="RightByteArray">The byte array to compare to.</param>
    /// <returns>True if this byte array contains the same data as <paramref name="RightByteArray"/>; false otherwise.</returns>
    public static bool EqualTo(this byte[] LeftByteArray, byte[] RightByteArray)
    {
      var Length = LeftByteArray.Length;
      var Result = Length == RightByteArray.Length;
      var Index = 0;

      while (Result && Index < Length)
      {
        Result = LeftByteArray[Index] == RightByteArray[Index];

        Index++;
      }

      return Result;
    }

    /// <summary>
    /// Compare this byte array to another byte array.
    /// </summary>
    /// <param name="LeftByteArray">This byte array.</param>
    /// <param name="RightByteArray">The byte array to compare to.</param>
    /// <returns><para>Zero if the two arrays are the same length and contain the same data.</para>
    /// <para>- otherwise -</para>
    /// <para>If the two arrays are of differing length: the result of this.Length.CompareTo(<paramref name="RightByteArray"/>.Length)</para>
    /// <para>- otherwise -</para>
    /// <para>If the two arrays of the same length, the result of CompareTo() on the bytes of each array at the first index where the two arrays differ in content.</para>
    /// </returns>
    public static int CompareTo(this byte[] LeftByteArray, byte[] RightByteArray)
    {
      var Length = LeftByteArray.Length;
      var Result = Length.CompareTo(RightByteArray.Length);
      var Index = 0;

      while (Result == 0 && Index < Length)
      {
        Result = LeftByteArray[Index].CompareTo(RightByteArray[Index]);

        Index++;
      }

      return Result;
    }

    /// <summary>
    /// Convert this byte array to a headecimal string.
    /// </summary>
    /// <param name="ByteArray">This byte array.</param>
    /// <param name="Prefix">A string prefix to place before the generated hexadecimal characters.</param>
    /// <returns>A string representing this byte array in hexadecimal digits plus the prefix string if specified.</returns>
    public static string ToHexadecimal(this byte[] ByteArray, string Prefix = null)
    {
      var ResultArray = new char[(ByteArray.Length * 2) + (Prefix == null ? 0 : Prefix.Length)];

      var ResultArrayIndex = 0;

      if (Prefix != null)
      {
        foreach (var PrefixChar in Prefix)
          ResultArray[ResultArrayIndex++] = PrefixChar;
      }

      foreach (var Byte in ByteArray)
      {
        ResultArray[ResultArrayIndex++] = HexadecimalTable[Byte >> 4];
        ResultArray[ResultArrayIndex++] = HexadecimalTable[Byte & 0xF];
      }

      return new string(ResultArray);
    }

    private static char[] HexadecimalTable = new char[16] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  }

  /// <summary>
  /// Static class with extension methods for number value types.
  /// </summary>
  public static class NumberHelper
  {
    public static IEnumerable<int> NumberSeries(this int Count)
    {
      for (var i = 0; i < Count; i++)
        yield return i;
    }

    /// <summary>
    /// Round this decimal value up to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded up to the specified level of accuracy.</returns>
    public static decimal RoundUp(this decimal Source, decimal AccuracyValue)
    {
      return Math.Ceiling(Source / AccuracyValue) * AccuracyValue;
    }
    /// <summary>
    /// Round this decimal value down to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded down to the specified level of accuracy.</returns>
    public static decimal RoundDown(this decimal Source, decimal AccuracyValue)
    {
      return Math.Floor(Source / AccuracyValue) * AccuracyValue;
    }
    /// <summary>
    /// Round this decimal value to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded to the specified level of accuracy.</returns>
    public static decimal RoundNearest(this decimal Source, decimal AccuracyValue)
    {
      return Math.Round(Source / AccuracyValue) * AccuracyValue;
    }
    public static Money? RoundBankers(this Money? Money)
    {
      return Money != null ? Money.Value.RoundBankers() : (Money?)null;
    }
    public static decimal RoundBankers(this decimal Source)
    {
      return Source.RoundToEven(2);
    }
    public static Money? RoundPricing(this Money? Money)
    {
      return Money != null ? Money.Value.RoundPricing() : (Money?)null;
    }
    public static decimal RoundPricing(this decimal Source)
    {
      return Source.RoundAwayFromZero(2);
    }
    public static decimal RoundAwayFromZero(this decimal Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.AwayFromZero);
    }
    public static double RoundAwayFromZero(this double Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.AwayFromZero);
    }
    public static decimal RoundToEven(this decimal Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.ToEven);
    }
    public static double RoundToEven(this double Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.ToEven);
    }
    public static bool Between(this int Value, int LeftValue, int RightValue)
    {
      return Value >= LeftValue && Value <= RightValue;
    }
    public static bool Between(this long Value, long LeftValue, long RightValue)
    {
      return Value >= LeftValue && Value <= RightValue;
    }
    public static decimal Ceiling(this decimal Value)
    {
      return Math.Ceiling(Value);
    }
    public static decimal Floor(this decimal Value)
    {
      return Math.Floor(Value);
    }
    public static decimal Truncate(this decimal Value)
    {
      return Math.Truncate(Value);
    }
    public static double Truncate(this double Value)
    {
      return Math.Truncate(Value);
    }
    public static string ToOrdinal(this int Value)
    {
      switch (Value % 100)
      {
        case 11:
        case 12:
        case 13:
          return Value.ToString() + "th";
      }

      switch (Value % 10)
      {
        case 1:
          return Value.ToString() + "st";
        case 2:
          return Value.ToString() + "nd";
        case 3:
          return Value.ToString() + "rd";
        default:
          return Value.ToString() + "th";
      }
    }
    public static string ToOrdinal(this long Value)
    {
      switch (Value % 100)
      {
        case 11:
        case 12:
        case 13:
          return Value.ToString() + "th";
      }

      switch (Value % 10)
      {
        case 1:
          return Value.ToString() + "st";
        case 2:
          return Value.ToString() + "nd";
        case 3:
          return Value.ToString() + "rd";
        default:
          return Value.ToString() + "th";
      }
    }
    public static int GetPrecision(this decimal Source)
    {
      return (int)BitConverter.GetBytes(decimal.GetBits(Source)[3])[2];
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="DateTime"/> values.
  /// </summary>
  public static class DateTimeHelper
  {
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the year of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfYear(this DateTime Source)
    {
      return new DateTime(Source.Year, 1, 1, 0, 0, 0, 0);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the year of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfYear(this DateTime Source)
    {
      return new DateTime(Source.Year, 12, 31, 23, 59, 59, 999);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the quarter of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the quarter of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfQuarter(this DateTime Source)
    {
      var Quarter = (Source.Month - 1) / 3;
      var Month = (Quarter * 3) + 1;

      return new DateTime(Source.Year, Month, 1);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the quarter of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the quarter of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfQuarter(this DateTime Source)
    {
      var Quarter = (Source.Month - 1) / 3;
      var Month = (Quarter * 3) + 3;

      return new DateTime(Source.Year, Month, DateTime.DaysInMonth(Source.Year, Month));
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the month of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the Month of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfMonth(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, 1, 0, 0, 0, 0);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the month of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the month of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfMonth(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, 1, 23, 59, 59, 999).AddMonths(1).AddDays(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the financial year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the financial year of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfFinancialYear(this DateTime Source)
    {
      var Target = new DateTime(Source.Year, 7, 1, 0, 0, 0, 0);

      if (Source < Target)
        Target = new DateTime(Source.Year - 1, 7, 1, 0, 0, 0, 0);

      return Target;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the financial year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the financial year of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfFinancialYear(this DateTime Source)
    {
      var Target = new DateTime(Source.Year, 06, 30, 23, 59, 59, 999);

      if (Source > Target)
        Target = new DateTime(Source.Year + 1, 06, 30, 23, 59, 59, 999);

      return Target;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the week of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="StartOfWeek">The day considered to be the start of the week.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the week of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfWeek(this DateTime Source, DayOfWeek StartOfWeek)
    {
      var diff = Source.DayOfWeek - StartOfWeek;

      if (diff < 0)
        diff += 7;

      return Source.AddDays(-1 * diff).Date;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the week of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="EndOfWeek">The day considered to be the end of the week.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the week of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfWeek(this DateTime Source, DayOfWeek EndOfWeek)
    {
      var Target = (int)EndOfWeek;

      if (Target < (int)Source.DayOfWeek)
        Target += 7;

      return Source.AddDays(Target - (int)Source.DayOfWeek);
    }
    public static DateTime EndOfWeek(this DateTime Source)
    {
      return EndOfWeek(Source, EnumHelper.Previous(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfDay(this DateTime Source)
    {
      return Source.Date;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfDay(this DateTime Source)
    {
      return Source.Date.AddDays(1).AddTicks(-1);
    }
    /// <summary>
    /// Check whether this <see cref="DateTime"/> is within a threshold amount of another <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Left">This <see cref="DateTime"/>.</param>
    /// <param name="Right">The <see cref="DateTime"/> to compare to.</param>
    /// <param name="Threshold">The amount of leeway to consider two <see cref="DateTime"/>s to be identical.</param>
    /// <returns>True if the difference between this <see cref="DateTime"/> and <paramref name="Right"/> is shorter than <paramref name="Threshold"/></returns>
    public static bool EqualTo(this System.DateTime Left, DateTime Right, TimeSpan Threshold)
    {
      return (Left - Right).Duration() <= Threshold;
    }
    public static bool IsWeekend(this System.DateTime Source)
    {
      var Day = Source.DayOfWeek;

      return Day == DayOfWeek.Saturday || Day == DayOfWeek.Sunday;
    }
    public static bool IsWeekday(this System.DateTime Source)
    {
      return !IsWeekend(Source);
    }
    public static IEnumerable<DateTime> DateSeries(this DateTime FirstDate, DateTime LastDate)
    {
      if (FirstDate > LastDate)
        yield break;

      var CurrentDate = FirstDate.Date;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      } while (CurrentDate <= LastDate.Date);
    }
    public static string ToShortDateString(this DateTimeOffset Source)
    {
      return Source.ToString("d");
    }
    public static string ToShortDateString(this DateTimeOffset? Source)
    {
      return Source == null ? null : Source.Value.ToString("d");
    }

    public static Inv.Date? AsDate(this DateTime? Source)
    {
      return Source != null ? new Inv.Date(Source.Value) : (Inv.Date?)null;
    }
    public static Inv.Date AsDate(this DateTime Source)
    {
      return new Inv.Date(Source);
    }
    public static Inv.Time? AsTime(this DateTime? Source)
    {
      return Source != null ? new Inv.Time(Source.Value) : (Inv.Time?)null;
    }
    public static Inv.Time AsTime(this DateTime Source)
    {
      return new Inv.Time(Source);
    }
    public static string ToShortDateTimeString(this DateTime Source)
    {
      var DateTimeFormat = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat;

      return Source.ToString(DateTimeFormat.ShortDatePattern + " " + DateTimeFormat.ShortTimePattern);
    }

    /// <summary>
    /// Determines if this <see cref="DateTime"/> is between the range of two <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="From">The starting <see cref="DateTime"/>.</param>
    /// <param name="Until">The ending <see cref="DateTime"/>.</param>
    /// <returns>True if this <see cref="DateTime"/> is between the calculated range.</returns>
    public static bool Between(this DateTime Source, DateTime From, DateTime Until)
    {
      return Source >= From && Source <= Until;
    }
  }

  /// <summary>
  /// Static class containing extension methods and helper functions for <see cref="DateTimeOffset"/>.
  /// </summary>
  public static class DateTimeOffsetHelper
  {
    /// <summary>
    /// Check whether this <see cref="DateTimeOffset"/> is within a threshold amount of another <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Left">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="Right">The <see cref="DateTimeOffset"/> to compare to.</param>
    /// <param name="Threshold">The amount of leeway to consider two <see cref="DateTimeOffset"/>s to be identical.</param>
    /// <returns>True if the difference between this <see cref="DateTimeOffset"/> and <paramref name="Right"/> is shorter than <paramref name="Threshold"/></returns>
    public static bool EqualTo(this System.DateTimeOffset Left, DateTimeOffset Right, TimeSpan Threshold)
    {
      return (Left - Right).Duration() <= Threshold;
    }
    public static IEnumerable<DateTimeOffset> DateSeries(this DateTimeOffset FirstDate, DateTimeOffset LastDate)
    {
      if (FirstDate > LastDate)
        yield break;

      var CurrentDate = FirstDate.Date;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      } while (CurrentDate <= LastDate.Date);
    }
    public static void AddRange(this ICollection<DateTimeOffset> Source, IEnumerable<DateTime> Range)
    {
      Source.AddRange(Range.Select(Index => (DateTimeOffset)Index));
    }
    /// <summary>
    /// Returns a <see cref="DateTimeOffset"/> representing the date and time from this <see cref="DateTimeOffset"/> in the specified time zone.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="NewTimeZone">The time zone to adjust this <see cref="DateTimeOffset"/> to.</param>
    /// <returns>A <see cref="DateTimeOffset"/> instance representing this <see cref="DateTimeOffset"/> in the specified time zone.</returns>
    public static DateTimeOffset AlterBaseTimeZone(this DateTimeOffset Source, TimeZoneInfo NewTimeZone)
    {
      var Converted = TimeZoneInfo.ConvertTime(Source, NewTimeZone);
      return Converted.Subtract(Converted.Offset.Subtract(Source.Offset));
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the day of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfDay(this DateTimeOffset Source)
    {
      Debug.Assert(Source.Date != DateTimeOffset.MinValue.Date, "Date must be at least one day greater than '1/01/0001'");

      return Source.Date;
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the day of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfDay(this DateTimeOffset Source)
    {
      Debug.Assert(Source.Date != DateTimeOffset.MaxValue.Date, "Date must be at least one day before '31/12/9999'");

      return Source.Date.AddDays(1).AddTicks(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the week of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="StartOfWeek">The day considered to be the start of the week.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the week of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfWeek(this DateTimeOffset Source, DayOfWeek StartOfWeek)
    {
      var Difference = Source.DayOfWeek - StartOfWeek;

      if (Difference < 0)
        Difference += 7;

      return Source.AddDays(-1 * Difference).Date;
    }
    public static DateTimeOffset StartOfWeek(this DateTimeOffset Source)
    {
      return StartOfWeek(Source, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the week of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="EndOfWeek">The day considered to be the end of the week.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the week of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfWeek(this DateTimeOffset Source, DayOfWeek EndOfWeek)
    {
      var Target = (int)EndOfWeek;

      if (Target < (int)Source.DayOfWeek)
        Target += 7;

      return Source.AddDays(Target - (int)Source.DayOfWeek).EndOfDay();
    }
    public static DateTimeOffset EndOfWeek(this DateTimeOffset Source)
    {
      return EndOfWeek(Source, EnumHelper.Previous(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
    }
    public static DateTimeOffset StartOfMonth(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, Source.Month, 1, 0, 0, 0, TimeSpan.Zero);
    }
    public static DateTimeOffset EndOfMonth(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, Source.Month, DateTime.DaysInMonth(Source.Year, Source.Month), 0, 0, 0, TimeSpan.Zero).EndOfDay();
    }
    public static DateTimeOffset StartOfYear(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, 1, 1, 0, 0, 0, TimeSpan.Zero);
    }
    public static DateTimeOffset EndOfYear(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, 12, 31, 0, 0, 0, TimeSpan.Zero).EndOfDay();
    }
    public static string ToVerboseString(this DateTimeOffset Source)
    {
      return string.Format("{0} {1}", Source.AsDate().ToVerboseString(), Source.AsTime().ToString());
    }
    public static string ToLocalShortDateTimeString(this DateTimeOffset Source)
    {
      return Source.ToLocalTime().DateTime.ToString("g");
    }
    public static string ToLocalShortDateTimeString(this DateTimeOffset? Source)
    {
      return Source == null ? null : Source.Value.ToLocalShortDateTimeString();
    }
    public static string ToShortDateString(this DateTimeOffset Source)
    {
      return Source.ToString("d");
    }
    public static string ToShortDateString(this DateTimeOffset? Source)
    {
      return Source == null ? null : Source.Value.ToString("d");
    }
    /// <summary>
    /// Converts the value of the current <see cref="DateTimeOffset"/> object to its equivalent string representation relative to the current date.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="string"/> representing this <see cref="DateTimeOffset"/> relative to the current date.</returns>
    public static string ToRelativeString(this DateTimeOffset Source)
    {
      return Source.ToRelativeString(DateTimeOffset.Now);
    }
    /// <summary>
    /// Converts the value of the current <see cref="DateTimeOffset"/> object to its equivalent string representation relative to some specified date.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="RelativeDate">The <see cref="DateTimeOffset"/> to treat as the base date.</param>
    /// <returns>A <see cref="string"/> representing this <see cref="DateTimeOffset"/> relative to the specified date.</returns>
    public static string ToRelativeString(this DateTimeOffset Source, DateTimeOffset RelativeDate)
    {
      string Format;

      if (Source.Date == RelativeDate.Date)
        Format = "{0:t}";
      else if (Source.Year != RelativeDate.Year)
        Format = string.Format("{{0:{0}}}", YearMonthDayFormat);
      else if (Source.Month != RelativeDate.Month)
        Format = string.Format("{{0:{0}}}", MonthDayFormat);
      else
        Format = string.Format("{{0:{0}}}", DayFormat);

      return string.Format(Format, Source);
    }
    /// <summary>
    /// Formats a range formed by two <see cref="DateTimeOffset"/> instances for display.
    /// </summary>
    /// <param name="From">The starting <see cref="DateTimeOffset"/>.</param>
    /// <param name="Until">The ending <see cref="DateTimeOffset"/>.</param>
    /// <param name="VerboseSameDay">Whether output should be verbose when both range parameters represent the same day.</param>
    /// <returns>A <see cref="string"/> representing the date range.</returns>
    public static string FormatDateRange(DateTimeOffset From, DateTimeOffset Until, bool VerboseSameDay = false)
    {
      var SameDayFormat = (VerboseSameDay ? "D" : YearMonthDayFormat);
      return string.Format(From.Date == Until.Date ? string.Format("{{0:{0}}} {{0:t}} - {{1:t}} ({{0:%z}})", SameDayFormat) : string.Format("{0} until {{1:{1}}}", From.ToRelativeString(Until), YearMonthDayFormat), From, Until);
    }
    /// <summary>
    /// Determines if this <see cref="DateTimeOffset"/> is between the range of two <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="From">The starting <see cref="DateTimeOffset"/>.</param>
    /// <param name="Until">The ending <see cref="DateTimeOffset"/>.</param>
    /// <returns>True if this <see cref="DateTimeOffset"/> is between the calculated range.</returns>
    public static bool IsBetween(this DateTimeOffset Source, DateTimeOffset From, DateTimeOffset Until)
    {
      return Source >= From && Source <= Until;
    }
    public static Inv.Date AsDate(this DateTimeOffset Source)
    {
      return new Inv.Date(Source.Date);
    }
    public static Inv.Date? AsDate(this DateTimeOffset? Source)
    {
      if (Source == null)
        return null;
      else
        return new Inv.Date(Source.Value.Date);
    }
    public static Inv.Time AsTime(this DateTimeOffset Source)
    {
      return new Inv.Time(Source.TimeOfDay);
    }
    public static Inv.Time? AsTime(this DateTimeOffset? Source)
    {
      if (Source == null)
        return null;
      else
        return new Inv.Time(Source.Value.TimeOfDay);
    }
    public static DateTimeOffset TruncateMilliseconds(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Ticks - (Source.Ticks % TimeSpan.TicksPerSecond), Source.Offset);
    }

    private const string YearMonthDayFormat = "ddd, d MMM yyyy";
    private const string MonthDayFormat = "ddd, d MMM";
    private const string DayFormat = "ddd, d";
  }

  /// <summary>
  /// Static class containing extension methods for <see cref="TimeSpan"/>.
  /// </summary>
  public static class TimeSpanHelper
  {
    /// <summary>
    /// Truncates the millisecond field from this <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="Source">This <see cref="TimeSpan"/>.</param>
    /// <returns>A <see cref="TimeSpan"/> object equivalent to this <see cref="TimeSpan"/> with the millisecond field zeroed out.</returns>
    public static TimeSpan TruncateMilliseconds(this TimeSpan Source)
    {
      return new TimeSpan(Source.Days, Source.Hours, Source.Minutes, Source.Seconds);
    }

    /// <summary>
    /// Format this <see cref="TimeSpan"/> into a human-readable string.
    /// </summary>
    /// <param name="Source">This <see cref="TimeSpan"/>.</param>
    /// <returns>A human-readable string representing this <see cref="TimeSpan"/>.</returns>
    public static string FormatTimeSpan(this TimeSpan Source)
    {
      var Result = "";

      if (Source < new TimeSpan(0, 0, 0, 1))
        Result = Source.Milliseconds + " ms";
      else if (Source < new TimeSpan(0, 1, 0))
        Result = Source.Seconds + " secs";
      else if (Source < new TimeSpan(1, 0, 0))
        Result = Source.Minutes + " mins";
      else if (Source < new TimeSpan(1, 0, 0, 0))
        Result = Source.Hours + " hrs";
      else
        Result = Source.Days + " days";

      return Result;
    }

    public static TimeSpan Max(this TimeSpan Left, TimeSpan Right)
    {
      if (Left > Right)
        return Left;
      else
        return Right;
    }

    public static TimeSpan Min(this TimeSpan Left, TimeSpan Right)
    {
      if (Left < Right)
        return Left;
      else
        return Right;
    }
  }

  public static class CharHelper
  {
    public static bool IsNumeric(this char Source)
    {
      int Value;
      return int.TryParse(Source.ToString(), out Value);
    }
    public static bool IsAlphabetic(this char Source)
    {
      return (Source >= 'a' && Source <= 'z') || (Source >= 'A' && Source <= 'Z');
    }
    public static bool IsVowel(this char Source)
    {
      var LowerSource = char.ToLower(Source);

      return LowerSource == 'a' || LowerSource == 'e' || LowerSource == 'i' || LowerSource == 'o' || LowerSource == 'u';
    }
  }

  public static class Int32Helper
  {
    public static int RoundUpToPowerOf2(this int Value)
    {
      var v = (uint)Value;

      v--;
      v |= v >> 1;
      v |= v >> 2;
      v |= v >> 4;
      v |= v >> 8;
      v |= v >> 16;
      v++;

      return (int)v;
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="String"/>.
  /// </summary>
  public static class StringHelper
  {
    public static string ToTitleCase(this string Source)
    {
      if (Source == null)
        return Source;

      var WordArray = Source.Split(' ');
      for (var WordIndex = 0; WordIndex < WordArray.Length; WordIndex++)
      {
        if (WordArray[WordIndex].Length == 0)
          continue;

        var FirstChar = char.ToUpper(WordArray[WordIndex][0]);
        var RestChars = "";

        if (WordArray[WordIndex].Length > 1)
          RestChars = WordArray[WordIndex].Substring(1).ToLower();

        WordArray[WordIndex] = FirstChar + RestChars;
      }

      return string.Join(" ", WordArray);
    }
    public static string ToSentenceCase(this string Source)
    {
#if NET40
      if (Source.Length <= 1)
      {
        return Source;
      }
      else
      {
        var Previous = Source[0];
        var Result = new StringBuilder();

        Result.Append(Previous);

        for (var Index = 1; Index < Source.Length - 1; Index++)
        {
          var Current = Source[Index];
          var Next = Source[Index + 1];

          if (char.IsWhiteSpace(Current))
          {
            Result.Append(Current);
          }
          else if (Current == '_')
          {
            Result.Append(' ');
          }
          else if (char.IsUpper(Previous) && char.IsUpper(Current) && char.IsLower(Next))
          {
            Result.Append(' ');

            if (Index > 1)
              Result.Append(char.ToLower(Current));
            else
              Result.Append(Current);
          }
          else
          {
            if (char.IsLower(Previous) && char.IsUpper(Current))
              Result.Append(' ');

            if (Index > 1 && char.IsLower(Next))
              Result.Append(char.ToLower(Current));
            else
              Result.Append(Current);
          }

          Previous = Current;
        }

        var Last = Source[Source.Length - 1];

        if (!char.IsUpper(Previous) && char.IsUpper(Last))
          Result.Append(' ');

        Result.Append(Last);

        return Result.ToString();
      }
#else // TODO: Regex implementation not supported in JSIL.
      var LowerCase = Source.ToLower();
      var Expression = new System.Text.RegularExpressions.Regex(@"(^[a-z])|\.\s+(.)", System.Text.RegularExpressions.RegexOptions.ExplicitCapture);
      return Expression.Replace(LowerCase, S => S.Value.ToUpper());
#endif
    }
    public static string Singular(this string Source, long Count = 1)
    {
      var Result = Source;

      if (!string.IsNullOrEmpty(Source) && Count == 1)
      {
        string[] Exceptions = { "series" };
        char[] ArrayJXO = { 'j', 'x', 'o' };
        char[] ArrayCS = { 'c', 's' };
        char[] ArraySH = { 's', 'h' };

        var StringLength = Source.Length;

        if ((StringLength <= 0) || (StringLength == 1) || (Source[StringLength - 1] != 's'))
        {
          Result = Source;
        }
        else
        {
          var Exception = false;
          var Loop = 0;
          var ExceptionCount = Exceptions.Length;

          while (!Exception && (Loop < ExceptionCount))
          {
            Exception = Source.EndsWith(Exceptions[Loop], StringComparison.CurrentCultureIgnoreCase);

            Loop++;
          }

          if (Exception)
          {
            Result = Source;
          }
          else if (Source[StringLength - 2] == 'e')
          {
            if ((StringLength > 2) && (Source[StringLength - 3] == 'i'))
            {
              if (StringLength > 3)
                Result = Source.Substring(0, StringLength - 3) + 'y';
              else
                Result = Source.Substring(0, StringLength - 1);
            }
            else if (((StringLength > 3) &&
                    (ArrayJXO.Contains(Source[StringLength - 3]) || // eg. boxes
                    (Source[StringLength - 4].Equals('z') && Source[StringLength - 3].Equals('z')) || // eg. fuzzes
                    (Source.EndsWith("aliases", StringComparison.CurrentCultureIgnoreCase)) || // eg. aliases
                    (ArrayCS.Contains(Source[StringLength - 4]) && ArraySH.Contains(Source[StringLength - 3])) // eg. beaches, gasses
                )))
            {
              Result = Source.Substring(0, StringLength - 2);
            }
            else
            {
              Result = Source.Substring(0, StringLength - 1);
            }
          }
          else
          {
            Result = Source.Substring(0, StringLength - 1);
          }
        }
      }

      return Result;
    }
    public static string Plural(this string Source, decimal Count)
    {
      return Plural(Source, Count == 1 ? 1L : 0L);
    }
    /// <summary>
    /// Pluralise this <see cref="String"/> for the given number of instances.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Count">The number to pluralise for.</param>
    /// <returns>A plural version of this <see cref="String"/>.</returns>
    public static string Plural(this string Source, int Count = 0)
    {
      string Result = Source;

      if (Count != 1 && !string.IsNullOrEmpty(Source))
      {
        if (Source.EndsWith("foot"))
        {
          Result = Source.Substring(Source.Length - 4) + "feet";
        }
        else
        {
          var Exceptions = new[] { "series", "sheep", "fish", "tuna", "manes", "tengu", "ronin", "undead" };
          var IsException = false;
          var Index = 0;

          while (!IsException && Index < Exceptions.Length)
          {
            IsException = Source.EndsWith(Exceptions[Index]);
            Index++;
          }

          if (!IsException)
          {
            Result = Source;

            char[] Vowels = { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };
            string Pluralisation = String.Empty;
            var LastCharacter = Source[Source.Length - 1];

            switch (char.ToUpper(LastCharacter))
            {
              case 'H':
                var SecondLastCharacter = char.ToUpper(Source[Source.Length - 2]);
                if (Source.Length > 1 && (SecondLastCharacter == 'T' || SecondLastCharacter == 'G'))
                  Pluralisation = "s";
                else
                  Pluralisation = "es";
                break;

              case 'S':
                if (Source.Length > 2 && Source.Substring(Source.Length - 2).Equals("es", StringComparison.OrdinalIgnoreCase))
                  Pluralisation = "";
                else
                  Pluralisation = "es";
                break;

              case 'X':
              case 'J':
              case 'Z':
                Pluralisation = "es";
                break;

              case 'Y':
                if (Source.Length > 2 && (!Vowels.Contains(Source[Source.Length - 2])))
                {
                  Result = Source.Substring(0, Source.Length - 1);
                  Pluralisation = "ies";
                }
                else
                {
                  Pluralisation = "s";
                }
                break;

              default:
                Pluralisation = "s";
                break;
            }

            if (char.IsUpper(LastCharacter))
              Pluralisation = Pluralisation.ToUpper();

            Result += Pluralisation;
          }
        }
      }

      return Result;
    }
    /// <summary>
    /// Return this <see cref="String"/>, or null if this <see cref="String"/> is empty.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, or null if this <see cref="String"/> is empty.</returns>
    public static string EmptyAsNull(this string Source)
    {
      if (Source == "")
        return null;
      else
        return Source;
    }
    /// <summary>
    /// Return this <see cref="String"/>, or an empty string if this <see cref="String"/> is null.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, or an empty string if this <see cref="String"/> is null.</returns>
    public static string NullAsEmpty(this string Source)
    {
      if (Source == null)
        return "";
      else
        return Source;
    }
    /// <summary>
    /// 'Reduce' a string by removing every second occurrence of <paramref name="Character"/> from the string.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Character">The character to remove.</param>
    /// <returns>A reduced string where every second occurrence of <paramref name="Character"/> has been removed.</returns>
    public static string Reduce(this string Source, char Character)
    {
      var ResultStringBuilder = new StringBuilder();

      var Reducing = false;

      foreach (var Item in Source)
      {
        if (Item == Character)
        {
          if (!Reducing)
          {
            Reducing = true;
            ResultStringBuilder.Append(Character);
          }
        }
        else
        {
          Reducing = false;
          ResultStringBuilder.Append(Item);
        }
      }

      return ResultStringBuilder.ToString();
    }
    /// <summary>
    /// Count the number of occurrences of <paramref name="Character"/> in this <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Character">The character to count.</param>
    /// <returns>The number of occurrences of <paramref name="Character"/> in this <see cref="String"/>.</returns>
    public static int Count(this string Source, char Character)
    {
      int Result = 0;

      foreach (var EachCharacter in Source)
      {
        if (EachCharacter == Character)
          Result++;
      }

      return Result;
    }
    /// <summary>
    /// Convert this <see cref="String"/> into a format valid for use as a C# identifier.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, converted into a format valid for use as a C# identifier.</returns>
    public static string ConvertToCSharpIdentifier(this string Source)
    {
      var StringBuilder = new StringBuilder();

      foreach (var Index in Source)
      {
        if (char.IsDigit(Index) || char.IsLetter(Index) || Index == '_')
        {
          if (StringBuilder.Length == 0 && char.IsDigit(Index))
            StringBuilder.Append('_');

          StringBuilder.Append(Index);
        }
      }

      return StringBuilder.ToString();
    }
    public static string ConvertToCSharpString(this string Source)
    {
      if (Source == null)
        return "null";
      else
        return "\"" + Source.Replace(@"\", @"\\").Replace("\t", @"\t").Replace("\r", @"\r").Replace("\n", @"\n").Replace("\"", "\\\"") + "\"";
    }
    /// <summary>
    /// Strip every occurrence of any character in <paramref name="StripArray"/> from this <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="StripArray">The characters to strip from this <see cref="String"/>.</param>
    /// <returns>A copy of this <see cref="String"/> where any characters in <paramref name="StripArray"/> have been removed.</returns>
    public static string Strip(this string Source, params char[] StripArray)
    {
      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (!StripArray.Contains(Index))
          Result.Append(Index);
      }

      return Result.ToString();
    }
    public static string Strip(this string Source, Func<char, bool> StripFunction)
    {
      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (!StripFunction(Index))
          Result.Append(Index);
      }

      return Result.ToString();
    }
    public static string Replace(this string Source, Func<char, bool> FindFunction, char ReplaceCharacter)
    {
      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (FindFunction(Index))
          Result.Append(ReplaceCharacter);
        else
          Result.Append(Index);
      }

      return Result.ToString();
    }
    /// <summary>
    /// Transform a Pascal case string to sentence case on the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>Sentence case version of this <see cref="String"/>.</returns>
    public static string PascalCaseToSentenceCase(this string Source)
    {
      if (Source.Length <= 1)
      {
        return Source;
      }
      else
      {
        var Previous = Source[0];
        var Result = new StringBuilder();

        Result.Append(Previous);

        for (var Index = 1; Index < Source.Length - 1; Index++)
        {
          var Current = Source[Index];
          var Next = Source[Index + 1];

          if (char.IsWhiteSpace(Current))
          {
            Result.Append(Current);
          }
          else if (Current == '_')
          {
            Result.Append(' ');
          }
          else if (char.IsUpper(Previous) && char.IsUpper(Current) && char.IsLower(Next))
          {
            Result.Append(' ');

            if (Index > 1)
              Result.Append(char.ToLower(Current));
            else
              Result.Append(Current);
          }
          else
          {
            if (char.IsLower(Previous) && char.IsUpper(Current))
              Result.Append(' ');

            if (Index > 1 && char.IsLower(Next))
              Result.Append(char.ToLower(Current));
            else
              Result.Append(Current);
          }

          Previous = Current;
        }

        var Last = Source[Source.Length - 1];

        if (!char.IsUpper(Previous) && char.IsUpper(Last))
          Result.Append(' ');

        Result.Append(Last);

        return Result.ToString();
      }
    }
    /// <summary>
    /// Transform a Pascal case string to title case on the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>Title case version of this <see cref="String"/>.</returns>
    public static string PascalCaseToTitleCase(this string Source)
    {
      if (Source == null || Source.Length <= 1)
      {
        return Source;
      }
      else
      {
        var Previous = Source[0];
        var Result = new StringBuilder();

        Result.Append(Previous);

        for (var Index = 1; Index < Source.Length - 1; Index++)
        {
          var Current = Source[Index];
          var Next = Source[Index + 1];

          if (char.IsWhiteSpace(Current))
          {
            Result.Append(Current);
          }
          else if (Current == '_')
          {
            Result.Append(' ');
          }
          else if (char.IsUpper(Previous) && char.IsUpper(Current) && char.IsLower(Next))
          {
            Result.Append(' ');
            Result.Append(Current);
          }
          else if (char.IsLower(Previous) && char.IsUpper(Current))
          {
            Result.Append(' ');
            Result.Append(Current);
          }
          else
          {
            Result.Append(Current);
          }

          Previous = Current;
        }

        var Last = Source[Source.Length - 1];

        if (!char.IsUpper(Previous) && char.IsUpper(Last))
          Result.Append(' ');

        Result.Append(Last);

        return Result.ToString();
      }
    }
    public static string ProperCaseToCommonCase(this string Source)
    {
      if (Source == null || Source.Length <= 1)
      {
        return Source;
      }
      else
      {
        var Result = new StringBuilder();

        var AllCapsRun = false;

        for (var Index = 0; Index < Source.Length - 1; Index++)
        {
          var Current = Source[Index];
          var Next = Source[Index + 1];

          if (!char.IsUpper(Current))
          {
            Result.Append(Current);
          }
          else if (char.IsUpper(Next))
          {
            Result.Append(Current);
            AllCapsRun = true;
          }
          else if (AllCapsRun)
          {
            AllCapsRun = false;
            Result.Append(Current);
          }
          else
          {
            Result.Append(char.ToLower(Current));
          }
        }

        var Last = Source[Source.Length - 1];

        if (AllCapsRun)
          Result.Append(Last);
        else
          Result.Append(char.ToLower(Last));

        return Result.ToString();
      }
    }
    public static string CapitaliseFirstCharacter(this string Source)
    {
      if (string.IsNullOrWhiteSpace(Source))
      {
        return Source;
      }
      else
      {
        var Builder = new StringBuilder(Source.Length);

        Builder.Append(Source[0].ToString().ToUpper());

        if (Source.Length > 1)
          Builder.Append(Source.Substring(1));

        return Builder.ToString();
      }
    }

    /// <summary>
    /// Returns a string array that contains the substrings in this string that are delimited by elements of a specified string array. A parameter specifies whether to return empty array elements.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Separator">The strings to split the <see cref="String"/> with.</param>
    /// <returns> An array whose elements contain the substrings in this string that are delimited by one or more strings in separator. For more information, see the Remarks section. </returns>
    public static string[] Split(this string Source, params string[] Separator)
    {
      return Source.Split(Separator, StringSplitOptions.RemoveEmptyEntries);
    }
    public static List<string> QuotedSplit(this string Source, params char[] Delimiter)
    {
      var EscapeChar = '\\';
      var PhraseChar = '"';
      var IgnoreDelimiter = false;
      var PreviousWasEscape = false;

      var Pieces = new List<string>();
      var ContentLength = Source.Length;

      var CurrentPiece = new StringBuilder();
      for (var i = 0; i < ContentLength; i++)
      {
        var CurrentChar = Source[i];

        if (CurrentChar == PhraseChar && !PreviousWasEscape)
        {
          if (IgnoreDelimiter)
          {
            if (CurrentPiece.Length > 0)
            {
              Pieces.Add(CurrentPiece.ToString());
              CurrentPiece = new StringBuilder();
            }

            IgnoreDelimiter = false;
          }
          else
          {
            IgnoreDelimiter = true;
          }
        }
        else if (Delimiter.Contains(CurrentChar))
        {
          if (!IgnoreDelimiter)
          {
            if (CurrentPiece.Length > 0)
            {
              Pieces.Add(CurrentPiece.ToString());
              CurrentPiece = new StringBuilder();
            }
          }
          else
          {
            CurrentPiece.Append(CurrentChar);
          }
        }
        else
        {
          CurrentPiece.Append(CurrentChar);
        }

        PreviousWasEscape = (CurrentChar == EscapeChar);
      }

      if (CurrentPiece.Length > 0)
        Pieces.Add(CurrentPiece.ToString());

      return Pieces;
    }

    public static string WordWrap(this string Source, int WrapWidth)
    {
      var PartList = new List<string>();
      var StartIndex = 0;
      while (true)
      {
        var NextIndex = Source.IndexOfAny(new char[] { ' ', '-', '\t' }, StartIndex);

        if (NextIndex == -1)
        {
          PartList.Add(Source.Substring(StartIndex));
          break;
        }

        var NextWord = Source.Substring(StartIndex, NextIndex - StartIndex);
        var NextChar = Source.Substring(NextIndex, 1)[0];

        if (char.IsWhiteSpace(NextChar))
        {
          PartList.Add(NextWord);
          PartList.Add(NextChar.ToString());
        }
        else
        {
          PartList.Add(NextWord + NextChar);
        }

        StartIndex = NextIndex + 1;
      }

      var CurrentLineLength = 0;
      var StringBuilder = new StringBuilder();

      foreach (var PartItem in PartList)
      {
        var Part = PartItem;

        if (CurrentLineLength + Part.Length > WrapWidth)
        {
          if (CurrentLineLength > 0)
          {
            StringBuilder.Append(Environment.NewLine);
            CurrentLineLength = 0;
          }

          while (Part.Length > WrapWidth)
          {
            StringBuilder.Append(Part.Substring(0, WrapWidth - 1) + "-");
            Part = Part.Substring(WrapWidth - 1);

            StringBuilder.Append(Environment.NewLine);
          }

          Part = Part.TrimStart();
        }

        StringBuilder.Append(Part);
        CurrentLineLength += Part.Length;
      }

      return StringBuilder.ToString();
    }
    /// <summary>
    /// Undo camel case on this <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>An un-camel-cased version of this <see cref="String"/>.</returns>
    public static string ReverseCamel(this string Source)
    {
      if (Source.Length == 0)
      {
        return Source;
      }
      else
      {
        var Previous = Source[0];
        var Result = new StringBuilder();

        Result.Append(Previous);

        for (var Index = 1; Index < Source.Length; Index++)
        {
          var Current = Source[Index];

          if (!char.IsUpper(Previous) && char.IsUpper(Current))
            Result.Append(' ');

          Result.Append(Current);

          Previous = Current;
        }

        return Result.ToString();
      }
    }
    /// <summary>
    /// Count the number of matching characters between this <see cref="String"/> and the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Left">This <see cref="String"/>.</param>
    /// <param name="Right">The <see cref="String"/> to compare to.</param>
    /// <returns>The zero-based index of the first mismatch between this <see cref="String"/> and <paramref name="Right"/>.</returns>
    public static int StartsWithMatchLength(this string Left, string Right)
    {
      var LeftLength = Left.Length;
      var RightLength = Right.Length;
      var MaxLength = Math.Min(LeftLength, RightLength);
      var MaxIndex = 0;

      for (MaxIndex = 0; MaxIndex < MaxLength; MaxIndex++)
      {
        if (Left[MaxIndex] != Right[MaxIndex])
          break;
      }

      return MaxIndex;
    }
    public static bool StartsWithVowelSound(this string Source)
    {
      if (Source.Length == 0)
        return false;

      var IsVowel = Source[0].IsVowel();

      if (StartsWithVowelSoundExceptionArray.Any(Exception => Source.StartsWith(Exception)))
        IsVowel = !IsVowel;

      return IsVowel;
    }
    public static bool StartsWithConsonantSound(this string Source)
    {
      return !StartsWithVowelSound(Source);
    }
    public static string PrefixLowerA(this string Source)
    {
      return (Source.StartsWithConsonantSound() ? "a " : "an ") + Source;
    }
    /// <summary>
    /// Calculate the Levenshtein distance between this <see cref="String"/> and another <see cref="String"/>.
    /// </summary>
    /// <param name="s">This <see cref="String"/>.</param>
    /// <param name="t">The <see cref="String"/> to calculate the Levenshtein distance to.</param>
    /// <returns>The Levenshtein distance between this <see cref="String"/> and <paramref name="t"/>.</returns>
    public static int LevenshteinDistance(this string s, string t)
    {
      var n = s.Length;
      var m = t.Length;
      var d = new int[n + 1, m + 1];

      // Step 1
      if (n == 0)
        return m;

      if (m == 0)
        return n;

      // Step 2
      for (var i = 0; i <= n; d[i, 0] = i++) ;
      for (var j = 0; j <= m; d[0, j] = j++) ;

      // Step 3
      for (var i = 1; i <= n; i++)
      {
        //Step 4
        for (var j = 1; j <= m; j++)
        {
          // Step 5
          var cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

          // Step 6
          d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
        }
      }

      // Step 7
      return d[n, m];
    }
    public static string Reverse(this string Source)
    {
      var CharArray = Source.ToCharArray();
      Array.Reverse(CharArray);
      return new string(CharArray);
    }
    public static bool Contains(this string Source, char Value)
    {
      return Source.IndexOf(Value) >= 0;
    }
    public static bool Contains(this string Source, params char[] CharacterArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      return Source.GetEnumerable().Intersect(CharacterArray).Any();
    }
    public static bool Contains(this string Source, string ToCheck, StringComparison Comparison)
    {
      return Source.IndexOf(ToCheck, Comparison) >= 0;
    }
    public static bool ContainsAny(this string Source, IEnumerable<string> SearchTerms, StringComparison Comparison = StringComparison.CurrentCultureIgnoreCase)
    {
      return SearchTerms.Any(T => Source.Contains(T, Comparison));
    }
    public static bool ContainsAll(this string Source, IEnumerable<string> SearchTerms, StringComparison Comparison = StringComparison.CurrentCultureIgnoreCase)
    {
      return SearchTerms.All(T => Source.Contains(T, Comparison));
    }
    public static bool ContainsOnly(this string Source, char[] Characters)
    {
      foreach (var Character in Source)
      {
        if (!Characters.Contains(Character))
          return false;
      }

      return true;
    }
    public static IEnumerable<char> GetEnumerable(this string Source)
    {
      for (var Index = 0; Index < Source.Length; Index++)
        yield return Source[Index];
    }
    public static char Last(this string Source)
    {
      if (Source != null && Source.Length > 0)
        return Source[Source.Length - 1];
      else
        throw new ArgumentException("Source string must not be null or empty for the last function.");
    }

    public static bool IsLowerOrNonLetter(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsLower(Index) || !char.IsLetter(Index));
    }
    public static bool IsUpperOrNonLetter(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsUpper(Index) || !char.IsLetter(Index));
    }
    public static bool IsDigit(this string Source)
    {
      return Source.Length > 0 && Source.GetEnumerable().All(Index => char.IsDigit(Index));
    }
    public static bool IsDigitOrNonLetter(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsDigit(Index) || !char.IsLetter(Index));
    }
    public static bool IsAlphabetic(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsLetter(Index));
    }
    public static bool IsAlphaNumeric(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsLetter(Index) || char.IsDigit(Index));
    }
    public static bool IsNumeric(this string Source)
    {
      return Source.GetEnumerable().All(Index => char.IsDigit(Index));
    }

    private static readonly string[] StartsWithVowelSoundExceptionArray = new string[]
    {
      "unicorn",
      "unique",
      "one",
      "hour"
    };
  }

  /// <summary>
  /// Static class with helper functions for converting values.
  /// </summary>
  public static class ConverterHelper
  {
    /// <summary>
    /// Convert a long value to a base 26 string.
    /// </summary>
    /// <param name="Value">The value to convert.</param>
    /// <returns>A string representing the base 26 conversion of <paramref name="Value"/>.</returns>
    public static string ToBase26(long Value)
    {
      const long BASE = 26L;
      var Dividend = Value;
      var Result = "";
      var Modulo = 0L;

      while (Dividend > 0)
      {
        Modulo = (Dividend - 1) % BASE;
        Result = Convert.ToChar(65 + Modulo).ToString() + Result;
        Dividend = (long)((Dividend - Modulo) / BASE);
      }

      return Result;
    }

    /// <summary>
    /// Convert a base 26 string to a long value.
    /// </summary>
    /// <param name="Value">The base 26 string to convert.</param>
    /// <returns><paramref name="Value"/> converted from base 26.</returns>
    public static long FromBase26(string Value)
    {
      var Result = 0;
      const int BASE = 26;
      foreach (var Char in Value.ToUpper().ToCharArray())
      {
        if (Char < 'A' || Char > 'Z')
          throw new Exception("Not a valid Base26 string: " + Value);

        Result = (Result * BASE) + (Char - '@');
      }

      return Result;
    }
  }

  /// <summary>
  /// Static class with extension methods for lists of strings.
  /// </summary>
  public static class StringListHelper
  {
    /// <summary>
    /// Prepend a string value to every element in this list.
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="PrefixValue">The value to prepend.</param>
    /// <returns>A new <see cref="List{String}"/> containing every element in this list with <paramref name="PrefixValue"/> prepended to each element.</returns>
    public static IList<string> PrependString(this IList<string> Source, string PrefixValue)
    {
      var ResultStringList = new List<string>();

      foreach (var SelfString in Source)
        ResultStringList.Add(PrefixValue + SelfString);

      return ResultStringList;
    }
    /// <summary>
    /// Prepend a formatted string to every element in this list.
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="PrefixFormat">The format string to prepend</param>
    /// <param name="PrefixArguments">The arguments to insert into <paramref name="PrefixFormat"/>.</param>
    /// <returns>A new <see cref="List{String}"/> containing every element in this list with the formatted string prepended.</returns>
    public static IList<string> PrependFormat(this IList<string> Source, string PrefixFormat, params object[] PrefixArguments)
    {
      return PrependString(Source, string.Format(CultureInfo.CurrentCulture, PrefixFormat, PrefixArguments));
    }
    /// <summary>
    /// Join the elements of the list together with the value of <paramref name="Separator"/>. Roughly equivalent to Perl's join().
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="Separator">The string to join the elements with.</param>
    /// <returns>A string containing each element of this list interspersed with <paramref name="Separator"/>.</returns>
    public static string AsSeparatedText(this IEnumerable<string> Source, string Separator, string FinalSeparator = null)
    {
      if (FinalSeparator == null)
        return string.Join(Separator, Source);

      var StringBuilder = new StringBuilder();

      var SourceCount = Source.Count();
      var SourceIndex = 0;
      foreach (var Element in Source)
      {
        StringBuilder.Append(Element);

        if (SourceCount > 1)
        {
          if (FinalSeparator != null && SourceIndex == SourceCount - 2)
            StringBuilder.Append(FinalSeparator);
          else if (SourceIndex < SourceCount - 1)
            StringBuilder.Append(Separator);
        }

        SourceIndex++;
      }

      return StringBuilder.ToString();
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="IEnumerable{T}"/>.
  /// </summary>
  public static class IEnumerableHelper
  {
    public static IEnumerable<T> Convert<T>(this System.Collections.IEnumerable Enumerable)
    {
      var enumerator = Enumerable.GetEnumerator();
      while (enumerator.MoveNext())
        yield return (T)enumerator.Current;
    }

    public static IEnumerable<T> ExceptNull<T>(this IEnumerable<T?> Source) where T : struct
    {
      return System.Linq.Enumerable.Where(Source, Item => Item != null).Select(Item => Item.Value);
    }
    public static IEnumerable<T> ExceptNull<T>(this IEnumerable<T> Source) where T : class
    {
      return System.Linq.Enumerable.Where(Source, Item => Item != null);
    }

    public static IEnumerable<T> Except<T>(this IEnumerable<T> Source, T Value)
    {
      return Source.Except(Value.ToEnumerable());
    }
    public static IEnumerable<T> Except<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, TValue> Function)
    {
      return Source.Except(Target, new ProjectionEqualityComparer<T, TValue>(Function));
    }
    public static IEnumerable<T> Except<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, T, bool> Function)
    {
      return Source.Except(Target, new ProjectionEqualityComparer<T>(Function));
    }

    public static void ForEachOfType<TCast>(this System.Collections.IEnumerable Enumerable, Action<TCast> Action)
      where TCast : class
    {
      foreach (var Item in Enumerable.OfType<TCast>())
        Action(Item);
    }
    public static bool Exists<T>(this IEnumerable<T> Source, Predicate<T> Predicate)
    {
      return Source.Any(Item => Predicate(Item));
    }

    public static Array ToEnumerableArray(this System.Collections.IEnumerable Source)
    {
      var Enumerator = Source.GetEnumerator();

      var List = new List<object>();

      while (Enumerator.MoveNext())
        List.Add(Enumerator.Current);

      return List.ToArray();
    }

    /// <summary>
    /// Map the elements of this <see cref="IEnumerable{TKey}"/> into a <see cref="HashSet{TValue}"/>using the specified delegate to derive values.
    /// </summary>
    /// <typeparam name="TKey">The type of the elements of <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <typeparam name="TValue">The type of derived values.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Function">A delegate that will generate values to store for each element in the <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TKey, TValue>(this IEnumerable<TKey> Source, Func<TKey, TValue> Function)
    {
      var ResultHashSet = new HashSet<TValue>();

      foreach (var SourceEntry in Source)
        ResultHashSet.Add(Function(SourceEntry));

      return ResultHashSet;
    }
    /// <summary>
    /// Map the elements of this <see cref="IEnumerable{TKey}"/> into a <see cref="HashSet{TValue}"/>using the specified delegate to derive values.
    /// </summary>
    /// <typeparam name="TKey">The type of the elements of <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <typeparam name="TValue">The type of derived values.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Function">A delegate that will generate values to store for each element in the <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Comparer">IEqualityComparer to use.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TKey, TValue>(this IEnumerable<TKey> Source, Func<TKey, TValue> Function, IEqualityComparer<TValue> Comparer)
    {
      var ResultHashSet = new HashSet<TValue>(Comparer);

      foreach (var SourceEntry in Source)
        ResultHashSet.Add(Function(SourceEntry));

      return ResultHashSet;
    }
    /// <summary>
    /// Convert the elements of this <see cref="IEnumerable{TValue}"/> into a <see cref="HashSet{TValue}"/>.
    /// </summary>
    /// <typeparam name="TValue">The type of the elements of this <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TValue>(this IEnumerable<TValue> Source)
    {
      return new HashSet<TValue>(Source);
    }
    /// <summary>
    /// Convert the elements of this <see cref="IEnumerable{TValue}"/> into a <see cref="HashSet{TValue}"/>.
    /// </summary>
    /// <typeparam name="TValue">The type of the elements of this <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Comparer">Comparer <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TValue>(this IEnumerable<TValue> Source, IEqualityComparer<TValue> Comparer)
    {
      return new HashSet<TValue>(Source, Comparer);
    }
    /// <summary>
    /// Trim from the start and the end of this <see cref="IEnumerable{T}"/> according to the specified function.
    /// </summary>
    /// <typeparam name="T">The type of elements in the <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining where to start and stop trimming.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element for which <paramref name="Function"/> returns false to the last element for which
    /// <paramref name="Function"/> returns false.</returns>
    public static IEnumerable<T> Trim<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      return Source.TrimStart(Function).TrimEnd(Function);
    }
    /// <summary>
    /// Trim elements from the start of this <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining which element to stop trimming at.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element for which <paramref name="Function"/> returns false until the 
    /// end of this <see cref="IEnumerable{T}"/></returns>
    public static IEnumerable<T> TrimStart<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      var Result = new List<T>();
      var Matching = true;

      foreach (var Entry in Source)
      {
        if (Matching)
          Matching = Function(Entry);

        if (!Matching)
          Result.Add(Entry);
      }

      return Result;
    }
    /// <summary>
    /// Trim elements from the end of this <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining which element to start trimming at.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element until the last element for which <paramref name="Function"/> returns false.</returns>
    public static IEnumerable<T> TrimEnd<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      var Result = new List<T>();
      var Window = new List<T>();
      var Matching = false;

      foreach (var Entry in Source)
      {
        Matching = Function(Entry);

        if (Matching)
        {
          Window.Add(Entry);
        }
        else
        {
          Result.AddRange(Window);
          Result.Add(Entry);

          Window.Clear();
        }
      }

      if (!Matching)
        Result.AddRange(Window);

      return Result;
    }
    /// <summary>
    /// Find the first element of this <see cref="IEnumerable{T}"/> which satisfies the matching function <paramref name="Function"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The matching delegate which will return true if an element matches.</param>
    /// <returns>The first element of this <see cref="IEnumerable{T}"/> for which <paramref name="Function"/> returns true, or the default value if no match is found.</returns>
    public static T Find<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      return Source.Where(Function).FirstOrDefault();
    }
    /// <summary>
    /// Get the next element in this <see cref="IEnumerable{T}"/> after <paramref name="Key"/>, if it exists, otherwise the previous element, or the default value if <paramref name="Key"/>
    /// is not found in this <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Key">The element to look for.</param>
    /// <returns>If <paramref name="Key"/> is found in the list, the next element after <paramref name="Key"/> (if it exists; the previous element otherwise).
    /// If <paramref name="Key"/> is not found, the default value of T.</returns>
    public static T NextOrLast<T>(this IEnumerable<T> Source, T Key)
    {
      if (Source == null)
        return default(T);

      T Previous = default(T);
      var Enumerator = Source.GetEnumerator();

      while (Enumerator.MoveNext())
      {
        if (Enumerator.Current.Equals(Key))
          return (Enumerator.MoveNext()) ? Enumerator.Current : Previous;
        Previous = Enumerator.Current;
      }

      return default(T);
    }
    /// <summary>
    /// Convert this <see cref="IEnumerable{T}"/> to an <see cref="Inv.DistinctList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns>A new <see cref="Inv.DistinctList{T}"/> containing the elements of this <see cref="IEnumerable{T}"/>.</returns>
    public static Inv.DistinctList<T> ToDistinctList<T>(this IEnumerable<T> Source)
    {
      return new Inv.DistinctList<T>(Source);
    }
    /// <summary>
    /// Create an <see cref="Inv.DistinctList{TSource}"/> containing this object.
    /// </summary>
    /// <typeparam name="TSource">The type of this object.</typeparam>
    /// <param name="Source">This object.</param>
    /// <returns>A new <see cref="Inv.DistinctList{TSource}"/> containing this object as its only element.</returns>
    public static Inv.DistinctList<TSource> SingleToDistinctList<TSource>(this TSource Source)
    {
      if (Source == null)
        return new Inv.DistinctList<TSource>();
      else
        return new Inv.DistinctList<TSource>() { Source };
    }
    /// <summary>
    /// Produces the set union of a sequence and the specified item using the default equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Left">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Right">The element to union into this <see cref="IEnumerable{TSource}"/>.</param>
    /// <returns>The result of the union.</returns>
    public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> Left, TSource Right)
    {
      return Left.Union(new TSource[] { Right });
    }
    /// <summary>
    /// Produces the set union of a sequence and the specified item using the specified equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Left">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Right">The element to union into this <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="comparer">The equality comparer to use.</param>
    /// <returns>The result of the union.</returns>
    public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> Left, TSource Right, IEqualityComparer<TSource> comparer)
    {
      return Left.Union(new TSource[] { Right }, comparer);
    }
    /// <summary>
    /// Get an <see cref="IEnumerable{TSource}"/> for this object.
    /// </summary>
    /// <typeparam name="TSource">The type of this object.</typeparam>
    /// <param name="Source">This object.</param>
    /// <returns>An <see cref="IEnumerable{TSource}"/> containing this object.</returns>
    public static IEnumerable<TSource> ToEnumerable<TSource>(this TSource Source)
    {
      return new TSource[] { Source };
    }
    /// <summary>
    /// Perform an action on every element of this <see cref="IEnumerable{TSource}"/>.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Action">The action to perform on each element.</param>
    public static void ForEach<TSource>(this IEnumerable<TSource> Source, Action<TSource> Action)
    {
      Debug.Assert(Action != null, "Active must be specified.");

      foreach (var Item in Source)
        Action(Item);
    }
    /// <summary>
    /// Build a frequency histogram from this <see cref="IEnumerable{TSource}"/> and the specified equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Comparer">The <see cref="IEqualityComparer{TSource}"/> to use, or null to use the default.</param>
    /// <returns>A list of <see cref="FrequencyRecord{TSource}"/> elements representing the frequencies with which each element in this <see cref="IEnumerable{TSource}"/> occur.</returns>
    public static IEnumerable<FrequencyRecord<TSource>> Frequency<TSource>(this IEnumerable<TSource> Source, IEqualityComparer<TSource> Comparer = null)
    {
      Dictionary<TSource, int> Dictionary;

      if (Comparer == null)
        Dictionary = new Dictionary<TSource, int>();
      else
        Dictionary = new Dictionary<TSource, int>(Comparer);

      foreach (var Item in Source)
        Dictionary[Item] = Dictionary.GetValueOrDefault(Item, 0) + 1;

      return Dictionary.Select(Index => new FrequencyRecord<TSource>(Index.Key, Index.Value));
    }
    /// <summary>
    /// Sort this <see cref="IEnumerable{TSource}"/>.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <returns>A sorted version of this <see cref="IEnumerable{TSource}"/>.</returns>
    public static IEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> Source)
    {
      return Source.OrderBy(Index => Index);
    }

    /// <summary>
    /// Represents a bucket in a frequency histogram.
    /// </summary>
    /// <typeparam name="T">The type of element being counted.</typeparam>
    public sealed class FrequencyRecord<T>
    {
      internal FrequencyRecord(T Object, int Count)
      {
        this.Key = Object;
        this.Count = Count;
      }

      /// <summary>
      /// The key value of this bucket.
      /// </summary>
      public T Key { get; private set; }

      /// <summary>
      /// The number of records in this bucket.
      /// </summary>
      public int Count { get; private set; }
    }
  }

  public sealed class ProjectionEqualityComparer<T, TIdentity> : IEqualityComparer<T>
  {
    public ProjectionEqualityComparer(Func<T, TIdentity> Function)
    {
      this.Function = Function;
    }
    public bool Equals(T x, T y)
    {
      return Equals(Function(x), Function(y));
    }
    public int GetHashCode(T obj)
    {
      return Function(obj).GetHashCode();
    }

    private Func<T, TIdentity> Function;
  }

  public sealed class ProjectionEqualityComparer<T> : IEqualityComparer<T>
  {
    public ProjectionEqualityComparer(Func<T, T, bool> Function)
    {
      this.Function = Function;
    }
    public bool Equals(T x, T y)
    {
      return Function(x, y);
    }
    public int GetHashCode(T obj)
    {
      return obj.GetHashCode();
    }

    private Func<T, T, bool> Function;
  }

  /// <summary>
  /// Static class containing extension methods for collections.
  /// </summary>
  public static class ICollectionHelper
  {
    /// <summary>
    /// Add a range of elements to this <see cref="ICollection{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Array">An <see cref="IEnumerable{T}"/> containing the elements to add to this <see cref="ICollection{T}"/>.</param>
    public static void AddRange<T>(this ICollection<T> Source, IEnumerable<T> Array)
    {
      foreach (var Item in Array)
        Source.Add(Item);
    }

    /// <summary>
    /// Remove a range of elements from this <see cref="ICollection{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Array">An <see cref="IEnumerable{T}"/> containing the elements to remove from this <see cref="ICollection{T}"/>.</param>
    public static void RemoveRange<T>(this ICollection<T> Source, IEnumerable<T> Array)
    {
      foreach (var Item in Array)
        Source.Remove(Item);
    }

    /// <summary>
    /// Split this <see cref="ICollection{T}"/> into partitions of <paramref name="Size"/> elements.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Size">The number of elements to place in each partition.</param>
    /// <returns>Multiple <see cref="IEnumerable{T}"/>, each containing a partition of elements from this <see cref="IEnumerable{T}"/> of up to <paramref name="Size"/> elements.</returns>
    public static IEnumerable<IEnumerable<T>> Partition<T>(this ICollection<T> Source, int Size)
    {
      for (var Index = 0; Index < Math.Ceiling(Source.Count / (double)Size); Index++)
        yield return Source.Skip(Size * Index).Take(Size);
    }

    /// <summary>
    /// Determine whether or not this <see cref="ICollection{T}"/> has an element numbered <paramref name="Index"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Index">The index value to check.</param>
    /// <returns>True if <paramref name="Index"/> is within the bounds of this <see cref="ICollection{T}"/>; false otherwise.</returns>
    public static bool ExistsAt<T>(this System.Collections.Generic.ICollection<T> Source, int Index)
    {
      return Index >= 0 && Index < Source.Count;
    }

    /// <summary>
    /// Determine whether or not there exists an element in this <see cref="ICollection{T}"/> that satisfies the given predicate.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Predicate">The predicate to check.</param>
    /// <returns>True if any element of this <see cref="ICollection{T}"/> satisfies <paramref name="Predicate"/>; false otherwise.</returns>
    public static bool Exists<T>(this System.Collections.ICollection Source, Predicate<T> Predicate)
    {
      foreach (T Item in Source)
      {
        if (Predicate(Item))
          return true;
      }

      return false;
    }
  }

  public static class HashSetHelper
  {
    public static void Assign<T>(this HashSet<T> Target, HashSet<T> Source)
    {
      Target.Clear();
      Target.AddRange(Source);
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="IList{T}"/>.
  /// </summary>
  public static class IListHelper
  {
    public static IEnumerable<T> ExceptLast<T>(this IList<T> Source) where T : class
    {
      return Source.Take(Source.Count - 1);
    }
    public static int ShallowCompareTo<T>(this IList<T> Left, IList<T> Right) where T : IComparable<T>
    {
      var Count = Math.Min(Left.Count, Right.Count);
      var Result = 0;
      var Index = 0;

      while (Result == 0 && Index < Count)
      {
        Result = Left[Index].CompareTo(Right[Index]);

        Index++;
      }

      if (Result == 0)
        Result = Left.Count.CompareTo(Right.Count);

      return Result;
    }
    /// <summary>
    /// Determine whether or not this <see cref="IList{T}"/> is equivalent to another <see cref="IList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Left">This <see cref="IList{T}"/>.</param>
    /// <param name="Right">The <see cref="IList{T}"/> to compare to.</param>
    /// <returns>True if this <see cref="IList{T}"/> is equivalent to <paramref name="Right"/>; false otherwise.</returns>
    public static bool ShallowEqualTo<T>(this IList<T> Left, IList<T> Right)
    {
      var Count = Left.Count;
      var Result = Count == Right.Count;
      var Index = 0;

      while (Result && Index < Count)
      {
        Result = object.Equals(Left[Index], Right[Index]);

        Index++;
      }

      return Result;
    }

    /// <summary>
    /// Determine whether or not a list contains only distinct elements.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns></returns>
    public static bool IsDistinct<T>(this IEnumerable<T> Source)
    {
      return Source.Distinct().Count() == Source.Count();
    }

    /// <summary>
    /// Determine whether or not this <see cref="IList{Int64}"/> is contiguous in nature.
    /// </summary>
    /// <param name="Source">This <see cref="IList{Int64}"/>.</param>
    /// <returns>True if the list contains a contiguous incrementing set of integers; false otherwise.</returns>
    public static bool IsContiguous(this IList<long> Source)
    {
      var Result = Source.Count > 0;

      if (Result)
      {
        var Value = Source[0] + 1;

        for (var Index = 1; Index < Source.Count; Index++)
        {
          if (Source[Index] != Value)
          {
            Result = false;
            break;
          }
          Value++;
        }
      }

      return Result;
    }
    public static bool IsContiguous(this IList<int> Source)
    {
      var Result = Source.Count > 0;

      if (Result)
      {
        var Value = Source[0] + 1;

        for (var Index = 1; Index < Source.Count; Index++)
        {
          if (Source[Index] != Value)
          {
            Result = false;
            break;
          }
          Value++;
        }
      }

      return Result;
    }

    /// <summary>
    /// Enumerate the elements of this <see cref="IList{T}"/> from the zero-based index <paramref name="Low"/> to the end of the <see cref="IList{T}"/>.
    /// </summary>
    /// <remarks>
    /// This should probably be deprecated in favour of Skip().
    /// </remarks>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Low">The zero-based index of the first element to enumerate.</param>
    /// <returns>The set of elements of this <see cref="IList{T}"/> from <paramref name="Low"/> to the end of the <see cref="IList{T}"/>.</returns>
    public static IEnumerable<T> EnumerateRange<T>(this IList<T> Source, int Low)
    {
      return EnumerateRange(Source, Low, Source.Count);
    }

    /// <summary>
    /// Enumerate the elements of this <see cref="IList{T}"/> from the zero-based index <paramref name="Low"/> to the zero-based index <paramref name="High"/>.
    /// </summary>
    /// <remarks>
    /// This should probably be deprecated in favour of Skip().Take().
    /// </remarks>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Low">The zero-based index of the first element to enumerate.</param>
    /// <param name="High">The zero-based index of the last element to enumerate.</param>
    /// <returns>The set of elements of this <see cref="IList{T}"/> from <paramref name="Low"/> to <paramref name="High"/>.</returns>
    public static IEnumerable<T> EnumerateRange<T>(this IList<T> Source, int Low, int High)
    {
      for (var Index = Low; Index < High; Index++)
        yield return Source[Index];
    }

    /// <summary>
    /// Create a new <see cref="ReadOnlyCollection{T}"/> based on this <see cref="IList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <returns>The new <see cref="ReadOnlyCollection{T}"/>.</returns>
    public static ReadOnlyCollection<T> AsReadOnly<T>(this IList<T> Source)
    {
      return new ReadOnlyCollection<T>(Source);
    }

    /// <summary>
    /// Pop the last element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Default">The optional default value to return.</param>
    /// <returns>The element removed from the list, or the value of <paramref name="Default"/> if the list has no elements.</returns>
    public static T RemoveLastOrDefault<T>(this IList<T> Source, T Default = default(T))
    {
      if (Source.Count == 0)
      {
        return Default;
      }
      else
      {
        var Result = Source[Source.Count - 1];

        Source.RemoveAt(Source.Count - 1);

        return Result;
      }
    }

    public static T RemoveLast<T>(this IList<T> Source, T Default = default(T))
    {
      var Result = Source[Source.Count - 1];

      Source.RemoveAt(Source.Count - 1);

      return Result;
    }

    /// <summary>
    /// Pop the first element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Default">The optional default value to return.</param>
    /// <returns>The element removed from the list, or the value of <paramref name="Default"/> if the list has no elements.</returns>
    public static T RemoveFirstOrDefault<T>(this IList<T> Source, T Default = default(T))
    {
      if (Source.Count == 0)
      {
        return Default;
      }
      else
      {
        var Result = Source[0];

        Source.RemoveAt(0);

        return Result;
      }
    }

    public static T RemoveFirst<T>(this IList<T> Source, T Default = default(T))
    {
      var Result = Source[0];

      Source.RemoveAt(0);

      return Result;
    }
    public static T RemoveFirst<T>(this IList<T> Source, Predicate<T> Predicate)
    {
      var Index = Source.FindIndex(Predicate);

      var Result = Source[Index];

      Source.RemoveAt(Index);

      return Result;
    }
    public static int FindIndex<T>(this IList<T> Source, Predicate<T> Match)
    {
      return FindIndex(Source, 0, Source.Count, Match);
    }
    public static int FindIndex<T>(this IList<T> Source, int StartIndex, Predicate<T> Match)
    {
      return FindIndex(Source, StartIndex, Source.Count - StartIndex, Match);
    }
    public static int FindIndex<T>(this IList<T> Source, int Index, int Length, Predicate<T> Match)
    {
      var Current = Index + Length;

      for (var i = Index; i < Current; i++)
      {
        if (Match(Source[i]))
          return i;
      }

      return -1;
    }
  }

  /// <summary>
  /// Static class with extension methods for queues.
  /// </summary>
  public static class QueueHelper
  {
    /// <summary>
    /// Try to dequeue an object from this <see cref="Queue{T}"/>, returning the dequeued object or the default value of T otherwise.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="Queue{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="Queue{T}"/>.</param>
    /// <param name="Item">When this method returns, contains the object that was dequeued if the dequeue succeeded, or the default value of T otherwise.</param>
    /// <returns>True if an object was dequeued successfully (stored in <paramref name="Item"/>), false otherwise.</returns>
    public static bool TryDequeue<T>(this Queue<T> Source, out T Item)
    {
      if (Source.Count > 0)
      {
        Item = Source.Dequeue();
        return true;
      }
      else
      {
        Item = default(T);
        return false;
      }
    }
  }

  /// <summary>
  /// Static class with extension methods for dictionaries.
  /// </summary>
  public static class DictionaryHelper
  {
    /// <summary>
    /// Fetch a value from a <see cref="Dictionary{TKey,TValue}"/> given a key, or a default value if the specified key does not exist in this <see cref="Dictionary{TKey,TValue}"/>.
    /// </summary>
    /// <typeparam name="TKey">The type of key elements in this <see cref="Dictionary{TKey,TValue}"/>.</typeparam>
    /// <typeparam name="TValue">The type of value elements in this <see cref="Dictionary{TKey,TValue}"/>.</typeparam>
    /// <param name="Dictionary">This <see cref="Dictionary{TKey,TValue}"/>.</param>
    /// <param name="Key">The key value to look up.</param>
    /// <param name="DefaultValue">The default value to return if the specified key is not found in the <see cref="Dictionary{TKey,TValue}"/>.</param>
    /// <returns>The value corresponding to <paramref name="Key"/> in the <see cref="Dictionary{TKey,TValue}"/>, or the value of <paramref name="DefaultValue"/> if <paramref name="Key"/>
    /// is not a key in this <see cref="Dictionary{TKey,TValue}"/>.</returns>
    public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default(TValue))
    {
      TValue ResultValue;

      if (Dictionary.TryGetValue(Key, out ResultValue))
        return ResultValue;
      else
        return DefaultValue;
    }

    /// <summary>
    /// Remove every key/value pair from this <see cref="Dictionary{TKey,TValue}"/> that satisfy the given <paramref name="Function"/>.
    /// </summary>
    /// <typeparam name="TKey">The type of key elements in this <see cref="Dictionary{TKey,TValue}"/>.</typeparam>
    /// <typeparam name="TValue">The type of value elements in this <see cref="Dictionary{TKey,TValue}"/>.</typeparam>
    /// <param name="Dictionary">This <see cref="Dictionary{TKey,TValue}"/>.</param>
    /// <param name="Function">A function taking a <see cref="KeyValuePair{TKey,TValue}"/> and returning true if the key/value pair should be removed from this
    /// <see cref="Dictionary{TKey,TValue}"/>.</param>
    public static void RemoveAll<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, Func<KeyValuePair<TKey, TValue>, bool> Function)
    {
      var RemoveKeyList = Dictionary.Where(Function).Select(Index => Index.Key).ToArray();

      foreach (var RemoveKey in RemoveKeyList)
        Dictionary.Remove(RemoveKey);
    }

    public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key, Func<TKey, TValue> ValueFunction)
    {
      TValue Result;

      if (!Dictionary.TryGetValue(Key, out Result))
      {
        Result = ValueFunction(Key);
        Dictionary.Add(Key, Result);
      }

      return Result;
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="Assembly"/>.
  /// </summary>
  public static class AssemblyHelper
  {
    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a byte array.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <returns>The contents of the named resource as a byte array.</returns>
    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a byte array.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <param name="DefaultNamespace">The default namespace used to find the resource.</param>
    /// <returns>The contents of the named resource as a byte array.</returns>
    public static byte[] ExtractResourceBuffer(this Assembly Assembly, string ResourceName, string DefaultNamespace = "")
    {
      var ResourceLocation = CanonicaliseResourceName(Assembly, ResourceName, DefaultNamespace);

      using (var ResourceStream = Assembly.GetManifestResourceStream(ResourceLocation))
      {
        Debug.Assert(ResourceStream != null, "Buffer '" + ResourceLocation + "' is not an embedded resource.");

        if (ResourceStream == null)
        {
          return null;
        }
        else
        {
          var Result = new byte[ResourceStream.Length];

          ResourceStream.Read(Result, 0, Result.Length);

          return Result;
        }
      }
    }

    public static byte[] ExtractResourceBufferOrDefault(this Assembly Assembly, string ResourceName, byte[] Default = null)
    {
      using (var ResourceStream = Assembly.GetManifestResourceStream(ResourceName))
      {
        if (ResourceStream == null)
          return Default;

        var Result = new byte[ResourceStream.Length];

        ResourceStream.Read(Result, 0, Result.Length);

        return Result;
      }
    }

    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a <see cref="String"/>.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <returns>The contents of the named resource as a <see cref="String"/>.</returns>
    public static string ExtractResourceString(this Assembly Assembly, string ResourceName)
    {
      using (var ResourceStream = Assembly.GetManifestResourceStream(ResourceName))
      {
        Debug.Assert(ResourceStream != null, "String '" + ResourceName + "' is not an embedded resource.");

        using (var StreamReader = new global::System.IO.StreamReader(ResourceStream))
        {
          return StreamReader.ReadToEnd();
        }
      }
    }

    public static IEnumerable<string> ExtractResourceTextLines(this Assembly Assembly, string ResourceName)
    {
      using (var ResourceStream = Assembly.GetManifestResourceStream(ResourceName))
      {
        Debug.Assert(ResourceStream != null, "Lines '" + ResourceName + "' is not an embedded resource.");

        using (var StreamReader = new global::System.IO.StreamReader(ResourceStream))
        {
          string Line;
          while ((Line = StreamReader.ReadLine()) != null)
            yield return Line;
        }
      }
    }

    /// <summary>
    /// Extract a <see cref="ResourceManager"/> from this <see cref="Assembly"/>.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource corresponding to the <see cref="ResourceManager"/>.</param>
    /// <returns>The <see cref="ResourceManager"/>.</returns>
    public static ResourceManager ExtractResourceManager(this Assembly Assembly, string ResourceName)
    {
      Debug.Assert(Assembly.GetManifestResourceInfo(ResourceName + ".resources") != null,
        "Unable to open resource manager for:\r\n\r\n" +
        ResourceName + ".resources\r\n\r\n" +
        "** Make sure your project file does not have a default namespace **\r\n\r\n" +
        "Available resource managers:\r\n\r\n" +
        Assembly.GetManifestResourceNames().Where(Index => Index.EndsWith(".resources", StringComparison.CurrentCultureIgnoreCase)).AsSeparatedText("\r\n"));

      return new ResourceManager(ResourceName, Assembly);
    }

    // Given a resource name, assembly, and optionally default namespace to look in, attempt to find the fully-qualified name of
    // a matching resource in the assembly.
    public static string CanonicaliseResourceName(this Assembly Assembly, string ResourceName, string DefaultNamespace = null)
    {
      var ResourceNameArray = Assembly.GetManifestResourceNames();

      string Candidate = null;

      // If a namespace is specified already, check that first.
      if (!string.IsNullOrWhiteSpace(DefaultNamespace))
        Candidate = ResourceNameArray.Where(x => x == DefaultNamespace + "." + ResourceName).FirstOrDefault();

      if (Candidate != null)
        return Candidate;

      // Otherwise, try to find resources that look like they match at least the name
      Candidate = ResourceNameArray.Where(x => x.EndsWith("." + ResourceName)).OrderByDescending(x => x.Length).FirstOrDefault();

      if (Candidate != null)
        return Candidate;

      // Otherwise, try a partial name match as a last resort - this probably won't be the correct resource.
      return ResourceNameArray.Where(x => x.EndsWith(ResourceName)).OrderByDescending(x => x.Length).FirstOrDefault() ?? ResourceName;
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="Random"/>.
  /// </summary>
  public static class RandomHelper
  {
    /// <summary>
    /// Fetches an <see cref="Int64"/> from this random number generator.
    /// </summary>
    /// <param name="Random">This <see cref="Random"/>.</param>
    /// <returns>The next 64 bits from this RNG, cast to <see cref="Int64"/>.</returns>
    public static long NextInt64(this Random Random)
    {
      var ByteArray = new byte[sizeof(Int64)];

      Random.NextBytes(ByteArray);

      return BitConverter.ToInt64(ByteArray, 0);
    }
    public static T NextItem<T>(this Random Random, IList<T> Source)
    {
      if (Source.Count == 0)
        return default(T);
      else
        return Source[Random.Next(Source.Count)];
    }
  }

  /// <summary>
  /// Static class with extension methods for arrays.
  /// </summary>
  public static class ArrayHelper
  {
    /// <summary>
    /// Fill this array with a specified value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this array.</typeparam>
    /// <param name="Array">This array.</param>
    /// <param name="Value">The value to fill the array with.</param>
    public static void Fill<T>(this T[] Array, T Value)
    {
      for (var Index = 0; Index < Array.Length; Index++)
        Array[Index] = Value;
    }
    public static void Fill<T>(this T[] Array, Func<int, T> ValueFunction)
    {
      for (var Index = 0; Index < Array.Length; Index++)
        Array[Index] = ValueFunction(Index);
    }
    public static T[] Subset<T>(this T[] Source, int StartIndex)
    {
      return Subset(Source, StartIndex, Source.Length - StartIndex);
    }
    public static T[] Subset<T>(this T[] Source, int StartIndex, int Length)
    {
      var Result = new T[Length];
      Array.Copy(Source, StartIndex, Result, 0, Length);
      return Result;
    }
  }

  public static class StopwatchHelper
  {
    public static void Measure(this Stopwatch Stopwatch, string Name, Action Action)
    {
#if DEBUG
      Stopwatch.Restart();
#endif
      Action();
#if DEBUG
      Stopwatch.Stop();
      Debug.WriteLine(Name + ": " + Stopwatch.ElapsedMilliseconds + "ms");
#endif
    }
    public static T Measure<T>(this Stopwatch Stopwatch, string Name, Func<T> Function)
    {
      var Result = default(T);

      Measure(Stopwatch, Name, () =>
      {
        Result = Function();
      });

      return Result;
    }
  }

  public static class HttpHelper
  {
    public static Stream GetRequestStream(this HttpWebRequest Request)
    {
      return Request.EndGetRequestStream(Request.BeginGetRequestStream(null, null));
    }
    public static HttpWebResponse GetResponse(this HttpWebRequest Request)
    {
      return (HttpWebResponse)Request.EndGetResponse(Request.BeginGetResponse(null, null));
    }
  }

  public static class BitArrayHelper
  {
    public static void CopyTo(this System.Collections.BitArray Source, Array Array, int Index)
    {
      ((System.Collections.ICollection)Source).CopyTo(Array, Index);
    }
  }

  public static class TaskHelper
  {
    public static void Forget(this System.Threading.Tasks.Task task)
    {
      // Absorbs this warning:
      // warning CS4014: Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
    }
  }

  public static class CsvHelper
  {
    public static string ToCsvLine(this string[] Source)
    {
      using (var StringWriter = new StringWriter())
      {
        using (var CsvWriter = new CsvWriter(StringWriter))
          CsvWriter.WriteRecord(Source);

        return StringWriter.ToString();
      }
    }
    public static string[] ToCsvArray(this string Source)
    {
      using (var CsvReader = new CsvReader(Source))
        return CsvReader.ReadRecord().ToArray();
    }
  }

  public static class PlacementHelper
  {
    public static bool IsHorizontalStretch(this Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.TopStretch || Placement == Inv.Placement.CenterStretch || Placement == Inv.Placement.BottomStretch;
    }
    public static bool IsVerticalStretch(this Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.StretchLeft || Placement == Inv.Placement.StretchCenter || Placement == Inv.Placement.StretchRight;
    }
  }

  public sealed class DelegateComparer<T> : System.Collections.IComparer, IComparer<T>
  {
    public DelegateComparer(Comparison<T> Delegate)
    {
      this.Delegate = Delegate;
    }

    public int Compare(T x, T y)
    {
      return Delegate(x, y);
    }

    int System.Collections.IComparer.Compare(object x, object y)
    {
      return Compare((T)x, (T)y);
    }

    private Comparison<T> Delegate;
  }

  public static class ParallelHelper
  {
    public static void ForEachInParallel<T>(this System.Collections.Generic.IEnumerable<T> Enumerable, Action<T> Action)
    {
      System.Threading.Tasks.Parallel.ForEach(Enumerable, new System.Threading.Tasks.ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, Action);
    }
    public static void ForEachInParallel<T>(this System.Collections.Generic.IEnumerable<T> Enumerable, Action<T, System.Threading.Tasks.ParallelLoopState> Action)
    {
      System.Threading.Tasks.Parallel.ForEach(Enumerable, new System.Threading.Tasks.ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, Action);
    }
  }

  public static class DayOfWeekHelper
  {
    public static DayOfWeek FirstDayOfWeek
    {
      get { return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek; }
    }
    public static DayOfWeek LastDayOfWeek
    {
      get
      {
        var DaysOfWeek = DayOfWeekSeries().ToDistinctList();
        var FirstDayOfWeekIndex = DaysOfWeek.IndexOf(FirstDayOfWeek);

        return FirstDayOfWeekIndex == 0 ? DaysOfWeek.Last() : DaysOfWeek[FirstDayOfWeekIndex - 1];
      }
    }
    public static IEnumerable<DayOfWeek> DayOfWeekSeries()
    {
      var FirstDayIndex = (int)FirstDayOfWeek;
      for (var DayIndex = 0; DayIndex < 7; DayIndex++)
        yield return (DayOfWeek)((FirstDayIndex + DayIndex) % 7);
    }
    public static int Rank(this DayOfWeek DayOfWeek)
    {
      return DayOfWeekSeries().ToList().IndexOf(DayOfWeek); // TODO: more efficient check?
    }

    public static string ToAbbreviatedString(this DayOfWeek DayOfWeek)
    {
      return CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek];
    }
  }

  public static class ComparableHelper
  {
    public static int CompareTo<T>(this T? A, T? B)
      where T : struct, IComparable
    {
      var Result = (A != null).CompareTo(B != null);

      if (Result == 0 && A != null)
        Result = A.Value.CompareTo(B.Value);

      return Result;
    }
  }
}