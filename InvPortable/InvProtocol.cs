﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Inv.Support;

namespace Inv
{
  public sealed class Protocol
  {
    internal Protocol(Inv.Persist.Governor Governor)
    {
      this.Governor = Governor;
    }

    public byte[] Encode<TData>(TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        Governor.Save(Data, MemoryStream);

        MemoryStream.Flush();

        return MemoryStream.ToArray();
      }
    }
    public TData Decode<TData>(byte[] DataBuffer)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        return (TData)Governor.Load(MemoryStream);
    }
    public void Decode<TData>(byte[] DataBuffer, TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        Governor.Load(Data, MemoryStream);
    }

    private Inv.Persist.Governor Governor;
  }
}
