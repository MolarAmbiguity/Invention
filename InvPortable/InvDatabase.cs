﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Database
{
  public sealed class Schematic
  {
    public Schematic()
    {
      this.TableList = new Inv.DistinctList<Table>();
    }

    public void CompileTables(object Self)
    {
      var SelfType = Self.GetType().GetReflectionInfo();

      foreach (var Field in SelfType.GetReflectionFields())
      {
        if (typeof(Inv.Database.Contract<Table>).IsAssignableFrom(Field.FieldType))
          Field.SetValue(Self, Field.FieldType.ReflectionCreate(typeof(Table), AddTable(Field.Name)));
      }
    }
    public Table AddTable(string Name)
    {
      var Result = new Table(Name);

      TableList.Add(Result);

      return Result;
    }
    public Table ForceTable(string Name)
    {
      var Result = TableList.Find(T => T.Name == Name);

      if (Result == null)
        Result = AddTable(Name);

      return Result;
    }

    private Inv.DistinctList<Table> TableList;
  }

  public interface Contract<TBase>
  {
    TBase Base { get; }
  }

  public sealed class Table
  {
    internal Table(string Name)
    {
      this.Name = Name;
      this.ColumnList = new Inv.DistinctList<Column>();
      this.ForeignKeyColumnList = new Inv.DistinctList<ForeignKeyColumn>();
    }

    public string Name { get; private set; }
    public PrimaryKeyColumn PrimaryKeyColumn { get; private set; }

    public void CompileColumns(object Self)
    {
      var SelfType = Self.GetType().GetReflectionInfo();

      foreach (var Field in SelfType.GetReflectionFields())
      {
        if (typeof(Inv.Database.Contract<Column>).IsAssignableFrom(Field.FieldType))
        {
          var Column = (Contract<Column>)Field.FieldType.ReflectionCreate(typeof(string), Field.Name, typeof(bool), false);
          ColumnList.Add(Column.Base);

          Field.SetValue(Self, Column);
        }
      }
    }
    public IEnumerable<Column> GetColumns()
    {
      return ColumnList;
    }
    public Column AddColumn(string Name, DataType DataType, bool Nullable)
    {
      var Result = new Column(Name, DataType, Nullable);

      ColumnList.Add(Result);

      return Result;
    }
    public PrimaryKeyColumn SetPrimaryKeyColumn(string Name)
    {
      var Result = AddColumn(new PrimaryKeyColumn(Name, false));

      this.PrimaryKeyColumn = Result;

      return Result;
    }
    public ForeignKeyColumn AddForeignKeyColumn(string Name, Table Table, bool Nullable)
    {
      var Result = new ForeignKeyColumn(Name, Table, Nullable);

      ForeignKeyColumnList.Add(Result);
      AddColumn(Result);

      return Result;
    }
    public StringColumn AddStringColumn(string Name, bool Nullable)
    {
      return AddColumn(new StringColumn(Name, Nullable));
    }
    public Integer32Column AddInteger32Column(string Name, bool Nullable)
    {
      return AddColumn(new Integer32Column(Name, Nullable));
    }
    public Integer64Column AddInteger64Column(string Name, bool Nullable)
    {
      return AddColumn(new Integer64Column(Name, Nullable));
    }
    public DateTimeOffsetColumn AddDateTimeOffsetColumn(string Name, bool Nullable)
    {
      return AddColumn(new DateTimeOffsetColumn(Name, Nullable));
    }
    public TimeSpanColumn AddTimeSpanColumn(string Name, bool Nullable)
    {
      return AddColumn(new TimeSpanColumn(Name, Nullable));
    }

    private TColumn AddColumn<TColumn>(TColumn Column)
      where TColumn : Contract<Column>
    {
      ColumnList.Add(Column.Base);

      return Column;
    }

    private Inv.DistinctList<Column> ColumnList;
    private Inv.DistinctList<ForeignKeyColumn> ForeignKeyColumnList;
  }

  public enum DataType
  {
    PrimaryKey,
    ForeignKey,
    Boolean,
    Character,
    String,
    Integer32,
    Integer64,
    DateTimeOffset,
    DateTime,
    TimeSpan
  }

  public sealed class Column
  {
    internal Column(string Name, DataType DataType, bool Nullable)
    {
      this.Name = Name;
      this.DataType = DataType;
      this.Nullable = Nullable;
    }

    public string Name { get; private set; }
    public DataType DataType { get; private set; }
    public bool Nullable { get; private set; }
  }

  public sealed class PrimaryKeyColumn : Contract<Column>
  {
    internal PrimaryKeyColumn(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.PrimaryKey, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class ForeignKeyColumn : Contract<Column>
  {
    internal ForeignKeyColumn(string Name, Table Table, bool Nullable)
    {
      this.Base = new Column(Name, DataType.ForeignKey, Nullable);
      this.Table = Table;
    }

    public Table Table { get; private set; }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class StringColumn : Contract<Column>
  {
    internal StringColumn(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.String, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class Integer32Column : Contract<Column>
  {
    internal Integer32Column(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.Integer32, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class Integer64Column : Contract<Column>
  {
    internal Integer64Column(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.Integer64, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class DateTimeOffsetColumn : Contract<Column>
  {
    internal DateTimeOffsetColumn(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.DateTimeOffset, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class DateTimeColumn : Contract<Column>
  {
    internal DateTimeColumn(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.DateTime, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class TimeSpanColumn : Contract<Column>
  {
    internal TimeSpanColumn(string Name, bool Nullable)
    {
      this.Base = new Column(Name, DataType.TimeSpan, Nullable);
    }

    Column Contract<Column>.Base
    {
      get { return Base; }
    }

    private Column Base;
  }

  public sealed class Connection<TDatabase> : IDisposable
    where TDatabase : Contract<Schematic>
  {
    internal Connection(TDatabase Database, string Context)
    {
      this.Database = Database;
      this.Base = new SqlConnection(Context);

      try
      {
        Base.Open();
      }
      catch
      {
        Base.Dispose();

        throw;
      }
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public InsertCommand<TTable> NewInsert<TTable>(Func<TDatabase, TTable> TableFunction)
      where TTable : Contract<Table>
    {
      return new InsertCommand<TTable>(Base.CreateCommand(), TableFunction(Database));
    }
    public FetchCommand<TTable> NewFetch<TTable>(Func<TDatabase, TTable> TableFunction)
      where TTable : Contract<Table>
    {
      return new FetchCommand<TTable>(Base.CreateCommand(), TableFunction(Database));
    }

    private TDatabase Database;
    private SqlConnection Base;
  }

  public sealed class InsertCommand<TTable> : IDisposable
    where TTable : Contract<Table>
  {
    internal InsertCommand(SqlCommand Base, TTable Table)
    {
      this.Base = Base;
      this.Table = Table;
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public void Set(Func<TTable, StringColumn> ColumnFunction, string Value)
    {
      Set(ColumnFunction(Table), Value);
    }
    public void Set(Func<TTable, Integer32Column> ColumnFunction, int Value)
    {
      Set(ColumnFunction(Table), Value);
    }
    public void Set(Func<TTable, Integer64Column> ColumnFunction, long Value)
    {
      Set(ColumnFunction(Table), Value);
    }
    public void Set(Func<TTable, TimeSpanColumn> ColumnFunction, TimeSpan Value)
    {
      Set(ColumnFunction(Table), Value.Ticks);
    }
    public void Set(Func<TTable, DateTimeOffsetColumn> ColumnFunction, DateTimeOffset Value)
    {
      Set(ColumnFunction(Table), Value);
    }
    public void Set(Func<TTable, DateTimeColumn> ColumnFunction, DateTime Value)
    {
      Set(ColumnFunction(Table), Value);
    }
    public void Execute()
    {
      var ColumnList = Table.Base.GetColumns().Where(C => C.DataType != DataType.PrimaryKey).ToDistinctList();

      if (Base.Parameters.Count != ColumnList.Count)
        throw new Exception("All columns must be supplied."); // TODO: full check by name.

      Base.CommandText = "insert into dbo.[" + Table.Base.Name + "] (" + ColumnList.Select(C => "[" + C.Name + "]").AsSeparatedText(", ") + ") values (" + ColumnList.Select(C => "@" + C.Name).AsSeparatedText(", ") + ");";

      Base.ExecuteNonQuery();
    }

    private void Set(Contract<Column> Column, object Value)
    {
      // TODO: translate Column.DataType to Sql.DataType?

      Base.Parameters.Add(new SqlParameter("@" + Column.Base.Name, Value));
    }

    private TTable Table;
    private SqlCommand Base;
  }

  public sealed class FetchCommand<TTable> : IDisposable
    where TTable : Contract<Table>
  {
    internal FetchCommand(SqlCommand Base, TTable Table)
    {
      this.Base = Base;
      this.Table = Table;
      this.SourceList = new DistinctList<Source>();
      this.OrderList = new DistinctList<Order>();
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public void SelectAll()
    {
      SourceList.AddRange(Table.Base.GetColumns().Select(C => new Source(C, null)));
    }
    public void Unselect(Func<TTable, Contract<Column>> ColumnFunction)
    {
      var Column = ColumnFunction(Table).Base;

      SourceList.RemoveAll(S => S.Column == Column);
    }
    public void Select(string Sql)
    {
      SourceList.Add(new Source(null, Sql));
    }
    public void Select(Func<TTable, Contract<Column>> ColumnFunction)
    {
      SourceList.Add(new Source(ColumnFunction(Table).Base, null));
    }
    public void Top(int? N)
    {
      this.TopN = N;
    }
    public void OrderByAscending(Func<TTable, Contract<Column>> ColumnFunction)
    {
      OrderList.Add(new Order(ColumnFunction(Table).Base, true));
    }
    public void OrderByDescending(Func<TTable, Contract<Column>> ColumnFunction)
    {
      OrderList.Add(new Order(ColumnFunction(Table).Base, false));
    }
    public void Where(string Sql)
    {
      this.WhereSql = Sql;
    }
    public FetchResult<TTable> Execute()
    {
      var Sql = "select";
      if (TopN != null)
        Sql += " top " + TopN.Value;

      if (SourceList.Count == 0)
        Sql += "\n  *";
      else
        Sql += "\n  " + SourceList.Select(C => C.AsSyntax()).AsSeparatedText(", ");

      Sql += "\nfrom\n  dbo.[" + Table.Base.Name + "] as _F";

      if (WhereSql != null)
        Sql += "\nwhere\n  " + WhereSql;

      if (OrderList.Count > 0)
        Sql += "\norder by\n  " + OrderList.Select(O => "_F.[" + O.Column.Name + "] " + (O.Ascending ? "asc" : "desc")).AsSeparatedText(", ");

      Base.CommandText = Sql;

      return new FetchResult<TTable>(Base.ExecuteReader(), Table);
    }

    private TTable Table;
    private SqlCommand Base;
    private int? TopN;
    private string WhereSql;
    private Inv.DistinctList<Source> SourceList;
    private Inv.DistinctList<Order> OrderList;
  }

  public sealed class Source
  {
    internal Source(Column Column, string Sql)
    {
      this.Column = Column;
      this.Sql = Sql;
    }

    public readonly Column Column;
    public readonly string Sql;

    public string AsSyntax()
    {
      return Sql ?? "_F.[" + Column.Name + "]";
    }
  }

  public sealed class Order
  {
    internal Order(Column Column, bool Ascending)
    {
      this.Column = Column;
      this.Ascending = Ascending;
    }

    public readonly Column Column;
    public readonly bool Ascending;
  }

  public sealed class FetchResult<TTable> : IDisposable
    where TTable : Contract<Table>
  {
    internal FetchResult(SqlDataReader Base, TTable Table)
    {
      this.Base = Base;
      this.Table = Table;
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public bool Read()
    {
      return Base.Read();
    }
    public string Get(Func<TTable, StringColumn> ColumnFunction)
    {
      return Get<string>(ColumnFunction(Table));
    }
    public int Get(Func<TTable, Integer32Column> ColumnFunction)
    {
      return Get<int>(ColumnFunction(Table));
    }
    public long Get(Func<TTable, Integer64Column> ColumnFunction)
    {
      return Get<long>(ColumnFunction(Table));
    }
    public DateTimeOffset Get(Func<TTable, DateTimeOffsetColumn> ColumnFunction)
    {
      return Get<DateTimeOffset>(ColumnFunction(Table));
    }
    public DateTime Get(Func<TTable, DateTimeColumn> ColumnFunction)
    {
      return Get<DateTime>(ColumnFunction(Table));
    }
    public TimeSpan Get(Func<TTable, TimeSpanColumn> ColumnFunction)
    {
      return new TimeSpan(Get<long>(ColumnFunction(Table)));
    }

    private T Get<T>(Contract<Column> Column)
    {
      return (T)Base[Column.Base.Name];
    }

    private TTable Table;
    private SqlDataReader Base;
  }
}
