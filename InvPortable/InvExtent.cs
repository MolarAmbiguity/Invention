﻿/*! 8 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Globalization;
using Inv.Support;

namespace Inv
{
  public interface Extent : IComparable
  {
    byte[] GetBuffer();
    DataSize GetSize();
    long GetLength();
    string GetFormat();
    bool IsEmpty();
  }

  [DataContract]
  public struct Binary : Extent
  {
    public static Binary Empty(string Format)
    {
      return new Binary(new byte[] { }, Format);
    }

    public Binary(byte[] Buffer, string Format)
    {
      Debug.Assert(Buffer != null, "Buffer must not be null.");

      this.Buffer = Buffer;
      this.Format = Format;
    }
    internal Binary(Binary Binary)
    {
      this.Buffer = Binary.Buffer;
      this.Format = Binary.Format;
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public long GetLength()
    {
      return this.Buffer.Length;
    }
    public string GetFormat()
    {
      return Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool DeepEqualTo(Binary Binary)
    {
      if (Binary.Format != this.Format)
        return false;
      else if (Binary.Buffer == this.Buffer)
        return true;
      else
        return this.GetBuffer().EqualTo(Binary.GetBuffer());
    }
    public int DeepCompareTo(Binary Binary)
    {
      if (Binary.Format != this.Format)
        return string.Compare(Binary.Format, this.Format);
      else if (Binary.Buffer == this.Buffer)
        return 0;
      else
        return this.GetBuffer().CompareTo(Binary.GetBuffer());
    }
    public Binary ChangeFormat(string NewFormat)
    {
      return new Binary(Buffer, NewFormat);
    }

    public override string ToString()
    {
      return "Binary[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Binary?;

      if (Source == null)
        return false;

      if (Source.Value.Format != this.Format)
        return false;

      if (Source.Value.Buffer != this.Buffer)
        return false;

      return true;
    }
    public override int GetHashCode()
    {
      return this.Buffer.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return DeepCompareTo((Binary)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly string Format;
  }

  [DataContract]
  public struct Image : Extent
  {
    public static Image Empty(string Format)
    {
      return new Image(new byte[] { }, Format);
    }

    public Image(byte[] Buffer, string Format)
    {
      Debug.Assert(Buffer != null, "Buffer must not be null.");

      this.Buffer = Buffer;
      this.Format = Format;
    }
    internal Image(Image Image)
    {
      this.Buffer = Image.Buffer;
      this.Format = Image.Format;
    }
    internal Image(Binary Binary)
    {
      this.Buffer = Binary.GetBuffer();
      this.Format = Binary.GetFormat();
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public long GetLength()
    {
      return this.Buffer.Length;
    }
    public string GetFormat()
    {
      return Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool DeepEqualTo(Image Image)
    {
      if (Image.Format != this.Format)
        return false;
      else if (Image.Buffer == this.Buffer)
        return true;
      else
        return this.GetBuffer().EqualTo(Image.GetBuffer());
    }
    public int DeepCompareTo(Image Image)
    {
      if (Image.Format != this.Format)
        return string.Compare(Image.Format, this.Format);
      else if (Image.Buffer == this.Buffer)
        return 0;
      else
        return this.GetBuffer().CompareTo(Image.GetBuffer());
    }

    public override string ToString()
    {
      return "Image[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Image?;

      if (Source == null)
        return false;

      if (Source.Value.Format != this.Format)
        return false;

      if (Source.Value.Buffer != this.Buffer)
        return false;

      return true;
    }
    public override int GetHashCode()
    {
      return this.Buffer.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return DeepCompareTo((Image)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly string Format;
  }

  [DataContract]
  public struct Sound : Extent
  {
    public static Sound Empty(string Format)
    {
      return new Sound(new byte[] { }, Format);
    }

    public Sound(byte[] Buffer, string Format)
    {
      Debug.Assert(Buffer != null, "Buffer must not be null.");

      this.Buffer = Buffer;
      this.Format = Format;
    }
    internal Sound(Sound Sound)
    {
      this.Buffer = Sound.Buffer;
      this.Format = Sound.Format;
    }
    internal Sound(Binary Binary)
    {
      this.Buffer = Binary.GetBuffer();
      this.Format = Binary.GetFormat();
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public long GetLength()
    {
      return this.Buffer.Length;
    }
    public string GetFormat()
    {
      return Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool DeepEqualTo(Sound Sound)
    {
      if (Sound.Format != this.Format)
        return false;
      else if (Sound.Buffer == this.Buffer)
        return true;
      else
        return this.GetBuffer().EqualTo(Sound.GetBuffer());
    }
    public int DeepCompareTo(Sound Sound)
    {
      if (Sound.Format != this.Format)
        return string.Compare(Sound.Format, this.Format);
      else if (Sound.Buffer == this.Buffer)
        return 0;
      else
        return this.GetBuffer().CompareTo(Sound.GetBuffer());
    }

    public override string ToString()
    {
      return "Sound[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Sound?;

      if (Source == null)
        return false;

      if (Source.Value.Format != this.Format)
        return false;

      if (Source.Value.Buffer != this.Buffer)
        return false;

      return true;
    }
    public override int GetHashCode()
    {
      return this.Buffer.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return DeepCompareTo((Sound)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly string Format;
  }
}
