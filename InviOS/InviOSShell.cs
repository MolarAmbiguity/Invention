﻿/*! 127 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class iOSShell
  {
    public static void Run(Inv.Application Application)
    {
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as Exception;
        if (Exception != null)
          Application.HandleExceptionInvoke(Exception);
      };
      
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException += (Sender, Event) =>
      {
        if (Event.Exception != null)
          Application.HandleExceptionInvoke(Event.Exception);
      };

      var Engine = new iOSEngine(Application);
      Engine.Run();
    }
  }

  internal sealed class iOSPlatform : Inv.Platform
  {
    public iOSPlatform(iOSEngine Engine)
    {
      this.Engine = Engine;
      this.SoundPlayerDictionary = new Dictionary<Inv.Sound, AVFoundation.AVAudioPlayer>();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();

      // NOTE: required to workaround AVAudioPlayer problems.
      ObjCRuntime.Class.ThrowOnInitFailure = false;

      // This allows music from other apps to continue playing.
      AVFoundation.AVAudioSession.SharedInstance().SetCategory(AVFoundation.AVAudioSession.CategoryAmbient);
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      var PopoverViewController = new iOSPopoverViewController();
      PopoverViewController.LoadEvent += () =>
      {
        PopoverViewController.PreferredContentSize = new CoreGraphics.CGSize(320, 216);
        PopoverViewController.View.BackgroundColor = UIKit.UIColor.White;

        var DatePicker = new iOSLayoutDatePicker();
        PopoverViewController.View.AddSubview(DatePicker);

        if (CalendarPicker.SetDate && CalendarPicker.SetTime)
          DatePicker.Mode = UIKit.UIDatePickerMode.DateAndTime;
        else if (CalendarPicker.SetDate)
          DatePicker.Mode = UIKit.UIDatePickerMode.Date;
        else if (CalendarPicker.SetTime)
          DatePicker.Mode = UIKit.UIDatePickerMode.Time;
        else
          throw new Exception("Must set date and/or set time");

        DatePicker.Date = (Foundation.NSDate)CalendarPicker.Value.ToLocalTime();
        //DatePicker.ValueChanged += (Sender, Event) => Debug.WriteLine(DatePicker.Date);

        DatePicker.AddConstraint(UIKit.NSLayoutConstraint.Create(DatePicker, UIKit.NSLayoutAttribute.Height, UIKit.NSLayoutRelation.Equal, null, UIKit.NSLayoutAttribute.NoAttribute, 1.0F, 216.0F));

        PopoverViewController.View.AddConstraint(UIKit.NSLayoutConstraint.Create(PopoverViewController.View, UIKit.NSLayoutAttribute.CenterX, UIKit.NSLayoutRelation.Equal, DatePicker, UIKit.NSLayoutAttribute.CenterX, 1.0F, 0.0F));
        PopoverViewController.View.AddConstraint(UIKit.NSLayoutConstraint.Create(PopoverViewController.View, UIKit.NSLayoutAttribute.CenterY, UIKit.NSLayoutRelation.Equal, DatePicker, UIKit.NSLayoutAttribute.CenterY, 1.0F, 0.0F));
        PopoverViewController.View.AddConstraint(UIKit.NSLayoutConstraint.Create(PopoverViewController.View, UIKit.NSLayoutAttribute.Width, UIKit.NSLayoutRelation.Equal, DatePicker, UIKit.NSLayoutAttribute.Width, 1.0F, 0.0F));
      };

      Engine.iOSWindow.RootViewController.PresentViewController(PopoverViewController, true, null);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Result = MessageUI.MFMailComposeViewController.CanSendMail;

      if (Result)
      {
        var MailController = new MessageUI.MFMailComposeViewController();
        MailController.SetToRecipients(EmailMessage.GetTos().Select(T => T.Address).ToArray());
        MailController.SetSubject(EmailMessage.Subject ?? "");
        MailController.SetMessageBody(EmailMessage.Body ?? "", false);

        foreach (var Attachment in EmailMessage.GetAttachments())
        {
          var FileData = Foundation.NSData.FromFile(SelectFilePath(Attachment.File));

          // TODO: better mime type analysis.
          string MimeType;

          switch (Attachment.File.Extension.ToLower())
          {
            case ".log":
            case ".txt":
              MimeType = "text/plain";
              break;

            case ".png":
              MimeType = "image/png";
              break;

            case ".jpg":
            case ".jpeg":
              MimeType = "image/jpeg";
              break;

            default:
              MimeType = "application/octect-stream";
              break;
          }

          MailController.AddAttachmentData(FileData, MimeType, Attachment.File.Name);
        }

        MailController.Finished += (Sender, Event) =>
        {
          Console.WriteLine(Event.Result.ToString());
          Event.Controller.DismissViewController(true, null);
        };
        Engine.iOSWindow.RootViewController.PresentViewController(MailController, true, null);
      }

      return Result;
    }
    bool Platform.PhoneIsSupported
    {
      get { throw new NotImplementedException(); }
    }
    bool Platform.PhoneDial(string PhoneNumber)
    {
      return UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("tel:" + PhoneNumber.Strip(' ')));
    }
    bool Platform.PhoneSMS(string PhoneNumber)
    {
      return UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("sms:" + PhoneNumber.Strip(' ')));
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      var ResourcePath = Foundation.NSBundle.MainBundle.PathForResource(System.IO.Path.GetFileNameWithoutExtension(Asset.Name), System.IO.Path.GetExtension(Asset.Name));

      return new System.IO.FileStream(ResourcePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
    }
    bool Platform.LocationIsSupported
    {
      get { return UIKit.UIDevice.CurrentDevice.CheckSystemVersion(6, 0) && CoreLocation.CLLocationManager.LocationServicesEnabled; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      var iOSGeocoder = new CoreLocation.CLGeocoder();
      iOSGeocoder.ReverseGeocodeLocation(new CoreLocation.CLLocation(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude), (PlacemarkArray, Error) =>
      {
        Engine.Guard(() => LocationLookup.SetPlacemarks(PlacemarkArray.Select(P => new Inv.Placemark
        (
          Name: P.Name,
          Locality: P.Locality,
          SubLocality: P.SubLocality,
          PostalCode: P.PostalCode,
          AdministrativeArea: P.AdministrativeArea,
          SubAdministrativeArea: P.SubAdministrativeArea,
          CountryName: P.Country,
          CountryCode: P.IsoCountryCode,
          Latitude: P.Location.Coordinate.Latitude,
          Longitude: P.Location.Coordinate.Longitude
        ))));
      });
    }
    void Platform.AudioPlaySound(Inv.Sound Sound, float Volume)
    {
      var SoundPlayer = SoundPlayerDictionary.GetOrAdd(Sound, S =>
      {
        var SoundBuffer = S.GetBuffer();

        // NOTE: this is an attempted workaround when a sound cannot be played:
        //       "Could not initialize an instance of the type 'AVFoundation.AVAudioPlayer': the native 'initWithContentsOfURL:error:' method returned nil."

        Foundation.NSError Error;
        var Retry = 0;
        do
        {
          var Result = AVFoundation.AVAudioPlayer.FromData(Foundation.NSData.FromArray(SoundBuffer), out Error);
          if (Error == null)
            return Result;
          else
            System.Threading.Thread.Sleep(10);

          Retry++;
        }
        while (Error != null && Retry < 100);

        return null;
      });

      if (SoundPlayer == null)
      {
        Debug.WriteLine("Play sound failed.");
      }
      else
      {
        // this is necessary to overlap the playing of the same SFX.
        if (SoundPlayer.Playing)
          SoundPlayer = AVFoundation.AVAudioPlayer.FromData(SoundPlayer.Data);

        // ensure volume is within a valid range.
        if (Volume > 1.0F)
          Volume = 1.0F;
        else if (Volume < 0.0F)
          Volume = 0.0F;

        SoundPlayer.Volume = Volume; 
        SoundPlayer.Play();
      }
    }
    void Platform.WindowAsynchronise(Action Action)
    {
      Engine.iOSWindow.BeginInvokeOnMainThread(Action);
    }
    Modifier Platform.WindowGetModifier()
    {
      return Engine.GetModifier();
    }
    long Platform.ProcessMemoryBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on iOS.
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.WebSocketConnect(WebSocket Socket)
    {
      var TcpClient = new Inv.Tcp.Client(Socket.Host, Socket.Port);
      TcpClient.Connect();

      Socket.Node = TcpClient;
      Socket.SetStreams(TcpClient.InputStream, TcpClient.OutputStream);
    }
    void Platform.WebSocketDisconnect(WebSocket Socket)
    {
      var TcpClient = (Inv.Tcp.Client)Socket.Node;
      if (TcpClient != null)
      {
        Socket.Node = null;
        Socket.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(Uri.AbsoluteUri));
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl("itms-apps://itunes.apple.com/app/id" + AppleiTunesID));
    }

    private string SelectFilePath(File File)
    {
      return Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      // /Library/ is the top-level directory for any files that are not user data files - ideal for application data files.
      var Result = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library");

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(Result, Folder.Name);

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private iOSEngine Engine;
    private Dictionary<Inv.Sound, AVFoundation.AVAudioPlayer> SoundPlayerDictionary;
    private Process CurrentProcess;
  }

  internal static class iOSKeyboard
  {
    static iOSKeyboard()
    {
      var ForwardDictionary = new Dictionary<Inv.Key, string>()
      {
        { Inv.Key.n0, "0" },
        { Inv.Key.n1, "1" },
        { Inv.Key.n2, "2" },
        { Inv.Key.n3, "3" },
        { Inv.Key.n4, "4" },
        { Inv.Key.n5, "5" },
        { Inv.Key.n6, "6" },
        { Inv.Key.n7, "7" },
        { Inv.Key.n8, "8" },
        { Inv.Key.n9, "9" },

        { Inv.Key.A, "A" },
        { Inv.Key.B, "B" },
        { Inv.Key.C, "C" },
        { Inv.Key.D, "D" },
        { Inv.Key.E, "E" },
        { Inv.Key.F, "F" },
        { Inv.Key.G, "G" },
        { Inv.Key.H, "H" },
        { Inv.Key.I, "I" },
        { Inv.Key.J, "J" },
        { Inv.Key.K, "K" },
        { Inv.Key.L, "L" },
        { Inv.Key.M, "M" },
        { Inv.Key.N, "N" },
        { Inv.Key.O, "O" },
        { Inv.Key.P, "P" },
        { Inv.Key.Q, "Q" },
        { Inv.Key.R, "R" },
        { Inv.Key.S, "S" },
        { Inv.Key.T, "T" },
        { Inv.Key.U, "U" },
        { Inv.Key.V, "V" },
        { Inv.Key.W, "W" },
        { Inv.Key.X, "X" },
        { Inv.Key.Y, "Y" },
        { Inv.Key.Z, "Z" },

        { Inv.Key.F1, "F1" },
        { Inv.Key.F2, "F2" },
        { Inv.Key.F3, "F3" },
        { Inv.Key.F4, "F4" },
        { Inv.Key.F5, "F5" },
        { Inv.Key.F6, "F6" },
        { Inv.Key.F7, "F7" },
        { Inv.Key.F8, "F8" },
        { Inv.Key.F9, "F9" },
        { Inv.Key.F10, "F10" },
        { Inv.Key.F11, "F11" },
        { Inv.Key.F12, "F12" },

        { Inv.Key.Escape, UIKit.UIKeyCommand.Escape },
        { Inv.Key.Enter, "\r" },
        { Inv.Key.Tab, "\t" },
        { Inv.Key.Space, " " },
        { Inv.Key.Period, "." },
        { Inv.Key.Comma, "," },
        { Inv.Key.Tilde, "~" },
        { Inv.Key.BackQuote, "`" },
        { Inv.Key.Up, UIKit.UIKeyCommand.UpArrow },
        { Inv.Key.Down, UIKit.UIKeyCommand.DownArrow },
        { Inv.Key.Left, UIKit.UIKeyCommand.LeftArrow },
        { Inv.Key.Right, UIKit.UIKeyCommand.RightArrow },
        { Inv.Key.Delete, "\b" },
        { Inv.Key.Backslash, "\\" },
        { Inv.Key.Slash, "/" },
      };

      ReverseDictionary = ForwardDictionary.ToDictionary(D => D.Value, D => D.Key);

      // TODO: DiscoverabilityTitle in iOS 9?

      // NOTE: the "KeyDown:" selector is actually a private method on the iOSSurfaceController.
      var KeyList =
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), (UIKit.UIKeyModifierFlags)0, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Shift, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Control, new ObjCRuntime.Selector("KeyDown:"))).Union(
        ForwardDictionary.Select(K => UIKit.UIKeyCommand.Create(new Foundation.NSString(K.Value), UIKit.UIKeyModifierFlags.Alternate, new ObjCRuntime.Selector("KeyDown:")))))).ToDistinctList();

      // TODO: page up/down, home/end keys - requires function modifier flag?
      //KeyList.Add(UIKit.UIKeyCommand.Create(new Foundation.NSString(UIKit.UIKeyCommand.UpArrow), UIKit.UIKeyModifierFlags.Fn, new ObjCRuntime.Selector("KeyDown:")));

      KeyCommandArray = KeyList.ToArray();
    }

    public static Inv.Key TranslateKey(string iOSInput)
    {
      return ReverseDictionary[iOSInput];
    }
    public static Inv.Modifier TranslateModifier(UIKit.UIKeyModifierFlags iOSModifierFlags)
    {
      var Result = new Inv.Modifier();

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Shift) != 0)
      {
        Result.IsLeftShift = true;
        Result.IsRightShift = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Control) != 0)
      {
        Result.IsLeftCtrl = true;
        Result.IsRightCtrl = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Alternate) != 0)
      {
        Result.IsLeftAlt = true;
        Result.IsRightAlt = true;
      }

      return Result;
    }

    public static readonly UIKit.UIKeyCommand[] KeyCommandArray;
    public static readonly UIKit.UIKeyCommand[] BlankCommandArray = new UIKit.UIKeyCommand[] { };

    private static Dictionary<string, Inv.Key> ReverseDictionary;
  }

  public sealed class iOSEngine
  {
    public iOSEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      this.UIColorDictionary = new Dictionary<Inv.Colour, UIKit.UIColor>();
      this.CGColorDictionary = new Dictionary<Inv.Colour, CoreGraphics.CGColor>();
      this.UIImageDictionary = new Dictionary<Inv.Image, UIKit.UIImage>();
      this.RouteDictionary = new Dictionary<Type, Func<Inv.Panel, iOSNode>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Render), TranslateRender },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        { typeof(Inv.Table), TranslateTable },
      };

      this.iOSFontWeightArray = new Inv.EnumArray<FontWeight, UIKit.UIFontWeight>();
      iOSFontWeightArray.Fill(W => Inv.Support.EnumHelper.Parse<UIKit.UIFontWeight>(W.ToString(), true));

      InvApplication.Platform = new iOSPlatform(this);
      InvApplication.Begin();
    }

    public void Run()
    {
      iOSAppDelegate.iOSEngine = this;
      try
      {
        UIKit.UIApplication.Main(new string[] { }, null, typeof(iOSAppDelegate));
      }
      finally
      {
        iOSAppDelegate.iOSEngine = null;
      }
    }

    internal UIKit.UIWindow iOSWindow { get; private set; }

    // NOTE: called from the AppDelegate.
    internal UIKit.UIWindow Launching()
    {
      GuardAndProcess(() =>
      {
        var CurrentDevice = UIKit.UIDevice.CurrentDevice;
        InvApplication.Device.Name = CurrentDevice.Name;
        InvApplication.Device.Model = iOSHardware.DeviceModel;
        InvApplication.Device.System = "iOS " + CurrentDevice.SystemVersion;

        this.iOSWindow = new UIKit.UIWindow();

        Resize();

        iOSWindow.MakeKeyAndVisible();

        InvApplication.StartInvoke();

        this.iOSRootController = new UIKit.UINavigationController();
        iOSWindow.RootViewController = iOSRootController;
        iOSRootController.Delegate = new iOSNavigationControllerDelegate();
        iOSRootController.NavigationBarHidden = true;

        this.iOSDisplayLink = CoreAnimation.CADisplayLink.Create(Process);
        iOSDisplayLink.AddToRunLoop(Foundation.NSRunLoop.Current, Foundation.NSRunLoopMode.Default);

        if (InvApplication.Location.IsRequired && InvApplication.Location.IsSupported)
        {
          this.iOSLocationManager = new CoreLocation.CLLocationManager();

          // iOS 8 requires you to manually request authorisation now.
          if (UIKit.UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
          {
            iOSLocationManager.RequestAlwaysAuthorization(); // works in background and foreground
            //iOSLocationManager.RequestWhenInUseAuthorization(); // works only in foreground
          }

          iOSLocationManager.DesiredAccuracy = CoreLocation.CLLocation.AccuracyNearestTenMeters;
          iOSLocationManager.DistanceFilter = 10.0F;
          iOSLocationManager.LocationsUpdated += (Sender, Event) =>
          {
            if (Event.Locations.Length > 0)
            {
              var Location = Event.Locations[Event.Locations.Length - 1];

              GuardAndProcess(() => InvApplication.Location.ChangeInvoke(new Inv.Coordinate(Location.Coordinate.Latitude, Location.Coordinate.Longitude, Location.Altitude)));
            }
          };

          iOSLocationManager.StartUpdatingLocation();
        }
      });

      return iOSWindow;
    }

    internal void Terminating()
    {
      Guard(() =>
      {
        if (iOSLocationManager != null)
        {
          iOSLocationManager.StopUpdatingLocation();
          iOSLocationManager = null;
        }

        InvApplication.StopInvoke();
      });
    }
    internal void Pausing()
    {
      Guard(() => InvApplication.SuspendInvoke());
    }
    internal void Resuming()
    {
      Guard(() => InvApplication.ResumeInvoke());
    }
    internal Inv.Modifier GetModifier()
    {
      return new Inv.Modifier();
    }
    internal void GuardAndProcess(Action Action)
    {
      Guard(Action);

      Process();
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);
      }
    }

    private void Resize()
    {
      var ScreenBounds = UIKit.UIScreen.MainScreen.Bounds;
      InvApplication.Window.Width = (int)ScreenBounds.Width;
      InvApplication.Window.Height = (int)ScreenBounds.Height;

      // NOTE: using iOSWindow.Bounds will work in 9.x but not work in 8.x (it will cause the view to be offset from the middle of the screen).
      iOSWindow.Frame = ScreenBounds;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        UIKit.UIApplication.SharedApplication.PerformSelector(new ObjCRuntime.Selector("terminateWithSuccess"), null, 0f);
        // ALTERNATIVE? [DllImport("__Internal", EntryPoint = "exit")] static extern void exit(int status); exit(0);
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;

          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var iOSTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) => InvWindow.Application.Platform.WindowAsynchronise(() => GuardAndProcess(() => InvTimer.IntervalInvoke()));
                return Result;
              });

              if (iOSTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                iOSTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !iOSTimer.Enabled)
                iOSTimer.Start();
              else if (!InvTimer.IsEnabled && iOSTimer.Enabled)
                iOSTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var iOSSurface = AccessSurface(InvSurface, S =>
            {
              var Result = new iOSLayoutController();
              Result.KeyQuery += () =>
              {
                if (IsTransitioning)
                  return iOSKeyboard.BlankCommandArray;
                else
                  return iOSKeyboard.KeyCommandArray;
              };
              Result.KeyEvent += (Modifier, Key) =>
              {
                S.KeystrokeInvoke(new Keystroke() { Key = iOSKeyboard.TranslateKey(Key), Modifier = iOSKeyboard.TranslateModifier(Modifier) });
              };
              Result.ResizeEvent += (Size) =>
              {
                InvWindow.Application.Platform.WindowAsynchronise(() =>
                {
                  GuardAndProcess(() =>
                  {
                    var ScreenBounds = UIKit.UIScreen.MainScreen.Bounds;
                    InvApplication.Window.Width = (int)Size.Width;
                    InvApplication.Window.Height = (int)Size.Height;
                    S.ArrangeInvoke();
                  });
                });
              };
              Result.LeftEdgeSwipeEvent += () =>
              {
                if (InvWindow.ActiveSurface == S)
                  GuardAndProcess(() => S.GestureBackwardInvoke());
              };
              Result.RightEdgeSwipeEvent += () =>
              {
                if (InvWindow.ActiveSurface == S)
                  GuardAndProcess(() => S.GestureForwardInvoke());
              };
              Result.LoadEvent += () =>
              {
                Guard(() =>
                {
                  if (!iOSRootController.ChildViewControllers.Contains(Result) && InvWindow.ActiveTransition != null)
                    S.ArrangeInvoke();

                  ProcessTransition(Result);

                  S.ComposeInvoke();

                  if (S.Render())
                  {
                    TranslateBackground(S.Background.Colour ?? InvWindow.Background.Colour ?? Inv.Colour.Black, Result.View);

                    Result.SetContentElement(TranslatePanel(S.Content));
                  }
                });
              };

              return Result;
            });

            iOSSurface.Reload();

            InvSurface.ProcessChanges(P => TranslatePanel(P));

            if (InvSurface.Focus != null)
            {
              var iOSFocus = InvSurface.Focus.Node as iOSNode;

              InvSurface.Focus = null;

              if (iOSFocus != null)
                iOSWindow.BeginInvokeOnMainThread(() => iOSFocus.PanelView.View.BecomeFirstResponder());
            }
          }

          if (InvWindow.Render())
          {
            // TODO: InvApplication.Title;
            iOSWindow.BackgroundColor = TranslateUIColor(InvWindow.Background.Colour);
          }

          if (InvSurface != null)
            ProcessAnimation(InvSurface);

          InvWindow.DisplayRate.Calculate();
        });
      }
    }
    private void ProcessTransition(iOSLayoutController iOSSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;

      if (InvTransition == null)
      {
        // no transition.
      }
      else if (!IsTransitioning)
      {
        var iOSFromController = iOSRootController.ChildViewControllers.Length == 0 ? null : iOSRootController.ChildViewControllers[0];

        if (iOSSurface == iOSFromController)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              if (iOSFromController != null)
                iOSFromController.RemoveFromParentViewController();

              iOSRootController.PushViewController(iOSSurface, false);
              break;

            case TransitionAnimation.Fade:
              this.IsTransitioning = true;

              if (iOSFromController != null)
              {
                var HalfTime = InvTransition.Duration.TotalSeconds / 2.0;

                iOSFromController.View.FadeOut(HalfTime, 0.0, () =>
                {
                  iOSFromController.RemoveFromParentViewController();
                  iOSFromController.View.Alpha = 1.0F;

                  iOSRootController.PushViewController(iOSSurface, false);

                  iOSSurface.View.FadeIn(HalfTime, 0.0, () => IsTransitioning = false);
                });
              }
              else
              {
                iOSRootController.PushViewController(iOSSurface, false);
                iOSSurface.View.FadeIn(InvTransition.Duration.TotalSeconds, 0.0, () => IsTransitioning = false);
              }
              break;

            // TODO: Carousel animations need to be implemented properly - there is a timing hole where this code will fail (click fast enough).

            case TransitionAnimation.CarouselBack:
              if (iOSFromController != null)
                iOSFromController.RemoveFromParentViewController();

              iOSRootController.PushViewController(iOSSurface, false);

              if (iOSFromController != null)
              {
                iOSRootController.PushViewController(iOSFromController, false);
                iOSRootController.PopViewController(true);
              }
              break;

            case TransitionAnimation.CarouselNext:
              iOSRootController.PushViewController(iOSSurface, true);
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var iOSAnimationSet = (iOSAnimationSet)InvAnimation.Node;
            InvAnimation.Node = null;

            foreach (var iOSAnimationItem in iOSAnimationSet.ItemList)
            {
              var InvPanel = iOSAnimationItem.InvPanel;
              var iOSPanel = iOSAnimationItem.iOSPanel;

              var PresentationOpacity = iOSPanel.Layer.PresentationLayer.Opacity;

              iOSPanel.Layer.Opacity = PresentationOpacity;
              InvPanel.Opacity.BypassSet(PresentationOpacity);

              iOSPanel.Layer.RemoveAnimation(iOSAnimationItem.iOSAnimationKey);
            }
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var iOSAnimationSet = new iOSAnimationSet();
          InvAnimation.Node = iOSAnimationSet;

          //CoreAnimation.CATransaction.Begin();

          var InvLastTarget = InvAnimation.GetTargets().LastOrDefault();

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvLastCommand = InvTarget.GetCommands().LastOrDefault();

            var InvPanel = InvTarget.Panel;
            var iOSPanel = TranslatePanel(InvPanel);

            var iOSAnimationList = new Inv.DistinctList<CoreAnimation.CAAnimation>();

            foreach (var InvCommand in InvTarget.GetCommands())
            {
              var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

              var FadeAnimation = CoreAnimation.CABasicAnimation.FromKeyPath("opacity");
              FadeAnimation.From = new Foundation.NSNumber(InvOpacityCommand.From);
              FadeAnimation.To = new Foundation.NSNumber(InvOpacityCommand.To);
              FadeAnimation.Duration = InvOpacityCommand.Duration.TotalSeconds;
              FadeAnimation.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
              FadeAnimation.FillMode = CoreAnimation.CAFillMode.Forwards;

              if (InvOpacityCommand.Offset != null)
                FadeAnimation.BeginTime = CoreAnimation.CAAnimation.CurrentMediaTime() + InvOpacityCommand.Offset.Value.TotalSeconds;

              iOSPanel.Layer.Opacity = InvOpacityCommand.From;
              InvPanel.Opacity.BypassSet(InvOpacityCommand.From);
                  
              FadeAnimation.AnimationStarted += (Sender, Event) =>
              {
                if (InvAnimation.IsActive)
                {
                  iOSPanel.Layer.Opacity = InvOpacityCommand.To;
                  InvPanel.Opacity.BypassSet(InvOpacityCommand.To);
                }
              };
              FadeAnimation.AnimationStopped += (Sender, Event) =>
              {
                if (InvAnimation.IsActive)
                {
                  InvCommand.Complete();

                  if (InvTarget == InvLastTarget && InvCommand == InvLastCommand)
                    InvAnimation.Complete();
                }
              };

              iOSAnimationSet.Add(InvPanel, iOSPanel, FadeAnimation);
            }
          }

          //CoreAnimation.CATransaction.Commit();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }

    private iOSLayoutContainer TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel).LayoutView;
    }
    private iOSNode TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var iOSNode = AccessPanel(InvButton, () =>
      {
        var Result = new iOSLayoutButton();
        Result.PreviewTouchesBeganEvent += () =>
        {
          TranslateBackground(InvButton.Background.Colour != null ? InvButton.Background.Colour.Value.Darken(0.25F) : (Inv.Colour?)null, Result);
        };
        Result.PreviewTouchesEndedEvent += () =>
        {
          TranslateBackground(InvButton.Background.Colour, Result);
        };
        Result.PreviewTouchesCancelledEvent += () =>
        {
          TranslateBackground(InvButton.Background.Colour, Result);
        };
        Result.AddGestureRecognizer(new UIKit.UITapGestureRecognizer(R =>
        {
          if (R.State == UIKit.UIGestureRecognizerState.Ended)
            GuardAndProcess(() => InvButton.SingleTapInvoke());
        }));
        Result.AddGestureRecognizer(new UIKit.UILongPressGestureRecognizer(R =>
        {
          if (R.State == UIKit.UIGestureRecognizerState.Began)
            GuardAndProcess(() => InvButton.ContextTapInvoke());
        }));

        return Result;
      });

      RenderPanel(InvButton, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSButton = iOSNode.PanelView;

        TranslateLayout(InvButton, iOSLayout, iOSButton);

        iOSLayout.Alpha = InvButton.IsEnabled ? 1.0F : 0.5F;
        iOSButton.Enabled = InvButton.IsEnabled;
        // TODO: InvButton.IsFocused

        if (InvButton.ContentSingleton.Render())
        {
          iOSButton.SetContentElement(null); // detach previous content in case it has moved.
          var iOSPanel = TranslatePanel(InvButton.ContentSingleton.Data);
          iOSButton.SetContentElement(iOSPanel);
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var iOSNode = AccessPanel(InvCanvas, () =>
      {
        var Result = new iOSLayoutCanvas();
        return Result;
      });

      RenderPanel(InvCanvas, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSCanvas = iOSNode.PanelView;

        TranslateLayout(InvCanvas, iOSLayout, iOSCanvas);

        if (InvCanvas.PieceCollection.Render())
        {
          iOSCanvas.RemoveElements();
          foreach (var InvElement in InvCanvas.PieceCollection)
          {
            var InvRect = InvElement.Rect;

            var iOSElement = TranslatePanel(InvElement.Panel);
            iOSCanvas.AddElement(iOSElement, new CoreGraphics.CGRect(InvRect.Left, InvRect.Top, InvRect.Width, InvRect.Height));
          }
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var iOSNode = AccessPanel(InvDock, () =>
      {
        var Result = new iOSLayoutDock();
        Result.SetOrientation(InvDock.Orientation == DockOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvDock, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSDock = iOSNode.PanelView;

        TranslateLayout(InvDock, iOSLayout, iOSDock);

        if (InvDock.CollectionRender())
          iOSDock.ComposeElements(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Reverse().Select(F => TranslatePanel(F)));
      });

      return iOSNode;
    }
    private iOSNode TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var iOSNode = AccessPanel(InvEdit, () =>
      {
        var Result = new iOSLayoutEdit();
        Result.EditingChanged += (Sender, Event) => GuardAndProcess(() => InvEdit.ChangeInvoke(Result.Text));
        Result.EditingDidEnd += (Sender, Event) => GuardAndProcess(() => InvEdit.ChangeInvoke(Result.Text));
        return Result;
      });

      RenderPanel(InvEdit, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSEdit = iOSNode.PanelView;

        TranslateLayout(InvEdit, iOSLayout, iOSEdit);
        TranslateFont(InvEdit.Font, (iOSFont, iOSColor) =>
        {
          iOSEdit.Font = iOSFont;
          iOSEdit.TextColor = iOSColor;
        });

        iOSEdit.Enabled = !InvEdit.IsReadOnly;
        iOSEdit.Text = InvEdit.Text;
      });

      return iOSNode;
    }
    private iOSNode TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var iOSNode = AccessPanel(InvGraphic, () =>
      {
        var Result = new iOSLayoutGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSGraphic = iOSNode.PanelView;

        TranslateLayout(InvGraphic, iOSLayout, iOSGraphic);

        if (InvGraphic.ImageSingleton.Render())
        {
          var iOSImage = TranslateUIImage(InvGraphic.Image, InvGraphic.Size.Width, InvGraphic.Size.Height);

          if (iOSGraphic.Image != iOSImage)
            iOSGraphic.Image = iOSImage;
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var iOSNode = AccessPanel(InvLabel, () =>
      {
        var Result = new iOSLayoutLabel();
        return Result;
      });

      RenderPanel(InvLabel, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSLabel = iOSNode.PanelView;

        TranslateLayout(InvLabel, iOSLayout, iOSLabel);
        TranslateFont(InvLabel.Font, (iOSFont, iOSColor) =>
        {
          iOSLabel.Font = iOSFont;
          iOSLabel.TextColor = iOSColor;
        });

        switch (InvLabel.Justification)
        {
          case null:
            break;

          case Inv.Justification.Left:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Left;
            break;

          case Inv.Justification.Center:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Center;
            break;

          case Inv.Justification.Right:
            iOSLabel.TextAlignment = UIKit.UITextAlignment.Right;
            break;

          default:
            throw new ApplicationException("Inv.Justification not handled: " + InvLabel.Justification);
        }

        iOSLabel.Lines = InvLabel.LineWrapping ? 0 : 1;
        iOSLabel.LineBreakMode = InvLabel.LineWrapping ? UIKit.UILineBreakMode.WordWrap : UIKit.UILineBreakMode.TailTruncation;

        iOSLabel.Text = InvLabel.Text;
      });

      return iOSNode;
    }
    private iOSNode TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var iOSNode = AccessPanel(InvMemo, () =>
      {
        var Result = new iOSLayoutMemo();
        Result.Changed += (Sender, Event) => GuardAndProcess(() => InvMemo.ChangeInvoke(Result.Text));
        return Result;
      });

      RenderPanel(InvMemo, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSMemo = iOSNode.PanelView;

        TranslateLayout(InvMemo, iOSLayout, iOSMemo);
        TranslateFont(InvMemo.Font, (iOSFont, iOSColor) =>
        {
          iOSMemo.Font = iOSFont;
          iOSMemo.TextColor = iOSColor;
        });

        iOSMemo.Editable = !InvMemo.IsReadOnly;
        iOSMemo.Text = InvMemo.Text;
      });

      return iOSNode;
    }
    private iOSNode TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var iOSNode = AccessPanel(InvOverlay, () =>
      {
        var Result = new iOSLayoutOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSOverlay = iOSNode.PanelView;

        TranslateLayout(InvOverlay, iOSLayout, iOSOverlay);

        if (InvOverlay.PanelCollection.Render())
          iOSOverlay.ComposeElements(InvOverlay.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    private iOSNode TranslateRender(Inv.Panel InvPanel)
    {
      var InvRender = (Inv.Render)InvPanel;

      var iOSNode = AccessPanel(InvRender, () =>
      {
        var Result = new iOSLayoutRender();
        Result.TouchDownEvent += (Point) => GuardAndProcess(() => InvRender.PressInvoke(TranslatePoint(Point)));
        Result.TouchMoveEvent += (Point) => GuardAndProcess(() => InvRender.MoveInvoke(TranslatePoint(Point)));
        Result.TouchUpEvent += (Point) => GuardAndProcess(() => InvRender.ReleaseInvoke(TranslatePoint(Point)));
        Result.SingleTapEvent += (Point) => GuardAndProcess(() => InvRender.SingleTapInvoke(TranslatePoint(Point)));
        Result.DoubleTapEvent += (Point) => GuardAndProcess(() => InvRender.DoubleTapInvoke(TranslatePoint(Point)));
        Result.LongPressEvent += (Point) => GuardAndProcess(() => InvRender.ContextTapInvoke(TranslatePoint(Point)));
        Result.ZoomEvent += (Delta) => GuardAndProcess(() => InvRender.ZoomInvoke(Delta));

        var iOSEllipseRect = new CoreGraphics.CGRect();
        var iOSRectangleRect = new CoreGraphics.CGRect();
        var iOSTextPoint = new CoreGraphics.CGPoint();
        var iOSImageRect = new CoreGraphics.CGRect();

        Result.DrawEvent += (DC) =>
        {
          foreach (var InvRenderElement in InvRender.GetCommands())
          {
            switch (InvRenderElement.Type)
            {
              case RenderType.Rectangle:
                var RectangleRect = InvRenderElement.RectangleRect;
                var RectangleFillColour = InvRenderElement.RectangleFillColour;
                var RectangleStrokeColour = InvRenderElement.RectangleStrokeColour;
                var RectangleStrokeThickness = InvRenderElement.RectangleStrokeThickness;

                iOSRectangleRect.X = RectangleRect.Left;
                iOSRectangleRect.Y = RectangleRect.Top;
                iOSRectangleRect.Width = RectangleRect.Width;
                iOSRectangleRect.Height = RectangleRect.Height;

                if (RectangleFillColour != null)
                {
                  DC.SetFillColor(TranslateCGColor(RectangleFillColour));
                  DC.FillRect(iOSRectangleRect);
                }

                if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
                {
                  iOSRectangleRect.X += RectangleStrokeThickness / 2;
                  iOSRectangleRect.Y += RectangleStrokeThickness / 2;

                  if (iOSRectangleRect.Width > RectangleStrokeThickness)
                    iOSRectangleRect.Width -= RectangleStrokeThickness;

                  if (iOSRectangleRect.Height > RectangleStrokeThickness)
                    iOSRectangleRect.Height -= RectangleStrokeThickness;

                  DC.SetStrokeColor(TranslateCGColor(RectangleStrokeColour));
                  DC.SetLineWidth(RectangleStrokeThickness);
                  DC.StrokeRect(iOSRectangleRect);
                }
                break;

              case RenderType.Ellipse:
                var EllipseCenter = InvRenderElement.EllipseCenter;
                var EllipseRadius = InvRenderElement.EllipseRadius;
                var EllipseFillColour = InvRenderElement.EllipseFillColour;
                var EllipseStrokeColour = InvRenderElement.EllipseStrokeColour;
                var EllipseStrokeThickness = InvRenderElement.EllipseStrokeThickness;

                iOSEllipseRect.X = EllipseCenter.X - EllipseRadius.X;
                iOSEllipseRect.Y = EllipseCenter.Y - EllipseRadius.Y;
                iOSEllipseRect.Width = EllipseRadius.X * 2;
                iOSEllipseRect.Height = EllipseRadius.Y * 2;

                if (EllipseFillColour != null)
                {
                  DC.SetFillColor(TranslateCGColor(EllipseFillColour));
                  DC.FillEllipseInRect(iOSEllipseRect);
                }

                if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
                {
                  DC.SetStrokeColor(TranslateCGColor(EllipseStrokeColour));
                  DC.SetLineWidth(EllipseStrokeThickness);
                  DC.StrokeEllipseInRect(iOSEllipseRect);
                }
                break;

              case RenderType.Text:
                var TextPoint = InvRenderElement.TextPoint;
                var TextFragment = InvRenderElement.TextFragment;
                var TextHorizontal = InvRenderElement.TextHorizontal;
                var TextVertical = InvRenderElement.TextVertical;

                var AttributedDictionary = new Foundation.NSMutableDictionary();
                AttributedDictionary["NSColor"] = TranslateUIColor(InvRenderElement.TextFontColour);
                AttributedDictionary["NSFont"] = TranslateUIFont(InvRenderElement.TextFontName.EmptyAsNull() ?? InvApplication.Window.DefaultFont.Name ?? "", InvRenderElement.TextFontSize, InvRenderElement.TextFontWeight);

                var AttributedString = new Foundation.NSAttributedString(TextFragment, new CoreText.CTStringAttributes(AttributedDictionary));

                var TextRect = AttributedString.GetBoundingRect(CoreGraphics.CGSize.Empty, Foundation.NSStringDrawingOptions.OneShot, null);
                var TextPointX = TextPoint.X;
                var TextPointY = TextPoint.Y;

                if (TextHorizontal != HorizontalPosition.Left)
                {
                  var TextWidth = (int)TextRect.Width;

                  if (TextHorizontal == HorizontalPosition.Right)
                    TextPointX -= TextWidth;
                  else if (TextHorizontal == HorizontalPosition.Center)
                    TextPointX -= TextWidth / 2;
                }

                if (TextVertical != VerticalPosition.Top)
                {
                  var TextHeight = (int)TextRect.Height;

                  if (TextVertical == VerticalPosition.Bottom)
                    TextPointY -= TextHeight;
                  else if (TextVertical == VerticalPosition.Center)
                    TextPointY -= TextHeight / 2;
                }

                iOSTextPoint.X = TextPointX;
                iOSTextPoint.Y = TextPointY;

                AttributedString.DrawString(iOSTextPoint);
                break;

              case RenderType.Image:
                var ImageRect = InvRenderElement.ImageRect;
                var ImageTint = InvRenderElement.ImageTint;
                var ImageMirror = InvRenderElement.ImageMirror;

                var iOSImageSource = TranslateCGImage(InvRenderElement.ImageSource, ImageRect.Width, ImageRect.Height);

                DC.SaveState();

                if (ImageMirror == null)
                {
                  var BoundsHeight = Result.Bounds.Height;

                  iOSImageRect.X = ImageRect.Left;
                  iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
                  iOSImageRect.Width = ImageRect.Width;
                  iOSImageRect.Height = ImageRect.Height;

                  DC.TranslateCTM(0, BoundsHeight);
                  DC.ScaleCTM(+1.0F, -1.0F);
                }
                else if (ImageMirror.Value == Mirror.Horizontal)
                {
                  var BoundsWidth = Result.Bounds.Width;
                  var BoundsHeight = Result.Bounds.Height;

                  iOSImageRect.X = BoundsWidth - ImageRect.Left - ImageRect.Width;
                  iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
                  iOSImageRect.Width = ImageRect.Width;
                  iOSImageRect.Height = ImageRect.Height;

                  DC.TranslateCTM(BoundsWidth, BoundsHeight);
                  DC.ScaleCTM(-1.0F, -1.0F);
                }
                else if (ImageMirror.Value == Mirror.Vertical)
                {
                  iOSImageRect.X = ImageRect.Left;
                  iOSImageRect.Y = ImageRect.Top;
                  iOSImageRect.Width = ImageRect.Width;
                  iOSImageRect.Height = ImageRect.Height;
                }

                if (InvRenderElement.ImageOpacity != 1.0F)
                  DC.SetAlpha(InvRenderElement.ImageOpacity);

                DC.DrawImage(iOSImageRect, iOSImageSource);

                // TODO: use of image tint severely impacts on FPS.
                if (ImageTint != null)
                {
                  DC.ClipToMask(iOSImageRect, iOSImageSource);
                  DC.SetFillColor(TranslateCGColor(ImageTint));
                  DC.FillRect(iOSImageRect);
                }

                DC.RestoreState();
                break;

              default:
                throw new ApplicationException("RenderType not handled: " + InvRenderElement.Type);
            }
          }
        };
        return Result;
      });

      RenderPanel(InvRender, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSRender = iOSNode.PanelView;

        TranslateLayout(InvRender, iOSLayout, iOSRender);

        InvRender.ContextSingleton.Data.Width = (int)iOSRender.Frame.Width;
        InvRender.ContextSingleton.Data.Height = (int)iOSRender.Frame.Height;
        if (InvRender.ContextSingleton.Render())
          iOSRender.SetNeedsDisplay();
      });

      return iOSNode;
    }
    private iOSNode TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var iOSNode = AccessPanel(InvScroll, () =>
      {
        var Result = new iOSLayoutScroll();
        Result.SetOrientation(InvScroll.Orientation == ScrollOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSScroll = iOSNode.PanelView;

        TranslateLayout(InvScroll, iOSLayout, iOSScroll);

        if (InvScroll.ContentSingleton.Render())
          iOSScroll.SetContentElement(TranslatePanel(InvScroll.Content));
      });

      return iOSNode;
    }
    private iOSNode TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var iOSNode = AccessPanel(InvFrame, () =>
      {
        var Result = new iOSLayoutFrame();
        return Result;
      });

      RenderPanel(InvFrame, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSFrame = iOSNode.PanelView;

        TranslateLayout(InvPanel, iOSLayout, iOSFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          iOSFrame.SetContentElement(null); // detach previous content in case it has moved.
          var iOSPanel = TranslatePanel(InvFrame.ContentSingleton.Data);
          iOSFrame.SetContentElement(iOSPanel);
        }
      });

      return iOSNode;
    }
    private iOSNode TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var iOSNode = AccessPanel(InvStack, () =>
      {
        var Result = new iOSLayoutStack();
        Result.SetOrientation(InvStack.Orientation == StackOrientation.Vertical ? iOSLayoutOrientation.Vertical : iOSLayoutOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvStack, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSStack = iOSNode.PanelView;

        TranslateLayout(InvStack, iOSLayout, iOSStack);

        if (InvStack.PanelCollection.Render())
          iOSStack.ComposeElements(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    private iOSNode TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var iOSNode = AccessPanel(InvTable, () =>
      {
        var Result = new iOSLayoutTable();
        return Result;
      });

      RenderPanel(InvTable, iOSNode, () =>
      {
        var iOSLayout = iOSNode.LayoutView;
        var iOSTable = iOSNode.PanelView;

        TranslateLayout(InvTable, iOSLayout, iOSTable);

        if (InvTable.CollectionRender())
        {
          var iOSRowList = new Inv.DistinctList<iOSLayoutTableRow>(InvTable.RowCollection.Count);

          foreach (var InvRow in InvTable.RowCollection)
          {
            var iOSRow = new iOSLayoutTableRow(iOSTable, TranslatePanel(InvRow.Content), InvRow.Length.Type == TableLengthType.Star, InvRow.Length.Type == TableLengthType.Fixed ? InvRow.Length.Value : InvRow.Length.Type == TableLengthType.Star ? 1 : (int?)null);
            iOSRowList.Add(iOSRow);
          }

          var iOSColumnList = new Inv.DistinctList<iOSLayoutTableColumn>(InvTable.ColumnCollection.Count);

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            var iOSColumn = new iOSLayoutTableColumn(iOSTable, TranslatePanel(InvColumn.Content), InvColumn.Length.Type == TableLengthType.Star, InvColumn.Length.Type == TableLengthType.Fixed ? InvColumn.Length.Value : InvColumn.Length.Type == TableLengthType.Star ? 1 : (int?)null);
            iOSColumnList.Add(iOSColumn);
          }

          var iOSCellList = new Inv.DistinctList<iOSLayoutTableCell>(InvTable.CellCollection.Count);

          foreach (var InvCell in InvTable.CellCollection)
          {
            var iOSCell = new iOSLayoutTableCell(iOSColumnList[InvCell.Column.Index], iOSRowList[InvCell.Row.Index], TranslatePanel(InvCell.Content));
            iOSCellList.Add(iOSCell);
          }

          iOSTable.ComposeElements(iOSRowList, iOSColumnList, iOSCellList);
        }
      });

      return iOSNode;
    }

    private void TranslateLayout(Panel InvPanel, iOSLayoutContainer iOSLayout, iOSLayoutElement iOSElement)
    {
      var iOSView = iOSElement.View;

      if (InvPanel.Opacity.Render())
        iOSView.Alpha = InvPanel.Opacity.Get();

      var InvBackground = InvPanel.Background;
      if (InvBackground.Render())
        TranslateBackground(InvBackground.Colour, iOSView);

      var InvBorder = InvPanel.Border;
      var RenderBorder = InvBorder.IsChanged;

      if (InvBorder.Render())
      {
        iOSView.Layer.BorderColor = TranslateCGColor(InvBorder.Colour);
        iOSView.Layer.BorderWidth = InvBorder.Thickness.Left; // TODO: implement all borders or simplify to single BorderWidth?
      }

      var InvElevation = InvPanel.Elevation;
      if (InvElevation.Render())
      {
        if (InvElevation.Depth == 0 || InvElevation.Colour == null)
        {
          //iOSView.Layer.ShadowColor = null;
          iOSView.Layer.ShadowOffset = CoreGraphics.CGSize.Empty;
          iOSView.Layer.ShadowRadius = 0;
          iOSView.Layer.ShadowOpacity = 0.0F;
        }
        else
        {
          iOSView.Layer.ShadowColor = TranslateCGColor(InvElevation.Colour);
          iOSView.Layer.ShadowOffset = new CoreGraphics.CGSize(0, InvElevation.Depth);
          iOSView.Layer.ShadowRadius = InvElevation.Depth;
          iOSView.Layer.ShadowOpacity = 1.0F;
        }
      }

      var InvMargin = InvPanel.Margin;

      if (InvMargin.Render())
      {
        var iOSMargins = InvPanel.Visibility.Get() ? new UIKit.UIEdgeInsets(InvMargin.Top, InvMargin.Left, InvMargin.Bottom, InvMargin.Right) : UIKit.UIEdgeInsets.Zero;

        //if (iOSLayout.LayoutMargins != iOSMargins)
        {
          iOSLayout.LayoutMargins = iOSMargins;
          iOSLayout.Arrange();
        }
      }

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render() || RenderBorder)
      {
        var Additional = InvPanel.Border.Thickness.Left;

        iOSView.LayoutMargins = new UIKit.UIEdgeInsets(InvPadding.Top + Additional, InvPadding.Left + Additional, InvPadding.Bottom + Additional, InvPadding.Right + Additional);
        iOSView.Arrange();
      }

      var InvCornerRadius = InvPanel.CornerRadius;
      if (InvCornerRadius.Render())
        iOSView.Layer.CornerRadius = InvCornerRadius.TopLeft; // TODO: implement all corners or simplify to a single CornerRadius?

      TranslateVisibilitySizeAlignment(InvPanel, iOSLayout, iOSElement);
    }
    private void TranslateVisibilitySizeAlignment(Inv.Panel InvPanel, iOSLayoutContainer iOSLayout, iOSLayoutElement iOSElement)
    {
      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        iOSLayout.SetContentVisiblity(InvVisibility.Get());

      var InvSize = InvPanel.Size;
      if (InvSize.Render())
      {
        iOSLayout.SetContentWidth(InvSize.Width ?? float.NaN);
        iOSLayout.SetContentHeight(InvSize.Height ?? float.NaN);
        iOSLayout.SetContentMinimumWidth(InvSize.MinimumWidth ?? float.NaN);
        iOSLayout.SetContentMinimumHeight(InvSize.MinimumHeight ?? float.NaN);
        iOSLayout.SetContentMaximumWidth(InvSize.MaximumWidth ?? float.NaN);
        iOSLayout.SetContentMaximumHeight(InvSize.MaximumHeight ?? float.NaN);
      }

      var InvAlignment = InvPanel.Alignment;
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Bottom);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Stretch);
            break;

          case Placement.BottomLeft:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Bottom);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Left);
            break;

          case Placement.BottomCenter:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Bottom);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Center);
            break;

          case Placement.BottomRight:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Bottom);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Right);
            break;

          case Placement.TopStretch:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Top);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Stretch);
            break;

          case Placement.TopLeft:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Top);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Left);
            break;

          case Placement.TopCenter:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Top);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Center);
            break;

          case Placement.TopRight:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Top);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Right);
            break;

          case Placement.CenterStretch:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Center);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Stretch);
            break;

          case Placement.CenterLeft:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Center);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Left);
            break;

          case Placement.Center:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Center);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Center);
            break;

          case Placement.CenterRight:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Center);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Right);
            break;

          case Placement.Stretch:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Stretch);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Stretch);
            break;

          case Placement.StretchLeft:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Stretch);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Left);
            break;

          case Placement.StretchCenter:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Stretch);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Center);
            break;

          case Placement.StretchRight:
            iOSLayout.SetContentVertical(iOSLayoutVertical.Stretch);
            iOSLayout.SetContentHorizontal(iOSLayoutHorizontal.Right);
            break;

          default:
            throw new ApplicationException("Placement not handled: " + InvPanel.Alignment.Get());
        }
      }
    }
    private void TranslateBackground(Inv.Colour? InvColour, UIKit.UIView iOSView)
    {
      iOSView.BackgroundColor = TranslateUIColor(InvColour);

      // TODO: this appears to have no effect?
      //iOSView.Opaque = true;// InvColour.IsOpaque;
    }
    private void TranslateFont(Inv.Font InvFont, Action<UIKit.UIFont, UIKit.UIColor> Action)
    {
      if (InvFont.Render())
      {
        var FontTypeface = TranslateUIFont(InvFont.Name ?? InvApplication.Window.DefaultFont.Name, InvFont.Size ?? InvApplication.Window.DefaultFont.Size ?? 14, InvFont.Weight);
        var FontColor = TranslateUIColor(InvFont.Colour ?? InvApplication.Window.DefaultFont.Colour ?? Inv.Colour.Black);

        Action(FontTypeface, FontColor);
      }
    }
    private Inv.Point TranslatePoint(CoreGraphics.CGPoint iOSPoint)
    {
      return new Inv.Point((int)iOSPoint.X, (int)iOSPoint.Y);
    }
    private UIKit.UIFont TranslateUIFont(Inv.Font InvFont)
    {
      return TranslateUIFont(InvFont.Name, InvFont.Size, InvFont.Weight);
    }

    private Inv.EnumArray<Inv.FontWeight, UIKit.UIFontWeight> iOSFontWeightArray;

    private UIKit.UIFont TranslateUIFont(string FontName, int? FontSize, Inv.FontWeight InvFontWeight)
    {
      // TODO: custom font name.

      var iOSFontWeight = iOSFontWeightArray[InvFontWeight];

      if (FontSize != null)
        return UIKit.UIFont.SystemFontOfSize(FontSize.Value, iOSFontWeight);
      else
        return UIKit.UIFont.SystemFontOfSize(UIKit.UIFont.SystemFontSize);
    }
    private CoreGraphics.CGImage TranslateCGImage(Inv.Image? Image, int? PointWidth, int? PointHeight)
    {
      if (Image == null)
        return null;
      else
        return TranslateUIImage(Image, PointWidth, PointHeight).CGImage;
    }
    private UIKit.UIImage TranslateUIImage(Inv.Image? Image, int? PointWidth, int? PointHeight)
    {
      if (Image == null)
      {
        return null;
      }
      else
      {
        return UIImageDictionary.GetOrAdd(Image.Value, K =>
        {
          var Result = UIKit.UIImage.LoadFromData(Foundation.NSData.FromArray(K.GetBuffer()), 3.0F);

          //Debug.WriteLine(Result.CGImage.AlphaInfo);
          //Debug.WriteLine("TranslateUIImage" + UIImageDictionary.Count);

          return Result;
        });
      }
    }
    private UIKit.UIColor TranslateUIColor(Inv.Colour? InvColour)
    {
      if (InvColour == null)
      {
        return UIKit.UIColor.Clear;
      }
      else
      {
        return UIColorDictionary.GetOrAdd(InvColour.Value, C =>
        {
          var Record = C.GetARGBRecord();

          //Debug.WriteLine("TranslateUIColor" + UIColorDictionary.Count);

          var Result = new UIKit.UIColor((float)Record.R / 255F, (float)Record.G / 255F, (float)Record.B / 255F, (float)Record.A / 255F);

          return Result;
        });
      }
    }
    private CoreGraphics.CGColor TranslateCGColor(Inv.Colour? InvColour)
    {
      if (InvColour == null)
      {
        return UIKit.UIColor.Clear.CGColor;
      }
      else
      {
        return CGColorDictionary.GetOrAdd(InvColour.Value, C =>
        {
          var Record = C.GetARGBRecord();

          //Debug.WriteLine("TranslateCGColor" + CGColorDictionary.Count);

          var Result = new CoreGraphics.CGColor((float)Record.R / 255F, (float)Record.G / 255F, (float)Record.B / 255F, (float)Record.A / 255F);

          return Result;
        });
      }
    }
    private iOSLayoutController AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, iOSLayoutController> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (iOSLayoutController)InvSurface.Node;
      }
    }
    private System.Timers.Timer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private iOSNode<TView> AccessPanel<TView>(Panel InvPanel, Func<TView> BuildFunction)
      where TView : iOSLayoutElement
    {
      if (InvPanel.Node == null)
      {
        var Result = new iOSNode<TView>();
        Result.LayoutView = new iOSLayoutContainer();
        Result.PanelView = BuildFunction();
        Result.LayoutView.SetContentElement(Result.PanelView);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (iOSNode<TView>)InvPanel.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, iOSNode iOSNode, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }

    private Inv.Application InvApplication;
    private Dictionary<Inv.Colour, UIKit.UIColor> UIColorDictionary;
    private Dictionary<Inv.Colour, CoreGraphics.CGColor> CGColorDictionary;
    private Dictionary<Inv.Image, UIKit.UIImage> UIImageDictionary;
    private Dictionary<Type, Func<Inv.Panel, iOSNode>> RouteDictionary;
    private CoreLocation.CLLocationManager iOSLocationManager;
    private CoreAnimation.CADisplayLink iOSDisplayLink;
    private UIKit.UINavigationController iOSRootController;
    private bool IsTransitioning;

    private sealed class iOSAnimationSet
    {
      public iOSAnimationSet()
      {
        this.ItemList = new DistinctList<iOSAnimationItem>();
      }

      public void Add(Inv.Panel InvPanel, UIKit.UIView iOSPanel, CoreAnimation.CAAnimation iOSAnimation)
      {
        var iOSAnimationKey = "a_" + AnimationCount++;
        iOSPanel.Layer.AddAnimation(iOSAnimation, iOSAnimationKey);

        ItemList.Add(new iOSAnimationItem(InvPanel, iOSPanel, iOSAnimationKey));
      }

      public readonly Inv.DistinctList<iOSAnimationItem> ItemList;

      private static int AnimationCount;
    }

    private sealed class iOSAnimationItem
    {
      public iOSAnimationItem(Inv.Panel InvPanel, UIKit.UIView iOSPanel, string iOSAnimationKey)
      {
        this.InvPanel = InvPanel;
        this.iOSPanel = iOSPanel;
        this.iOSAnimationKey = iOSAnimationKey;
      }

      public readonly Inv.Panel InvPanel;
      public readonly UIKit.UIView iOSPanel;
      public readonly string iOSAnimationKey;
    }
  }

  internal abstract class iOSNode
  {
    public iOSLayoutContainer LayoutView { get; set; }
    public iOSLayoutElement PanelView { get; set; }
  }

  internal sealed class iOSNode<T> : iOSNode
    where T : iOSLayoutElement
  {
    public new T PanelView
    {
      get { return (T)base.PanelView; }
      set { base.PanelView = value; }
    }
  }
}