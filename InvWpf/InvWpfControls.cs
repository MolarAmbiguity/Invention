﻿/*! 20 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Inv.Support;
using System.Windows.Media;

namespace Inv
{
  internal sealed class SoundPlayer
  {
    public SoundPlayer()
    {
      try
      {
        this.SoundEngine = new IrrKlang.ISoundEngine();
      }
      catch (Exception Exception)
      {
        this.SoundException = Exception;
      }

      this.SourceDictionary = new Dictionary<Inv.Sound, IrrKlang.ISoundSource>();
    }
    public void Play(Inv.Sound Sound, float VolumeScale = 1.0f)
    {
      if (SoundEngine != null)
      {
        var SoundSource = SourceDictionary.GetValueOrDefault(Sound);

        if (SoundSource == null)
        {
          SoundSource = SoundEngine.AddSoundSourceFromMemory(Sound.GetBuffer(), SourceDictionary.Count.ToString());
          SourceDictionary.Add(Sound, SoundSource);
        }

        SoundEngine.Play2D(SoundSource, false, false, true).Volume = VolumeScale;
      }
    }

    public Exception SoundException { get; private set; }

    private IrrKlang.ISoundEngine SoundEngine;
    private Dictionary<Inv.Sound, IrrKlang.ISoundSource> SourceDictionary;
  }

  internal interface WpfOverrideFocusContract
  {
    void OverrideFocus();
  }

  internal sealed class WpfSurface : System.Windows.Controls.Border
  {
    public WpfSurface()
    {
      this.ClipToBounds = true;
    }
  }

  internal sealed class WpfButton : System.Windows.Controls.Button
  {
    static WpfButton()
    {
      HoverBackgroundProperty = System.Windows.DependencyProperty.Register("HoverBackground", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BackgroundProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      PressedBackgroundProperty = System.Windows.DependencyProperty.Register("PressedBackground", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BackgroundProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      CornerRadiusProperty = System.Windows.DependencyProperty.Register("CornerRadius", typeof(System.Windows.CornerRadius), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.CornerRadius(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      HoverBorderBrushProperty = System.Windows.DependencyProperty.Register("HoverBorderBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Controls.Button.BorderBrushProperty.DefaultMetadata.DefaultValue, System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      HoverBorderThicknessProperty = System.Windows.DependencyProperty.Register("HoverBorderThickness", typeof(System.Windows.Thickness), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.Thickness(1), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));
      InnerBorderThicknessProperty = System.Windows.DependencyProperty.Register("InnerBorderThickness", typeof(System.Windows.Thickness), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.Thickness(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderPressedBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderPressedBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderHoverBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderHoverBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      InnerBorderFocusBrushProperty = System.Windows.DependencyProperty.Register("InnerBorderFocusBrush", typeof(System.Windows.Media.Brush), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(System.Windows.Media.Brushes.Transparent, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));
      PressedTransformProperty = System.Windows.DependencyProperty.Register("PressedTransform", typeof(System.Windows.Media.Transform), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(null, System.Windows.FrameworkPropertyMetadataOptions.AffectsRender));

      var StyleDictionary = new System.Windows.ResourceDictionary();
      StyleDictionary.Source = new Uri("/Inv.Wpf;component/InvWpfStyles.xaml", UriKind.RelativeOrAbsolute);

      DefaultStyle = (System.Windows.Style)StyleDictionary["InvDefaultButton"];
    }

    public WpfButton()
    {
      this.BorderThickness = new System.Windows.Thickness(0);
      this.InnerBorderThickness = new System.Windows.Thickness(0);
      this.HoverBorderThickness = new System.Windows.Thickness(0);
      this.Margin = new System.Windows.Thickness(0);
      this.Padding = new System.Windows.Thickness(0);
      this.Focusable = false;
      this.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;

      this.MouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(RightClickButton_MouseRightButtonDown);
      this.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(RightClickButton_MouseRightButtonUp);

      this.Style = DefaultStyle;
    }

    public static readonly System.Windows.DependencyProperty HoverBackgroundProperty;
    public static readonly System.Windows.DependencyProperty PressedBackgroundProperty;
    public static readonly System.Windows.DependencyProperty CornerRadiusProperty;
    public static readonly System.Windows.DependencyProperty HoverBorderBrushProperty;
    public static readonly System.Windows.DependencyProperty HoverBorderThicknessProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderThicknessProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderPressedBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderHoverBrushProperty;
    public static readonly System.Windows.DependencyProperty InnerBorderFocusBrushProperty;
    public static readonly System.Windows.DependencyProperty PressedTransformProperty;

    public System.Windows.Media.Brush HoverBackgroundBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(HoverBackgroundProperty); }
      set { SetValue(HoverBackgroundProperty, value); }
    }
    public System.Windows.Media.Brush PressedBackgroundBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(PressedBackgroundProperty); }
      set { SetValue(PressedBackgroundProperty, value); }
    }
    public System.Windows.CornerRadius CornerRadius
    {
      get { return (System.Windows.CornerRadius)GetValue(CornerRadiusProperty); }
      set { SetValue(CornerRadiusProperty, value); }
    }
    public System.Windows.Media.Brush HoverBorderBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(HoverBorderBrushProperty); }
      set { SetValue(HoverBorderBrushProperty, value); }
    }
    public System.Windows.Thickness HoverBorderThickness
    {
      get { return (System.Windows.Thickness)GetValue(HoverBorderThicknessProperty); }
      set { SetValue(HoverBorderThicknessProperty, value); }
    }
    public System.Windows.Thickness InnerBorderThickness
    {
      get { return (System.Windows.Thickness)GetValue(InnerBorderThicknessProperty); }
      set { SetValue(InnerBorderThicknessProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderBrushProperty); }
      set { SetValue(InnerBorderBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderPressedBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderPressedBrushProperty); }
      set { SetValue(InnerBorderPressedBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderHoverBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderHoverBrushProperty); }
      set { SetValue(InnerBorderHoverBrushProperty, value); }
    }
    public System.Windows.Media.Brush InnerBorderFocusBrush
    {
      get { return (System.Windows.Media.Brush)GetValue(InnerBorderFocusBrushProperty); }
      set { SetValue(InnerBorderFocusBrushProperty, value); }
    }
    public System.Windows.Media.Transform PressedTransform
    {
      get { return (System.Windows.Media.Transform)GetValue(PressedTransformProperty); }
      set { SetValue(PressedTransformProperty, value); }
    }
    public event System.Windows.RoutedEventHandler LeftClick
    {
      add { Click += value; }
      remove { Click -= value; }
    }
    public event System.Windows.RoutedEventHandler RightClick;

    protected override void OnClick()
    {
      base.OnClick();
    }

    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (this.IsMouseCaptured)
      {
        var isInside = false;

        System.Windows.Media.VisualTreeHelper.HitTest(
            this,
            d =>
            {
              if (d == this)
                isInside = true;

              return System.Windows.Media.HitTestFilterBehavior.Stop;
            },
            ht => System.Windows.Media.HitTestResultBehavior.Stop, new System.Windows.Media.PointHitTestParameters(e.GetPosition(this)));

        this.IsPressed = isInside;
      }
    }

    void RightClickButton_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      this.IsPressed = true;
      CaptureMouse();
      this.IsClicked = true;
    }
    void RightClickButton_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      ReleaseMouseCapture();

      if (this.IsMouseOver && IsClicked)
      {
        if (RightClick != null)
          RightClick.Invoke(this, e);
      }

      this.IsClicked = false;
      this.IsPressed = false;
    }

    private bool IsClicked;
    private static System.Windows.Style DefaultStyle;
  }

  [System.Windows.Data.ValueConversion(typeof(System.Windows.CornerRadius), typeof(System.Windows.CornerRadius))]
  internal sealed class InnerCornerRadiusConverter : System.Windows.Data.IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var InputRadius = (System.Windows.CornerRadius)value;

      return new System.Windows.CornerRadius(Math.Max(0, InputRadius.TopLeft - 1), Math.Max(0, InputRadius.TopRight - 1), Math.Max(0, InputRadius.BottomRight - 1), Math.Max(0, InputRadius.BottomLeft - 1));
    }
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }

  internal sealed class WpfScroll : WpfClipper
  {
    internal WpfScroll(bool IsVertical, bool IsHorizontal)
    {
      this.Inner = new WpfScrollViewer()
      {
        CanContentScroll = false,
        Focusable = false
      };
      Inner.Loaded += (Sender, Event) =>
      {
        if (Inner.VerticalScrollBar != null && Inner.HorizontalScrollBar != null)
        {
          Inner.VerticalScrollBar.Opacity = 0.50;
          Inner.HorizontalScrollBar.Opacity = 0.50;
        }
      };

      if (!IsVertical && IsHorizontal)
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.PanningMode = System.Windows.Controls.PanningMode.HorizontalOnly;
      }
      else if (IsVertical && !IsHorizontal)
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
        Inner.PanningMode = System.Windows.Controls.PanningMode.VerticalOnly;
      }
      else
      {
        Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
        Inner.PanningMode = System.Windows.Controls.PanningMode.Both;
      }

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.FrameworkElement Content
    {
      get { return (System.Windows.FrameworkElement)Inner.Content; }
      set { Inner.Content = value; }
    }

    private sealed class WpfScrollViewer : System.Windows.Controls.ScrollViewer
    {
      public WpfScrollViewer()
      {
        this.PreviewMouseWheel += PreviewMouseWheelHandler;
        this.PanningMode = System.Windows.Controls.PanningMode.Both;
        this.ManipulationBoundaryFeedback += (Sender, Event) =>
        {
          // Prevent the truly nasty default window moving behaviour on reaching the scroll boundary.
          Event.Handled = true;
        };
      }

      static WpfScrollViewer()
      {
        KeyDownEventsArgsList = new HashSet<System.Windows.Input.KeyEventArgs>();
        MouseWheelEventArgsList = new HashSet<System.Windows.Input.MouseWheelEventArgs>();
      }

      public bool MousePanningEnabled { get; set; }

      internal double? ScrollBarSize
      {
        get { return ScrollBarSizeField; }
        set
        {
          if (ScrollBarSizeField != value)
          {
            ScrollBarSizeField = value;

            UpdateScrollBarSize();
          }
        }
      }
      internal System.Windows.Controls.Primitives.ScrollBar VerticalScrollBar { get; private set; }
      internal System.Windows.Controls.Primitives.ScrollBar HorizontalScrollBar { get; private set; }

      public override void OnApplyTemplate()
      {
        base.OnApplyTemplate();

        if (Template != null)
        {
          VerticalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;
          HorizontalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;

          UpdateScrollBarSize();
        }
      }

      protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
      {
        base.OnMouseMove(e);

        if (MousePanningEnabled && IsMouseCaptured)
        {
          var CurrentPosition = e.GetPosition(this);

          var Delta = new System.Windows.Point();
          Delta.X = (CurrentPosition.X > PanStartPosition.X) ? -(CurrentPosition.X - PanStartPosition.X) : (PanStartPosition.X - CurrentPosition.X);
          Delta.Y = (CurrentPosition.Y > PanStartPosition.Y) ? -(CurrentPosition.Y - PanStartPosition.Y) : (PanStartPosition.Y - CurrentPosition.Y);

          ScrollToHorizontalOffset(PanStartOffset.X + Delta.X);
          ScrollToVerticalOffset(PanStartOffset.Y + Delta.Y);
        }
      }
      protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
      {
        base.OnPreviewMouseDown(e);

        if (MousePanningEnabled && IsMouseOver)
        {
          PanStartPosition = e.GetPosition(this);
          PanStartOffset.X = HorizontalOffset;
          PanStartOffset.Y = VerticalOffset;

          Cursor = System.Windows.Input.Cursors.ScrollAll;
          CaptureMouse();
        }
      }
      protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
      {
        base.OnPreviewMouseUp(e);

        if (MousePanningEnabled && IsMouseCaptured)
        {
          Cursor = System.Windows.Input.Cursors.Arrow;
          ReleaseMouseCapture();
        }
      }

      private void UpdateScrollBarSize()
      {
        if (ScrollBarSizeField.HasValue)
        {
          var Value = ScrollBarSizeField.Value;

          if (VerticalScrollBar != null)
            VerticalScrollBar.Width = Value;

          if (HorizontalScrollBar != null)
            HorizontalScrollBar.Height = Value;
        }
      }

      private System.Windows.Point PanStartPosition;
      private System.Windows.Point PanStartOffset;
      private double? ScrollBarSizeField;

      private static void PreviewMouseWheelHandler(object Sender, System.Windows.Input.MouseWheelEventArgs e)
      {
        var Control = Sender as System.Windows.Controls.ScrollViewer;

        Debug.Assert(Control != null, "Sender is not assigned or is not a ScrollViewer");

        if (!e.Handled && !MouseWheelEventArgsList.Contains(e))
        {
          var PreviewEventArg = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
          {
            RoutedEvent = System.Windows.UIElement.PreviewMouseWheelEvent,
            Source = Sender
          };

          var OriginalSource = e.OriginalSource as System.Windows.UIElement;

          if (OriginalSource != null)
          {
            MouseWheelEventArgsList.Add(PreviewEventArg);
            OriginalSource.RaiseEvent(PreviewEventArg);
            MouseWheelEventArgsList.Remove(PreviewEventArg);
          }

          e.Handled = PreviewEventArg.Handled;

          if (!e.Handled)
          {
            var Delta = e.Delta;

            if ((Delta > 0 && Control.VerticalOffset == 0) || (Delta <= 0 && Control.VerticalOffset >= Control.ExtentHeight - Control.ViewportHeight))
            {
              var LogicalParent = (System.Windows.UIElement)((System.Windows.FrameworkElement)Sender).Parent;
              if (LogicalParent != null)
              {
                var EventArgs = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, Delta)
                {
                  RoutedEvent = System.Windows.UIElement.MouseWheelEvent,
                  Source = Sender
                };

                LogicalParent.RaiseEvent(EventArgs);

                e.Handled = EventArgs.Handled;
              }
            }
          }
        }
      }

      private static readonly HashSet<System.Windows.Input.KeyEventArgs> KeyDownEventsArgsList;
      private static readonly HashSet<System.Windows.Input.MouseWheelEventArgs> MouseWheelEventArgsList;
    }

    private WpfScrollViewer Inner;
  }

  internal sealed class WpfRender : WpfClipper
  {
    internal WpfRender()
    {
      this.VisualList = new List<System.Windows.Media.Visual>();

      this.ClipToBounds = true; // prevents drawing outside of the control.

      this.CacheMode = new System.Windows.Media.BitmapCache();

      this.DrawingVisual = AddVisual();
    }

    public static readonly System.Windows.RoutedEvent MouseDoubleClickEvent = System.Windows.Controls.Control.MouseDoubleClickEvent.AddOwner(typeof(WpfRender));

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public event System.Windows.Input.MouseButtonEventHandler MouseDoubleClick
    {
      add { base.AddHandler(WpfRender.MouseDoubleClickEvent, value); }
      remove { base.RemoveHandler(WpfRender.MouseDoubleClickEvent, value); }
    }
    public System.Windows.Media.DrawingVisual DrawingVisual { get; private set; }

    protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (e.ClickCount == 2)
      {
        RaiseEvent(new System.Windows.Input.MouseButtonEventArgs(System.Windows.Input.Mouse.PrimaryDevice, 0, e.ChangedButton)
        {
          RoutedEvent = WpfRender.MouseDoubleClickEvent,
          Source = this,
        });

        e.Handled = true;
      }
      else
      {
        base.OnMouseDown(e);
      }
    }
    protected override int VisualChildrenCount
    {
      get { return VisualList.Count; }
    }
    protected override System.Windows.Media.Visual GetVisualChild(int index)
    {
      return VisualList[index];
    }

    public System.Windows.Media.DrawingVisual AddVisual()
    {
      var Result = new System.Windows.Media.DrawingVisual();
      VisualList.Add(Result);

      AddVisualChild(Result);

      return Result;
    }
    public void RemoveVisual(System.Windows.Media.Visual Visual)
    {
      VisualList.Remove(Visual);

      RemoveVisualChild(Visual);
    }

    private List<System.Windows.Media.Visual> VisualList;
  }

  internal sealed class WpfWindow : System.Windows.Window
  {
  }

  public abstract class WpfClipper : System.Windows.Controls.Border
  {
    protected override void OnRender(DrawingContext dc)
    {
      OnApplyChildClip();
      base.OnRender(dc);
    }

    public override System.Windows.UIElement Child
    {
      get { return base.Child; }
      set
      {
        if (this.Child != value)
        {
          if (this.Child != null)
            this.Child.SetValue(System.Windows.UIElement.ClipProperty, OldClip); // Restore original clipping

          if (value != null)
            OldClip = value.ReadLocalValue(System.Windows.UIElement.ClipProperty);
          else
            OldClip = null; // If we dont set it to null we could leak a Geometry object

          base.Child = value;
        }
      }
    }

    protected virtual void OnApplyChildClip()
    {
      var ApplyChild = this.Child;

      if (ApplyChild != null)
      {
        if (this.CornerRadius.TopLeft != 0)
        {
          var Radius = Math.Max(0.0, this.CornerRadius.TopLeft - (this.BorderThickness.Left * 0.5));

          ClipRect.RadiusX = Radius;
          ClipRect.RadiusY = Radius;
          ClipRect.Rect = new System.Windows.Rect(Child.RenderSize);

          ApplyChild.Clip = ClipRect;
        }
        else
        {
          ApplyChild.Clip = null;
        }
      }
    }

    private RectangleGeometry ClipRect = new RectangleGeometry();
    private object OldClip;
  }

  internal sealed class WpfCanvasElement : System.Windows.Controls.Border
  {
  }

  internal sealed class WpfFrame : WpfClipper
  {
  }

  internal sealed class WpfStack : WpfClipper
  {
    public WpfStack()
    {
      this.Inner = new System.Windows.Controls.StackPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get { return Inner.Orientation; }
      set { Inner.Orientation = value; }
    }
    public System.Windows.Controls.UIElementCollection Children
    {
      get { return Inner.Children; }
    }

    private System.Windows.Controls.StackPanel Inner;
  }

  internal sealed class WpfGrid : WpfClipper
  {
    public WpfGrid()
    {
      this.Inner = new System.Windows.Controls.Grid();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Grid Inner { get; private set; }

    public System.Windows.Controls.ColumnDefinitionCollection ColumnDefinitions
    {
      get { return Inner.ColumnDefinitions; }
    }
    public System.Windows.Controls.RowDefinitionCollection RowDefinitions
    {
      get { return Inner.RowDefinitions; }
    }
    public System.Windows.Controls.UIElementCollection Children
    {
      get { return Inner.Children; }
    }
  }

  internal sealed class WpfCanvas : WpfClipper
  {
    public WpfCanvas()
    {
      this.Inner = new System.Windows.Controls.Canvas();
      base.Child = Inner;

      this.UseLayoutRounding = true;
      this.SnapsToDevicePixels = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.UIElementCollection Children
    {
      get { return Inner.Children; }
    }

    private System.Windows.Controls.Canvas Inner;
  }

  internal sealed class WpfGraphic : WpfClipper
  {
    public WpfGraphic()
    {
      this.Inner = new System.Windows.Controls.Image()
      {
        Stretch = System.Windows.Media.Stretch.Uniform
      };
      RenderOptions.SetBitmapScalingMode(Inner, BitmapScalingMode.Fant);
      
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Media.ImageSource Source
    {
      get { return Inner.Source; }
      set { Inner.Source = value; }
    }
    public System.Windows.Media.Stretch Stretch
    {
      get { return Inner.Stretch; }
      set { Inner.Stretch = value; }
    }

    private System.Windows.Controls.Image Inner;
  }

  internal sealed class WpfLabel : WpfClipper
  {
    public WpfLabel()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis
      };
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get { return Inner.TextAlignment; }
      set { Inner.TextAlignment = value; }
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get { return Inner.TextWrapping; }
      set { Inner.TextWrapping = value; }
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get { return Inner.TextTrimming; }
      set { Inner.TextTrimming = value; }
    }
    public System.Windows.Media.Brush Foreground
    {
      get { return Inner.Foreground; }
      set { Inner.Foreground = value; }
    }
    public double FontSize
    {
      get { return Inner.FontSize; }
      set { Inner.FontSize = value; }
    }
    public string Text
    {
      get { return Inner.Text; }
      set { Inner.Text = value; }
    }

    public System.Windows.Controls.TextBlock GetTextBlock()
    {
      return Inner;
    }

    private System.Windows.Controls.TextBlock Inner;
  }

  internal sealed class WpfEdit : WpfClipper, WpfOverrideFocusContract
  {
    public WpfEdit()
    {
      this.Inner = new System.Windows.Controls.TextBox()
      {
        IsReadOnlyCaretVisible = true,
        AcceptsReturn = false,
        AcceptsTab = false,
        TextWrapping = System.Windows.TextWrapping.Wrap,
        Background = System.Windows.Media.Brushes.Transparent,
        BorderBrush = System.Windows.Media.Brushes.Transparent,
        BorderThickness = new System.Windows.Thickness(0),
      };
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get { return Inner.IsReadOnly; }
      set { Inner.IsReadOnly = value; }
    }
    public string Text
    {
      get { return Inner.Text; }
      set { Inner.Text = value; }
    }
    public event System.Windows.Controls.TextChangedEventHandler TextChanged
    {
      add { Inner.TextChanged += value; }
      remove { Inner.TextChanged -= value; }
    }

    public System.Windows.Controls.TextBox GetTextBox()
    {
      return Inner;
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      Inner.Focus();
    }

    private System.Windows.Controls.TextBox Inner;
  }

  internal sealed class WpfMemo : WpfClipper, WpfOverrideFocusContract
  {
    public WpfMemo()
    {
      this.Inner = new System.Windows.Controls.TextBox()
      {
        AcceptsReturn = true,
        AcceptsTab = true,
        TextWrapping = System.Windows.TextWrapping.Wrap,
        IsReadOnlyCaretVisible = true,
        VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
        HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
        Background = System.Windows.Media.Brushes.Transparent,
        BorderBrush = System.Windows.Media.Brushes.Transparent,
        BorderThickness = new System.Windows.Thickness(0),
      };
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get { return Inner.IsReadOnly; }
      set { Inner.IsReadOnly = value; }
    }
    public string Text
    {
      get { return Inner.Text; }
      set { Inner.Text = value; }
    }
    public event System.Windows.Controls.TextChangedEventHandler TextChanged
    {
      add { Inner.TextChanged += value; }
      remove { Inner.TextChanged -= value; }
    }

    public System.Windows.Controls.TextBox GetTextBox()
    {
      return Inner;
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      Inner.Focus();
    }

    private System.Windows.Controls.TextBox Inner;
  }

  public sealed class SystemMutex : IDisposable
  {
    public SystemMutex(string Name)
    {
      var MutexId = string.Format("Global\\{0}", Name);

      this.Handle = new Mutex(false, MutexId);
    }
    public void Dispose()
    {
      if (Handle != null)
      {
        try
        {
          Unlock();
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);

          if (Debugger.IsAttached)
            Debugger.Break();
        }

        Handle.Dispose();
        Handle = null;
      }
    }

    public bool Lock(TimeSpan Timeout)
    {
      if (!IsAcquired)
      {
        try
        {
          this.IsAcquired = Handle.WaitOne(Timeout);
        }
        catch (AbandonedMutexException)
        {
          this.IsAcquired = true;
        }
      }

      return IsAcquired;
    }
    public void Unlock()
    {
      if (IsAcquired)
      {
        Handle.ReleaseMutex();
        IsAcquired = false;
      }
    }

    private System.Threading.Mutex Handle;
    private bool IsAcquired;
  }
}
