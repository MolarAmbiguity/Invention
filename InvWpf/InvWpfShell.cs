﻿/*! 108 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class WpfDeviceEmulation
  {
    private WpfDeviceEmulation(string Name, int Width, int Height)
    {
      this.Name = Name;
      this.Width = Width;
      this.Height = Height;
    }

    public string Name { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public static readonly WpfDeviceEmulation iPhone5 = new WpfDeviceEmulation("Apple iPhone 5", 320, 568);
    public static readonly WpfDeviceEmulation iPhone6 = new WpfDeviceEmulation("Apple iPhone 6", 375, 667);
    public static readonly WpfDeviceEmulation iPhone6Plus = new WpfDeviceEmulation("Apple iPhone 6+", 414, 736);
    public static readonly WpfDeviceEmulation iPad2 = new WpfDeviceEmulation("Apple iPad 2", 1024, 768);
    public static readonly WpfDeviceEmulation Nexus5 = new WpfDeviceEmulation("Google Nexus 5", 360, 592);
    public static readonly WpfDeviceEmulation Nexus7 = new WpfDeviceEmulation("Google Nexus 7", 888, 600);
    public static readonly WpfDeviceEmulation Nexus9 = new WpfDeviceEmulation("Google Nexus 9", 1024, 768);
    public static readonly WpfDeviceEmulation GalaxyNote4 = new WpfDeviceEmulation("Samsung Galaxy Note 4", 450, 800); // 5.7"
    public static readonly WpfDeviceEmulation GalaxyS10 = new WpfDeviceEmulation("Samsung Galaxy Tab S 10\"", 1280, 800);
    public static readonly WpfDeviceEmulation Surface3 = new WpfDeviceEmulation("Microsoft Surface 3", 1600, 1064); // TODO: is this right?
    public static readonly WpfDeviceEmulation VaioDuo11 = new WpfDeviceEmulation("Sony Vaio Duo 11", 1920, 1080); // TODO: is this right?

    public static readonly Inv.DistinctList<WpfDeviceEmulation> List;

    static WpfDeviceEmulation()
    {
      List = new DistinctList<WpfDeviceEmulation>();

      var TypeInfo = typeof(WpfDeviceEmulation).GetReflectionInfo();

      foreach (var ReflectionField in TypeInfo.GetReflectionFields().Where(F => F.IsStatic && F.FieldType == typeof(WpfDeviceEmulation)))
        List.Add((WpfDeviceEmulation)ReflectionField.GetValue(null));
    }
  }

  public static class WpfShell
  {
    static WpfShell()
    {
      RespectPhysicalDimensions = true;
      FullScreenMode = true;
      DefaultWindowWidth = 800;
      DefaultWindowHeight = 600;
      DefaultFontName = "Segoe UI";

      const double OneCmInInches = 0.393700787;

      var DesktopHandle = System.Drawing.Graphics.FromHwnd(IntPtr.Zero).GetHdc();

      var PhysicalWidthMms = GetDeviceCaps(DesktopHandle, (int)DeviceCap.HORZSIZE);
      var PhysicalHeightMms = GetDeviceCaps(DesktopHandle, (int)DeviceCap.VERTSIZE);

      PhysicalWidthInches = PhysicalWidthMms * OneCmInInches / 10;
      PhysicalHeightInches = PhysicalHeightMms * OneCmInInches / 10;

      MainFolderPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
    }

    public static readonly double PhysicalWidthInches;
    public static readonly double PhysicalHeightInches;
    public static bool FullScreenMode { get; set; }
    public static int DefaultWindowWidth { get; set; }
    public static int DefaultWindowHeight { get; set; }
    public static string DefaultFontName { get; set; }
    public static bool RespectPhysicalDimensions { get; set; }
    public static WpfDeviceEmulation DeviceEmulation { get; set; }
    public static bool DeviceRotation { get; set; }
    public static string MainFolderPath { get; set; }

    public static void CheckRequirements(Action Action)
    {
      if (!IsNet45OrNewer())
      {
        System.Windows.MessageBox.Show("Please install the Microsoft .NET 4.5 runtime.", "");
        return;
      }

      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        System.Windows.MessageBox.Show(Exception.AsReport());
      }
    }
    public static void Run(Inv.Application InvApplication)
    {
      var Engine = new WpfEngine(InvApplication);
      Engine.Run();
    }
    public static void Host(Inv.Application InvApplication, System.Windows.Controls.ContentControl WpfContainer)
    {
      Debug.Assert(InvApplication != null);
      Debug.Assert(WpfContainer != null);

      var Host = new WpfHost(InvApplication, WpfContainer);

      var WpfWindow = WpfContainer as System.Windows.Window;
      if (WpfWindow != null)
        WpfWindow.Closed += (Sender, Event) => InvApplication.Exit();

      Host.ShutdownEvent += () => Host.Stop();
      Host.Start();
    }

    [System.Runtime.InteropServices.DllImport("gdi32.dll", ExactSpelling = true)]
    private static extern int GetDeviceCaps(IntPtr hDC, int nIndex);

    internal static void CopyAnimatedValuesToLocalValues(this System.Windows.DependencyObject obj)
    {
      if (obj is System.Windows.Media.Imaging.BitmapImage)
        return;

      // Recurse down tree
      for (var i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(obj); i++)
        CopyAnimatedValuesToLocalValues(System.Windows.Media.VisualTreeHelper.GetChild(obj, i));

      var enumerator = obj.GetLocalValueEnumerator();
      while (enumerator.MoveNext())
      {
        var prop = enumerator.Current.Property;
        var value = enumerator.Current.Value as System.Windows.Freezable;

        // Recurse into eg. brushes that may be set by storyboard, as long as they aren't frozen
        if (value != null && !value.IsFrozen)
          CopyAnimatedValuesToLocalValues(value);

        // *** This is the key bit of code ***
        if (System.Windows.DependencyPropertyHelper.GetValueSource(obj, prop).IsAnimated)
          obj.SetValue(prop, obj.GetValue(prop));
      }
    }

    private static bool IsNet45OrNewer()
    {
      // Class "ReflectionContext" exists from .NET 4.5 onwards.
      return Type.GetType("System.Reflection.ReflectionContext", false) != null;
    }
  }

  internal enum DeviceCap
  {
    #region http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
    /// <summary>
    /// Device driver version
    /// </summary>
    DRIVERVERSION = 0,
    /// <summary>
    /// Device classification
    /// </summary>
    TECHNOLOGY = 2,
    /// <summary>
    /// Horizontal size in millimeters
    /// </summary>
    HORZSIZE = 4,
    /// <summary>
    /// Vertical size in millimeters
    /// </summary>
    VERTSIZE = 6,
    /// <summary>
    /// Horizontal width in pixels
    /// </summary>
    HORZRES = 8,
    /// <summary>
    /// Vertical height in pixels
    /// </summary>
    VERTRES = 10,
    /// <summary>
    /// Number of bits per pixel
    /// </summary>
    BITSPIXEL = 12,
    /// <summary>
    /// Number of planes
    /// </summary>
    PLANES = 14,
    /// <summary>
    /// Number of brushes the device has
    /// </summary>
    NUMBRUSHES = 16,
    /// <summary>
    /// Number of pens the device has
    /// </summary>
    NUMPENS = 18,
    /// <summary>
    /// Number of markers the device has
    /// </summary>
    NUMMARKERS = 20,
    /// <summary>
    /// Number of fonts the device has
    /// </summary>
    NUMFONTS = 22,
    /// <summary>
    /// Number of colors the device supports
    /// </summary>
    NUMCOLORS = 24,
    /// <summary>
    /// Size required for device descriptor
    /// </summary>
    PDEVICESIZE = 26,
    /// <summary>
    /// Curve capabilities
    /// </summary>
    CURVECAPS = 28,
    /// <summary>
    /// Line capabilities
    /// </summary>
    LINECAPS = 30,
    /// <summary>
    /// Polygonal capabilities
    /// </summary>
    POLYGONALCAPS = 32,
    /// <summary>
    /// Text capabilities
    /// </summary>
    TEXTCAPS = 34,
    /// <summary>
    /// Clipping capabilities
    /// </summary>
    CLIPCAPS = 36,
    /// <summary>
    /// Bitblt capabilities
    /// </summary>
    RASTERCAPS = 38,
    /// <summary>
    /// Length of the X leg
    /// </summary>
    ASPECTX = 40,
    /// <summary>
    /// Length of the Y leg
    /// </summary>
    ASPECTY = 42,
    /// <summary>
    /// Length of the hypotenuse
    /// </summary>
    ASPECTXY = 44,
    /// <summary>
    /// Shading and Blending caps
    /// </summary>
    SHADEBLENDCAPS = 45,

    /// <summary>
    /// Logical pixels inch in X
    /// </summary>
    LOGPIXELSX = 88,
    /// <summary>
    /// Logical pixels inch in Y
    /// </summary>
    LOGPIXELSY = 90,

    /// <summary>
    /// Number of entries in physical palette
    /// </summary>
    SIZEPALETTE = 104,
    /// <summary>
    /// Number of reserved entries in palette
    /// </summary>
    NUMRESERVED = 106,
    /// <summary>
    /// Actual color resolution
    /// </summary>
    COLORRES = 108,

    // Printing related DeviceCaps. These replace the appropriate Escapes
    /// <summary>
    /// Physical Width in device units
    /// </summary>
    PHYSICALWIDTH = 110,
    /// <summary>
    /// Physical Height in device units
    /// </summary>
    PHYSICALHEIGHT = 111,
    /// <summary>
    /// Physical Printable Area x margin
    /// </summary>
    PHYSICALOFFSETX = 112,
    /// <summary>
    /// Physical Printable Area y margin
    /// </summary>
    PHYSICALOFFSETY = 113,
    /// <summary>
    /// Scaling factor x
    /// </summary>
    SCALINGFACTORX = 114,
    /// <summary>
    /// Scaling factor y
    /// </summary>
    SCALINGFACTORY = 115,

    /// <summary>
    /// Current vertical refresh rate of the display device (for displays only) in Hz
    /// </summary>
    VREFRESH = 116,
    /// <summary>
    /// Vertical height of entire desktop in pixels
    /// </summary>
    DESKTOPVERTRES = 117,
    /// <summary>
    /// Horizontal width of entire desktop in pixels
    /// </summary>
    DESKTOPHORZRES = 118,
    /// <summary>
    /// Preferred blt alignment
    /// </summary>
    BLTALIGNMENT = 119

    #endregion
  }

  internal sealed class WpfPlatform : Inv.Platform
  {
    public WpfPlatform(WpfHost Host)
    {
      this.Host = Host;
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
    }

    int Platform.ThreadAffinity()
    {
      return Thread.CurrentThread.ManagedThreadId;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      // TODO: wpf date/time picker.
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Mapi = new Inv.Mapi();

      foreach (var To in EmailMessage.GetTos())
        Mapi.AddRecipient(To.Name, To.Address, false);

      foreach (var Attachment in EmailMessage.GetAttachments())
        Mapi.Attach(SelectFilePath(Attachment.File), Attachment.Name);

      var Result = Mapi.Send(EmailMessage.Subject, EmailMessage.Body);

      // NOTE: seems to mess up the full screen mode slightly in the bottom right edge.
      //if (Result)
      //  WpfWindow.WindowState = System.Windows.WindowState.Minimized;

      return Result;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    bool Platform.PhoneDial(string PhoneNumber)
    {
      return false;
    }
    bool Platform.PhoneSMS(string PhoneNumber)
    {
      return false;
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new FileStream(Path.Combine(Inv.WpfShell.MainFolderPath, "Assets", Asset.Name), FileMode.Open, FileAccess.Read);
    }
    bool Platform.LocationIsSupported
    {
#if DEBUG
      get { return true; }
#else
      get { return false; }
#endif
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
#if DEBUG
      if (LocationLookup.Coordinate.Latitude == -31.953 && LocationLookup.Coordinate.Longitude == 115.814)
        LocationLookup.SetPlacemarks(new Placemark[] { new Inv.Placemark
        (
          Name: "8 Sadlier St",
          Locality: "Subiaco",
          SubLocality: "Perth",
          PostalCode: "6008",
          AdministrativeArea: "WA",
          SubAdministrativeArea: null,
          CountryName: "Australia",
          CountryCode : "AU",
          Latitude: -31.953,
          Longitude: 115.814
        )});
      else
#endif
        // TODO: implement using Bing web services?
        LocationLookup.SetPlacemarks(new Placemark[] { });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume)
    {
      Host.SoundPlayer.Play(Sound, Volume);
    }
    void Platform.WindowAsynchronise(Action Action)
    {
      Host.Asynchronise(Action);
    }
    Modifier Platform.WindowGetModifier()
    {
      return Host.GetModifier();
    }
    long Platform.ProcessMemoryBytes()
    {
      CurrentProcess.Refresh();
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.WebSocketConnect(WebSocket Socket)
    {
      var SocketClient = new Inv.Tcp.Client(Socket.Host, Socket.Port);
      SocketClient.Connect();
      
      Socket.Node = SocketClient;
      Socket.SetStreams(SocketClient.InputStream, SocketClient.OutputStream);
    }
    void Platform.WebSocketDisconnect(WebSocket Socket)
    {
      var SocketClient = (Inv.Tcp.Client)Socket.Node;
      if (SocketClient != null)
      {
        Socket.Node = null;
        Socket.SetStreams(null, null);

        SocketClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Process.Start(Uri.AbsoluteUri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }

    private string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(WpfShell.MainFolderPath, Folder.Name);
      else
        Result = WpfShell.MainFolderPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private WpfHost Host;
    private Process CurrentProcess;
  }

  internal sealed class WpfEngine
  {
    internal WpfEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      this.WpfApplication = new System.Windows.Application()
      {
        ShutdownMode = System.Windows.ShutdownMode.OnMainWindowClose
      };

      // TODO: this is not re-entrant.
      WpfApplication.DispatcherUnhandledException += (Sender, Event) =>
      {
        var Exception = Event.Exception;

        InvApplication.HandleExceptionInvoke(Event.Exception);

        Event.Handled = true;
      };

      // TODO: this is not re-entrant.
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as Exception;

        if (Exception == null && Event.ExceptionObject != null)
          System.Windows.MessageBox.Show(Event.ExceptionObject.GetType().AssemblyQualifiedName + " - " + Event.ExceptionObject.ToString());
        else
          InvApplication.HandleExceptionInvoke(Exception);
      };

      // TODO: this is not re-entrant.
      TaskScheduler.UnobservedTaskException += (Sender, Event) =>
      {
        InvApplication.HandleExceptionInvoke(Event.Exception);

        Event.SetObserved();
      };

      // NOTE: the first window becomes the WpfApplication.MainWindow.
      this.WpfWindow = new WpfWindow()
      {
        Owner = null,
        UseLayoutRounding = true,
        SnapsToDevicePixels = true,
        Topmost = false,
        IsTabStop = false,
        //TaskbarItemInfo = new System.Windows.Shell.TaskbarItemInfo(),
        Width = WpfShell.DefaultWindowWidth,
        Height = WpfShell.DefaultWindowHeight,
        FontFamily = new System.Windows.Media.FontFamily(WpfShell.DefaultFontName),
        //SizeToContent = System.Windows.SizeToContent.Manual,
        WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen
      };
      WpfWindow.Closing += (Sender, Event) =>
      {
        Event.Cancel = !InvApplication.ExitQueryInvoke();
      };
      WpfWindow.PreviewKeyDown += (Sender, Event) =>
      {
        var IsAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt) || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt);

        if (IsAlt && Event.Key == System.Windows.Input.Key.System && Event.SystemKey == System.Windows.Input.Key.Return)
        {
          WpfShell.FullScreenMode = !WpfShell.FullScreenMode;

          if (WpfShell.FullScreenMode)
            EnterFullScreen();
          else
            ExitFullScreen();

          WpfHost.Rearrange();

          Event.Handled = true;
        }
      };
      if (Debugger.IsAttached && WpfWindow.Icon == null)
      {
        using (var FileIcon = System.Drawing.Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location))
        {
          var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(FileIcon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
          Result.Freeze();

          WpfWindow.Icon = Result;
        }
      }

      if (WpfShell.FullScreenMode)
        EnterFullScreen();

      this.WpfHost = new WpfHost(InvApplication, WpfWindow);
      WpfHost.ShutdownEvent += () => WpfApplication.Shutdown();
    }

    public void Run()
    {
      using (var SingletonMutex = InvApplication.Title != null ? new SystemMutex(InvApplication.Title) : null)
      {
        var SingletonBound = SingletonMutex == null;

        if (!SingletonBound)
        {
          var SingletonAttempt = 0;
          while (SingletonAttempt < 10)
          {
            SingletonBound = SingletonMutex.Lock(TimeSpan.FromMilliseconds(500));

            if (SingletonBound)
            {
              break; // We control the mutex, can continue as the singleton process.
            }
            else
            {
              SingletonAttempt++;
            }
          }
        }

        if (SingletonBound)
        {
          WpfHost.Start();
          try
          {
            var InvWindow = InvApplication.Window;

            WpfWindow.Title = InvApplication.Title ?? "";

            if (!WpfWindow.IsVisible)
              WpfWindow.Show();

            WpfApplication.Run();
          }
          finally
          {
            WpfHost.Stop();
          }
        }
      }
    }

    internal Inv.Application InvApplication { get; private set; }
    internal System.Windows.Application WpfApplication { get; private set; }
    
    internal void EnterFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.None;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.NoResize;

      this.WpfWindowChrome = System.Windows.Shell.WindowChrome.GetWindowChrome(WpfWindow);
      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, null);

      WpfWindow.WindowState = System.Windows.WindowState.Maximized;
    }
    internal void ExitFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.CanResize;

      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, WpfWindowChrome);

      WpfWindow.WindowState = System.Windows.WindowState.Normal;
    }

    private WpfWindow WpfWindow;
    private WpfHost WpfHost;
    private System.Windows.Shell.WindowChrome WpfWindowChrome;
  }

  internal sealed class WpfHost
  {
    public WpfHost(Inv.Application InvApplication, System.Windows.Controls.ContentControl WpfContainer)
    {
      this.InvApplication = InvApplication;
      this.WpfContainer = WpfContainer;

      this.WpfBorder = new System.Windows.Controls.Border();
      WpfContainer.Content = WpfBorder;
      WpfBorder.Background = System.Windows.Media.Brushes.DimGray;

      this.WpfMaster = new WpfGrid();
      WpfMaster.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      WpfMaster.VerticalAlignment = System.Windows.VerticalAlignment.Center;
      WpfMaster.ClipToBounds = true;
      System.Windows.NameScope.SetNameScope(WpfMaster, new System.Windows.NameScope());

#if DEBUG
      var WpfEmulationGrid = new WpfGrid();
      WpfBorder.Child = WpfEmulationGrid;
      WpfEmulationGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      WpfEmulationGrid.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;

      this.WpfEmulationDeviceLabel = new WpfLabel();
      WpfEmulationGrid.Children.Add(WpfEmulationDeviceLabel);
      WpfEmulationDeviceLabel.Foreground = System.Windows.Media.Brushes.White;
      WpfEmulationDeviceLabel.Padding = new System.Windows.Thickness(10);
      WpfEmulationDeviceLabel.FontSize = 30;
      WpfEmulationDeviceLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
      WpfEmulationDeviceLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;

      this.WpfEmulationFrameLabel = new WpfLabel();
      WpfEmulationGrid.Children.Add(WpfEmulationFrameLabel);
      WpfEmulationFrameLabel.Foreground = System.Windows.Media.Brushes.White;
      WpfEmulationFrameLabel.Padding = new System.Windows.Thickness(10);
      WpfEmulationFrameLabel.FontSize = 20;
      WpfEmulationFrameLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
      WpfEmulationFrameLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

      this.WpfEmulationInstructionLabel = new WpfLabel();
      WpfEmulationGrid.Children.Add(WpfEmulationInstructionLabel);
      WpfEmulationInstructionLabel.Foreground = System.Windows.Media.Brushes.White;
      WpfEmulationInstructionLabel.Padding = new System.Windows.Thickness(10);
      WpfEmulationInstructionLabel.FontSize = 20;
      WpfEmulationInstructionLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
      WpfEmulationInstructionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

      WpfEmulationGrid.Children.Add(WpfMaster);
#else
      WpfBorder.Child = WpfMaster;
#endif

      var SizeChangedOnceOnly = false;

      var LeftEdgeSwipe = false;
      var RightEdgeSwipe = false;

      WpfMaster.PreviewMouseDown += (Sender, Event) =>
      {
        var MousePoint = Event.GetPosition(WpfMaster);

        LeftEdgeSwipe = MousePoint.X <= 10;
        RightEdgeSwipe = MousePoint.X >= WpfMaster.ActualWidth - 10;
      };
      WpfMaster.PreviewMouseUp += (Sender, Event) =>
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;
      };
      WpfMaster.PreviewMouseMove += (Sender, Event) =>
      {
        if (LeftEdgeSwipe || RightEdgeSwipe)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (InvSurface != null)
          {
            var MousePoint = Event.GetPosition(WpfMaster);

            if (LeftEdgeSwipe && MousePoint.X >= 0)
            {
              if (MousePoint.X >= 20)
              {
                InvSurface.GestureBackwardInvoke();
                LeftEdgeSwipe = false;
              }
            }
            else if (RightEdgeSwipe && MousePoint.X <= WpfMaster.ActualWidth)
            {
              if (MousePoint.X <= WpfMaster.ActualWidth - 20)
              {
                InvSurface.GestureForwardInvoke();
                RightEdgeSwipe = false;
              }
            }
            else
            {
              LeftEdgeSwipe = false;
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }

          Event.Handled = true;
        }
      };
      WpfContainer.SizeChanged += (Sender, Event) =>
      {
        if (!SizeChangedOnceOnly)
        {
          SizeChangedOnceOnly = true;

          WpfContainer.Dispatcher.BeginInvoke((Action)delegate
          {
            SizeChangedOnceOnly = false;

            Rearrange();
          }, System.Windows.Threading.DispatcherPriority.Render);
        }
      };
      WpfContainer.PreviewKeyDown += (Sender, Event) =>
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          var Modifier = GetModifier();

#if DEBUG
          if (Modifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Left || Event.Key == System.Windows.Input.Key.Right))
          {
            WpfShell.DeviceRotation = !WpfShell.DeviceRotation;

            var RotateLogical = InvApplication.Window.Height;
            InvApplication.Window.Height = InvApplication.Window.Width;
            InvApplication.Window.Width = RotateLogical;

            var RotatePhysical = WpfMaster.Height;
            WpfMaster.Height = WpfMaster.Width;
            WpfMaster.Width = RotatePhysical;

            InvSurface.ArrangeInvoke();

            return;
          }
          else if (Modifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Up || Event.Key == System.Windows.Input.Key.Down))
          {
            var EmulationIndex = WpfShell.DeviceEmulation != null ? WpfDeviceEmulation.List.IndexOf(WpfShell.DeviceEmulation) : -1;

            if (Event.Key == System.Windows.Input.Key.Down)
              EmulationIndex++;
            else
              EmulationIndex--;

            if (EmulationIndex >= WpfDeviceEmulation.List.Count)
              EmulationIndex = 0;
            else if (EmulationIndex < 0)
              EmulationIndex = WpfDeviceEmulation.List.Count - 1;

            WpfShell.DeviceEmulation = WpfDeviceEmulation.List[EmulationIndex];
            Resize();

            InvSurface.ArrangeInvoke();

            return;
          }
          else if (Modifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Clear))
          {
            WpfShell.DeviceEmulation = null;
            Resize();

            InvSurface.ArrangeInvoke();

            return;
          }
          else if (Modifier.IsCtrl && (Event.Key == System.Windows.Input.Key.F8))
          {
            var SnapshotFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), string.Format("{0}_inv-snapshot.txt", InvApplication.Title));

            System.IO.File.WriteAllText(SnapshotFilePath, InvSurface.GetPanelDisplay());

            System.Diagnostics.Process.Start(SnapshotFilePath);

            return;
          }
#endif
          if (WpfMaster.Children.Count != 1)
          {
            // ignore keystrokes while transitioning.
            Event.Handled = true;
          }
          else
          {
            var WpfKey = Event.Key == System.Windows.Input.Key.System ? Event.SystemKey : Event.Key;

            var InvKey = TranslateKey(WpfKey);
            if (InvKey != null)
            {
              var Keystroke = new Keystroke()
              {
                Key = InvKey.Value,
                Modifier = Modifier
              };
              InvSurface.KeystrokeInvoke(Keystroke);
            }
          }
        }
      };
      WpfMaster.PreviewMouseDown += (Sender, Event) =>
      {
        if (Event.ButtonState == System.Windows.Input.MouseButtonState.Pressed)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton1 && InvSurface != null)
          {
            InvSurface.GestureBackwardInvoke();

            Event.Handled = true;
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton2 && InvSurface != null)
          {
            InvSurface.GestureForwardInvoke();

            Event.Handled = true;
          }
        }
      };

      Microsoft.Win32.SystemEvents.DisplaySettingsChanged += (Sender, Event) =>
      {
        InvApplication.Platform.WindowAsynchronise(() =>
        {
          Resize();

          var InvSurface = InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
            InvSurface.ArrangeInvoke();
        });
      };

      this.SoundPlayer = new SoundPlayer();

      this.ColourBrushDictionary = new Dictionary<Inv.Colour, System.Windows.Media.Brush>();
      this.PenDictionary = new Dictionary<PenKey, System.Windows.Media.Pen>();
      this.ImageDictionary = new Dictionary<Inv.Image, System.Windows.Media.Imaging.BitmapSource>();
      this.LookupPenKey = new PenKey();
      this.MirrorTransformArray = new Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform>()
      {
        { Mirror.Horizontal, new System.Windows.Media.ScaleTransform(-1, +1) },
        { Mirror.Vertical, new System.Windows.Media.ScaleTransform(+1, +1) },
      };

      #region Key mapping.
      this.KeyDictionary = new Dictionary<System.Windows.Input.Key, Key>()
      {
        { System.Windows.Input.Key.D0, Key.n0 },
        { System.Windows.Input.Key.D1, Key.n1 },
        { System.Windows.Input.Key.D2, Key.n2 },
        { System.Windows.Input.Key.D3, Key.n3 },
        { System.Windows.Input.Key.D4, Key.n4 },
        { System.Windows.Input.Key.D5, Key.n5 },
        { System.Windows.Input.Key.D6, Key.n6 },
        { System.Windows.Input.Key.D7, Key.n7 },
        { System.Windows.Input.Key.D8, Key.n8 },
        { System.Windows.Input.Key.D9, Key.n9 },
        { System.Windows.Input.Key.A, Key.A },
        { System.Windows.Input.Key.B, Key.B },
        { System.Windows.Input.Key.C, Key.C },
        { System.Windows.Input.Key.D, Key.D },
        { System.Windows.Input.Key.E, Key.E },
        { System.Windows.Input.Key.F, Key.F },
        { System.Windows.Input.Key.G, Key.G },
        { System.Windows.Input.Key.H, Key.H },
        { System.Windows.Input.Key.I, Key.I },
        { System.Windows.Input.Key.J, Key.J },
        { System.Windows.Input.Key.K, Key.K },
        { System.Windows.Input.Key.L, Key.L },
        { System.Windows.Input.Key.M, Key.M },
        { System.Windows.Input.Key.N, Key.N },
        { System.Windows.Input.Key.O, Key.O },
        { System.Windows.Input.Key.P, Key.P },
        { System.Windows.Input.Key.Q, Key.Q },
        { System.Windows.Input.Key.R, Key.R },
        { System.Windows.Input.Key.S, Key.S },
        { System.Windows.Input.Key.T, Key.T },
        { System.Windows.Input.Key.U, Key.U },
        { System.Windows.Input.Key.V, Key.V },
        { System.Windows.Input.Key.W, Key.W },
        { System.Windows.Input.Key.X, Key.X },
        { System.Windows.Input.Key.Y, Key.Y },
        { System.Windows.Input.Key.Z, Key.Z },
        { System.Windows.Input.Key.F1, Key.F1 },
        { System.Windows.Input.Key.F2, Key.F2 },
        { System.Windows.Input.Key.F3, Key.F3 },
        { System.Windows.Input.Key.F4, Key.F4 },
        { System.Windows.Input.Key.F5, Key.F5 },
        { System.Windows.Input.Key.F6, Key.F6 },
        { System.Windows.Input.Key.F7, Key.F7 },
        { System.Windows.Input.Key.F8, Key.F8 },
        { System.Windows.Input.Key.F9, Key.F9 },
        { System.Windows.Input.Key.F10, Key.F10 },
        { System.Windows.Input.Key.F11, Key.F11 },
        { System.Windows.Input.Key.F12, Key.F12 },
        { System.Windows.Input.Key.Escape, Key.Escape },
        { System.Windows.Input.Key.Enter, Key.Enter },
        { System.Windows.Input.Key.Tab, Key.Tab },
        { System.Windows.Input.Key.Space, Key.Space },
        { System.Windows.Input.Key.OemPeriod, Key.Period },
        { System.Windows.Input.Key.OemComma, Key.Comma },
        { System.Windows.Input.Key.OemTilde, Key.Tilde },
        //{ System.Windows.Input.Key.OemTilde, Key.BackQuote }, // TODO: plus shift.
        { System.Windows.Input.Key.OemPlus, Key.Plus },
        { System.Windows.Input.Key.OemMinus, Key.Minus },
        { System.Windows.Input.Key.Up, Key.Up },
        { System.Windows.Input.Key.Down, Key.Down },
        { System.Windows.Input.Key.Left, Key.Left },
        { System.Windows.Input.Key.Right, Key.Right },
        { System.Windows.Input.Key.Home, Key.Home },
        { System.Windows.Input.Key.End, Key.End },
        { System.Windows.Input.Key.PageUp, Key.PageUp },
        { System.Windows.Input.Key.PageDown, Key.PageDown },
        { System.Windows.Input.Key.Clear, Key.Clear },
        { System.Windows.Input.Key.Insert, Key.Insert },
        { System.Windows.Input.Key.Delete, Key.Delete },
        { System.Windows.Input.Key.Oem2, Key.Slash },
        { System.Windows.Input.Key.Oem5, Key.Backslash },
        { System.Windows.Input.Key.Add, Key.Plus },
        { System.Windows.Input.Key.Subtract, Key.Minus },
        { System.Windows.Input.Key.Divide, Key.Slash },
        { System.Windows.Input.Key.Multiply, Key.Asterisk },
        { System.Windows.Input.Key.OemBackslash, Key.Backslash },
      };
      #endregion

      this.RouteDictionary = new Dictionary<Type, Func<Panel, System.Windows.FrameworkElement>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Render), TranslateRender },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        { typeof(Inv.Table), TranslateTable },
      };

      InvApplication.Platform = new WpfPlatform(this);
      InvApplication.Begin();
    }

    internal event Action ShutdownEvent;
    internal SoundPlayer SoundPlayer { get; private set; }

    internal Modifier GetModifier()
    {
      return new Modifier()
      {
        IsLeftShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift),
        IsRightShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift),
        IsLeftAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt),
        IsRightAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt),
        IsLeftCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl),
        IsRightCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl)
      };
    }
    internal void Start()
    {
      if (SoundPlayer.SoundException != null)
      {
        System.Windows.MessageBox.Show(
          "There was a problem initialising the sound engine which may be caused by a bad sound card driver. Please check your sound card driver." + Environment.NewLine + Environment.NewLine +
          "'" + SoundPlayer.SoundException.Message + "'" + Environment.NewLine + Environment.NewLine +
          "This application will run with all sound effects disabled.");
      }

      InvApplication.Device.Name = Environment.MachineName;
      InvApplication.Device.Model = "";

      using (var mc = new System.Management.ManagementClass("Win32_ComputerSystem"))
      {
        var moc = mc.GetInstances();
        if (moc.Count != 0)
        {
          foreach (var mo in mc.GetInstances())
            InvApplication.Device.Model += mo["Manufacturer"].ToString() + " " + mo["Model"].ToString();
        }
      }

      InvApplication.Device.System = WpfSystem.Name + " " + WpfSystem.Edition + " " + WpfSystem.Version;

      Resize();

      InvApplication.StartInvoke();

      if (InvApplication.Location.IsRequired)
      {
#if DEBUG
        InvApplication.Location.ChangeInvoke(new Inv.Coordinate(-31.953, 115.814, 0.0));
#endif
      }

      Process();

      System.Windows.Media.CompositionTarget.Rendering += Rendering;
    }
    internal void Stop()
    {
      System.Windows.Media.CompositionTarget.Rendering -= Rendering;

      InvApplication.StopInvoke();
    }
    internal void Rearrange()
    {
      var LastLogicalScreenWidthPoints = InvApplication.Window.Width;
      var LastLogicalScreenHeightPoints = InvApplication.Window.Height;

      Resize();

      if (LastLogicalScreenWidthPoints != InvApplication.Window.Width || LastLogicalScreenHeightPoints != InvApplication.Window.Height)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      }
    }
    internal void Asynchronise(Action Action)
    {
      WpfContainer.Dispatcher.BeginInvoke(Action);
    }

    private void Resize()
    {
      var OwnerWindow = System.Windows.Window.GetWindow(WpfContainer);
      var FormsScreen = System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(OwnerWindow).Handle);

      var ScreenWidth = FormsScreen.Bounds.Width; // (int)System.Windows.SystemParameters.PrimaryScreenWidth
      var ScreenHeight = FormsScreen.Bounds.Height; // (int)System.Windows.SystemParameters.PrimaryScreenHeight

      var ResolutionWidthPixels = WpfShell.FullScreenMode ? ScreenWidth : (int)WpfContainer.ActualWidth;
      if (ResolutionWidthPixels <= 0)
        ResolutionWidthPixels = (int)WpfContainer.Width;

      var ResolutionHeightPixels = WpfShell.FullScreenMode ? ScreenHeight : (int)WpfContainer.ActualHeight;
      if (ResolutionHeightPixels <= 0)
        ResolutionHeightPixels = (int)WpfContainer.Height;

      if (!WpfShell.FullScreenMode && WpfContainer is System.Windows.Window)
      {
        var HorizontalBorderHeight = (int)System.Windows.SystemParameters.ResizeFrameHorizontalBorderHeight;
        var VerticalBorderWidth = (int)System.Windows.SystemParameters.ResizeFrameVerticalBorderWidth;
        var CaptionHeight = (int)System.Windows.SystemParameters.CaptionHeight;
      
        ResolutionWidthPixels -= (VerticalBorderWidth * 2) + 8;
        ResolutionHeightPixels -= (HorizontalBorderHeight * 2) + CaptionHeight + 9;
      }

      // NOTE: fallback to resolution pixels, based on 24" monitor.

      var LogicalScreenWidthPoints = WpfShell.RespectPhysicalDimensions ? (int)(WpfShell.PhysicalWidthInches * 160.0) : 0;
      if (LogicalScreenWidthPoints <= 0)
        LogicalScreenWidthPoints = ResolutionWidthPixels;

      var LogicalScreenHeightPoints = WpfShell.RespectPhysicalDimensions ? (int)(WpfShell.PhysicalHeightInches * 160.0) : 0;
      if (LogicalScreenHeightPoints <= 0)
        LogicalScreenHeightPoints = ResolutionHeightPixels;

      //this.ConvertHorizontalPtToPxFactor = (float)ResolutionWidthPixels / (float)LogicalScreenWidthPoints;
      //if (ConvertHorizontalPtToPxFactor < 1.0F)
      //{
      //  this.ConvertHorizontalPtToPxFactor = 1.0F;
      //  LogicalScreenWidthPoints = ResolutionWidthPixels;
      //}

      //this.ConvertVerticalPtToPxFactor = (float)ResolutionHeightPixels / (float)LogicalScreenHeightPoints;
      //if (ConvertVerticalPtToPxFactor < 1.0F)
      //{
      //  this.ConvertVerticalPtToPxFactor = 1.0F;
      //  LogicalScreenHeightPoints = ResolutionHeightPixels;
      //}

#if DEBUG
      WpfEmulationDeviceLabel.Text = WpfShell.DeviceEmulation != null ? WpfShell.DeviceEmulation.Name + " Emulation" : null;
      WpfEmulationInstructionLabel.Text = WpfShell.DeviceEmulation != null ? "Ctrl + Up/Down to change the device\nCtrl + Left/Right to rotate this device" : null;

      if (WpfShell.DeviceEmulation != null)
      {
        if (WpfShell.DeviceRotation)
        {
          LogicalScreenWidthPoints = WpfShell.DeviceEmulation.Height;
          LogicalScreenHeightPoints = WpfShell.DeviceEmulation.Width;
        }
        else
        {
          LogicalScreenWidthPoints = WpfShell.DeviceEmulation.Width;
          LogicalScreenHeightPoints = WpfShell.DeviceEmulation.Height;
        }

        ResolutionWidthPixels = VerticalPtToPx(LogicalScreenWidthPoints);
        ResolutionHeightPixels = HorizontalPtToPx(LogicalScreenHeightPoints);
      }
#endif

      InvApplication.Window.Width = LogicalScreenWidthPoints;
      InvApplication.Window.Height = LogicalScreenHeightPoints;

      WpfMaster.Width = ResolutionWidthPixels;
      WpfMaster.Height = ResolutionHeightPixels;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        if (ShutdownEvent != null)
          ShutdownEvent();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var WpfTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new System.Windows.Threading.DispatcherTimer();
              Result.Tick += (Sender, Event) => InvTimer.IntervalInvoke();
              return Result;
            });

            if (WpfTimer.Interval != InvTimer.IntervalTime)
              WpfTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !WpfTimer.IsEnabled)
              WpfTimer.Start();
            else if (!InvTimer.IsEnabled && WpfTimer.IsEnabled)
              WpfTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var WpfSurface = AccessSurface(InvSurfaceActive, S =>
          {
            var Result = new WpfSurface();
            Result.Name = "Surface" + SurfaceCount++;
            WpfMaster.RegisterName(Result.Name, Result);
            return Result;
          });

          if (!WpfMaster.Children.Contains(WpfSurface))
            InvSurfaceActive.ArrangeInvoke();

          ProcessTransition(WpfSurface);

          InvSurfaceActive.ComposeInvoke();

          if (InvSurfaceActive.Render())
          {
            WpfSurface.Background = TranslateBrush(InvSurfaceActive.Background.Colour);
            WpfSurface.Child = TranslatePanel(InvSurfaceActive.Content);
          }

          InvSurfaceActive.ProcessChanges(P => TranslatePanel(P));

          if (InvSurfaceActive.Focus != null)
          {
            var WpfFocus = InvSurfaceActive.Focus.Node as System.Windows.FrameworkElement;
            var WpfOverrideFocus = WpfFocus as WpfOverrideFocusContract;

            InvSurfaceActive.Focus = null;

            if (WpfOverrideFocus != null)
              WpfMaster.Dispatcher.BeginInvoke((Action)delegate { WpfOverrideFocus.OverrideFocus(); }, System.Windows.Threading.DispatcherPriority.Input);
            else if (WpfFocus != null)
              WpfMaster.Dispatcher.BeginInvoke((Action)delegate { WpfFocus.Focus(); }, System.Windows.Threading.DispatcherPriority.Input);
          }
        }
        else
        {
          WpfMaster.Children.Clear();
        }

        if (InvSurfaceActive != null)
          ProcessAnimation(InvSurfaceActive);

        if (InvWindow.Render())
        {
          WpfMaster.Background = TranslateBrush(InvWindow.Background.Colour ?? Inv.Colour.Black);
          TranslateFont(InvWindow.DefaultFont, WpfContainer, WpfShell.DefaultFontName);
        }

        InvWindow.DisplayRate.Calculate();

#if DEBUG
        WpfEmulationFrameLabel.Text = WpfShell.DeviceEmulation != null ? string.Format("{0} x {1} | {2} fps | {3} pc | {4:F1} MB",
          InvWindow.Width, InvWindow.Height, InvWindow.DisplayRate.PerSecond, InvWindow.ActiveSurface != null ? InvWindow.ActiveSurface.GetPanelCount() : 0, InvApplication.GetProcessMemoryMB()) : null;
#endif
      }
    }
    private void ProcessTransition(WpfSurface WpfSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(WpfMaster.Children.Contains(WpfSurface));
      }
      else if (WpfMaster.Children.Count <= 1) // don't transition while animating a transition.
      {
        var WpfFromSurface = WpfMaster.Children.Count == 0 ? null : (WpfSurface)WpfMaster.Children[0];
        var WpfToSurface = WpfSurface;

        if (WpfFromSurface == WpfToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              WpfMaster.Children.Clear();
              WpfMaster.Children.Add(WpfToSurface);
              break;

            case TransitionAnimation.Fade:
              WpfMaster.Children.Add(WpfToSurface);

              WpfToSurface.IsHitTestVisible = false;
              WpfToSurface.Opacity = 0.0;

              var WpfFadeDuration = new System.Windows.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2));

              var WpfFadeInAnimation = new System.Windows.Media.Animation.DoubleAnimation()
              {
                From = 0.0,
                To = 1.0,
                Duration = WpfFadeDuration,
                FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
              };
              WpfFadeInAnimation.Completed += (Sender, Event) =>
              {
                WpfToSurface.IsHitTestVisible = true;
                WpfToSurface.Opacity = 1.0;
              };
              WpfFadeInAnimation.Freeze();

              if (WpfFromSurface == null)
              {
                WpfToSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
              }
              else
              {
                WpfFromSurface.IsHitTestVisible = false;

                var WpfFadeOutAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  From = 1.0,
                  To = 0.0,
                  Duration = WpfFadeDuration,
                  FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
                };
                WpfFadeOutAnimation.Completed += (Sender, Event) =>
                {
                  WpfFromSurface.IsHitTestVisible = true;
                  WpfMaster.Children.Remove(WpfFromSurface);

                  WpfToSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
                };
                WpfFadeOutAnimation.Freeze();

                WpfFromSurface.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeOutAnimation);
              }
              break;

            case TransitionAnimation.CarouselBack:
            case TransitionAnimation.CarouselNext:
              var CarouselForward = InvTransition.Animation == TransitionAnimation.CarouselNext;

              var WpfCarouselDuration = new System.Windows.Duration(InvTransition.Duration);

              var WpfCarouselStoryboard = new System.Windows.Media.Animation.Storyboard();

              if (WpfFromSurface != null)
              {
                var FromCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
                WpfFromSurface.RenderTransform = FromCarouselTransform;

                WpfFromSurface.IsHitTestVisible = false;

                var WpfFromAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  AccelerationRatio = 0.5,
                  DecelerationRatio = 0.5,
                  Duration = WpfCarouselDuration,
                  From = 0,
                  To = CarouselForward ? -WpfMaster.ActualWidth : WpfMaster.ActualWidth
                };
                System.Windows.Media.Animation.Storyboard.SetTarget(WpfFromAnimation, FromCarouselTransform);
                System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfFromAnimation, new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.XProperty));
                WpfFromAnimation.Freeze();

                WpfCarouselStoryboard.Children.Add(WpfFromAnimation);
              }

              if (WpfToSurface != null)
              {
                var ToCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
                WpfToSurface.RenderTransform = ToCarouselTransform;

                WpfMaster.Children.Add(WpfToSurface);

                WpfToSurface.IsHitTestVisible = false;
                var WpfToAnimation = new System.Windows.Media.Animation.DoubleAnimation()
                {
                  AccelerationRatio = 0.5,
                  DecelerationRatio = 0.5,
                  Duration = WpfCarouselDuration,
                  From = CarouselForward ? WpfMaster.ActualWidth : -WpfMaster.ActualWidth,
                  To = 0
                };
                System.Windows.Media.Animation.Storyboard.SetTarget(WpfToAnimation, ToCarouselTransform);
                System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfToAnimation, new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.XProperty));
                WpfToAnimation.Freeze();

                WpfCarouselStoryboard.Children.Add(WpfToAnimation);
              }

              WpfCarouselStoryboard.Completed += (Sender, Event) =>
              {
                if (WpfFromSurface != null)
                {
                  WpfFromSurface.IsHitTestVisible = true;
                  WpfMaster.Children.Remove(WpfFromSurface);
                  WpfFromSurface.RenderTransform = null;
                }

                if (WpfToSurface != null)
                {
                  WpfToSurface.IsHitTestVisible = true;
                  WpfToSurface.RenderTransform = null;
                }

                WpfCarouselStoryboard.Remove(WpfMaster);
              };
              WpfCarouselStoryboard.Freeze();

              WpfCarouselStoryboard.Begin(WpfMaster, null);
              break;

            default:
              throw new ApplicationException("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var WpfStoryboard = (System.Windows.Media.Animation.Storyboard)InvAnimation.Node;

            foreach (var InvTarget in InvAnimation.GetTargets())
            {
              var WpfPanel = TranslatePanel(InvTarget.Panel);
              if (WpfPanel != null)
              {
                //if (System.Windows.DependencyPropertyHelper.GetValueSource(WpfPanel, System.Windows.UIElement.OpacityProperty).IsAnimated)
                //  WpfPanel.SetValue(System.Windows.UIElement.OpacityProperty, WpfPanel.GetValue(System.Windows.UIElement.OpacityProperty));

                WpfPanel.Opacity = WpfPanel.Opacity;
                InvTarget.Panel.Opacity.BypassSet((float)WpfPanel.Opacity);
              }
            }

            WpfStoryboard.Stop();
            //WpfStoryboard.Remove(); // Remove stuffs things up.
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var WpfStoryboard = new System.Windows.Media.Animation.Storyboard();
          InvAnimation.Node = WpfStoryboard;
          WpfStoryboard.Completed += (Sender, Event) =>
          {
            InvAnimation.Complete();
            InvAnimation.Node = null;
          };

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var WpfPanel = TranslatePanel(InvTarget.Panel);
            foreach (var InvCommand in InvTarget.GetCommands())
              WpfStoryboard.Children.Add(TranslateAnimationCommand(InvTarget.Panel, WpfPanel, InvCommand));
          }

          WpfStoryboard.Freeze();
          WpfStoryboard.Begin();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }
    private System.Windows.Media.Animation.Timeline TranslateAnimationCommand(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      // TODO: more types of animation commands.
      var Result = TranslateAnimationOpacityCommand(InvPanel, WpfPanel, InvCommand);
      Result.Completed += (Sender, Event) => InvCommand.Complete();
      Result.Freeze();
      return Result;
    }
    private System.Windows.Media.Animation.Timeline TranslateAnimationOpacityCommand(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

      var Result = new System.Windows.Media.Animation.DoubleAnimation()
      {
        From = InvOpacityCommand.From,
        To = InvOpacityCommand.To,
        Duration = InvOpacityCommand.Duration,
        FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvOpacityCommand.Offset != null)
        Result.BeginTime = InvOpacityCommand.Offset;

      System.Windows.Media.Animation.Storyboard.SetTarget(Result, WpfPanel);
      System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(System.Windows.UIElement.OpacityProperty));

      WpfPanel.Opacity = InvOpacityCommand.From;
      InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

      Result.RemoveRequested += (Sender, Event) =>
      {
        Debug.WriteLine("Test");
      };
      Result.Completed += (Sender, Event) =>
      {
        WpfPanel.Opacity = InvPanel.Opacity.Get();
      };

      return Result;
    }

    private System.Windows.FrameworkElement TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var WpfButton = AccessPanel(InvButton, P =>
      {
        var Result = new WpfButton();
        Result.LeftClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.SingleTapInvoke();

          Event.Handled = true;
        };
        Result.RightClick += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.ContextTapInvoke();

          Event.Handled = true;
        };
        return Result;
      });

      RenderPanel(InvButton, WpfButton, () =>
      {
        TranslateVisibility(InvButton.Visibility, WpfButton);
        TranslateMargin(InvButton.Margin, WpfButton);
        TranslatePadding(InvButton.Padding, WpfButton);
        TranslateOpacity(InvButton.Opacity, WpfButton);
        TranslateAlignment(InvButton.Alignment, WpfButton);
        TranslateSize(InvButton.Size, WpfButton);
        TranslateElevation(InvButton.Elevation, WpfButton);

        if (InvButton.Border.IsChanged)
        {
          TranslateBorder(InvButton.Border, WpfButton);
          WpfButton.HoverBorderThickness = WpfButton.BorderThickness;
          WpfButton.HoverBorderBrush = WpfButton.BorderBrush;
        }

        if (InvButton.CornerRadius.Render())
          WpfButton.CornerRadius = TranslateCornerRadius(InvButton.CornerRadius);

        if (InvButton.Background.Render())
        {
          WpfButton.Background = TranslateBrush(InvButton.Background.Colour);
          WpfButton.HoverBackgroundBrush = TranslateHoverBrush(InvButton.Background.Colour);
          WpfButton.PressedBackgroundBrush = TranslatePressBrush(InvButton.Background.Colour);
        }

        WpfButton.IsEnabled = InvButton.IsEnabled;
        WpfButton.Focusable = InvButton.IsFocusable;

        if (InvButton.ContentSingleton.Render())
        {
          WpfButton.Content = null; // detach previous content in case it has moved.
          WpfButton.Content = TranslatePanel(InvButton.ContentSingleton.Data);
        }
      });

      return WpfButton;
    }
    private System.Windows.FrameworkElement TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var WpfCanvas = AccessPanel(InvCanvas, P =>
      {
        return new WpfCanvas();
      });

      RenderPanel(InvCanvas, WpfCanvas, () =>
      {
        if (InvCanvas.Background.Render())
          WpfCanvas.Background = TranslateBrush(InvCanvas.Background.Colour);

        if (InvCanvas.CornerRadius.Render())
          WpfCanvas.CornerRadius = TranslateCornerRadius(InvCanvas.CornerRadius);

        TranslateMargin(InvCanvas.Margin, WpfCanvas);
        TranslatePadding(InvCanvas.Padding, WpfCanvas);
        TranslateBorder(InvCanvas.Border, WpfCanvas);
        TranslateOpacity(InvCanvas.Opacity, WpfCanvas);
        TranslateAlignment(InvCanvas.Alignment, WpfCanvas);
        TranslateVisibility(InvCanvas.Visibility, WpfCanvas);
        TranslateSize(InvCanvas.Size, WpfCanvas);
        TranslateElevation(InvCanvas.Elevation, WpfCanvas);

        if (InvCanvas.PieceCollection.Render())
        {
          WpfCanvas.Children.Clear();
          foreach (var InvElement in InvCanvas.PieceCollection)
          {
            var WpfElement = new WpfCanvasElement();
            WpfCanvas.Children.Add(WpfElement);
            System.Windows.Controls.Canvas.SetLeft(WpfElement, HorizontalPtToPx(InvElement.Rect.Left));
            System.Windows.Controls.Canvas.SetTop(WpfElement, VerticalPtToPx(InvElement.Rect.Top));
            WpfElement.Width = InvElement.Rect.Width;
            WpfElement.Height = InvElement.Rect.Height;

            var WpfPanel = TranslatePanel(InvElement.Panel);
            WpfElement.Child = WpfPanel;
          }
        }
      });

      return WpfCanvas;
    }
    private System.Windows.FrameworkElement TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var WpfDock = AccessPanel(InvDock, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvDock, WpfDock, () =>
      {
        if (InvDock.Background.Render())
          WpfDock.Background = TranslateBrush(InvDock.Background.Colour);

        if (InvDock.CornerRadius.Render())
          WpfDock.CornerRadius = TranslateCornerRadius(InvDock.CornerRadius);

        TranslateMargin(InvDock.Margin, WpfDock);
        TranslatePadding(InvDock.Padding, WpfDock);
        TranslateOpacity(InvDock.Opacity, WpfDock);
        TranslateAlignment(InvDock.Alignment, WpfDock);
        TranslateVisibility(InvDock.Visibility, WpfDock);
        TranslateSize(InvDock.Size, WpfDock);
        TranslateBorder(InvDock.Border, WpfDock);
        TranslateElevation(InvDock.Elevation, WpfDock);

        if (InvDock.CollectionRender())
        {
          WpfDock.Children.Clear();
          WpfDock.RowDefinitions.Clear();
          WpfDock.ColumnDefinitions.Clear();

          var Position = 0;

          foreach (var InvHeader in InvDock.HeaderCollection)
          {
            var WpfPanel = TranslatePanel(InvHeader);
            WpfDock.Children.Add(WpfPanel);

            if (IsHorizontal)
            {
              WpfDock.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = System.Windows.GridLength.Auto });
              System.Windows.Controls.Grid.SetColumn(WpfPanel, Position);
            }
            else
            {
              WpfDock.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = System.Windows.GridLength.Auto });
              System.Windows.Controls.Grid.SetRow(WpfPanel, Position);
            }

            Position++;
          }

          if (InvDock.HasClients())
          {
            foreach (var InvClient in InvDock.ClientCollection)
            {
              var WpfPanel = TranslatePanel(InvClient);
              WpfDock.Children.Add(WpfPanel);

              if (IsHorizontal)
              {
                WpfDock.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
                System.Windows.Controls.Grid.SetColumn(WpfPanel, Position);
              }
              else
              {
                WpfDock.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
                System.Windows.Controls.Grid.SetRow(WpfPanel, Position);
              }

              Position++;
            }
          }
          else
          {
            if (IsHorizontal)
              WpfDock.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });
            else
              WpfDock.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = new System.Windows.GridLength(1.0, System.Windows.GridUnitType.Star) });

            Position++;
          }

          foreach (var InvFooter in InvDock.FooterCollection)
          {
            var WpfPanel = TranslatePanel(InvFooter);
            WpfDock.Children.Add(WpfPanel);

            if (IsHorizontal)
            {
              WpfDock.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = System.Windows.GridLength.Auto });
              System.Windows.Controls.Grid.SetColumn(WpfPanel, Position);
            }
            else
            {
              WpfDock.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = System.Windows.GridLength.Auto });
              System.Windows.Controls.Grid.SetRow(WpfPanel, Position);
            }

            Position++;
          }
        }
      });

      return WpfDock;
    }
    private System.Windows.FrameworkElement TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var WpfEdit = AccessPanel(InvEdit, P =>
      {
        var Result = new WpfEdit();
        Result.TextChanged += (Sender, Event) => P.ChangeInvoke(Result.Text);
        return Result;
      });

      RenderPanel(InvEdit, WpfEdit, () =>
      {
        if (InvEdit.CornerRadius.Render())
          WpfEdit.CornerRadius = TranslateCornerRadius(InvEdit.CornerRadius);

        if (InvEdit.Background.Render())
          WpfEdit.Background = TranslateBrush(InvEdit.Background.Colour);

        TranslateBorder(InvEdit.Border, WpfEdit);
        TranslateMargin(InvEdit.Margin, WpfEdit);
        TranslatePadding(InvEdit.Padding, WpfEdit);
        TranslateOpacity(InvEdit.Opacity, WpfEdit);
        TranslateAlignment(InvEdit.Alignment, WpfEdit);
        TranslateVisibility(InvEdit.Visibility, WpfEdit);
        TranslateSize(InvEdit.Size, WpfEdit);
        TranslateElevation(InvEdit.Elevation, WpfEdit);
        TranslateFont(InvEdit.Font, WpfEdit.GetTextBox());

        WpfEdit.IsReadOnly = InvEdit.IsReadOnly;
        WpfEdit.Text = InvEdit.Text;
      });

      return WpfEdit;
    }
    private System.Windows.FrameworkElement TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var WpfGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new WpfGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, WpfGraphic, () =>
      {
        if (InvGraphic.Background.Render())
          WpfGraphic.Background = TranslateBrush(InvGraphic.Background.Colour);

        if (InvGraphic.CornerRadius.Render())
          WpfGraphic.CornerRadius = TranslateCornerRadius(InvGraphic.CornerRadius);

        var SizeChanged = InvGraphic.Size.IsChanged;

        TranslateMargin(InvGraphic.Margin, WpfGraphic);
        TranslatePadding(InvGraphic.Padding, WpfGraphic);
        TranslateBorder(InvGraphic.Border, WpfGraphic);
        TranslateOpacity(InvGraphic.Opacity, WpfGraphic);
        TranslateAlignment(InvGraphic.Alignment, WpfGraphic);
        TranslateVisibility(InvGraphic.Visibility, WpfGraphic);
        TranslateSize(InvGraphic.Size, WpfGraphic);
        TranslateElevation(InvGraphic.Elevation, WpfGraphic);

        if (InvGraphic.ImageSingleton.Render() || SizeChanged)
          WpfGraphic.Source = TranslateImage(InvGraphic.Image);
      });

      return WpfGraphic;
    }
    private System.Windows.FrameworkElement TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var WpfLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new WpfLabel();
        return Result;
      });

      RenderPanel(InvLabel, WpfLabel, () =>
      {
        if (InvLabel.Background.Render())
          WpfLabel.Background = TranslateBrush(InvLabel.Background.Colour);

        if (InvLabel.CornerRadius.Render())
          WpfLabel.CornerRadius = TranslateCornerRadius(InvLabel.CornerRadius);

        TranslateBorder(InvLabel.Border, WpfLabel);
        TranslateMargin(InvLabel.Margin, WpfLabel);
        TranslatePadding(InvLabel.Padding, WpfLabel);
        TranslateOpacity(InvLabel.Opacity, WpfLabel);
        TranslateAlignment(InvLabel.Alignment, WpfLabel);
        TranslateVisibility(InvLabel.Visibility, WpfLabel);
        TranslateSize(InvLabel.Size, WpfLabel);
        TranslateElevation(InvLabel.Elevation, WpfLabel);
        TranslateFont(InvLabel.Font, WpfLabel.GetTextBlock());

        WpfLabel.TextWrapping = InvLabel.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
        WpfLabel.TextAlignment = InvLabel.Justification == null || InvLabel.Justification == Justification.Left ? System.Windows.TextAlignment.Left : InvLabel.Justification == Justification.Right ? System.Windows.TextAlignment.Right : System.Windows.TextAlignment.Center;
        WpfLabel.Text = InvLabel.Text;
      });

      return WpfLabel;
    }
    private System.Windows.FrameworkElement TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var WpfMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new WpfMemo();
        Result.TextChanged += (Sender, Event) => P.ChangeInvoke(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, WpfMemo, () =>
      {
        if (InvMemo.CornerRadius.Render())
          WpfMemo.CornerRadius = TranslateCornerRadius(InvMemo.CornerRadius);

        if (InvMemo.Background.Render())
          WpfMemo.Background = TranslateBrush(InvMemo.Background.Colour);

        TranslateOpacity(InvMemo.Opacity, WpfMemo);
        TranslateAlignment(InvMemo.Alignment, WpfMemo);
        TranslateVisibility(InvMemo.Visibility, WpfMemo);
        TranslateSize(InvMemo.Size, WpfMemo);
        TranslateBorder(InvMemo.Border, WpfMemo);
        TranslateMargin(InvMemo.Margin, WpfMemo);
        TranslatePadding(InvMemo.Padding, WpfMemo);
        TranslateElevation(InvMemo.Elevation, WpfMemo);
        TranslateFont(InvMemo.Font, WpfMemo.GetTextBox());

        WpfMemo.IsReadOnly = InvMemo.IsReadOnly;
        WpfMemo.Text = InvMemo.Text;
      });

      return WpfMemo;
    }
    private System.Windows.FrameworkElement TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var WpfOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvOverlay, WpfOverlay, () =>
      {
        if (InvOverlay.Background.Render())
          WpfOverlay.Background = TranslateBrush(InvOverlay.Background.Colour);

        if (InvOverlay.CornerRadius.Render())
          WpfOverlay.CornerRadius = TranslateCornerRadius(InvOverlay.CornerRadius);

        TranslateMargin(InvOverlay.Margin, WpfOverlay);
        TranslatePadding(InvOverlay.Padding, WpfOverlay);
        TranslateBorder(InvOverlay.Border, WpfOverlay);
        TranslateOpacity(InvOverlay.Opacity, WpfOverlay);
        TranslateAlignment(InvOverlay.Alignment, WpfOverlay);
        TranslateVisibility(InvOverlay.Visibility, WpfOverlay);
        TranslateSize(InvOverlay.Size, WpfOverlay);
        TranslateElevation(InvOverlay.Elevation, WpfOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          WpfOverlay.Children.Clear();
          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var WpfPanel = TranslatePanel(InvElement);
            WpfOverlay.Children.Add(WpfPanel);
          }
        }
      });

      return WpfOverlay;
    }
    private System.Windows.FrameworkElement TranslateRender(Inv.Panel InvPanel)
    {
      var InvRender = (Inv.Render)InvPanel;

      var WpfRender = AccessPanel(InvRender, P =>
      {
        var Result = new WpfRender();

        var IsLeftPressed = false;
        var IsRightPressed = false;
        var MouseDownTimestamp = 0;

        Result.SizeChanged += (Sender, Event) =>
        {
          InvRender.ContextSingleton.Data.Width = HorizontalPxToPt((int)Result.ActualWidth);
          InvRender.ContextSingleton.Data.Height = VerticalPxToPt((int)Result.ActualHeight);
        };
        Result.MouseMove += (Sender, Event) =>
        {
          var MovePoint = TranslatePoint(Event.GetPosition(Result));
          P.MoveInvoke(MovePoint);
        };
        Result.MouseDown += (Sender, Event) =>
        {
          System.Windows.Input.Mouse.Capture(Result);

          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left && Event.ClickCount < 2)
          {
            IsLeftPressed = true;
            MouseDownTimestamp = Event.Timestamp;

            var PressPoint = TranslatePoint(Event.GetPosition(Result));
            P.PressInvoke(PressPoint);
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right && Event.ClickCount < 2)
          {
            IsRightPressed = true;
          }
        };
        Result.MouseUp += (Sender, Event) =>
        {
          Result.ReleaseMouseCapture();

          var ReleasePoint = TranslatePoint(Event.GetPosition(Result));

          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left && Event.ClickCount < 2)
          {
            if (IsLeftPressed)
            {
              IsLeftPressed = false;
              P.ReleaseInvoke(ReleasePoint);

              if (Event.Timestamp - MouseDownTimestamp <= 250)
                P.SingleTapInvoke(ReleasePoint);
            }
          }
          else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right && Event.ClickCount < 2)
          {
            if (IsRightPressed)
            {
              IsRightPressed = false;
              P.ContextTapInvoke(ReleasePoint);
            }
          }
        };
        Result.MouseDoubleClick += (Sender, Event) =>
        {
          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left)
            P.DoubleTapInvoke(TranslatePoint(Event.GetPosition(Result)));
        };
        Result.MouseWheel += (Sender, Event) =>
        {
          P.ZoomInvoke(Event.Delta > 0 ? +1 : -1);
        };

        return Result;
      });

      RenderPanel(InvRender, WpfRender, () =>
      {
        if (InvRender.Background.Render())
          WpfRender.Background = TranslateBrush(InvRender.Background.Colour);

        if (InvRender.CornerRadius.Render())
          WpfRender.CornerRadius = TranslateCornerRadius(InvRender.CornerRadius);

        TranslateMargin(InvRender.Margin, WpfRender);
        TranslatePadding(InvRender.Padding, WpfRender);
        TranslateBorder(InvRender.Border, WpfRender);
        TranslateOpacity(InvRender.Opacity, WpfRender);
        TranslateAlignment(InvRender.Alignment, WpfRender);
        TranslateVisibility(InvRender.Visibility, WpfRender);
        TranslateSize(InvRender.Size, WpfRender);
        TranslateElevation(InvRender.Elevation, WpfRender);

        if (InvRender.ContextSingleton.Render())
        {
          var WpfRectangleRect = new System.Windows.Rect();
          var WpfImageRect = new System.Windows.Rect();

          using (var WpfContext = WpfRender.DrawingVisual.RenderOpen())
          {
            foreach (var InvRenderElement in InvRender.GetCommands())
            {
              switch (InvRenderElement.Type)
              {
                case RenderType.Rectangle:
                  TranslateRect(InvRenderElement.RectangleRect, ref WpfRectangleRect);

                  var StrokePixels = VerticalPtToPx(InvRenderElement.RectangleStrokeThickness);

                  if (StrokePixels > 0)
                  {
                    WpfRectangleRect.X += StrokePixels / 2;
                    WpfRectangleRect.Y += StrokePixels / 2;

                    if (WpfRectangleRect.Width >= StrokePixels)
                      WpfRectangleRect.Width -= StrokePixels;

                    if (WpfRectangleRect.Height >= StrokePixels)
                      WpfRectangleRect.Height -= StrokePixels;
                  }

                  WpfContext.DrawRectangle(
                    TranslateBrush(InvRenderElement.RectangleFillColour),
                    TranslatePen(InvRenderElement.RectangleStrokeColour,
                    StrokePixels),
                    WpfRectangleRect);
                  break;

                case RenderType.Ellipse:
                  WpfContext.DrawEllipse(
                    TranslateBrush(InvRenderElement.EllipseFillColour),
                    TranslatePen(InvRenderElement.EllipseStrokeColour,
                    VerticalPtToPx(InvRenderElement.EllipseStrokeThickness)),
                    TranslatePoint(InvRenderElement.EllipseCenter), HorizontalPtToPx(InvRenderElement.EllipseRadius.X), VerticalPtToPx(InvRenderElement.EllipseRadius.Y));
                  break;

                case RenderType.Text:
                  var WpfFormattedText = new System.Windows.Media.FormattedText
                  (
                    InvRenderElement.TextFragment,
                    CultureInfo.CurrentCulture,
                    System.Windows.FlowDirection.LeftToRight,
                    new System.Windows.Media.Typeface(new System.Windows.Media.FontFamily(InvRenderElement.TextFontName.EmptyAsNull() ?? InvApplication.Window.DefaultFont.Name ?? WpfShell.DefaultFontName), System.Windows.FontStyles.Normal, TranslateFontWeight(InvRenderElement.TextFontWeight), System.Windows.FontStretches.Normal),
                    InvRenderElement.TextFontSize,
                    TranslateBrush(InvRenderElement.TextFontColour)
                  );

                  var WpfTextPoint = TranslatePoint(InvRenderElement.TextPoint);

                  var TextHorizontal = InvRenderElement.TextHorizontal;
                  var TextVertical = InvRenderElement.TextVertical;

                  if (TextHorizontal != HorizontalPosition.Left)
                  {
                    var TextWidth = (int)WpfFormattedText.Width;

                    if (TextHorizontal == HorizontalPosition.Right)
                      WpfTextPoint.X -= TextWidth;
                    else if (TextHorizontal == HorizontalPosition.Center)
                      WpfTextPoint.X -= TextWidth / 2;
                  }

                  if (TextVertical != VerticalPosition.Top)
                  {
                    var TextHeight = (int)WpfFormattedText.Height;

                    if (TextVertical == VerticalPosition.Bottom)
                      WpfTextPoint.Y -= TextHeight;
                    else if (TextVertical == VerticalPosition.Center)
                      WpfTextPoint.Y -= TextHeight / 2;
                  }

                  WpfContext.DrawText(WpfFormattedText, WpfTextPoint);
                  break;

                case RenderType.Image:
                  var ImageRect = InvRenderElement.ImageRect;
                  var ImageOpacity = InvRenderElement.ImageOpacity;
                  var ImageTintColour = InvRenderElement.ImageTint;
                  var ImageMirror = InvRenderElement.ImageMirror;

                  TranslateRect(ImageRect, ref WpfImageRect);

                  if (ImageOpacity != 1.0F)
                  {
                    // NOTE: WpfContext.PushOpacity performs terribly if you push/pop in quick succession (this is a workaround).
                    var WpfTintBrush = TranslateBrush(Inv.Colour.White.Opacity(ImageOpacity));
                    WpfContext.PushOpacityMask(WpfTintBrush);
                  }

                  if (ImageMirror != null)
                  {
                    WpfContext.PushTransform(MirrorTransformArray[ImageMirror.Value]);

                    if (ImageMirror.Value == Mirror.Horizontal)
                      WpfImageRect.X = -WpfImageRect.X - WpfImageRect.Width;

                    if (ImageMirror.Value == Mirror.Vertical)
                      WpfImageRect.Y = -WpfImageRect.Y - WpfImageRect.Height;
                  }

                  var WpfImage = TranslateImage(InvRenderElement.ImageSource);

                  WpfContext.DrawImage(WpfImage, WpfImageRect);

                  if (ImageTintColour != null)
                  {
                    var WpfTintBrush = TranslateBrush(ImageTintColour);

                    var WpfImageBrush = new System.Windows.Media.ImageBrush(WpfImage);
                    WpfImageBrush.Freeze();

                    WpfContext.PushOpacityMask(WpfImageBrush);
                    WpfContext.DrawRectangle(WpfTintBrush, null, WpfImageRect);
                    WpfContext.Pop();
                  }

                  if (ImageMirror != null)
                    WpfContext.Pop();

                  if (ImageOpacity != 1.0F)
                    WpfContext.Pop();
                  break;

                default:
                  throw new ApplicationException("RenderType not handled: " + InvRenderElement.Type);
              }
            }
          }
        }
      });

      return WpfRender;
    }
    private System.Windows.FrameworkElement TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var WpfScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new WpfScroll(P.Orientation == ScrollOrientation.Vertical, P.Orientation == ScrollOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, WpfScroll, () =>
      {
        if (InvScroll.Background.Render())
          WpfScroll.Background = TranslateBrush(InvScroll.Background.Colour);

        if (InvScroll.CornerRadius.Render())
          WpfScroll.CornerRadius = TranslateCornerRadius(InvScroll.CornerRadius);

        TranslateMargin(InvScroll.Margin, WpfScroll);
        TranslatePadding(InvScroll.Padding, WpfScroll);
        TranslateBorder(InvScroll.Border, WpfScroll);
        TranslateOpacity(InvScroll.Opacity, WpfScroll);
        TranslateAlignment(InvScroll.Alignment, WpfScroll);
        TranslateVisibility(InvScroll.Visibility, WpfScroll);
        TranslateSize(InvScroll.Size, WpfScroll);
        TranslateElevation(InvScroll.Elevation, WpfScroll);

        if (InvScroll.ContentSingleton.Render())
          WpfScroll.Content = TranslatePanel(InvScroll.ContentSingleton.Data);
      });

      return WpfScroll;
    }
    private System.Windows.FrameworkElement TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var WpfFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new WpfFrame();
        return Result;
      });

      RenderPanel(InvFrame, WpfFrame, () =>
      {
        if (InvFrame.CornerRadius.Render())
          WpfFrame.CornerRadius = TranslateCornerRadius(InvFrame.CornerRadius);

        if (InvFrame.Background.Render())
          WpfFrame.Background = TranslateBrush(InvFrame.Background.Colour);

        TranslateBorder(InvFrame.Border, WpfFrame);
        TranslateMargin(InvFrame.Margin, WpfFrame);
        TranslatePadding(InvFrame.Padding, WpfFrame);
        TranslateOpacity(InvFrame.Opacity, WpfFrame);
        TranslateAlignment(InvFrame.Alignment, WpfFrame);
        TranslateVisibility(InvFrame.Visibility, WpfFrame);
        TranslateSize(InvFrame.Size, WpfFrame);
        TranslateElevation(InvFrame.Elevation, WpfFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          WpfFrame.Child = null; // detach previous content in case it has moved.
          WpfFrame.Child = TranslatePanel(InvFrame.ContentSingleton.Data);
        }
      });

      return WpfFrame;
    }
    private System.Windows.FrameworkElement TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var WpfStack = AccessPanel(InvStack, P =>
      {
        var Result = new WpfStack();
        Result.Orientation = P.Orientation == StackOrientation.Horizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;
        return Result;
      });

      RenderPanel(InvStack, WpfStack, () =>
      {
        if (InvStack.Background.Render())
          WpfStack.Background = TranslateBrush(InvStack.Background.Colour);

        if (InvStack.CornerRadius.Render())
          WpfStack.CornerRadius = TranslateCornerRadius(InvStack.CornerRadius);

        TranslateMargin(InvStack.Margin, WpfStack);
        TranslatePadding(InvStack.Padding, WpfStack);
        TranslateBorder(InvStack.Border, WpfStack);
        TranslateOpacity(InvStack.Opacity, WpfStack);
        TranslateAlignment(InvStack.Alignment, WpfStack);
        TranslateVisibility(InvStack.Visibility, WpfStack);
        TranslateSize(InvStack.Size, WpfStack);
        TranslateElevation(InvStack.Elevation, WpfStack);

        if (InvStack.PanelCollection.Render())
        {
          WpfStack.Children.Clear();
          foreach (var InvElement in InvStack.GetPanels())
          {
            var WpfPanel = TranslatePanel(InvElement);
            WpfStack.Children.Add(WpfPanel);
          }
        }
      });

      return WpfStack;
    }
    private System.Windows.FrameworkElement TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var WpfTable = AccessPanel(InvTable, P =>
      {
        var Result = new WpfGrid();
        return Result;
      });

      RenderPanel(InvTable, WpfTable, () =>
      {
        if (InvTable.Background.Render())
          WpfTable.Background = TranslateBrush(InvTable.Background.Colour);

        if (InvTable.CornerRadius.Render())
          WpfTable.CornerRadius = TranslateCornerRadius(InvTable.CornerRadius);

        TranslateMargin(InvTable.Margin, WpfTable);
        TranslatePadding(InvTable.Padding, WpfTable);
        TranslateBorder(InvTable.Border, WpfTable);
        TranslateOpacity(InvTable.Opacity, WpfTable);
        TranslateAlignment(InvTable.Alignment, WpfTable);
        TranslateVisibility(InvTable.Visibility, WpfTable);
        TranslateSize(InvTable.Size, WpfTable);
        TranslateElevation(InvTable.Elevation, WpfTable);

        if (InvTable.CollectionRender())
        {
          WpfTable.Children.Clear();
          WpfTable.RowDefinitions.Clear();
          WpfTable.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            WpfTable.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn.Length, true) });

            var WpfColumn = TranslatePanel(TableColumn.Content);

            if (WpfColumn != null)
            {
              WpfTable.Children.Add(WpfColumn);

              System.Windows.Controls.Grid.SetColumn(WpfColumn, TableColumn.Index);
              System.Windows.Controls.Grid.SetRow(WpfColumn, 0);
              System.Windows.Controls.Grid.SetRowSpan(WpfColumn, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            WpfTable.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = TranslateTableLength(TableRow.Length, false) });

            var WpfRow = TranslatePanel(TableRow.Content);

            if (WpfRow != null)
            {
              WpfTable.Children.Add(WpfRow);

              System.Windows.Controls.Grid.SetRow(WpfRow, TableRow.Index);
              System.Windows.Controls.Grid.SetColumn(WpfRow, 0);
              System.Windows.Controls.Grid.SetColumnSpan(WpfRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var WpfCell = TranslatePanel(TableCell.Content);

            if (WpfCell != null)
            {
              WpfTable.Children.Add(WpfCell);

              System.Windows.Controls.Grid.SetColumn(WpfCell, TableCell.Column.Index);
              System.Windows.Controls.Grid.SetRow(WpfCell, TableCell.Row.Index);
            }
          }
        }
      });

      return WpfTable;
    }

    private System.Windows.FrameworkElement TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel);
    }
    private Inv.Key? TranslateKey(System.Windows.Input.Key WpfKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(WpfKey, out Result))
        return Result;
      else
        return null;
    }
    private System.Windows.Media.Brush TranslateHoverBrush(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return TranslateBrush(InvColour.Value.Lighten(0.25F));
    }
    private System.Windows.Media.Brush TranslatePressBrush(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return TranslateBrush(InvColour.Value.Darken(0.25F));
    }
    private System.Windows.Media.Brush TranslateBrush(Inv.Colour? InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else
      {
        return ColourBrushDictionary.GetOrAdd(InvColour.Value, C =>
        {
          var Result = new System.Windows.Media.SolidColorBrush(TranslateColour(C));
          Result.Freeze();

          return Result;
        });
      }
    }
    private System.Windows.Media.Color TranslateColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return System.Windows.Media.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    private System.Windows.Media.Pen TranslatePen(Inv.Colour? Colour, int Thickness)
    {
      if (Colour == null || Thickness <= 0)
        return null;

      LookupPenKey.Colour = Colour.Value;
      LookupPenKey.Thickness = Thickness;

      return PenDictionary.GetOrAdd(LookupPenKey, C =>
      {
        var Result = new System.Windows.Media.Pen(TranslateBrush(Colour), HorizontalPtToPx(Thickness));
        Result.Freeze();
        return Result;
      });
    }
    private void TranslateRect(Inv.Rect InvRect, ref System.Windows.Rect WpfRect)
    {
      WpfRect.X = HorizontalPtToPx(InvRect.Left);
      WpfRect.Y = VerticalPtToPx(InvRect.Top);
      WpfRect.Width = HorizontalPtToPx(InvRect.Width);
      WpfRect.Height = VerticalPtToPx(InvRect.Height);
    }
    private System.Windows.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new System.Windows.Point(HorizontalPtToPx(InvPoint.X), VerticalPtToPx(InvPoint.Y));
    }
    private Inv.Point TranslatePoint(System.Windows.Point WpfPoint)
    {
      return new Inv.Point(HorizontalPxToPt((int)WpfPoint.X), VerticalPxToPt((int)WpfPoint.Y));
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Control WpfControl)
    {
      if (InvBorder.Render())
      {
        WpfControl.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfControl.BorderThickness = TranslateEdge(InvBorder.Thickness);
      }
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Border WpfBorder)
    {
      if (InvBorder.Render())
      {
        WpfBorder.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfBorder.BorderThickness = TranslateEdge(InvBorder.Thickness);
      }
    }
    private void TranslateMargin(Inv.Edge InvMargin, System.Windows.FrameworkElement WpfElement)
    {
      if (InvMargin.Render())
        WpfElement.Margin = TranslateEdge(InvMargin);
    }
    private void TranslatePadding(Inv.Edge InvPadding, System.Windows.Controls.Control WpfControl)
    {
      if (InvPadding.Render())
        WpfControl.Padding = TranslateEdge(InvPadding);
    }
    private void TranslatePadding(Inv.Edge InvPadding, System.Windows.Controls.Border WpfBorder)
    {
      if (InvPadding.Render())
        WpfBorder.Padding = TranslateEdge(InvPadding);
    }
    private void TranslateSize(Inv.Size InvSize, System.Windows.FrameworkElement WpfElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          WpfElement.Width = HorizontalPtToPx(InvSize.Width.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.WidthProperty);

        if (InvSize.Height != null)
          WpfElement.Height = VerticalPtToPx(InvSize.Height.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.HeightProperty);

        if (InvSize.MinimumWidth != null)
          WpfElement.MinWidth = HorizontalPtToPx(InvSize.MinimumWidth.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          WpfElement.MinHeight = VerticalPtToPx(InvSize.MinimumHeight.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          WpfElement.MaxWidth = HorizontalPtToPx(InvSize.MaximumWidth.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          WpfElement.MaxHeight = VerticalPtToPx(InvSize.MaximumHeight.Value);
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxHeightProperty);
      }
    }
    private void TranslateVisibility(Inv.Visibility InvVisibility, System.Windows.FrameworkElement WpfElement)
    {
      if (InvVisibility.Render())
        WpfElement.Visibility = InvVisibility.Get() ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, System.Windows.FrameworkElement WpfElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          default:
            throw new ApplicationException("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateElevation(Inv.Elevation InvElevation, System.Windows.FrameworkElement WpfElement)
    {
      if (InvElevation.Render())
      {
        if (InvElevation.Depth == 0 || InvElevation.Colour == null)
        {
          WpfElement.Effect = null;
        }
        else
        {
          WpfElement.Effect = new System.Windows.Media.Effects.DropShadowEffect()
          {
            Direction = 315,
            Color = TranslateColour(InvElevation.Colour.Value),
            ShadowDepth = VerticalPtToPx(InvElevation.Depth),
            Opacity = InvElevation.Colour.Value.GetARGBRecord().A / 255.0F
          };
        }
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.Control WpfElement, string DefaultFontName)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null || DefaultFontName != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name ?? DefaultFontName);
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.Control.ForegroundProperty);

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBlock WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Controls.TextBlock.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.ForegroundProperty);

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBox WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontSizeProperty);

        var WpfBrush = TranslateBrush(InvFont.Colour);

        if (WpfBrush != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, WpfBrush);
        else
          WpfElement.ClearValue(System.Windows.Documents.TextElement.ForegroundProperty);

        WpfElement.CaretBrush = WpfBrush;

        WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private System.Windows.FontWeight TranslateFontWeight(FontWeight InvFontWeight)
    {
      switch (InvFontWeight)
      {
        case FontWeight.Thin:
          return System.Windows.FontWeights.Thin;

        case FontWeight.Light:
          return System.Windows.FontWeights.Light;

        case FontWeight.Regular:
          return System.Windows.FontWeights.Regular;

        case FontWeight.Medium:
          return System.Windows.FontWeights.Medium;

        case FontWeight.Bold:
          return System.Windows.FontWeights.Bold;

        case FontWeight.Black:
          return System.Windows.FontWeights.Black;

        default:
          throw new ApplicationException("FontWeight not handled: " + InvFontWeight);
      }
    }
    private System.Windows.GridLength TranslateTableLength(TableLength InvTableLength, bool Horizontal)
    {
      switch (InvTableLength.Type)
      {
        case TableLengthType.Auto:
          return System.Windows.GridLength.Auto;

        case TableLengthType.Fixed:
          return new System.Windows.GridLength(Horizontal ? HorizontalPtToPx(InvTableLength.Value) : VerticalPtToPx(InvTableLength.Value), System.Windows.GridUnitType.Pixel);

        case TableLengthType.Star:
          return new System.Windows.GridLength(InvTableLength.Value, System.Windows.GridUnitType.Star);

        default:
          throw new Exception("Inv.TableLength not handled: " + InvTableLength.Type);
      }
    }
    private System.Windows.Media.Imaging.BitmapSource TranslateImage(Inv.Image? Image)
    {
      if (Image == null)
        return null;
      else
        return ImageDictionary.GetOrAdd(Image.Value, K => LoadImage(K));
    }
    private System.Windows.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new System.Windows.Thickness(HorizontalPtToPx(InvEdge.Left), VerticalPtToPx(InvEdge.Top), HorizontalPtToPx(InvEdge.Right), VerticalPtToPx(InvEdge.Bottom));
    }
    private System.Windows.CornerRadius TranslateCornerRadius(Inv.CornerRadius InvCornerRadius)
    {
      return new System.Windows.CornerRadius(HorizontalPtToPx(InvCornerRadius.TopLeft), VerticalPtToPx(InvCornerRadius.TopRight), HorizontalPtToPx(InvCornerRadius.BottomRight), VerticalPtToPx(InvCornerRadius.BottomLeft));
    }
    private void TranslateOpacity(Opacity Opacity, System.Windows.FrameworkElement WpfElement)
    {
      if (Opacity.Render())
        WpfElement.Opacity = Opacity.Get();
    }
    private System.Windows.Media.Imaging.BitmapSource LoadImage(Image Image)
    {
      // NOTE: input images are assumed to be 1pt=3px.
      using (var MemoryStream = new MemoryStream(Image.GetBuffer()))
      {
        var Result = new System.Windows.Media.Imaging.BitmapImage();

        Result.BeginInit();
        Result.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
        Result.StreamSource = MemoryStream;
        Result.EndInit();
        Result.Freeze();

        return Result;
      }
    }
    private int TranslateFontSize(int FontSize)
    {
      return FontSize;
      //return (int)(FontSize * ConvertVerticalPtToPxFactor + 0.5F);
    }
    private int HorizontalPtToPx(int Points)
    {
      return Points;
      //return (int)(Points * ConvertHorizontalPtToPxFactor + 0.5F);
    }
    private int VerticalPtToPx(int Points)
    {
      return Points;
      //return (int)(Points * ConvertVerticalPtToPxFactor + 0.5F);
    }
    private int HorizontalPxToPt(int Pixels)
    {
      return Pixels;
      //return (int)(Pixels / ConvertHorizontalPtToPxFactor + 0.5F);
    }
    private int VerticalPxToPt(int Pixels)
    {
      return Pixels;
      //return (int)(Pixels / ConvertVerticalPtToPxFactor + 0.5F);
    }

    private WpfSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, WpfSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (WpfSurface)InvSurface.Node;
      }
    }
    private System.Windows.Threading.DispatcherTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, System.Windows.Threading.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Windows.Threading.DispatcherTimer)InvTimer.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfElement, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : System.Windows.FrameworkElement
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);
        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }
    private void Rendering(object Sender, EventArgs Event)
    {
      Process();
    }

    private Inv.Application InvApplication;
    private System.Windows.Controls.ContentControl WpfContainer;
    private PenKey LookupPenKey;
    private Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform> MirrorTransformArray;
    private Dictionary<System.Windows.Input.Key, Inv.Key> KeyDictionary;
    private Dictionary<Type, Func<Inv.Panel, System.Windows.FrameworkElement>> RouteDictionary;
    private Dictionary<Inv.Image, System.Windows.Media.Imaging.BitmapSource> ImageDictionary;
    private Dictionary<Inv.Colour, System.Windows.Media.Brush> ColourBrushDictionary;
    private Dictionary<PenKey, System.Windows.Media.Pen> PenDictionary;
    private WpfGrid WpfMaster;
#if DEBUG
    private WpfLabel WpfEmulationDeviceLabel;
    private WpfLabel WpfEmulationInstructionLabel;
    private WpfLabel WpfEmulationFrameLabel;
#endif
    //private float ConvertHorizontalPtToPxFactor;
    //private float ConvertVerticalPtToPxFactor;
    private int SurfaceCount;
    private System.Windows.Controls.Border WpfBorder;

    private struct PenKey
    {
      public Inv.Colour Colour;
      public int Thickness;

      public override int GetHashCode()
      {
        return Colour.GetHashCode() ^ Thickness.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (PenKey)obj;

        return Key.Thickness == Thickness && Key.Colour == Colour;
      }
    }
  }
}