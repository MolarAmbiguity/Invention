﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  internal static class UwaStyles
  {
    static UwaStyles()
    {
      var StyleDictionary = new Windows.UI.Xaml.ResourceDictionary();
      StyleDictionary.Source = new Uri("ms-appx:///Inv.Uwa/InvUwaStyles.xaml", UriKind.RelativeOrAbsolute);

      ButtonStyle = GetStyle(StyleDictionary, "InvUwaButton");
      MemoStyle = GetStyle(StyleDictionary, "InvUwaMemo");
    }

    public static readonly Windows.UI.Xaml.Style ButtonStyle;
    public static readonly Windows.UI.Xaml.Style MemoStyle;

    private static Windows.UI.Xaml.Style GetStyle(Windows.UI.Xaml.ResourceDictionary Dictionary, string Name)
    {
      return (Windows.UI.Xaml.Style)Dictionary[Name];
    }
  }

  internal interface UwaOverrideFocusContract
  {
    void OverrideFocus();
  }

  internal abstract class UwaPanel : Windows.UI.Xaml.Controls.ContentControl
  {
    public UwaPanel()
    {
      this.Border = new Windows.UI.Xaml.Controls.Border();
      Content = Border;

      HorizontalContentAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      VerticalContentAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
    }

    public Windows.UI.Xaml.Controls.Border Border { get; private set; }
  }

  [Windows.UI.Xaml.TemplatePart(Name = "PART_Border", Type = typeof(Windows.UI.Xaml.Controls.Border))]
  internal sealed class UwaButton : Windows.UI.Xaml.Controls.Button
  {
    public UwaButton()
    {
      this.Style = UwaStyles.ButtonStyle;
      this.Border = new Windows.UI.Xaml.Controls.Border();
    }

    public Windows.UI.Xaml.Controls.Border Border { get; private set; }

    protected override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      this.PartBorder = this.GetTemplateChild("PART_Border") as Windows.UI.Xaml.Controls.Border;

      if (PartBorder != null)
        PartBorder.Child = Border;
    }

    private Windows.UI.Xaml.Controls.Border PartBorder;
  }

  internal sealed class UwaCanvas : UwaPanel
  {
    public UwaCanvas()
    {
      this.Canvas = new Windows.UI.Xaml.Controls.Canvas();
      Border.Child = Canvas;
    }

    public Windows.UI.Xaml.Controls.Canvas Canvas { get; private set; }
  }

  internal sealed class UwaDock : UwaPanel
  {
    public UwaDock()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }
  }

  internal sealed class UwaEdit : UwaPanel, UwaOverrideFocusContract
  {
    public UwaEdit()
    {
      this.TextBox = new Windows.UI.Xaml.Controls.TextBox()
      {
        AcceptsReturn = false,
        Background = null,
        BorderBrush = null,
        TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
        BorderThickness = new Windows.UI.Xaml.Thickness(0)
      };
      Border.Child = TextBox;
    }

    public Windows.UI.Xaml.Controls.TextBox TextBox { get; private set; }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      TextBox.Focus(Windows.UI.Xaml.FocusState.Keyboard);
    }
  }

  internal sealed class UwaGraphic : UwaPanel
  {
    public UwaGraphic()
    {
      this.Image = new Windows.UI.Xaml.Controls.Image()
      {
        Stretch = Windows.UI.Xaml.Media.Stretch.Uniform
      };
      Image.CacheMode = new Windows.UI.Xaml.Media.BitmapCache();
      //RenderOptions.SetBitmapScalingMode(Inner, BitmapScalingMode.Fant);

      Border.Child = Image;
    }

    public Windows.UI.Xaml.Controls.Image Image { get; private set; }
  }

  internal sealed class UwaLabel : UwaPanel
  {
    public UwaLabel()
    {
      this.TextBlock = new Windows.UI.Xaml.Controls.TextBlock()
      {
        VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center,
        TextTrimming = Windows.UI.Xaml.TextTrimming.CharacterEllipsis
      };
      Border.Child = TextBlock;
    }

    public Windows.UI.Xaml.Controls.TextBlock TextBlock { get; private set; }
  }

  internal sealed class UwaMemo : UwaPanel, UwaOverrideFocusContract
  {
    public UwaMemo()
    {
      this.TextBox = new Windows.UI.Xaml.Controls.TextBox()
      {
        AcceptsReturn = true,
        //AcceptsTabs = true, // Not available in WinRT?
        TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
        Background = null,
        BorderBrush = null,
        BorderThickness = new Windows.UI.Xaml.Thickness(0),
        Style = UwaStyles.MemoStyle
      };
      Border.Child = TextBox;
    }

    public Windows.UI.Xaml.Controls.TextBox TextBox { get; private set; }
    public string Text
    {
      get { return TextBox.Text; }
      set { TextBox.Text = value; }
      /*
      get
      {
        string Result;
        TextBox.Document.GetText(Windows.UI.Text.TextGetOptions.None, out Result);
        return Result;
      }
      set { TextBox.Document.SetText(Windows.UI.Text.TextSetOptions.None, value); }*/
    }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      TextBox.Focus(Windows.UI.Xaml.FocusState.Keyboard);
    }
  }

  internal sealed class UwaOverlay : UwaPanel
  {
    public UwaOverlay()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }
  }

  internal sealed class UwaRender : UwaPanel
  {
    public UwaRender()
    {
      this.Canvas = new Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl();
      Border.Child = Canvas;
      Canvas.CreateResources += (Sender, Event) =>
      {
        if (ResetEvent != null)
          ResetEvent();
      };
      Canvas.Draw += (Sender, Event) =>
      {
        DrawEvent(Event.DrawingSession);
      };
    }

    public event Action ResetEvent;
    public event Action<Microsoft.Graphics.Canvas.CanvasDrawingSession> DrawEvent;

    public Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl Canvas { get; private set; }
  }

  internal sealed class UwaScroll : UwaPanel
  {
    public UwaScroll(bool IsVertical, bool IsHorizontal)
    {
      this.ScrollViewer = new Windows.UI.Xaml.Controls.ScrollViewer();
      Border.Child = ScrollViewer;

      if (!IsVertical && IsHorizontal)
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
      }
      else if (IsVertical && !IsHorizontal)
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
      }
      else
      {
        ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
        ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
      }
    }

    public Windows.UI.Xaml.Controls.ScrollViewer ScrollViewer { get; private set; }
  }

  internal sealed class UwaStack : UwaPanel
  {
    public UwaStack()
    {
      this.StackPanel = new Windows.UI.Xaml.Controls.StackPanel();
      Border.Child = StackPanel;
    }

    public Windows.UI.Xaml.Controls.StackPanel StackPanel { get; private set; }
  }

  internal sealed class UwaSurface : UwaPanel
  {
    public UwaSurface()
    {
    }
  }

  internal sealed class UwaTable : UwaPanel
  {
    public UwaTable()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }
  }
}
