﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public static class UwaShell
  {
    static UwaShell()
    {
      RespectPhysicalDimensions = false;
    }

    public static bool RespectPhysicalDimensions { get; set; }

    public static void Attach(Windows.UI.Xaml.Controls.Page UwaPage, Func<Inv.Application> ApplicationFunction)
    {
      try
      {
        var InvApplication = ApplicationFunction();

        var UwaEngine = new UwaEngine(UwaPage, InvApplication);
        UwaEngine.Start();

#if DEBUG
        //throw new Exception("I'm a fail");
#endif
      }
      catch (Exception Exception)
      {
        var Grid = new Windows.UI.Xaml.Controls.Grid();
        UwaPage.Content = Grid;
        Grid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
        Grid.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;

        var Scroll = new Windows.UI.Xaml.Controls.ScrollViewer();
        Grid.Children.Add(Scroll);
        Scroll.Margin = new Windows.UI.Xaml.Thickness(0, 100, 0, 0);
        Scroll.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        Scroll.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;

        var TextBox = new Windows.UI.Xaml.Controls.TextBox();
        Scroll.Content = TextBox;
        TextBox.IsReadOnly = true;
        TextBox.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;

        var Message = Environment.NewLine + Exception.Message + Environment.NewLine + Environment.NewLine + Exception.StackTrace;
        TextBox.Text = Message;

        var Button = new Windows.UI.Xaml.Controls.Button();
        Grid.Children.Add(Button);
        Button.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
        Button.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
        Button.Margin = new Windows.UI.Xaml.Thickness(0, 30, 0, 0);
        Button.Content = "Send this start up fault to the author";
        Button.Click += (Sender, Event) => MailTo("hodgskin.callan@gmail.com", "Start Up Fault", Message);
      }
    }

    internal static void MailTo(string To, string Subject, string Body)
    {
      var MailUri = new Uri(string.Format("mailto:{0}?subject={1}&body={2}", To, Uri.EscapeDataString(Subject), Uri.EscapeDataString(Body)));

      // TODO: attachments not supported by mailto protocol.

      var MailTask = Windows.System.Launcher.LaunchUriAsync(MailUri).AsTask();
      MailTask.Wait();
    }
  }

  internal sealed class UwaPlatform : Inv.Platform
  {
    public UwaPlatform(UwaEngine Engine)
    {
      this.Engine = Engine;
      this.LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
    }

    int Platform.ThreadAffinity()
    {
      return Environment.CurrentManagedThreadId;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      // TODO: Uwa date/time picker.
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      // TODO: Windows.ApplicationModel.Email.EmailMessage in Windows 10 only?
      //var em = new Windows.ApplicationModel.Email.EmailMessage() ;   
      //em.To.Add(new EmailRecipient("yourname@yourdomain.com"));    
      //em.Subject = "Your Subject...";    
      //em.Body = "Your email body...";    
      //em.Attachments.Add(new EmailAttachment(...); 
      //EmailManager.ShowComposeNewEmailAsync(em).AsTask().Wait();

      UwaShell.MailTo(EmailMessage.GetTos().Select(T => T.Address).AsSeparatedText(","), EmailMessage.Subject ?? "", EmailMessage.Body ?? "");

      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    bool Platform.PhoneDial(string PhoneNumber)
    {
      return false;
    }
    bool Platform.PhoneSMS(string PhoneNumber)
    {
      return false;
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetBasicPropertiesAsync().AsTask();
      FileTask.Wait();

      return (long)FileTask.Result.Size;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      var PropertiesTask = FileTask.Result.GetBasicPropertiesAsync().AsTask();
      PropertiesTask.Wait();

      return PropertiesTask.Result.DateModified.UtcDateTime;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      // TODO: System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.ReplaceExisting);
      WriteTask.Wait();

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.OpenIfExists);
      WriteTask.Wait();

      WriteTask.Result.Seek(0, System.IO.SeekOrigin.End);

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var ReadTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForReadAsync(StorageFolder, File.Name); 
      ReadTask.Wait();

      return ReadTask.Result;
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.TryGetItemAsync(File.Name).AsTask();
      FileTask.Wait();

      return FileTask.Result != null;
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();

      var DeleteTask = FileTask.Result.DeleteAsync().AsTask();
      DeleteTask.Wait();
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      var SourceFolder = SelectStorageFolder(SourceFile.Folder);
      var TargetFolder = SelectStorageFolder(TargetFile.Folder);

      var SourceFileTask = SourceFolder.GetFileAsync(SourceFile.Name).AsTask();
      SourceFileTask.Wait();

      var CopyTask = SourceFileTask.Result.CopyAsync(TargetFolder, TargetFile.Name).AsTask();
      CopyTask.Wait();
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      var SourceFolder = SelectStorageFolder(SourceFile.Folder);
      var TargetFolder = SelectStorageFolder(TargetFile.Folder);

      var SourceFileTask = SourceFolder.GetFileAsync(SourceFile.Name).AsTask();
      SourceFileTask.Wait();

      var MoveTask = SourceFileTask.Result.MoveAsync(TargetFolder, TargetFile.Name).AsTask();
      MoveTask.Wait();
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      var StorageFolder = SelectStorageFolder(Folder);

      var FilesTask = StorageFolder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.OrderByName).AsTask();
      FilesTask.Wait();

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FileMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var StorageFile in FilesTask.Result)
      {
        if (Regex.IsMatch(StorageFile.Name))
          yield return new File(Folder, StorageFile.Name);
      }
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      var InstallFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;

      var FileTask = InstallFolder.GetFileAsync(System.IO.Path.Combine("Assets", Asset.Name)).AsTask();
      FileTask.Wait();

      var StreamTask = FileTask.Result.OpenStreamForReadAsync();
      StreamTask.Wait();

      return StreamTask.Result;
    }
    bool Platform.LocationIsSupported
    {
      get { return false; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      // TODO: implement windows 8 app location services.
      LocationLookup.SetPlacemarks(new Placemark[] { });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume)
    {
      MediaUtilities.Play(Sound, Volume);
    }
    void Platform.WindowAsynchronise(Action Action)
    {
      Engine.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Action()).AsTask().Forget();
    }
    Modifier Platform.WindowGetModifier()
    {
      return Engine.GetModifier();
    }
    long Platform.ProcessMemoryBytes()
    {
      return 0L; // TODO: process memory.
    }
    void Platform.WebSocketConnect(WebSocket Socket)
    {
      var TcpClient = new Inv.Tcp.Client(Socket.Host, Socket.Port);
      TcpClient.Connect();
      
      Socket.Node = TcpClient;
      Socket.SetStreams(TcpClient.InputStream, TcpClient.OutputStream);
    }
    void Platform.WebSocketDisconnect(WebSocket Socket)
    {
      var TcpClient = (Inv.Tcp.Client)Socket.Node;
      if (TcpClient != null)
      {
        Socket.Node = null;
        Socket.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Windows.System.Launcher.LaunchUriAsync(Uri).AsTask().Wait();
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=" + WindowsStoreID)).AsTask().Wait();
    }

    private Windows.Storage.StorageFolder SelectStorageFolder(Folder Folder)
    {
      if (Folder.Name == null)
        return LocalFolder;

      var FolderTask = LocalFolder.TryGetItemAsync(Folder.Name).AsTask();
      FolderTask.Wait();

      if (FolderTask.Result != null)
        return FolderTask.Result as Windows.Storage.StorageFolder;

      var CreateTask = LocalFolder.CreateFolderAsync(Folder.Name).AsTask();
      CreateTask.Wait();
      return CreateTask.Result;
    }

    private UwaEngine Engine;
    private Windows.Storage.StorageFolder LocalFolder;
  }
  /*
  public static class MediaUtilities
  {
    static MediaUtilities()
    {
      SoundIdDictionary = new Dictionary<Sound, Windows.Storage.Streams.InMemoryRandomAccessStream>();
    }

    public static void Play(Inv.Sound InvSound, float VolumeScale)
    {
      var MediaStream = SoundIdDictionary.GetOrAdd(InvSound, S =>
      {
        return ProduceMediaStream(S);
      });

      var Result = new Windows.UI.Xaml.Controls.MediaElement();
      Result.SetSource(MediaStream, "mp3");
      Result.MediaEnded += (Sender, Event) => Result.Stop();
      Result.Play();
    }
    private static Windows.Storage.Streams.InMemoryRandomAccessStream ProduceMediaStream(Inv.Sound Sound)
    {
      var Result = new Windows.Storage.Streams.InMemoryRandomAccessStream();

      using (var DataWriter = new Windows.Storage.Streams.DataWriter(Result))
      {
        // Write the bytes to the stream
        DataWriter.WriteBytes(Sound.GetBuffer());

        // Store the bytes to the MemoryStream
        DataWriter.StoreAsync().AsTask().Wait();

        // Not necessary, but do it anyway
        DataWriter.FlushAsync().AsTask().Wait();

        // Detach from the Memory stream so we don't close it
        DataWriter.DetachStream();
      }

      return Result;
    }

    private static Dictionary<Inv.Sound, Windows.Storage.Streams.InMemoryRandomAccessStream> SoundIdDictionary;
  }
  */
  public static class MediaUtilities
  {
    static MediaUtilities()
    {
      SoundIdDictionary = new Dictionary<Sound, Windows.UI.Xaml.Controls.MediaElement>();
    }

    public static void Play(Inv.Sound InvSound, float VolumeScale)
    {
      var MediaElement = SoundIdDictionary.GetOrAdd(InvSound, S =>
      {
        return ProduceMediaElement(S);
      });

      if (MediaElement.CurrentState == Windows.UI.Xaml.Media.MediaElementState.Playing)
        ProduceMediaElement(InvSound).Play();
      else
        MediaElement.Play();
    }
    private static Windows.UI.Xaml.Controls.MediaElement ProduceMediaElement(Inv.Sound Sound)
    {
      var Result = new Windows.UI.Xaml.Controls.MediaElement();

      var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();

      using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
      {
        // Write the bytes to the stream
        DataWriter.WriteBytes(Sound.GetBuffer());

        // Store the bytes to the MemoryStream
        DataWriter.StoreAsync().AsTask().Wait();

        // Not necessary, but do it anyway
        DataWriter.FlushAsync().AsTask().Wait();

        // Detach from the Memory stream so we don't close it
        DataWriter.DetachStream();
      }

      Result.SetSource(MemoryStream, "mp3");

      Result.MediaEnded += (Sender, Event) => Result.Stop();

      return Result;
    }

    private static Dictionary<Inv.Sound, Windows.UI.Xaml.Controls.MediaElement> SoundIdDictionary;
  }

  internal sealed class UwaEngine
  {
    public UwaEngine(Windows.UI.Xaml.Controls.Page UwaPage, Inv.Application Application)
    {
      this.UwaPage = UwaPage;
      this.InvApplication = Application;

      this.UwaMaster = new Windows.UI.Xaml.Controls.Grid();
      UwaPage.Content = UwaMaster;

      this.ColourBrushDictionary = new Dictionary<Inv.Colour, Windows.UI.Xaml.Media.Brush>();
      this.MediaImageDictionary = new Dictionary<Inv.Image, Windows.UI.Xaml.Media.Imaging.BitmapSource>();
      this.CanvasBitmapDictionary = new Dictionary<Inv.Image, Microsoft.Graphics.Canvas.ICanvasImage>();

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Windows.System.VirtualKey, Inv.Key>()
      {
        { Windows.System.VirtualKey.Number0, Key.n0 },
        { Windows.System.VirtualKey.Number1, Key.n1 },
        { Windows.System.VirtualKey.Number2, Key.n2 },
        { Windows.System.VirtualKey.Number3, Key.n3 },
        { Windows.System.VirtualKey.Number4, Key.n4 },
        { Windows.System.VirtualKey.Number5, Key.n5 },
        { Windows.System.VirtualKey.Number6, Key.n6 },
        { Windows.System.VirtualKey.Number7, Key.n7 },
        { Windows.System.VirtualKey.Number8, Key.n8 },
        { Windows.System.VirtualKey.Number9, Key.n9 },
        { Windows.System.VirtualKey.A, Key.A },
        { Windows.System.VirtualKey.B, Key.B },
        { Windows.System.VirtualKey.C, Key.C },
        { Windows.System.VirtualKey.D, Key.D },
        { Windows.System.VirtualKey.E, Key.E },
        { Windows.System.VirtualKey.F, Key.F },
        { Windows.System.VirtualKey.G, Key.G },
        { Windows.System.VirtualKey.H, Key.H },
        { Windows.System.VirtualKey.I, Key.I },
        { Windows.System.VirtualKey.J, Key.J },
        { Windows.System.VirtualKey.K, Key.K },
        { Windows.System.VirtualKey.L, Key.L },
        { Windows.System.VirtualKey.M, Key.M },
        { Windows.System.VirtualKey.N, Key.N },
        { Windows.System.VirtualKey.O, Key.O },
        { Windows.System.VirtualKey.P, Key.P },
        { Windows.System.VirtualKey.Q, Key.Q },
        { Windows.System.VirtualKey.R, Key.R },
        { Windows.System.VirtualKey.S, Key.S },
        { Windows.System.VirtualKey.T, Key.T },
        { Windows.System.VirtualKey.U, Key.U },
        { Windows.System.VirtualKey.V, Key.V },
        { Windows.System.VirtualKey.W, Key.W },
        { Windows.System.VirtualKey.X, Key.X },
        { Windows.System.VirtualKey.Y, Key.Y },
        { Windows.System.VirtualKey.Z, Key.Z },
        { Windows.System.VirtualKey.F1, Key.F1 },
        { Windows.System.VirtualKey.F2, Key.F2 },
        { Windows.System.VirtualKey.F3, Key.F3 },
        { Windows.System.VirtualKey.F4, Key.F4 },
        { Windows.System.VirtualKey.F5, Key.F5 },
        { Windows.System.VirtualKey.F6, Key.F6 },
        { Windows.System.VirtualKey.F7, Key.F7 },
        { Windows.System.VirtualKey.F8, Key.F8 },
        { Windows.System.VirtualKey.F9, Key.F9 },
        { Windows.System.VirtualKey.F10, Key.F10 },
        { Windows.System.VirtualKey.F11, Key.F11 },
        { Windows.System.VirtualKey.F12, Key.F12 },
        { Windows.System.VirtualKey.Escape, Key.Escape },
        { Windows.System.VirtualKey.Enter, Key.Enter },
        { Windows.System.VirtualKey.Tab, Key.Tab },
        { Windows.System.VirtualKey.Space, Key.Space },
        { Windows.System.VirtualKey.Decimal, Key.Period },
        { (Windows.System.VirtualKey)188, Key.Comma },
        { (Windows.System.VirtualKey)192, Key.BackQuote }, 
        { Windows.System.VirtualKey.Add, Key.Plus },
        { Windows.System.VirtualKey.Subtract, Key.Minus },
        { Windows.System.VirtualKey.Up, Key.Up },
        { Windows.System.VirtualKey.Down, Key.Down },
        { Windows.System.VirtualKey.Left, Key.Left },
        { Windows.System.VirtualKey.Right, Key.Right },
        { Windows.System.VirtualKey.Home, Key.Home },
        { Windows.System.VirtualKey.End, Key.End },
        { Windows.System.VirtualKey.PageUp, Key.PageUp },
        { Windows.System.VirtualKey.PageDown, Key.PageDown },
        { Windows.System.VirtualKey.Clear, Key.Clear },
        { Windows.System.VirtualKey.Insert, Key.Insert },
        { Windows.System.VirtualKey.Delete, Key.Delete },
        { Windows.System.VirtualKey.Divide, Key.Slash },
        { Windows.System.VirtualKey.Multiply, Key.Asterisk },
        { (Windows.System.VirtualKey)190, Key.Period },
        { (Windows.System.VirtualKey)191, Key.Slash },
        { (Windows.System.VirtualKey)220, Key.Backslash },
        //{ (Windows.System.VirtualKey)255,  }, // Happens when F10 key is spammed?
      };
      #endregion

      this.RouteDictionary = new Dictionary<Type, Func<Inv.Panel, Windows.UI.Xaml.FrameworkElement>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Render), TranslateRender },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        { typeof(Inv.Table), TranslateTable },
      };

      InvApplication.Platform = new UwaPlatform(this);
      InvApplication.Begin();
    }

    private void KeyDown(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.KeyEventArgs Event)
    {
      // ignore keystrokes while transitioning.
      if (UwaMaster.Children.Count == 1)
        KeyPress(Event.VirtualKey);

      Event.Handled = true; 
    }
    private void AcceleratorKeyActivated(Windows.UI.Core.CoreDispatcher Sender, Windows.UI.Core.AcceleratorKeyEventArgs Event)
    {
      var IsSystemKey = (Event.VirtualKey == Windows.System.VirtualKey.Tab || 
        Event.VirtualKey == Windows.System.VirtualKey.Menu || 
        Event.VirtualKey == Windows.System.VirtualKey.LeftMenu || 
        Event.VirtualKey == Windows.System.VirtualKey.RightMenu) && Event.EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.KeyDown;

      var IsFunctionKey = !Event.KeyStatus.IsMenuKeyDown && Event.VirtualKey >= Windows.System.VirtualKey.F1 && Event.VirtualKey <= Windows.System.VirtualKey.F12 && Event.EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown;

      if (IsSystemKey || IsFunctionKey)
      {
        // ignore keystrokes while transitioning.
        if (UwaMaster.Children.Count == 1)
          KeyPress(Event.VirtualKey);

        Event.Handled = true;
      }
      else if (Event.KeyStatus.IsMenuKeyDown &&  Event.EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown && Event.VirtualKey >= Windows.System.VirtualKey.F4)
      {
        // we can prevent the closing of the app from Alt+F4.
        Event.Handled = !InvApplication.ExitQueryInvoke();
      }
    }
    private void KeyPress(Windows.System.VirtualKey VirtualKey)
    {
      var InvSurface = InvApplication.Window.ActiveSurface;

      if (InvSurface != null)
      {
        var InvKey = TranslateKey(VirtualKey);
        if (InvKey != null)
        {
          var Keystroke = new Keystroke()
          {
            Key = InvKey.Value,
            Modifier = GetModifier()
          };
          InvSurface.KeystrokeInvoke(Keystroke);

          Process();
        }
      }
    }

    public void Start()
    {
      Resize();

      // TODO: unique hardware identifier: Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);

      InvApplication.Device.Name = ""; // TODO: is this hanging? UwaSystem.GetMachineName();
      InvApplication.Device.Model = ""; // TODO: is this hanging? UwaSystem.GetDeviceModel();
      InvApplication.Device.System = "";// TODO: this is hanging? UwaSystem.GetWindowsVersion();

      InvApplication.StartInvoke();

      Process();

      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected += UnhandledErrorDetected;
      Windows.ApplicationModel.Core.CoreApplication.Exiting += Exiting;
      Windows.ApplicationModel.Core.CoreApplication.Suspending += Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming += Resuming;

      Windows.UI.Xaml.Media.CompositionTarget.Rendering += Rendering;

      this.CoreWindow = Windows.UI.Core.CoreWindow.GetForCurrentThread();
      CoreWindow.KeyDown += KeyDown;
      CoreWindow.Dispatcher.AcceleratorKeyActivated += AcceleratorKeyActivated;
      CoreWindow.SizeChanged += SizeChanged;
      CoreWindow.PointerPressed += PointerPressed;
      CoreWindow.PointerReleased += PointerReleased;
      CoreWindow.PointerMoved += PointerMoved;
    }
    public void Stop()
    {
      // NOTE: InvApplication.StopInvoke() is called by Exiting.

      if (CoreWindow != null)
      {
        CoreWindow.Dispatcher.AcceleratorKeyActivated -= AcceleratorKeyActivated;
        CoreWindow.KeyDown -= KeyDown;
        CoreWindow.SizeChanged -= SizeChanged;
        CoreWindow.PointerPressed -= PointerPressed;
        CoreWindow.PointerReleased -= PointerReleased;
        CoreWindow.PointerMoved -= PointerMoved;
      }

      Windows.UI.Xaml.Media.CompositionTarget.Rendering -= Rendering;

      Windows.ApplicationModel.Core.CoreApplication.Suspending -= Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming -= Resuming;
      Windows.ApplicationModel.Core.CoreApplication.Exiting -= Exiting;
      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected -= UnhandledErrorDetected;
    }

    internal Windows.UI.Core.CoreWindow CoreWindow { get; private set; }

    internal Modifier GetModifier()
    {
      return new Modifier()
      {
        IsLeftShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down
      };
    }

    private void Resize()
    {
      var Display = Windows.Graphics.Display.DisplayInformation.GetForCurrentView();

      var ResolutionWidthPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Width;
      var ResolutionHeightPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Height;

      var LogicalScreenWidthPoints = UwaShell.RespectPhysicalDimensions ? (int)(ResolutionWidthPixels / Display.RawDpiX * 160) : 0;
      if (LogicalScreenWidthPoints <= 0)
        LogicalScreenWidthPoints = ResolutionWidthPixels;

      var LogicalScreenHeightPoints = UwaShell.RespectPhysicalDimensions ? (int)(ResolutionHeightPixels / Display.RawDpiY * 160) : 0;
      if (LogicalScreenHeightPoints <= 0)
        LogicalScreenHeightPoints = ResolutionHeightPixels;

      this.ConvertHorizontalPtToPxFactor = (float)ResolutionWidthPixels / (float)LogicalScreenWidthPoints;
      if (ConvertHorizontalPtToPxFactor < 1.0F)
      {
        this.ConvertHorizontalPtToPxFactor = 1.0F;
        LogicalScreenWidthPoints = ResolutionWidthPixels;
      }

      this.ConvertVerticalPtToPxFactor = (float)ResolutionHeightPixels / (float)LogicalScreenHeightPoints;
      if (ConvertVerticalPtToPxFactor < 1.0F)
      {
        this.ConvertVerticalPtToPxFactor = 1.0F;
        LogicalScreenHeightPoints = ResolutionHeightPixels;
      }

      InvApplication.Window.Width = LogicalScreenWidthPoints;
      InvApplication.Window.Height = LogicalScreenHeightPoints;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        Windows.ApplicationModel.Core.CoreApplication.Exit();
        Stop();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var UwaTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new Windows.UI.Xaml.DispatcherTimer();
              Result.Tick += (Sender, Event) => InvTimer.IntervalInvoke();
              return Result;
            });

            if (UwaTimer.Interval != InvTimer.IntervalTime)
              UwaTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !UwaTimer.IsEnabled)
              UwaTimer.Start();
            else if (!InvTimer.IsEnabled && UwaTimer.IsEnabled)
              UwaTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurface = InvWindow.ActiveSurface;

        if (InvSurface != null)
        {
          var UwaSurface = AccessSurface(InvSurface, S =>
          {
            var Result = new UwaSurface();
            return Result;
          });

          if (!UwaMaster.Children.Contains(UwaSurface))
            InvSurface.ArrangeInvoke();

          ProcessTransition(UwaSurface);

          InvSurface.ComposeInvoke();

          if (InvSurface.Render())
          {
            UwaSurface.Border.Background = TranslateMediaBrush(InvSurface.Background.Colour);
            UwaSurface.Border.Child = TranslatePanel(InvSurface.Content);
          }

          InvSurface.ProcessChanges(P => TranslatePanel(P));

          if (InvSurface.Focus != null)
          {
            var UwaOverrideFocus = InvSurface.Focus.Node as UwaOverrideFocusContract;

            InvSurface.Focus = null;

            if (UwaOverrideFocus != null)
              UwaMaster.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => UwaOverrideFocus.OverrideFocus()).AsTask().Forget();
          }
        }
        else
        {
          UwaPage.Content = null;
        }

        if (InvWindow.Render())
        {
          //UwaWindow.Title = InvApplication.Title ?? "";

          //UwaMaster.Background = TranslateBrush(InvWindow.Background.Colour);
        }

        if (InvSurface != null)
          ProcessAnimation(InvSurface);

        InvWindow.DisplayRate.Calculate();
      }
    }
    private void ProcessTransition(UwaSurface UwaSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(UwaMaster.Children.Contains(UwaSurface));
      }
      else if (UwaMaster.Children.Count <= 1) // don't transition while animating a previous transition.
      {
        var UwaFromSurface = UwaMaster.Children.Count == 0 ? null : (UwaSurface)UwaMaster.Children[0];
        var UwaToSurface = UwaSurface;

        if (UwaFromSurface == UwaToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          switch (InvTransition.Animation)
          {
            case TransitionAnimation.None:
              UwaMaster.Children.Clear();
              UwaMaster.Children.Add(UwaToSurface);
              break;

            case TransitionAnimation.Fade:
              UwaMaster.Children.Add(UwaToSurface);

              UwaToSurface.IsHitTestVisible = false;
              UwaToSurface.Opacity = 0.0;

              var UwaFadeDuration = new Windows.UI.Xaml.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2));

              var UwaFadeOutStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();
              var UwaFadeInStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();

              var UwaFadeInAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                From = 0.0,
                To = 1.0,
                Duration = UwaFadeDuration
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeInAnimation, UwaToSurface);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeInAnimation, "Opacity");
              UwaFadeInStoryboard.Children.Add(UwaFadeInAnimation);
              UwaFadeInAnimation.Completed += (Sender, Event) =>
              {
                UwaToSurface.IsHitTestVisible = true;
                UwaToSurface.Opacity = 1.0;
              };

              if (UwaFromSurface == null)
              {
                UwaFadeInStoryboard.Begin();
              }
              else
              {
                UwaFromSurface.IsHitTestVisible = false;

                var UwaFadeOutAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                  From = 1.0,
                  To = 0.0,
                  Duration = UwaFadeDuration                  
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeOutAnimation, UwaFromSurface);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeOutAnimation, "Opacity");
                UwaFadeOutStoryboard.Children.Add(UwaFadeOutAnimation);
                UwaFadeOutAnimation.Completed += (Sender, Event) =>
                {
                  UwaFromSurface.IsHitTestVisible = true;
                  UwaFromSurface.Opacity = 1.0;
                  UwaMaster.Children.Remove(UwaFromSurface);

                  UwaFadeInStoryboard.Begin();
                };

                UwaFadeOutStoryboard.Begin();
              }
              break;

            case TransitionAnimation.CarouselBack:
            case TransitionAnimation.CarouselNext:
              var CarouselForward = InvTransition.Animation == TransitionAnimation.CarouselNext;

              var UwaCarouselDuration = new Windows.UI.Xaml.Duration(InvTransition.Duration);

              var UwaCarouselStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();

              if (UwaFromSurface != null)
              {
                var FromCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
                UwaFromSurface.RenderTransform = FromCarouselTransform;
                UwaFromSurface.IsHitTestVisible = false;

                var UwaFromAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  //AccelerationRatio = 0.5,
                  //DecelerationRatio = 0.5,
                  Duration = UwaCarouselDuration,
                  From = 0,
                  To = CarouselForward ? -UwaMaster.ActualWidth : UwaMaster.ActualWidth
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFromAnimation, FromCarouselTransform);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFromAnimation, "X");
                UwaCarouselStoryboard.Children.Add(UwaFromAnimation);
              }

              if (UwaToSurface != null)
              {
                var ToCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
                UwaToSurface.RenderTransform = ToCarouselTransform;
                UwaToSurface.IsHitTestVisible = false;

                UwaMaster.Children.Add(UwaToSurface);

                var UwaToAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
                {
                  //AccelerationRatio = 0.5,
                  //DecelerationRatio = 0.5,
                  Duration = UwaCarouselDuration,
                  From = CarouselForward ? UwaMaster.ActualWidth : -UwaMaster.ActualWidth,
                  To = 0
                };
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaToAnimation, ToCarouselTransform);
                Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaToAnimation, "X");
                UwaCarouselStoryboard.Children.Add(UwaToAnimation);
              }

              UwaCarouselStoryboard.Completed += (Sender, Event) =>
              {
                if (UwaFromSurface != null)
                {
                  UwaFromSurface.IsHitTestVisible = true;
                  UwaMaster.Children.Remove(UwaFromSurface);
                  UwaFromSurface.RenderTransform = null;
                }

                if (UwaToSurface != null)
                {
                  UwaToSurface.IsHitTestVisible = true;
                  UwaToSurface.RenderTransform = null;
                }
              };

              UwaCarouselStoryboard.Begin();
              break;

            default:
              throw new Exception("TransitionAnimation not handled: " + InvTransition.Animation);
          }
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessAnimation(Inv.Surface InvSurfaceActive)
    {
      if (InvSurfaceActive.StopAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StopAnimationSet)
        {
          if (InvAnimation.Node != null)
          {
            var WpfStoryboard = (Windows.UI.Xaml.Media.Animation.Storyboard)InvAnimation.Node;

            foreach (var InvTarget in InvAnimation.GetTargets())
            {
              var WpfPanel = TranslatePanel(InvTarget.Panel);
              if (WpfPanel != null)
              {
                WpfPanel.Opacity = (float)WpfPanel.Opacity;

                InvTarget.Panel.Opacity.BypassSet((float)WpfPanel.Opacity);
              }
            }

            WpfStoryboard.Stop();
          }
        }
        InvSurfaceActive.StopAnimationSet.Clear();
      }
      else if (InvSurfaceActive.StartAnimationSet.Count > 0)
      {
        foreach (var InvAnimation in InvSurfaceActive.StartAnimationSet)
        {
          var WpfStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();
          InvAnimation.Node = WpfStoryboard;
          WpfStoryboard.Completed += (Sender, Event) =>
          {
            InvAnimation.Complete();
            InvAnimation.Node = null;
          };

          foreach (var InvTarget in InvAnimation.GetTargets())
          {
            var InvPanel = InvTarget.Panel;
            var WpfPanel = TranslatePanel(InvPanel);
            foreach (var InvCommand in InvTarget.GetCommands())
              WpfStoryboard.Children.Add(TranslateAnimationCommand(InvPanel, WpfPanel, InvCommand));
          }

          WpfStoryboard.Begin();
        }
        InvSurfaceActive.StartAnimationSet.Clear();
      }
    }
    private Windows.UI.Xaml.Media.Animation.Timeline TranslateAnimationCommand(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      // TODO: more types of animation commands.
      var Result = TranslateAnimationOpacityCommand(InvPanel, WpfPanel, InvCommand);
      Result.Completed += (Sender, Event) => InvCommand.Complete();
      return Result;
    }
    private Windows.UI.Xaml.Media.Animation.Timeline TranslateAnimationOpacityCommand(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement WpfPanel, AnimationCommand InvCommand)
    {
      var InvOpacityCommand = (Inv.AnimationOpacityCommand)InvCommand;

      var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
      {
        From = InvOpacityCommand.From,
        To = InvOpacityCommand.To,
        Duration = InvOpacityCommand.Duration,
        FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvOpacityCommand.Offset != null)
        Result.BeginTime = InvOpacityCommand.Offset;

      Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, WpfPanel);
      Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, "Opacity");

      WpfPanel.Opacity = InvOpacityCommand.From;
      InvPanel.Opacity.BypassSet(InvOpacityCommand.To);

      Result.Completed += (Sender, Event) =>
      {
        WpfPanel.Opacity = InvPanel.Opacity.Get();
      };

      return Result;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var UwaButton = AccessPanel(InvButton, P =>
      {
        var Result = new UwaButton();
        Result.PointerEntered += (Sender, Event) =>
        {
          Result.Border.Background = TranslateMediaBrush(P.Background.Colour != null ? P.Background.Colour.Value.Lighten(0.25F) : (Inv.Colour?)null);
        };
        Result.PointerExited += (Sender, Event) =>
        {
          Result.Border.Background = TranslateMediaBrush(P.Background.Colour != null ? P.Background.Colour : null);
        };
        Result.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerPressedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
        {
          // NOTE: .PointerPressed += doesn't fire for buttons.
          Result.Border.Background = TranslateMediaBrush(P.Background.Colour != null ? P.Background.Colour.Value.Darken(0.25F) : (Inv.Colour?)null);
        }), true);
        Result.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerReleasedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
        {
          // NOTE: .PointerReleased += doesn't fire for buttons.
          Result.Border.Background = TranslateMediaBrush(P.Background.Colour);
        }), true);

        Result.Tapped += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.SingleTapInvoke();

          Event.Handled = true;
        };
        Result.RightTapped += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.ContextTapInvoke();

          Event.Handled = true;
        };
        return Result;
      });

      RenderPanel(InvButton, UwaButton, () =>
      {
        TranslateLayout(InvButton, UwaButton, UwaButton.Border);

        UwaButton.IsEnabled = InvButton.IsEnabled;
        //UwaButton.Button.Focusable = InvButton.IsFocused;

        if (InvButton.ContentSingleton.Render())
        {
          UwaButton.Border.Child = null; // detach previous content in case it has moved.
          UwaButton.Border.Child = TranslatePanel(InvButton.ContentSingleton.Data);
        }
      });

      return UwaButton;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var UwaCanvas = AccessPanel(InvCanvas, P =>
      {
        return new UwaCanvas();
      });

      RenderPanel(InvCanvas, UwaCanvas, () =>
      {
        TranslateLayout(InvCanvas, UwaCanvas.Border, UwaCanvas.Border);

        if (InvCanvas.PieceCollection.Render())
        {
          UwaCanvas.Canvas.Children.Clear();
          foreach (var InvElement in InvCanvas.PieceCollection)
          {
            var UwaElement = new Windows.UI.Xaml.Controls.Border();
            UwaCanvas.Canvas.Children.Add(UwaElement);
            Windows.UI.Xaml.Controls.Canvas.SetLeft(UwaElement, HorizontalPtToPx(InvElement.Rect.Left));
            Windows.UI.Xaml.Controls.Canvas.SetTop(UwaElement, VerticalPtToPx(InvElement.Rect.Top));
            UwaElement.Width = InvElement.Rect.Width;
            UwaElement.Height = InvElement.Rect.Height;

            var UwaPanel = TranslatePanel(InvElement.Panel);
            UwaElement.Child = UwaPanel;
          }
        }
      });

      return UwaCanvas;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var UwaDock = AccessPanel(InvDock, P =>
      {
        var Result = new UwaDock();
        return Result;
      });

      RenderPanel(InvDock, UwaDock, () =>
      {
        TranslateLayout(InvDock, UwaDock.Border, UwaDock.Border);

        if (InvDock.CollectionRender())
        {
          UwaDock.Grid.Children.Clear();
          UwaDock.Grid.RowDefinitions.Clear();
          UwaDock.Grid.ColumnDefinitions.Clear();

          var Position = 0;

          foreach (var InvHeader in InvDock.HeaderCollection)
          {
            var UwaPanel = TranslatePanel(InvHeader);
            UwaDock.Grid.Children.Add(UwaPanel);

            if (IsHorizontal)
            {
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
            }
            else
            {
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
            }

            Position++;
          }

          if (InvDock.HasClients())
          {
            foreach (var InvClient in InvDock.ClientCollection)
            {
              var UwaPanel = TranslatePanel(InvClient);
              UwaDock.Grid.Children.Add(UwaPanel);

              if (IsHorizontal)
              {
                UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
                Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
              }
              else
              {
                UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
                Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
              }

              Position++;
            }
          }
          else
          {
            if (IsHorizontal)
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });
            else
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = new Windows.UI.Xaml.GridLength(1.0, Windows.UI.Xaml.GridUnitType.Star) });

            Position++;
          }

          foreach (var InvFooter in InvDock.FooterCollection)
          {
            var UwaPanel = TranslatePanel(InvFooter);
            UwaDock.Grid.Children.Add(UwaPanel);

            if (IsHorizontal)
            {
              UwaDock.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaPanel, Position);
            }
            else
            {
              UwaDock.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = Windows.UI.Xaml.GridLength.Auto });
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaPanel, Position);
            }

            Position++;
          }
        }
      });

      return UwaDock;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var UwaEdit = AccessPanel(InvEdit, P =>
      {
        var Result = new UwaEdit();
        Result.TextBox.TextChanged += (Sender, Event) => P.ChangeInvoke(Result.TextBox.Text);
        return Result;
      });

      RenderPanel(InvEdit, UwaEdit, () =>
      {
        TranslateLayout(InvEdit, UwaEdit.Border, UwaEdit.Border);
        TranslateFont(InvEdit.Font, UwaEdit.TextBox);

        UwaEdit.TextBox.IsReadOnly = InvEdit.IsReadOnly;
        UwaEdit.TextBox.Text = InvEdit.Text ?? "";
      });

      return UwaEdit;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var UwaGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new UwaGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, UwaGraphic, () =>
      {
        var SizeChanged = InvGraphic.Size.IsChanged;

        TranslateLayout(InvGraphic, UwaGraphic.Border, UwaGraphic.Border);

        if (InvGraphic.ImageSingleton.Render() || SizeChanged)
          UwaGraphic.Image.Source = TranslateMediaImage(InvGraphic.Image);
      });

      return UwaGraphic;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var UwaLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new UwaLabel();
        return Result;
      });

      RenderPanel(InvLabel, UwaLabel.Border, () =>
      {
        TranslateLayout(InvLabel, UwaLabel.Border, UwaLabel.Border);
        TranslateFont(InvLabel.Font, UwaLabel.TextBlock);

        UwaLabel.TextBlock.TextWrapping = InvLabel.LineWrapping ? Windows.UI.Xaml.TextWrapping.Wrap : Windows.UI.Xaml.TextWrapping.NoWrap;
        UwaLabel.TextBlock.TextAlignment = InvLabel.Justification == null || InvLabel.Justification == Justification.Left ? Windows.UI.Xaml.TextAlignment.Left : InvLabel.Justification == Justification.Right ? Windows.UI.Xaml.TextAlignment.Right : Windows.UI.Xaml.TextAlignment.Center;
        UwaLabel.TextBlock.Text = InvLabel.Text ?? "";
      });

      return UwaLabel;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var UwaMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new UwaMemo();
        Result.TextBox.TextChanged += (Sender, Event) => P.ChangeInvoke(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, UwaMemo, () =>
      {
        var IsAligning = InvMemo.Alignment.IsChanged;

        TranslateLayout(InvMemo, UwaMemo.Border, UwaMemo.Border);

        if (IsAligning)
        {
          // NOTE: this is a workaround to get the textbox to autosize properly.

          UwaMemo.TextBox.HorizontalAlignment = UwaMemo.Border.HorizontalAlignment;
          UwaMemo.TextBox.VerticalAlignment = UwaMemo.Border.VerticalAlignment;
        }

        TranslateFont(InvMemo.Font, UwaMemo.TextBox);

        UwaMemo.TextBox.IsReadOnly = InvMemo.IsReadOnly;
        UwaMemo.Text = InvMemo.Text ?? "";
      });

      return UwaMemo;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var UwaOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new UwaOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, UwaOverlay, () =>
      {
        TranslateLayout(InvOverlay, UwaOverlay.Border, UwaOverlay.Border);

        if (InvOverlay.PanelCollection.Render())
        {
          UwaOverlay.Grid.Children.Clear();
          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var UwaPanel = TranslatePanel(InvElement);
            UwaOverlay.Grid.Children.Add(UwaPanel);
          }
        }
      });

      return UwaOverlay;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateRender(Inv.Panel InvPanel)
    {
      var InvRender = (Inv.Render)InvPanel;

      var UwaRender = AccessPanel(InvRender, P =>
      {
        var Result = new UwaRender();

        var IsLeftPressed = false;
        var PressedTimestamp = DateTime.Now;

        Result.PointerMoved += (Sender, Event) =>
        {
          var MovedPoint = Event.GetCurrentPoint(Result);
          P.MoveInvoke(TranslatePoint(MovedPoint.Position));
        };
        Result.PointerPressed += (Sender, Event) =>
        {
          Result.CapturePointer(Event.Pointer);

          var PressedPoint = Event.GetCurrentPoint(Result);

          if (PressedPoint.PointerDevice.PointerDeviceType != Windows.Devices.Input.PointerDeviceType.Mouse || PressedPoint.Properties.IsLeftButtonPressed)
          {
            IsLeftPressed = true;

            PressedTimestamp = DateTime.Now;

            P.PressInvoke(TranslatePoint(PressedPoint.Position));
          }
        };
        Result.PointerReleased += (Sender, Event) =>
        {
          Result.ReleasePointerCapture(Event.Pointer);

          var ReleasedPoint = Event.GetCurrentPoint(Result);
          var ReleasePoint = TranslatePoint(ReleasedPoint.Position);

          P.ReleaseInvoke(ReleasePoint);

          if (IsLeftPressed)
          {
            IsLeftPressed = false;

            if (DateTime.Now - PressedTimestamp <= TimeSpan.FromMilliseconds(250))
              P.SingleTapInvoke(ReleasePoint);
          }
        };
        Result.RightTapped += (Sender, Event) =>
        {
          var RightTappedPoint = Event.GetPosition(Result);

          P.ContextTapInvoke(TranslatePoint(RightTappedPoint));

          IsLeftPressed = false;

          Event.Handled = true;
        };
        Result.DoubleTapped += (Sender, Event) =>
        {
          var DoubleTappedPoint = Event.GetPosition(Result);

          P.DoubleTapInvoke(TranslatePoint(DoubleTappedPoint));

          IsLeftPressed = false;

          Event.Handled = true;
        };
        // TODO: touch pinch and zoom?
        Result.PointerWheelChanged += (Sender, Event) =>
        {
          var WheelChangedPoint = Event.GetCurrentPoint(Result);

          P.ZoomInvoke(WheelChangedPoint.Properties.MouseWheelDelta > 0 ? +1 : -1);
        };
        Result.ManipulationDelta += (Sender, Event) =>
        {
          P.ZoomInvoke(Event.Delta.Scale > 0 ? +1 : -1);
        };
        Result.ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.Scale;

        var ImageSourceRect = new Windows.Foundation.Rect();
        var ImageDestinationRect = new Windows.Foundation.Rect();

        Result.ResetEvent += () =>
        {
          CanvasBitmapDictionary.Clear();
        };
        Result.DrawEvent += (DS) =>
        {
          //DS.Antialiasing = Microsoft.Graphics.Canvas.CanvasAntialiasing.Aliased;

          foreach (var InvRenderElement in InvRender.GetCommands())
          {
            switch (InvRenderElement.Type)
            {
              case RenderType.Rectangle:
                var RectangleRect = InvRenderElement.RectangleRect;
                var RectangleFillColour = InvRenderElement.RectangleFillColour;
                var RectangleStrokeColour = InvRenderElement.RectangleStrokeColour;
                var RectangleStrokeThickness = InvRenderElement.RectangleStrokeThickness;

                var RectangleX = HorizontalPtToPx(RectangleRect.Left);
                var RectangleY = VerticalPtToPx(RectangleRect.Top);
                var RectangleWidth = HorizontalPtToPx(RectangleRect.Width);
                var RectangleHeight = VerticalPtToPx(RectangleRect.Height);

                if (InvRenderElement.RectangleFillColour != null)
                  DS.FillRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, TranslateMediaColour(InvRenderElement.RectangleFillColour.Value));

                if (InvRenderElement.RectangleStrokeColour != null && RectangleStrokeThickness > 0)
                {
                  var StrokePixels = VerticalPtToPx(RectangleStrokeThickness);

                  RectangleX += StrokePixels / 2;
                  RectangleY += StrokePixels / 2;

                  if (RectangleWidth >= StrokePixels)
                    RectangleWidth -= StrokePixels;

                  if (RectangleHeight >= StrokePixels)
                    RectangleHeight -= StrokePixels;

                  DS.DrawRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, TranslateMediaColour(InvRenderElement.RectangleStrokeColour.Value), StrokePixels);
                }
                break;

              case RenderType.Ellipse:
                var EllipseCenter = InvRenderElement.EllipseCenter;
                var EllipseRadius = InvRenderElement.EllipseRadius;
                var EllipseFillColour = InvRenderElement.EllipseFillColour;
                var EllipseStrokeColour = InvRenderElement.EllipseStrokeColour;
                var EllipseStrokeThickness = InvRenderElement.EllipseStrokeThickness;

                var EllipseX = HorizontalPtToPx(EllipseCenter.X);
                var EllipseY = VerticalPtToPx(EllipseCenter.Y);
                var EllipseRadiusX = HorizontalPtToPx(EllipseRadius.X);
                var EllipseRadiusY = VerticalPtToPx(EllipseRadius.Y);

                if (InvRenderElement.EllipseFillColour != null)
                  DS.FillEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, TranslateMediaColour(InvRenderElement.EllipseFillColour.Value));

                if (InvRenderElement.EllipseStrokeColour != null && InvRenderElement.EllipseStrokeThickness > 0)
                  DS.DrawEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, TranslateMediaColour(InvRenderElement.EllipseStrokeColour.Value), VerticalPtToPx(InvRenderElement.EllipseStrokeThickness));
                break;

              case RenderType.Text:
                var TextPoint = InvRenderElement.TextPoint;

                var TextFormat = new Microsoft.Graphics.Canvas.Text.CanvasTextFormat()
                {
                  FontSize = TranslateFontSize(InvRenderElement.TextFontSize),
                  WordWrapping = Microsoft.Graphics.Canvas.Text.CanvasWordWrapping.NoWrap,
                  FontWeight = TranslateFontWeight(InvRenderElement.TextFontWeight)
                };

                var TextLayout = new Microsoft.Graphics.Canvas.Text.CanvasTextLayout(DS, InvRenderElement.TextFragment, TextFormat, 0.0f, 0.0f);

                var TextX = HorizontalPtToPx(TextPoint.X) - (int)TextLayout.DrawBounds.X;
                var TextY = HorizontalPtToPx(TextPoint.Y) - (int)TextLayout.DrawBounds.Y;

                var TextHorizontal = InvRenderElement.TextHorizontal;
                var TextVertical = InvRenderElement.TextVertical;

                if (TextHorizontal != HorizontalPosition.Left)
                {
                  var TextWidth = (int)TextLayout.DrawBounds.Width;

                  if (TextHorizontal == HorizontalPosition.Right)
                    TextX -= TextWidth;
                  else if (TextHorizontal == HorizontalPosition.Center)
                    TextX -= TextWidth / 2;
                }

                if (TextVertical != VerticalPosition.Top)
                {
                  var TextHeight = (int)TextLayout.DrawBounds.Height;

                  if (TextVertical == VerticalPosition.Bottom)
                    TextY -= TextHeight;
                  else if (TextVertical == VerticalPosition.Center)
                    TextY -= TextHeight / 2;
                }

                DS.DrawTextLayout(TextLayout, TextX, TextY, TranslateMediaColour(InvRenderElement.TextFontColour.Value));
                break;

              case RenderType.Image:
                var ImageRect = InvRenderElement.ImageRect;
                var ImageOpacity = InvRenderElement.ImageOpacity;
                var ImageTint = InvRenderElement.ImageTint;
                var ImageMirror = InvRenderElement.ImageMirror;
                var ImageBitmap = TranslateCanvasBitmap(DS, InvRenderElement.ImageSource);

                ImageDestinationRect.X = HorizontalPtToPx(ImageRect.Left);
                ImageDestinationRect.Y = HorizontalPtToPx(ImageRect.Top);
                ImageDestinationRect.Width = HorizontalPtToPx(ImageRect.Width);
                ImageDestinationRect.Height = HorizontalPtToPx(ImageRect.Height);

                var ImageBounds = ImageBitmap.GetBounds(DS);
                ImageSourceRect.Width = ImageBounds.Width;
                ImageSourceRect.Height = ImageBounds.Height;

                if (ImageTint != null)
                {
                  var TintTarget = new Microsoft.Graphics.Canvas.CanvasRenderTarget(DS, (int)ImageBounds.Width, (int)ImageBounds.Height);
                  using (var TintSession = TintTarget.CreateDrawingSession())
                  {
                    TintSession.Clear(Windows.UI.Colors.Transparent);
                    TintSession.DrawImage(ImageBitmap, ImageBounds, ImageBounds);
                    TintSession.FillRectangle(ImageBounds, new Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush(TintSession, TranslateMediaColour(ImageTint.Value)), new Microsoft.Graphics.Canvas.Brushes.CanvasImageBrush(TintSession, ImageBitmap));
                  }

                  ImageBitmap = TintTarget;
                }

                if (ImageMirror != null)
                {
                  ImageBitmap = new Microsoft.Graphics.Canvas.Effects.Transform2DEffect()
                  {
                    Source = ImageBitmap,
                    TransformMatrix = System.Numerics.Matrix3x2.CreateScale(ImageMirror.Value == Mirror.Horizontal ? -1 : +1, ImageMirror.Value == Mirror.Vertical ? -1 : +1, new System.Numerics.Vector2((float)ImageBounds.Width / 2.0F, (float)ImageBounds.Height / 2.0F))
                  };
                }

                DS.DrawImage(ImageBitmap, ImageDestinationRect, ImageSourceRect, InvRenderElement.ImageOpacity);
                break;

              default:
                throw new Exception("RenderType not handled: " + InvRenderElement.Type);
            }
          }
        };

        return Result;
      });

      RenderPanel(InvRender, UwaRender, () =>
      {
        TranslateLayout(InvRender, UwaRender.Border, UwaRender.Border);

        InvRender.ContextSingleton.Data.Width = HorizontalPxToPt((int)UwaRender.ActualWidth);
        InvRender.ContextSingleton.Data.Height = VerticalPxToPt((int)UwaRender.ActualHeight);
        if (InvRender.ContextSingleton.Render())
          UwaRender.Canvas.Invalidate();
      });

      return UwaRender;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var UwaScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new UwaScroll(P.Orientation == ScrollOrientation.Vertical, P.Orientation == ScrollOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvScroll, UwaScroll, () =>
      {
        TranslateLayout(InvScroll, UwaScroll.Border, UwaScroll.Border);

        if (InvScroll.ContentSingleton.Render())
          UwaScroll.ScrollViewer.Content = TranslatePanel(InvScroll.ContentSingleton.Data);
      });

      return UwaScroll;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var UwaFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new Windows.UI.Xaml.Controls.Border();
        return Result;
      });

      RenderPanel(InvFrame, UwaFrame, () =>
      {
        TranslateLayout(InvFrame, UwaFrame, UwaFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          UwaFrame.Child = null; // detach previous content in case it has moved.
          UwaFrame.Child = TranslatePanel(InvFrame.ContentSingleton.Data);
        }
      });

      return UwaFrame;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var UwaStack = AccessPanel(InvStack, P =>
      {
        var Result = new UwaStack();
        Result.StackPanel.Orientation = P.Orientation == Inv.StackOrientation.Horizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;
        return Result;
      });

      RenderPanel(InvStack, UwaStack, () =>
      {
        TranslateLayout(InvStack, UwaStack.Border, UwaStack.Border);

        if (InvStack.PanelCollection.Render())
        {
          UwaStack.StackPanel.Children.Clear();
          foreach (var InvElement in InvStack.GetPanels())
          {
            var UwaPanel = TranslatePanel(InvElement);
            UwaStack.StackPanel.Children.Add(UwaPanel);
          }
        }
      });

      return UwaStack;
    }
    private Windows.UI.Xaml.FrameworkElement TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var UwaTable = AccessPanel(InvTable, P =>
      {
        var Result = new UwaTable();
        return Result;
      });

      RenderPanel(InvTable, UwaTable, () =>
      {
        TranslateLayout(InvTable, UwaTable.Border, UwaTable.Border);

        if (InvTable.CollectionRender())
        {
          UwaTable.Grid.Children.Clear();
          UwaTable.Grid.RowDefinitions.Clear();
          UwaTable.Grid.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            UwaTable.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn.Length, true) });

            var UwaColumn = TranslatePanel(TableColumn.Content);

            if (UwaColumn != null)
            {
              UwaTable.Grid.Children.Add(UwaColumn);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaColumn, TableColumn.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaColumn, 0);
              Windows.UI.Xaml.Controls.Grid.SetRowSpan(UwaColumn, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            UwaTable.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = TranslateTableLength(TableRow.Length, false) });

            var UwaRow = TranslatePanel(TableRow.Content);

            if (UwaRow != null)
            {
              UwaTable.Grid.Children.Add(UwaRow);

              Windows.UI.Xaml.Controls.Grid.SetRow(UwaRow, TableRow.Index);
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaRow, 0);
              Windows.UI.Xaml.Controls.Grid.SetColumnSpan(UwaRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var UwaCell = TranslatePanel(TableCell.Content);

            if (UwaCell != null)
            {
              UwaTable.Grid.Children.Add(UwaCell);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaCell, TableCell.Column.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaCell, TableCell.Row.Index);
            }
          }
        }
      });

      return UwaTable;
    }

    private UwaSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, UwaSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (UwaSurface)InvSurface.Node;
      }
    }
    private Windows.UI.Xaml.FrameworkElement TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel);
    }
    private void RenderPanel(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement UwaPanel, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : Windows.UI.Xaml.FrameworkElement
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }
    private Windows.UI.Xaml.DispatcherTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, Windows.UI.Xaml.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (Windows.UI.Xaml.DispatcherTimer)InvTimer.Node;
      }
    }
    private void TranslateLayout(Inv.Panel InvPanel, Windows.UI.Xaml.FrameworkElement UwaControl, Windows.UI.Xaml.Controls.Border UwaBorder)
    {
      if (InvPanel.Background.Render())
        UwaBorder.Background = TranslateMediaBrush(InvPanel.Background.Colour);

      if (InvPanel.CornerRadius.Render())
        UwaBorder.CornerRadius = TranslateCornerRadius(InvPanel.CornerRadius);

      var InvBorder = InvPanel.Border;
      if (InvBorder.Render())
      {
        UwaBorder.BorderBrush = TranslateMediaBrush(InvBorder.Colour);
        UwaBorder.BorderThickness = TranslateEdge(InvBorder.Thickness);
      }

      var InvMargin = InvPanel.Margin;
      if (InvMargin.Render())
        UwaControl.Margin = TranslateEdge(InvMargin);

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render())
        UwaBorder.Padding = TranslateEdge(InvPadding);

      //var InvElevation = InvPanel.Elevation;
      //if (InvElevation.Render())
      //{
      //  if (InvElevation.Depth == 0 || InvElevation.Colour == null)
      //  {
      //    UwaControl.Effect = null;
      //  }
      //  else
      //  {
      //    UwaControl.Effect = new System.Windows.Media.Effects.DropShadowEffect()
      //    {
      //      Direction = 315,
      //      Color = TranslateColour(InvElevation.Colour),
      //      ShadowDepth = VerticalPtToPx(InvElevation.Depth),
      //      Opacity = InvElevation.Colour.GetARGBRecord().A / 255.0F
      //    };
      //  }
      //}
      
      var InvOpacity = InvPanel.Opacity;
      if (InvOpacity.Render())
        UwaControl.Opacity = InvOpacity.Get();

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        UwaControl.Visibility = InvVisibility.Get() ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;

      TranslateSize(InvPanel.Size, UwaControl);

      TranslateAlignment(InvPanel.Alignment, UwaControl);
    }
    private Windows.UI.Xaml.CornerRadius TranslateCornerRadius(Inv.CornerRadius InvCornerRadius)
    {
      return new Windows.UI.Xaml.CornerRadius(HorizontalPtToPx(InvCornerRadius.TopLeft), VerticalPtToPx(InvCornerRadius.TopRight), HorizontalPtToPx(InvCornerRadius.BottomRight), VerticalPtToPx(InvCornerRadius.BottomLeft));
    }
    private void TranslateSize(Inv.Size InvSize, Windows.UI.Xaml.FrameworkElement UwaElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          UwaElement.Width = HorizontalPtToPx(InvSize.Width.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.WidthProperty);

        if (InvSize.Height != null)
          UwaElement.Height = VerticalPtToPx(InvSize.Height.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.HeightProperty);

        if (InvSize.MinimumWidth != null)
          UwaElement.MinWidth = HorizontalPtToPx(InvSize.MinimumWidth.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          UwaElement.MinHeight = VerticalPtToPx(InvSize.MinimumHeight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          UwaElement.MaxWidth = HorizontalPtToPx(InvSize.MaximumWidth.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          UwaElement.MaxHeight = VerticalPtToPx(InvSize.MaximumHeight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.FrameworkElement.MaxHeightProperty);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, Windows.UI.Xaml.FrameworkElement UwaElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          default:
            throw new Exception("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private Windows.UI.Xaml.Media.Brush TranslateMediaBrush(Inv.Colour? InvColour)
    {
      if (InvColour == null)
        return null;

      return ColourBrushDictionary.GetOrAdd(InvColour.Value, C =>
      {
        var Result = new Windows.UI.Xaml.Media.SolidColorBrush(TranslateMediaColour(C));
        return Result;
      });
    }
    private Windows.UI.Color TranslateMediaColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return Windows.UI.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    private Windows.Foundation.Rect TranslateRect(Inv.Rect InvRect)
    {
      return new Windows.Foundation.Rect(HorizontalPtToPx(InvRect.Left), VerticalPtToPx(InvRect.Top), HorizontalPtToPx(InvRect.Width), VerticalPtToPx(InvRect.Height));
    }
    private Windows.Foundation.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new Windows.Foundation.Point(HorizontalPtToPx(InvPoint.X), VerticalPtToPx(InvPoint.Y));
    }
    private Inv.Point TranslatePoint(Windows.Foundation.Point UwaPoint)
    {
      return new Inv.Point(HorizontalPxToPt((int)UwaPoint.X), VerticalPxToPt((int)UwaPoint.Y));
    }
    private Windows.UI.Xaml.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new Windows.UI.Xaml.Thickness(HorizontalPtToPx(InvEdge.Left), VerticalPtToPx(InvEdge.Top), HorizontalPtToPx(InvEdge.Right), VerticalPtToPx(InvEdge.Bottom));
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBlock UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          UwaElement.Foreground = TranslateMediaBrush(InvFont.Colour);
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.RichEditBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = TranslateFontSize(InvFont.Size.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight);
      }
    }
    private Windows.UI.Text.FontWeight TranslateFontWeight(FontWeight InvFontWeight)
    {
      switch (InvFontWeight)
      {
        case FontWeight.Thin:
          return Windows.UI.Text.FontWeights.Thin;

        case FontWeight.Light:
          return Windows.UI.Text.FontWeights.Light;

        case FontWeight.Regular:
          return Windows.UI.Text.FontWeights.Normal;

        case FontWeight.Medium:
          return Windows.UI.Text.FontWeights.Medium;

        case FontWeight.Bold:
          return Windows.UI.Text.FontWeights.Bold;

        case FontWeight.Black:
          return Windows.UI.Text.FontWeights.Black;

        default:
          throw new Exception("FontWeight not handled: " + InvFontWeight);
      }
    }
    private int TranslateFontSize(int FontSize)
    {
      return (int)(FontSize * ConvertVerticalPtToPxFactor + 0.5F);
    }
    private Windows.UI.Xaml.GridLength TranslateTableLength(TableLength InvTableLength, bool Horizontal)
    {
      switch (InvTableLength.Type)
      {
        case TableLengthType.Auto:
          return Windows.UI.Xaml.GridLength.Auto;

        case TableLengthType.Fixed:
          return new Windows.UI.Xaml.GridLength(Horizontal ? HorizontalPtToPx(InvTableLength.Value) : VerticalPtToPx(InvTableLength.Value), Windows.UI.Xaml.GridUnitType.Pixel);

        case TableLengthType.Star:
          return new Windows.UI.Xaml.GridLength(InvTableLength.Value, Windows.UI.Xaml.GridUnitType.Star);

        default:
          throw new Exception("Inv.TableLength not handled: " + InvTableLength.Type);
      }
    }
    private Microsoft.Graphics.Canvas.Effects.Matrix5x4 MakeColorMatrix(Windows.UI.Color color)
    {
      return new Microsoft.Graphics.Canvas.Effects.Matrix5x4()
      {
        M11 = color.R, M12 = 0,       M13 = 0,       M14 = 0,
        M21 = 0,       M22 = color.G, M23 = 0,       M24 = 0,
        M31 = 0,       M32 = 0,       M33 = color.B, M34 = 0,
        M41 = 0,       M42 = 0,       M43 = 0,       M44 = color.A,
        M51 = 0,       M52 = 0,       M53 = 0,       M54 = 0,
      };
    }
    private Inv.Key? TranslateKey(Windows.System.VirtualKey UwaKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(UwaKey, out Result))
        return Result;
      else
        return null;
    }
    private Windows.UI.Xaml.Media.Imaging.BitmapSource TranslateMediaImage(Inv.Image? InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        return MediaImageDictionary.GetOrAdd(InvImage.Value, K =>
        {
          var Task = LoadMediaImage(K);
          Task.Wait();
          return Task.Result;
        });
      }
    }
    private async Task<Windows.UI.Xaml.Media.Imaging.BitmapImage> LoadMediaImage(Inv.Image InvImage)
    {
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
        {
          // Write the bytes to the stream
          DataWriter.WriteBytes(InvImage.GetBuffer());

          // Store the bytes to the MemoryStream
          await DataWriter.StoreAsync();

          // Not necessary, but do it anyway
          await DataWriter.FlushAsync();

          // Detach from the Memory stream so we don't close it
          DataWriter.DetachStream();
        }

        MemoryStream.Seek(0);

        var Result = new Windows.UI.Xaml.Media.Imaging.BitmapImage();

        Result.SetSource(MemoryStream);

        return Result;
      }
    }
    private Microsoft.Graphics.Canvas.ICanvasImage TranslateCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image? InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        return CanvasBitmapDictionary.GetOrAdd(InvImage.Value, K =>
        {
          var ImageTask = LoadCanvasBitmap(DS, InvImage.Value);
          ImageTask.Wait();
          return ImageTask.Result;
        });
      }
    }
    private async Task<Microsoft.Graphics.Canvas.CanvasBitmap> LoadCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image InvImage)
    {
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
        {
          // Write the bytes to the stream
          DataWriter.WriteBytes(InvImage.GetBuffer());

          // Store the bytes to the MemoryStream
          await DataWriter.StoreAsync();

          // Not necessary, but do it anyway
          await DataWriter.FlushAsync();

          // Detach from the Memory stream so we don't close it
          DataWriter.DetachStream();
        }

        MemoryStream.Seek(0);

        //return await Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96);

        var LoadTask = Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, 96).AsTask();
        LoadTask.Wait();
        return LoadTask.Result;
      }
    }
    private int HorizontalPtToPx(int Points)
    {
      return (int)(Points * ConvertHorizontalPtToPxFactor + 0.5F);
    }
    private int VerticalPtToPx(int Points)
    {
      return (int)(Points * ConvertVerticalPtToPxFactor + 0.5F);
    }
    private int HorizontalPxToPt(int Pixels)
    {
      return (int)(Pixels / ConvertHorizontalPtToPxFactor + 0.5F);
    }
    private int VerticalPxToPt(int Pixels)
    {
      return (int)(Pixels / ConvertVerticalPtToPxFactor + 0.5F);
    }
    private void Rendering(object Sender, object Event)
    {
      Process();
    }
    private void Suspending(object Sender, object Event)
    {
      InvApplication.SuspendInvoke();
    }
    private void Resuming(object Sender, object Event)
    {
      InvApplication.ResumeInvoke();
    }
    private void Exiting(object Sender, object Event)
    {
      // TODO: this is not running in the UI thread... so we have to wait for it to run before exiting.
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => InvApplication.StopInvoke()).AsTask().Wait();
    }
    private void SizeChanged(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.WindowSizeChangedEventArgs Event)
    {
      Resize();

      var InvSurface = InvApplication.Window.ActiveSurface;
      if (InvSurface != null)
        InvSurface.ArrangeInvoke();
    }
    private void PointerPressed(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      if (Event.CurrentPoint.Properties.IsXButton1Pressed)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
          InvSurface.GestureBackwardInvoke();
      }
      else if (Event.CurrentPoint.Properties.IsXButton2Pressed)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
          InvSurface.GestureForwardInvoke();
      }
      else
      {
        var PointerPosition = Event.CurrentPoint.Position;

        LeftEdgeSwipe = PointerPosition.X <= 10;
        RightEdgeSwipe = PointerPosition.X >= UwaPage.ActualWidth - 10;
      }
    }
    private void PointerReleased(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      LeftEdgeSwipe = false;
      RightEdgeSwipe = false;
    }
    private void PointerMoved(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      if (LeftEdgeSwipe || RightEdgeSwipe)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          var PointerPosition = Event.CurrentPoint.Position;

          if (LeftEdgeSwipe && PointerPosition.X >= 0)
          {
            if (PointerPosition.X >= 20)
            {
              LeftEdgeSwipe = false;
              InvSurface.GestureBackwardInvoke();
            }
          }
          else if (RightEdgeSwipe && PointerPosition.X <= UwaPage.ActualWidth)
          {
            if (PointerPosition.X <= UwaPage.ActualWidth - 20)
            {
              RightEdgeSwipe = false;
              InvSurface.GestureForwardInvoke();
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        }
        else
        {
          LeftEdgeSwipe = false;
          RightEdgeSwipe = false;
        }

        Event.Handled = true;
      }
    }
    private void UnhandledErrorDetected(object Sender, Windows.ApplicationModel.Core.UnhandledErrorDetectedEventArgs Event)
    {
      // Unhandled errors result in the app being terminated once this event propagates to the Windows Runtime system.
      if (!Event.UnhandledError.Handled)
      {
        try
        {
          Event.UnhandledError.Propagate();
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);
        }
      }
    }

    private Windows.UI.Xaml.Controls.Page UwaPage;
    private Windows.UI.Xaml.Controls.Grid UwaMaster;
    private Application InvApplication;
    private Dictionary<Type, Func<Inv.Panel, Windows.UI.Xaml.FrameworkElement>> RouteDictionary;
    private Dictionary<Inv.Colour, Windows.UI.Xaml.Media.Brush> ColourBrushDictionary;
    private float ConvertHorizontalPtToPxFactor;
    private float ConvertVerticalPtToPxFactor;
    private Dictionary<Windows.System.VirtualKey, Key> KeyDictionary;
    private Dictionary<Inv.Image, Windows.UI.Xaml.Media.Imaging.BitmapSource> MediaImageDictionary;
    private Dictionary<Inv.Image, Microsoft.Graphics.Canvas.ICanvasImage> CanvasBitmapDictionary;
    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;
  }
}