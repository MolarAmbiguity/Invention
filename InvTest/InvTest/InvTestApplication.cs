﻿/*! 82 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

namespace InvTest
{
  public sealed class Application
  {
    public Application()
    {
      this.LogoImage = Resources.Images.PhoenixLogo960x540;

      this.Base = new Inv.Material.Application();
      Base.Title = "InvTest";
      Base.Window.PrimaryColour = Inv.Colour.HotPink;
      Base.HandleExceptionEvent += (Exception) =>
      {
        Debug.WriteLine(Exception.StackTrace);
      };
      Base.StartEvent += () => Compile(CustomRender);
      Base.ExitQuery += () =>
      {
        var LastSurface = Base.Window.ActiveSurface;

        var CloseSurface = Base.Window.NewCustomSurface();
        CloseSurface.Background.Colour = Inv.Colour.Black;

        var CloseDock = CloseSurface.NewVerticalDock();
        CloseSurface.Content = CloseDock;
        CloseDock.Alignment.Center();

        var CloseButton = CloseSurface.NewButton();
        CloseDock.AddHeader(CloseButton);
        CloseButton.Background.Colour = Inv.Colour.Red;
        CloseButton.Padding.Set(20);
        CloseButton.Margin.Set(20);
        CloseButton.SingleTapEvent += () => Base.Exit();

        var CloseLabel = CloseSurface.NewLabel();
        CloseButton.Content = CloseLabel;
        CloseLabel.Font.Size = 40;
        CloseLabel.Font.Colour = Inv.Colour.White;
        CloseLabel.JustifyCenter();
        CloseLabel.Text = "CLOSE";

        var CancelButton = CloseSurface.NewButton();
        CloseDock.AddHeader(CancelButton);
        CancelButton.Background.Colour = Inv.Colour.WhiteSmoke;
        CancelButton.Padding.Set(20);
        CancelButton.Margin.Set(20);
        CancelButton.SingleTapEvent += () => Base.Window.Transition(LastSurface).Fade();

        var CancelLabel = CloseSurface.NewLabel();
        CancelButton.Content = CancelLabel;
        CancelLabel.Font.Size = 40;
        CancelLabel.Font.Colour = Inv.Colour.Black;
        CancelLabel.JustifyCenter();
        CancelLabel.Text = "CANCEL";

        Base.Window.Transition(CloseSurface).Fade();

        return false;
      };
    }

    private Inv.Panel ActionFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var ActionPanel = new ActionPanel(Surface);
      Flyout.AddPanel(ActionPanel);
      ActionPanel.Alignment.BottomStretch();

      var LeftButton = ActionPanel.AddLeftButton();
      LeftButton.Caption = "LEFTY";

      var RightButton = ActionPanel.AddRightButton();
      RightButton.Caption = "RIGHTY";

      return Flyout;
    }
    private Inv.Panel Animations(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 30;
      StartLabel.JustifyCenter();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 30;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.JustifyCenter();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 30;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.JustifyCenter();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.LightBlue;
      TargetButton.Padding.Set(20);

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.JustifyCenter();
      TargetLabel.Text = "ANIMATION TARGET";

      var Animation = (Inv.Animation)null;

      StartButton.SingleTapEvent += () =>
      {
        Animation = Surface.NewAnimation();

        var Target = Animation.AddTarget(TargetButton);
        Target.FadeOpacity(1.0F, 0.0F, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));

        Animation.Start();
      };

      StopButton.SingleTapEvent += () =>
      {
        if (Animation != null)
        {
          Animation.Stop();
          Animation = null;
        }
      };

      RestartButton.SingleTapEvent += () =>
      {
        StopButton.SingleTap();
        StartButton.SingleTap();
      };

      TargetButton.SingleTapEvent += () =>
      {
        if (Animation.IsActive)
        {
          Animation.Stop();
        }
        else
        {
          Animation.RemoveTargets();
          var Target = Animation.AddTarget(TargetButton);
          var Opacity = TargetLabel.Opacity.Get();

          if (Opacity < 1.0F)
            Target.FadeOpacity(Opacity, 1.0F, TimeSpan.FromSeconds(1.0F * (1.0F - Opacity)));
          else
            Target.FadeOpacity(Opacity, 0.0F, TimeSpan.FromSeconds(1.0F * Opacity));

          Animation.Start();
        }
      };
      return Overlay;
    }
    private Inv.Panel HelpFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Memo = Surface.NewMemo();
      Flyout.AddPanel(Memo);
      Memo.Background.Colour = Inv.Colour.Black;
      Memo.Alignment.TopLeft();
      Memo.Margin.Set(50);
      Memo.Padding.Set(50);
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Font.Size = 20;
      Memo.IsReadOnly = true;
      Memo.Text = "1. This is some help\n2. It is multi line\n3. The rest should be obvious\n4. Read this all again and it makes sense\n5. Last line";

      var Button = Surface.NewButton();
      Flyout.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Blue;
      Button.Size.SetHeight(200);
      Button.Alignment.BottomStretch();
      Button.SingleTapEvent += () =>
      {
        Surface.Rearrange();

        //var FadeOutAnimation = Surface.NewAnimation();
        //FadeOutAnimation.AddTarget(Memo).FadeOpacityOut(TimeSpan.FromSeconds(1));
        //FadeOutAnimation.Start();
      };

      var FadeInAnimation = Surface.NewAnimation();
      FadeInAnimation.AddTarget(Memo).FadeOpacityIn(TimeSpan.FromSeconds(1));
      FadeInAnimation.Start();
      return Flyout;
    }
    private Inv.Panel CustomRender(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.WhiteSmoke;

      var PressPoint = (Inv.Point)null;

      var Render = Surface.NewRender();
      Overlay.AddPanel(Render);
      Render.SingleTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.Yellow;
        Debug.WriteLine("single");
      };
      Render.DoubleTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.Purple;
        Debug.WriteLine("double");
      };
      Render.ContextTapEvent += (Point) =>
      {
        Overlay.Background.Colour = Inv.Colour.SteelBlue;
        Debug.WriteLine("context");
      };
      Render.PressEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Render.MoveEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Render.ReleaseEvent += (Point) =>
      {
        PressPoint = null;
      };

      var ZoomFactor = 0;

      Render.ZoomEvent += (Delta) =>
      {
        ZoomFactor += Delta > 0 ? +10 : -10;
      };

      var Position = 0;

      var Image1Rect = new Inv.Rect(0, 100, 320, 180);
      var Image2Rect = new Inv.Rect(0, 500, 320, 180);
      var Image3Rect = new Inv.Rect(400, 300, 320, 180);
      var Image4Rect = new Inv.Rect(400, 500, 320, 180);
      var Image5Rect = new Inv.Rect(400, 40, 320, 180);

      Surface.ComposeEvent += () =>
      {
        Render.Draw(RC =>
        {
          RC.DrawText(Surface.Window.DisplayRate.PerSecond.ToString() + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(RC.Width - 2, RC.Height - 2), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);

          var Opacity = Position++ / 1000.0F;
          if (Opacity > 1.0F)
            Opacity = 1.0F;

          RC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Green, 1, new Inv.Rect(280, 280, 100 + ZoomFactor, 100 + ZoomFactor));

          RC.DrawRectangle(Inv.Colour.Red.Opacity(Opacity), Inv.Colour.Blue.Opacity(Opacity), 1, new Inv.Rect(80, 80, 100, 100));
          RC.DrawEllipse(Inv.Colour.Green.Opacity(Opacity), Inv.Colour.Pink.Opacity(Opacity), 10, new Inv.Point(180, 180), new Inv.Point(50, 50));

          var TintOpacity = 0.50F;

          RC.DrawImage(LogoImage, Image1Rect, Opacity, Inv.Colour.Red.Opacity(TintOpacity), Inv.Mirror.Horizontal);
          RC.DrawImage(LogoImage, Image2Rect, Opacity, Inv.Colour.Green.Opacity(TintOpacity), Inv.Mirror.Vertical);
          RC.DrawImage(LogoImage, Image3Rect, Opacity, Inv.Colour.Yellow.Opacity(TintOpacity));
          RC.DrawImage(LogoImage, Image4Rect, Opacity, Inv.Colour.Blue.Opacity(TintOpacity));
          RC.DrawImage(LogoImage, Image5Rect, Opacity, Inv.Colour.Purple.Opacity(TintOpacity));

          RC.DrawText("Hello World", "", 20, Inv.FontWeight.Regular, Inv.Colour.Purple.Opacity(Opacity), new Inv.Point(100, 300), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

          var LabelBrush = Inv.Colour.DarkRed.Opacity(0.75F);
          var LabelPen = Inv.Colour.DarkRed.Darken(0.25F);
          var LabelRect = new Inv.Rect(600, 600, 300, 35);
          RC.DrawRectangle(LabelBrush, LabelPen, 1, LabelRect);
          RC.DrawText("AbcdefghijklmnopqrstuvwxyZ", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(LabelRect.Left + LabelRect.Width / 2, LabelRect.Top + (LabelRect.Height / 2)), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

          if (PressPoint != null)
            RC.DrawEllipse(Inv.Colour.DarkOrange.Opacity(0.50F), Inv.Colour.Orange.Opacity(0.50F), 5, PressPoint, new Inv.Point(25, 25));
        });
      };

      var Button = Surface.NewButton();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Orange;
      Button.Padding.Set(20);
      Button.Alignment.TopStretch();
      Button.SingleTapEvent += () =>
      {
        var TransitionSurface = Base.Window.NewCustomSurface();
        Base.Window.Transition(TransitionSurface);

        var TransitionRender = TransitionSurface.NewRender();
        TransitionSurface.Content = TransitionRender;
        TransitionRender.Background.Colour = Inv.Colour.WhiteSmoke;
        TransitionRender.SingleTapEvent += (Point) =>
        {
          Debug.WriteLine(string.Format("{0}, {1}", Point.X, Point.Y));

          TransitionRender.Draw(RC =>
          {
            RC.DrawText(string.Format("{0}, {1}", Point.X, Point.Y), "", 20, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(RC.Width / 2, RC.Height / 2), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);
            //RC.DrawText(string.Format("{0}, {1}", Application.Window.Width, Application.Window.Height), "", 20, Inv.Colour.Red, new Point(RC.Width / 2, RC.Height / 2 + 50), HorizontalPosition.Left, VerticalPosition.Top);

            RC.DrawText("TOP LEFT", "", 30, Inv.FontWeight.Thin, Inv.Colour.Red, new Inv.Point(0, 0), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Top);
            RC.DrawText("TOP MIDDLE", "", 30, Inv.FontWeight.Light, Inv.Colour.Green, new Inv.Point(RC.Width / 2, 0), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Top);
            RC.DrawText("TOP RIGHT", "", 30, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(RC.Width, 0), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Top);

            RC.DrawText("BOT LEFT", "", 30, Inv.FontWeight.Medium, Inv.Colour.Red, new Inv.Point(0, RC.Height), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Bottom);
            RC.DrawText("BOT MIDDLE", "", 30, Inv.FontWeight.Bold, Inv.Colour.Green, new Inv.Point(RC.Width / 2, RC.Height), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
            RC.DrawText("BOT RIGHT", "", 30, Inv.FontWeight.Black, Inv.Colour.Blue, new Inv.Point(RC.Width, RC.Height), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
          });
        };
      };
      return Overlay;
    }
    private Inv.Panel DockOnOverlay(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;

      for (var Index = 0; Index < 1; Index++)
      {
        var IndexButton = Surface.NewButton();
        Stack.AddPanel(IndexButton);
        IndexButton.Background.Colour = Inv.Colour.SteelBlue;
        IndexButton.Padding.Set(10);
        IndexButton.Margin.Set(5);

        var IndexLabel = Surface.NewLabel();
        IndexButton.Content = IndexLabel;
        IndexLabel.Alignment.Center();
        IndexLabel.Text = "INDEX " + Index;
        IndexLabel.Size.SetHeight(1000);
      }

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.TopLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(200, 100);

      var Dock = Surface.NewVerticalDock();
      Overlay.AddPanel(Dock);
      Dock.Alignment.StretchLeft();

      //var TopButton = Surface.NewButton();
      //Dock.AddClient(TopButton);
      //TopButton.Background.Colour = Inv.Colour.Pink;

      var RightButton = Surface.NewButton();
      Dock.AddFooter(RightButton);
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(200, 100);

      return Overlay;
    }
    private Inv.Panel HiddenOverlays(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = new ComplexButton(Surface);
      Overlay.AddPanel(Button);
      Button.Alignment.Center();

      var Label = Surface.NewButton();
      Overlay.AddPanel(Label);
      Label.Background.Colour = Inv.Colour.Black;
      Label.Visibility.Collapse();

      return Overlay;
    }
    private Inv.Panel OverlayAlignments(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      foreach (var Placement in Inv.Support.EnumHelper.GetEnumerable<Inv.Placement>())
      {
        var Label = Surface.NewLabel();
        Label.Font.Colour = Inv.Colour.Black;
        Label.Alignment.Set(Placement);

        var LabelPlacement = Label.Alignment.Get();

        if (LabelPlacement.IsVerticalStretch() && LabelPlacement.IsHorizontalStretch())
        {
          Label.Background.Colour = Inv.Colour.Green.Opacity(0.50F);
          Label.Margin.Set(100);
        }
        else if (LabelPlacement.IsVerticalStretch())
        {
          Label.Background.Colour = Inv.Colour.Red.Opacity(0.50F);
          Label.Size.SetWidth(50);
        }
        else if (LabelPlacement.IsHorizontalStretch())
        {
          Label.Background.Colour = Inv.Colour.Blue.Opacity(0.50F);
          Label.Size.SetHeight(50);
        }
        else
        {
          Label.Text = Placement.ToString().Strip(C => char.IsLower(C));
        }

        Overlay.AddPanel(Label);
      }
      return Overlay;
    }
    private Inv.Panel CallButton(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      //var ContactScroll = Surface.NewVerticalScroll();
      //Overlay.AddElement(ContactScroll);
      //ContactScroll.Alignment.Center();

      var ContactStack = Surface.NewVerticalStack();
      //ContactScroll.Content = ContactStack;
      Overlay.AddPanel(ContactStack);

      var EmailButton = Surface.NewButton();
      ContactStack.AddPanel(EmailButton);
      EmailButton.Background.Colour = Inv.Colour.DarkOrange;
      EmailButton.SingleTapEvent += () =>
      {
        EmailButton.Background.Colour = Inv.Colour.Red;
        EmailButton.Visibility.Collapse();

        var Shade = Surface.NewButton();
        Overlay.AddPanel(Shade);
        Shade.Background.Colour = Inv.Colour.Black.Opacity(0.50F);
        Shade.SingleTapEvent += () => Overlay.RemovePanel(Shade);
        //Application.Email.NewMessage().Send();
      };

      var EmailLabel = Surface.NewLabel();
      EmailButton.Content = EmailLabel;
      EmailLabel.Alignment.Center();
      EmailLabel.Font.Size = 18;
      EmailLabel.Font.Colour = Inv.Colour.White;
      EmailLabel.Padding.Set(15);
      EmailLabel.Text = "john.carey@kestral.com.au";

      var CallButton = Surface.NewButton();
      ContactStack.AddPanel(CallButton);
      CallButton.Background.Colour = Inv.Colour.DarkGreen;
      CallButton.SingleTapEvent += () =>
      {
        CallButton.Visibility.Collapse();
        /*
        if (CallButton.Margin.IsSet)
          CallButton.Margin.Reset();
        else
          CallButton.Margin.Set(20);*/
      };

      var CallLabel = Surface.NewLabel();
      CallButton.Content = CallLabel;
      //CallLabel.Alignment.Center();
      CallLabel.Font.Size = 30;
      CallLabel.Font.Colour = Inv.Colour.White;
      CallLabel.Padding.Set(30);
      CallLabel.Text = "CALL\n+61412171734";

      var SMSButton = Surface.NewButton();
      ContactStack.AddPanel(SMSButton);
      SMSButton.Background.Colour = Inv.Colour.DarkGreen;
      SMSButton.IsEnabled = false;
      SMSButton.SingleTapEvent += () =>
      {
        SMSButton.Visibility.Collapse();
        /*
        if (SMSButton.Padding.IsSet)
          SMSButton.Padding.Reset();
        else
          SMSButton.Padding.Set(20);*/
      };

      var SMSLabel = Surface.NewLabel();
      SMSButton.Content = SMSLabel;
      SMSLabel.Alignment.Center();
      SMSLabel.Font.Size = 30;
      SMSLabel.Font.Colour = Inv.Colour.White;
      SMSLabel.Padding.Set(30);
      SMSLabel.Text = "SMS\n+61412171734";

      var ResetButton = Surface.NewButton();
      ContactStack.AddPanel(ResetButton);
      ResetButton.Background.Colour = Inv.Colour.DarkGreen;
      ResetButton.SingleTapEvent += () =>
      {
        EmailButton.Visibility.Show();
        CallButton.Visibility.Show();
        SMSButton.Visibility.Show();
      };

      var ResetLabel = Surface.NewLabel();
      ResetButton.Content = ResetLabel;
      //ResetLabel.Alignment.Center();
      ResetLabel.Font.Size = 30;
      ResetLabel.Font.Colour = Inv.Colour.White;
      ResetLabel.Padding.Set(30);
      ResetLabel.Text = "RESET";

      return Overlay;
    }
    private Inv.Panel CenteredScrollingStack(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.Yellow;
      Scroll.Alignment.Center();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Background.Colour = Inv.Colour.Green;
      Stack.Padding.Set(10);

      var Button = Surface.NewButton();
      Stack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.DarkGray;
      //Button.Size.SetHeight(100);

      var HelloLabel = Surface.NewLabel();
      Stack.AddPanel(HelloLabel);
      HelloLabel.Text = "Hello";
      HelloLabel.Font.Size = 30;
      HelloLabel.Background.Colour = Inv.Colour.Orange;

      var WorldLabel = Surface.NewLabel();
      Stack.AddPanel(WorldLabel);
      WorldLabel.Text = "World";
      WorldLabel.Font.Size = 30;
      WorldLabel.Background.Colour = Inv.Colour.Pink;

      return Scroll;
    }
    private Inv.Panel ComplexLayout(Inv.Surface Surface)
    {
      var D1 = Surface.NewVerticalDock();

      var D2 = Surface.NewHorizontalDock();
      D1.AddHeader(D2);

      D2.AddHeader(NewLabel(Surface, "D2.H1", Inv.Colour.Blue));
      D2.AddClient(NewLabel(Surface, "D2.C1", Inv.Colour.Green));
      D2.AddFooter(NewLabel(Surface, "D2.F1", Inv.Colour.Red));

      var Scroll = Surface.NewVerticalScroll();
      D1.AddClient(Scroll);

      var S1 = Surface.NewVerticalStack();
      Scroll.Content = S1;
      S1.AddPanel(NewLabel(Surface, "S1.E1", Inv.Colour.Yellow));
      S1.AddPanel(NewLabel(Surface, "S1.E2", Inv.Colour.Gray));
      S1.AddPanel(NewLabel(Surface, "S1.E3", Inv.Colour.Turquoise));
      S1.AddPanel(NewLabel(Surface, "S1.E4", Inv.Colour.Violet));

      var S2 = Surface.NewHorizontalStack();
      D1.AddFooter(S2);
      S2.AddPanel(NewLabel(Surface, "S2.E1", Inv.Colour.Orange));
      S2.AddPanel(NewLabel(Surface, "S2.E2", Inv.Colour.OldLace));
      S2.AddPanel(NewLabel(Surface, "S2.E3", Inv.Colour.Peru));

      return D1;
    }
    private Inv.Panel ComplexMarginAndPadding(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();

      var Test1Button = new ComplexButton(Surface);
      Stack.AddPanel(Test1Button);
      Test1Button.Margin.Set(20);

      var Test2Button = new ComplexButton(Surface);
      Stack.AddPanel(Test2Button);
      Test2Button.Margin.Set(20);

      return Stack;
    }
    private Inv.Panel MaterialExperiment(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var ListFrame = Surface.NewFrame();
      Overlay.AddPanel(ListFrame);
      ListFrame.Alignment.Center();
      ListFrame.Size.SetWidth(400);

      var List = new Inv.Material.List(Base.Window, Surface);
      ListFrame.Content = List;
      List.AddItem(Resources.Images.PhoenixLogo960x540, "Phoenix Team").SingleTapEvent += () => Overlay.RemovePanel(ListFrame);
      List.AddDivider();
      List.AddSubheading("Staff members");
      List.AddItem(Resources.Images.Waves, "Callan Hodgskin").AddIcon(Inv.Material.Resources.Images.StarsBlack);
      List.AddDivider();
      List.AddItem(Resources.Images.Waves, "Kyle Vermaes").AddIcon(Inv.Material.Resources.Images.StarsBlack);
      List.AddDivider();
      List.AddItem(Resources.Images.Waves, "Simon Williams").AddIcon(Inv.Material.Resources.Images.StarsBlack);

      var FavouriteFrame = Surface.NewFrame();
      Overlay.AddPanel(FavouriteFrame);
      FavouriteFrame.Alignment.TopRight();
      FavouriteFrame.Margin.Set(100);

      var FavouriteButton = new Inv.Material.FloatingActionButton(Base.Window, Surface);
      FavouriteFrame.Content = FavouriteButton;
      FavouriteButton.Image = Inv.Material.Resources.Images.ViewModuleWhite;
      FavouriteButton.SingleTapEvent += () =>
      {
        var Dialog = new Inv.Material.Dialog(Surface);
        Overlay.AddPanel(Dialog);
        Dialog.HideEvent += () => Overlay.RemovePanel(Dialog);

        Dialog.Title = "Use Google's location service?";

        Dialog.SetContentText("Let Google help apps determine location.  This means sending anonymous location data to Google, even when no apps are running.");

        var DisagreeAction = Dialog.Strip.AddAffirmitiveAction();
        DisagreeAction.Caption = "DISAGREE";
        DisagreeAction.Colour = Inv.Colour.DodgerBlue;

        var AgreeAction = Dialog.Strip.AddAffirmitiveAction();
        AgreeAction.Caption = "AGREE";
        AgreeAction.Colour = Inv.Colour.DodgerBlue;
      };

      var RaisedFrame = Surface.NewFrame();
      Overlay.AddPanel(RaisedFrame);
      RaisedFrame.Alignment.CenterRight();
      RaisedFrame.Margin.Set(100);

      var RaisedButton = new Inv.Material.RaisedButton(Surface);
      RaisedFrame.Content = RaisedButton;
      RaisedButton.Caption = "Hello Button World";
      RaisedButton.Colour = Inv.Colour.Purple;

      var ChipStack = Surface.NewHorizontalStack();
      Overlay.AddPanel(ChipStack);
      ChipStack.Alignment.TopLeft();
      ChipStack.Margin.Set(100);

      var CallanChip = new Inv.Material.Chip(Surface);
      ChipStack.AddPanel(CallanChip);
      CallanChip.Icon = Resources.Images.Waves;
      CallanChip.Text = "Callan Hodgskin";
      CallanChip.RemoveEvent += () => { };

      var PhoenixChip = new Inv.Material.Chip(Surface);
      ChipStack.AddPanel(PhoenixChip);
      PhoenixChip.Icon = Resources.Images.PhoenixLogo960x540;
      PhoenixChip.Text = "Phoenix Team";
      PhoenixChip.RemoveEvent += () => { };

      var Snackbar = new Inv.Material.Snackbar(Surface);
      Overlay.AddPanel(Snackbar);
      Snackbar.Text = "Archived";
      Snackbar.Button.Caption = "UNDO";
      Snackbar.Button.Colour = Inv.Colour.Yellow;

      var MenuFrame = Surface.NewFrame();
      Overlay.AddPanel(MenuFrame);
      MenuFrame.Margin.Set(100);
      MenuFrame.Alignment.BottomRight();

      var Menu = new Inv.Material.Menu(Surface);
      MenuFrame.Content = Menu;

      Menu.AddItem(Inv.Material.Resources.Images.LoopBlack, "Cut", "Ctrl+X");
      Menu.AddItem(Inv.Material.Resources.Images.HelpBlack, "Copy", "Ctrl+C");
      Menu.AddItem(Inv.Material.Resources.Images.HelpBlack, "Paste", "Ctrl+V");
      Menu.AddDivider();
      Menu.AddItem(Inv.Material.Resources.Images.LoopBlack, "Refresh", "F5");
      Menu.AddItem(Inv.Material.Resources.Images.HelpBlack, "Help & feedback", "F1");
      Menu.AddItem(Inv.Material.Resources.Images.SettingsApplicationsBlack, "Settings", "F10");
      Menu.AddItem("Sign out").SingleTapEvent += () => Overlay.RemovePanel(MenuFrame);

      var CardFrame = Surface.NewFrame();
      Overlay.AddPanel(CardFrame);
      CardFrame.Margin.Set(100);
      CardFrame.Alignment.BottomLeft();
      CardFrame.Size.SetWidth(400);

      var Card = new Inv.Material.Card(Surface);
      CardFrame.Content = Card;

      Card.Header.Avatar = Inv.Material.Resources.Images.BookmarkWhite;
      Card.Header.Title = "Title goes here";
      Card.Header.Subtitle = "subheading";
      Card.Header.BackgroundColour = Inv.Colour.DarkGoldenrod;
      Card.Header.ForegroundColour = Inv.Colour.White;

      Card.AddMedia(Resources.Images.Waves);
      Card.AddSubject("Card subject");
      Card.AddSupport("This is a long line of supporting text that should wordwrap. If you need any more text you should add extra lines. What a long story you are reading.  I don't know why you haven't stopped yet.");

      Card.Strip.AddAffirmitiveAction("SHARE").Colour = Inv.Colour.DarkGoldenrod;
      Card.Strip.AddAffirmitiveAction("EXPLORE").Colour = Inv.Colour.DarkGoldenrod;

      Card.Strip.AddDismissiveIcon(Inv.Material.Resources.Images.AnnouncementBlack).SingleTapEvent += () => Overlay.RemovePanel(CardFrame);
      Card.Strip.AddDismissiveIcon(Inv.Material.Resources.Images.AccountBoxBlack);

      return Overlay;
    }
    private Inv.Panel DeepNested(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();

      var LastFrame = (Inv.Frame)null;
      var Index = 0;

      Button.SingleTapEvent += () =>
      {
        // NOTE: testing for Android 4.x

        var Frame = Surface.NewFrame();
        Frame.Background.Colour = Index++ % 2 == 0 ? Inv.Colour.Red : Inv.Colour.Blue;
        Frame.Padding.Set(1);

        if (LastFrame == null)
          Button.Content = Frame;
        else
          LastFrame.Content = Frame;

        LastFrame = Frame;
      };

      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();

      return Button;
    }
    private Inv.Panel BasicMarginAndPadding(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Alignment.Center();
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(100, 100);
      Button.Margin.Set(10);
      Button.Padding.Set(20, 0, 0, 0);
      Button.SingleTapEvent += () => Surface.Rearrange();

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Padding.Set(10);
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
      Label.Text = "Hello Mr Wordwrap";
      return Button;
    }
    private Inv.Panel ComplexButton(Inv.Surface Surface)
    {
      var Button = new ComplexButton(Surface);
      Button.Alignment.TopStretch();

      return Button;
    }
    private Inv.Panel LayoutCanvas(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();
      Canvas.Background.Colour = Inv.Colour.Black;

      var Header1Label = Surface.NewLabel();
      Canvas.AddPanel(Header1Label, new Inv.Rect(100, 100, 200, 200));
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.JustifyCenter();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = 30;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      return Canvas;
    }
    private Inv.Panel LayoutDock(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      //Dock.Alignment.Center();
      //Dock.Padding.Set(1);

      const int HeadingFontSize = 30;

      var HeaderOverlay = Surface.NewOverlay();
      //Dock.AddHeader(HeaderOverlay);
      HeaderOverlay.Background.Colour = Inv.Colour.Orange;
      //HeaderOverlay.Text = "Hello";

      var Header1Label = Surface.NewLabel();
      Dock.AddHeader(Header1Label);
      //Header1Label.Alignment.Center();
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.JustifyCenter();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = HeadingFontSize;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      //Header1Label.Visibility.Collapse();

      var Header2Label = Surface.NewLabel();
      Dock.AddHeader(Header2Label);
      Header2Label.Background.Colour = Inv.Colour.SteelBlue;
      Header2Label.JustifyCenter();
      Header2Label.Font.Colour = Inv.Colour.DarkBlue;
      Header2Label.Font.Size = HeadingFontSize;
      Header2Label.Padding.Set(10);
      Header2Label.Text = "Header2";

      var Client1Label = Surface.NewLabel();
      Dock.AddClient(Client1Label);
      Client1Label.Background.Colour = Inv.Colour.Pink;
      Client1Label.JustifyCenter();
      Client1Label.Font.Colour = Inv.Colour.DarkRed;
      Client1Label.Font.Size = HeadingFontSize;
      Client1Label.Text = "Client1";

      var Client2Label = Surface.NewLabel();
      Dock.AddClient(Client2Label);
      Client2Label.Background.Colour = Inv.Colour.WhiteSmoke;
      Client2Label.JustifyCenter();
      Client2Label.Font.Colour = Inv.Colour.BlueViolet;
      Client2Label.Font.Size = HeadingFontSize;
      Client2Label.Text = "Client2";

      var Footer1Label = Surface.NewLabel();
      Dock.AddFooter(Footer1Label);
      Footer1Label.Background.Colour = Inv.Colour.Yellow;
      Footer1Label.JustifyCenter();
      Footer1Label.Font.Colour = Inv.Colour.Purple;
      Footer1Label.Font.Size = HeadingFontSize;
      Footer1Label.Padding.Set(10);
      Footer1Label.Text = "Footer1";
      //Footer1Label.Visibility.Collapse();

      var Footer2Label = Surface.NewLabel();
      Dock.AddFooter(Footer2Label);
      Footer2Label.Background.Colour = Inv.Colour.DarkGreen;
      Footer2Label.JustifyCenter();
      Footer2Label.Font.Colour = Inv.Colour.White;
      Footer2Label.Font.Size = HeadingFontSize;
      Footer2Label.Padding.Set(20);
      Footer2Label.Text = "Footer2";

      return Dock;
    }
    private Inv.Panel LabelWordwrap(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.White;
      Dock.Padding.Set(0, 24, 0, 0);
      Dock.Alignment.Center();
      Dock.Elevation.Set(24, Inv.Colour.Black.Opacity(0.25F));

      var TitleLabel = Surface.NewLabel();
      //Dock.AddHeader(TitleLabel);
      TitleLabel.Margin.Set(24, 0, 24, 20);
      TitleLabel.JustifyLeft();
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();
      TitleLabel.Text = "Use Google's location service?";

      //var ContentFrame = Surface.NewFrame();
      //Dock.AddClient(ContentFrame);
      //ContentFrame.Margin.Set(24, 0, 24, 24);

      var ContentLabel = Surface.NewLabel();
      //ContentFrame.Content = ContentLabel;
      Dock.AddClient(ContentLabel);
      ContentLabel.JustifyLeft();
      ContentLabel.Font.Colour = Inv.Colour.DimGray;
      ContentLabel.Font.Size = 16;
      ContentLabel.Text = "Let Google help apps determine location.  This means sending anonymous location data to Google, even when no apps are running.";

      var ActionDock = Surface.NewHorizontalDock();
      //Dock.AddFooter(ActionDock);
      ActionDock.Margin.Set(24, 4, 4, 4);

      return Dock;
    }
    private Inv.Panel EditAndMemo(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Button = Surface.NewButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.Green;
      Button.Size.SetHeight(100);

      var Edit = Surface.NewEdit();
      Dock.AddHeader(Edit);
      Edit.Background.Colour = Inv.Colour.WhiteSmoke;
      Edit.Font.Colour = Inv.Colour.Black;
      Edit.Font.Size = 30;

      var EditLabel = Surface.NewLabel();
      Dock.AddHeader(EditLabel);
      EditLabel.Background.Colour = Inv.Colour.Black;
      EditLabel.Font.Colour = Inv.Colour.White;
      EditLabel.Font.Size = 30;

      Edit.ChangeEvent += () => EditLabel.Text = Edit.Text;

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.Background.Colour = Inv.Colour.LightGray;

      var MemoLabel = Surface.NewLabel();
      Dock.AddFooter(MemoLabel);
      MemoLabel.Background.Colour = Inv.Colour.Black;
      MemoLabel.Font.Colour = Inv.Colour.White;
      MemoLabel.Font.Size = 30;

      Memo.ChangeEvent += () => MemoLabel.Text = Memo.Text;

      Button.SingleTapEvent += () => Surface.SetFocus(Edit);

      Surface.SetFocus(Edit);

      return Dock;
    }
    private Inv.Panel FileIO(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);

      var Folder = Base.Directory.NewFolder("Data");
      var File = Folder.NewFile("Myfile.dat");

      var CreateButton = TilePanel.AddButton();
      CreateButton.Title = "Create file";
      CreateButton.SingleTapEvent += () =>
      {
        File.AsText().WriteAll("Hello World");
      };

      var ExistsButton = TilePanel.AddButton();
      ExistsButton.Title = "Exists file";
      ExistsButton.SingleTapEvent += () =>
      {
        ExistsButton.Description = File.Exists() ? "YES" : "NO";
      };

      var ReadButton = TilePanel.AddButton();
      ReadButton.Title = "Read file";
      ReadButton.SingleTapEvent += () =>
      {
        ReadButton.Description = File.AsText().ReadAll();
      };

      var DeleteButton = TilePanel.AddButton();
      DeleteButton.Title = "Delete file";
      DeleteButton.SingleTapEvent += () =>
      {
        File.Delete();
      };

      var CopyButton = TilePanel.AddButton();
      CopyButton.Title = "Copy file";
      CopyButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mycopy.dat");

        File.CopyReplace(TargetFile);
      };

      var MoveButton = TilePanel.AddButton();
      MoveButton.Title = "Move file";
      MoveButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mymove.dat");

        File.MoveReplace(TargetFile);
      };

      return TilePanel;
    }
    private Inv.Panel OverlayAndMargins(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.BottomLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(150, 50);

      var RightButton = Surface.NewButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.BottomRight();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(150, 50);
      RightButton.Margin.Set(0, 0, 50, 0);

      return Overlay;
    }
    private Inv.Panel OverlayButtons(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.Center();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(200, 200);
      LeftButton.SingleTapEvent += () => LeftButton.Background.Colour = Inv.Colour.Red;

      var RightButton = Surface.NewButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.Center();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(100, 100);
      RightButton.SingleTapEvent += () => RightButton.Background.Colour = Inv.Colour.Blue;

      return Overlay;
    }
    private Inv.Panel Portal(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);

      for (var Index = 1; Index <= 20; Index++)
      {
        var TileButton = TilePanel.AddButton();
        TileButton.Image = LogoImage;
        TileButton.Title = Index + ". TITLE";
        TileButton.Description = "description" + Index;
        TileButton.SingleTapEvent += () =>
        {
          TilePanel.RemoveButton(TileButton);
          Surface.Rearrange();
        };
      }

      return TilePanel;
    }
    private Inv.Panel Records(Inv.Surface Surface)
    {
      var Count = 1;

      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Alignment.Center();

      var RecordPanel = new RecordPanel(Surface);
      Button.Content = RecordPanel;

      Button.SingleTapEvent += () =>
      {
        Count++;

        var RecordButton = RecordPanel.AddButton();
        RecordButton.Rank = Count;
        RecordButton.Identity = "Identity" + Count;
        RecordButton.Summary = "long summary text";
        RecordButton.Fame = (10 - Count + 1) * 1000;
        RecordButton.AddGlyph(LogoImage);
      };

      Button.SingleTap();

      return Button;
    }
    private Inv.Panel RoundButton(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();

      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Margin.Set(20);
      Button.Padding.Set(20);
      Button.Size.Set(200, 200);
      Button.Border.Thickness.Set(20, 10, 5, 0);
      Button.Border.Colour = Inv.Colour.Blue;
      Button.CornerRadius.Set(100, 100, 100, 0);
      Button.Alignment.Center();

      return Button;
    }
    private Inv.Panel FrameFeatures(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Alignment.Center();
      Frame.Size.Set(200, 200);
      Frame.Margin.Set(10);
      Frame.Padding.Set(10);
      Frame.CornerRadius.Set(10);
      Frame.Border.Colour = Inv.Colour.Red;
      Frame.Border.Thickness.Set(10);
      Frame.Background.Colour = Inv.Colour.Green;

      var Label = Surface.NewLabel();
      Frame.Content = Label;
      Label.Alignment.Stretch();
      Label.Background.Colour = Inv.Colour.Purple;

      return Frame;
    }
    private Inv.Panel SettingsFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Flyout.AddPanel(Scroll);
      Scroll.Alignment.StretchLeft();
      Scroll.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);
      Scroll.Size.SetWidth(250);
      Scroll.Padding.Set(20);

      var Dock = Surface.NewVerticalDock();
      //Flyout.AddElement(Dock);
      Scroll.Content = Dock;
      //Dock.Alignment.StretchLeft();

      var ButtonHeight = 100;
      var ButtonMargin = 10;

      var TitleLabel = Surface.NewLabel();
      Dock.AddFooter(TitleLabel);
      TitleLabel.Alignment.Center();
      TitleLabel.Margin.Set(ButtonMargin);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 30;
      TitleLabel.Text = "SETTINGS";

      var LanguageStack = Surface.NewVerticalStack();
      Dock.AddFooter(LanguageStack);
      LanguageStack.Margin.Set(ButtonMargin);

      var LanguageArray = new[]
        {
          new
          {
            Code = "en",
            Name = "English"
          },
          new
          {
            Code = "fr",
            Name = "Français"
          }
        };

      var LanguageButtonList = new Inv.DistinctList<CaptionButton>(LanguageArray.Length);

      foreach (var Language in LanguageArray)
      {
        var LanguageButton = new CaptionButton(Surface);
        LanguageStack.AddPanel(LanguageButton);
        LanguageButton.Padding.Set(20);
        LanguageButton.SingleTapEvent += () =>
        {
          foreach (var EachButton in LanguageButtonList)
            EachButton.Colour = LanguageButton == EachButton ? Inv.Colour.Purple : Inv.Colour.DimGray;
        };
        LanguageButton.Colour = Language.Code == System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName ? Inv.Colour.Purple : Inv.Colour.DimGray;
        LanguageButton.Text = Language.Name;

        LanguageButtonList.Add(LanguageButton);
      }

      var SoundButton = Surface.NewButton();
      Dock.AddFooter(SoundButton);
      SoundButton.Alignment.Center();
      SoundButton.Size.Set(100, 100);
      SoundButton.CornerRadius.Set(25);
      SoundButton.Margin.Set(0, 10, 0, 10);

      var SoundGraphic = Surface.NewGraphic();
      SoundButton.Content = SoundGraphic;
      SoundGraphic.Size.Set(64, 64);

      var Muted = false;

      SoundButton.SingleTapEvent += () =>
      {
        Muted = !Muted;

        SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
        SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;
      };

      SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
      SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;

      var DiagnosticsButton = new CaptionButton(Surface);
      Dock.AddFooter(DiagnosticsButton);
      DiagnosticsButton.Colour = Inv.Colour.DimGray;
      DiagnosticsButton.Size.SetHeight(ButtonHeight / 2);
      DiagnosticsButton.Margin.Set(ButtonMargin);
      DiagnosticsButton.Text = "DIAGNOSTICS";
      DiagnosticsButton.SingleTapEvent += () =>
      {
      };

      return Flyout;
    }
    private Inv.Panel SideFlyout(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = null;

      var SideDock = Surface.NewVerticalDock();
      Overlay.AddPanel(SideDock);
      SideDock.Alignment.Stretch();
      SideDock.Background.Colour = Inv.Colour.Black;
      SideDock.Size.SetWidth(420);

      var FooterDock = Surface.NewVerticalDock();
      SideDock.AddClient(FooterDock);
      FooterDock.Background.Colour = Inv.Colour.Green;
      FooterDock.Alignment.BottomStretch();

      var TrailerLabel = Surface.NewLabel();
      SideDock.AddFooter(TrailerLabel);
      TrailerLabel.Background.Colour = Inv.Colour.Pink;
      TrailerLabel.Font.Size = 20;
      TrailerLabel.Text = "TRAILER";

      var FooterTitle = Surface.NewLabel();
      FooterDock.AddHeader(FooterTitle);
      FooterTitle.Font.Colour = Inv.Colour.White;
      FooterTitle.Font.Size = 30;
      FooterTitle.Text = "FOOTER";

      //var FooterScroll = Surface.NewVerticalScroll();
      //FooterDock.AddClient(FooterScroll);

      var FooterStack = Surface.NewVerticalStack();
      FooterDock.AddClient(FooterStack);
      //FooterScroll.Content = FooterStack;

      var SummaryButton = Surface.NewButton();
      FooterStack.AddPanel(SummaryButton);

      var SummaryDock = Surface.NewHorizontalDock();
      SummaryButton.Content = SummaryDock;

      var SummaryGraphic = Surface.NewGraphic();
      SummaryDock.AddHeader(SummaryGraphic);
      SummaryGraphic.Size.Set(64, 64);
      SummaryGraphic.Image = LogoImage;

      var SummaryStack = Surface.NewVerticalStack();
      SummaryDock.AddClient(SummaryStack);
      SummaryStack.Background.Colour = Inv.Colour.Orange;
      SummaryStack.Padding.Set(10);

      var SummaryTitle = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryTitle);
      SummaryTitle.Background.Colour = Inv.Colour.Blue;
      SummaryTitle.Font.Colour = Inv.Colour.White;
      SummaryTitle.Font.Size = 30;
      SummaryTitle.Text = "TITLE";

      var SummaryDescription = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryDescription);
      SummaryDescription.Background.Colour = Inv.Colour.Red;
      SummaryDescription.Font.Colour = Inv.Colour.White;
      SummaryDescription.Font.Size = 30;
      SummaryDescription.Text = "description";

      return Overlay;
    }
    private Inv.Panel StackAlignmentHorizontal(Inv.Surface Surface)
    {
      var VerticalStack = Surface.NewVerticalStack();
      VerticalStack.Background.Colour = Inv.Colour.Black;

      var H1 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H1);
      H1.Background.Colour = Inv.Colour.WhiteSmoke;
      H1.Margin.Set(5);
      H1.Alignment.StretchLeft();

      var B1 = Surface.NewButton();
      H1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewButton();
      H1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var H2 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H2);
      H2.Background.Colour = Inv.Colour.Wheat;
      H1.Margin.Set(5);
      H2.Alignment.StretchRight();

      var B3 = Surface.NewButton();
      H2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewButton();
      H2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return VerticalStack;
    }
    private Inv.Panel StackAlignmentVertical(Inv.Surface Surface)
    {
      var HorizontalStack = Surface.NewHorizontalStack();
      HorizontalStack.Background.Colour = Inv.Colour.Black;

      var V1 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V1);
      V1.Background.Colour = Inv.Colour.WhiteSmoke;
      V1.Margin.Set(5);
      V1.Alignment.TopStretch();

      var B1 = Surface.NewButton();
      V1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewButton();
      V1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var V2 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V2);
      V2.Background.Colour = Inv.Colour.Wheat;
      V1.Margin.Set(5);
      V2.Alignment.BottomStretch();

      var B3 = Surface.NewButton();
      V2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewButton();
      V2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return HorizontalStack;
    }
    private Inv.Panel StackInStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Blue;
      S1.Alignment.TopStretch();
      S1.Margin.Set(10);
      S1.Padding.Set(10);

      var OneLabel = Surface.NewLabel();
      S1.AddPanel(OneLabel);
      OneLabel.Background.Colour = Inv.Colour.White;
      OneLabel.Text = "One";
      OneLabel.Padding.Set(10);
      OneLabel.Margin.Set(10);

      var TwoLabel = Surface.NewLabel();
      S1.AddPanel(TwoLabel);
      TwoLabel.Background.Colour = Inv.Colour.White;
      TwoLabel.LineWrapping = true;
      TwoLabel.Text = "Two\nwith a second\nline...";
      TwoLabel.Padding.Set(10);
      TwoLabel.Margin.Set(10);

      var S2 = Surface.NewVerticalStack();
      S1.AddPanel(S2);
      S2.Background.Colour = Inv.Colour.Orange;
      S2.Padding.Set(10);
      S2.Margin.Set(10);

      var ALabel = Surface.NewLabel();
      S2.AddPanel(ALabel);
      ALabel.Background.Colour = Inv.Colour.White;
      ALabel.Text = "A";
      ALabel.Padding.Set(10);
      ALabel.Margin.Set(10);

      var BLabel = Surface.NewLabel();
      S2.AddPanel(BLabel);
      BLabel.Background.Colour = Inv.Colour.White;
      BLabel.Text = "B";
      BLabel.Padding.Set(10);
      BLabel.Margin.Set(10);

      return S1;
    }
    private Inv.Panel TopStretchStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Orange;
      S1.Alignment.TopStretch();
      S1.Padding.Set(7, 7, 7, 0);

      var Button = Surface.NewButton();
      S1.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(48, 48);

      var Graphic = Surface.NewGraphic();
      //S1.AddElement(Graphic);
      Graphic.Background.Colour = Inv.Colour.Red;
      Graphic.Size.Set(48, 48);
      Graphic.Image = LogoImage;

      var DescriptionLabel = Surface.NewLabel();
      //S1.AddElement(DescriptionLabel);
      DescriptionLabel.Alignment.CenterLeft();
      DescriptionLabel.Background.Colour = Inv.Colour.Blue;
      DescriptionLabel.Font.Size = 20;
      //DescriptionLabel.Margin.Set(5, 0, 5, 0);
      DescriptionLabel.Text = "Description goes here";
      return S1;
    }
    private Inv.Panel TimerCountdown(Inv.Surface Surface)
    {
      var Countdown = 10;

      var Label = Surface.NewLabel();
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Size.Set(100, 100);
      Label.CornerRadius.Set(50);
      Label.JustifyCenter();
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 50;
      Label.Text = Countdown.ToString();

      var Timer = Base.Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromSeconds(0.5);
      Timer.IntervalEvent += () =>
      {
        Countdown--;

        if (Countdown <= 0)
          Timer.Stop();

        Label.Text = Countdown.ToString();
      };
      Timer.Start();

      return Label;
    }
    private Inv.Panel TestPattern(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DarkGray;

      var LogicalSizeLabel = Surface.NewLabel();
      Overlay.AddPanel(LogicalSizeLabel);
      LogicalSizeLabel.Alignment.BottomLeft();
      LogicalSizeLabel.Font.Size = 18;
      LogicalSizeLabel.Text = Base.Window.Width + " x " + Base.Window.Height;

      var FrameRateLabel = Surface.NewLabel();
      Overlay.AddPanel(FrameRateLabel);
      FrameRateLabel.Alignment.BottomRight();
      FrameRateLabel.Font.Size = 18;

      var LocationLabel = Surface.NewLabel();
      Overlay.AddPanel(LocationLabel);
      LocationLabel.Alignment.TopCenter();
      LocationLabel.Font.Size = 18;
      LocationLabel.Text = "LOCATION!!!ABC";
      LocationLabel.Margin.Set(40);

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      Graphic.Alignment.BottomCenter();
      Graphic.Image = LogoImage;
      Graphic.Padding.Set(20);

      var Button = Surface.NewButton();
      Overlay.AddPanel(Button);
      //Button.Visibility.Collapse();
      Button.Alignment.Center();
      //Button.Size.Set(320, 320);
      Button.Padding.Set(50);
      Button.Background.Colour = Inv.Colour.Green;
      Button.SingleTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang, 0.5F);
        Debug.WriteLine("left click");
      };
      Button.ContextTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang);
        Debug.WriteLine("right click");
      };

      var Stack = Surface.NewVerticalStack();
      Button.Content = Stack;
      Stack.Alignment.Center();

      var HeadingFontSize = 72;

      var FirstLabel = Surface.NewLabel();
      Stack.AddPanel(FirstLabel);
      FirstLabel.Background.Colour = Inv.Colour.Blue;
      FirstLabel.JustifyCenter();
      FirstLabel.Font.Colour = Inv.Colour.Red;
      FirstLabel.Font.Size = HeadingFontSize;
      FirstLabel.Text = "HW";

      var SecondLabel = Surface.NewLabel();
      Stack.AddPanel(SecondLabel);
      SecondLabel.Background.Colour = Inv.Colour.Yellow;
      SecondLabel.JustifyCenter();
      SecondLabel.Font.Colour = Inv.Colour.Purple;
      SecondLabel.Font.Size = HeadingFontSize;
      SecondLabel.Text = "GW";

      Base.Location.ChangeEvent += (Coordinate) =>
      {
        LocationLabel.Text = Coordinate.Latitude + ", " + Coordinate.Longitude + ", " + Coordinate.Altitude;
        /*
        var PlacemarkList = Application.Location.Lookup(Coordinate).GetPlacemarks().ToDistinctList();

        MobileBroker.Call(C => C.UpdateLocationCoordinate(MobileContext, new Kestral.HQ.Mobile.LocationCoordinate()
        {
          Latitude = Coordinate.Latitude,
          Longitude = Coordinate.Longitude,
          Description = PlacemarkList.Count > 0 ? PlacemarkList[0].Name : null
        }));*/
      };

      Surface.ComposeEvent += () =>
      {
        FrameRateLabel.Text = string.Format("{0} FPS | {1} PC | {2:F1} MB", Base.Window.DisplayRate.PerSecond, Surface.GetPanelCount(), Base.GetProcessMemoryMB());
      };

      return Overlay;
    }
    /*private Inv.Panel Transitions(Inv.Surface Surface)
    {
      var FirstSurface = Base.Window.NewCustomSurface();
      var SecondSurface = Base.Window.NewCustomSurface();
      FirstSurface.ArrangeEvent += () =>
      {
        FirstSurface.Background.Colour = Inv.Colour.Black;

        var FirstButton = FirstSurface.NewButton();
        FirstSurface.Content = FirstButton;
        FirstButton.SingleTapEvent += () => Base.Window.Transition(SecondSurface).CarouselNext();

        var FirstLabel = FirstSurface.NewLabel();
        FirstButton.Content = FirstLabel;
        FirstLabel.Background.Colour = Inv.Colour.LightBlue;
        FirstLabel.Font.Size = 60;
        FirstLabel.JustifyCenter();
        FirstLabel.Text = "FIRST SURFACE";
      };
      SecondSurface.ArrangeEvent += () =>
      {
        SecondSurface.Background.Colour = Inv.Colour.Black;

        var SecondButton = SecondSurface.NewButton();
        SecondSurface.Content = SecondButton;
        SecondButton.SingleTapEvent += () => Base.Window.Transition(FirstSurface).CarouselBack();

        var SecondLabel = SecondSurface.NewLabel();
        SecondButton.Content = SecondLabel;
        SecondLabel.Background.Colour = Inv.Colour.LightPink;
        SecondLabel.Font.Size = 60;
        SecondLabel.JustifyCenter();
        SecondLabel.Text = "SECOND SURFACE";
      };

      return FirstSurface;
    }*/
    private Inv.Panel UniformTable(Inv.Surface Surface)
    {
      var Table = Surface.NewTable();
      Table.Background.Colour = Inv.Colour.Black;
      Table.Margin.Set(5);
      Table.Compose(9, 9);

      Table.GetColumn(0).Length.Star();
      Table.GetColumn(8).Length.Star();

      var ColumnFrame = Surface.NewFrame();
      Table.GetColumn(1).Content = ColumnFrame;
      ColumnFrame.Background.Colour = Inv.Colour.Pink.Opacity(0.50F);

      var RowFrame = Surface.NewFrame();
      Table.GetRow(1).Content = RowFrame;
      RowFrame.Background.Colour = Inv.Colour.Orange.Opacity(0.50F);

      Table.GetColumn(4).Length.Fixed(150);
      Table.GetRow(4).Length.Fixed(150);

      Table.GetRow(0).Length.Star();
      Table.GetRow(8).Length.Star();

      Table.SetCell((X, Y) =>
      {
        var Button = Surface.NewButton();
        Button.Background.Colour = Inv.Colour.DimGray.Opacity(0.50F);
        Button.Margin.Set(5);
        Button.SingleTapEvent += () => Button.Background.Colour = Inv.Colour.Green;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Alignment.Center();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 20;
        Label.Text = "[" + X + ", " + Y + "]";

        return Button;
      });

      return Table;
    }
    private Inv.Panel UnhandledException(Inv.Surface Surface)
    {
      var Button = Surface.NewButton();
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Padding.Set(100);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Press for an exception";

      var Memo = Surface.NewMemo();
      Memo.IsReadOnly = true;

      Button.SingleTapEvent += () =>
      {
        if (Button.Content == Label)
        {
          Button.Content = Memo;

          try
          {
            throw new Exception("i'm an exception");
          }
          catch (Exception Ex)
          {
            Memo.Text = Ex.StackTrace;

            throw Ex;
          }
        }
        else
        {
          var FaultLogFile = Surface.Window.Application.Directory.NewFolder("Logs").NewFile("Fault.log");
          FaultLogFile.AsText().WriteAll(Memo.Text);

          var EmailMessage = Surface.Window.Application.Email.NewMessage();
          EmailMessage.Subject = "Fault";
          EmailMessage.Attach("Fault.log", FaultLogFile);
          EmailMessage.Send();
        }
      };

      return Button;
    }

    private void Compile(Func<Inv.Surface, Inv.Panel> Default)
    {
      var DefaultMethodInfo = Default != null ? Default.GetReflectionInfo() : null;

      var TypeInfo = typeof(Application).GetReflectionInfo();

      var MethodList = TypeInfo.GetReflectionMethods().Where(M => M.GetParameters().Length == 1 && M.GetParameters()[0].ParameterType == typeof(Inv.Surface) && M.ReturnType == typeof(Inv.Panel)).ToDistinctList();
      MethodList.Sort((A, B) => string.Compare(A.Name, B.Name));

      var MethodIndex = 0;
      var MethodItemList = new Inv.DistinctList<Inv.Material.ListItem>(MethodList.Count);

      var NavigationSurface = Base.Window.NewNavigationSurface();

      var ReflectionSurface = (Inv.Surface)NavigationSurface;

      NavigationSurface.Title = "Invention";
      NavigationSurface.Toolbar.Title = "Material";
      //NavigationSurface.Toolbar.AddTrailingIcon(Inv.Material.Resources.Images.SearchWhite);
      NavigationSurface.KeystrokeEvent += (Keystroke) =>
      {
        switch (Keystroke.Key)
        {
          case Inv.Key.Space:
            var MethodItem = MethodItemList[MethodIndex];
            MethodItem.SingleTap();

            NavigationSurface.Rearrange();  
            break;

          case Inv.Key.Up:
            NavigationSurface.GestureBackward();
            break;

          case Inv.Key.Down:
            NavigationSurface.GestureForward();
            break;
        }

        Debug.WriteLine("KEYSTROKE: " + Keystroke.ToString());
      };
      NavigationSurface.GestureBackwardEvent += () =>
      {
        MethodIndex--;
        if (MethodIndex < 0)
          MethodIndex = MethodList.Count - 1;

        var MethodItem = MethodItemList[MethodIndex];
        MethodItem.SingleTap();
      };
      NavigationSurface.GestureForwardEvent += () =>
      {
        MethodIndex++;
        if (MethodIndex >= MethodItemList.Count)
          MethodIndex = 0;

        var MethodItem = MethodItemList[MethodIndex];
        MethodItem.SingleTap();
      };

      var FirstFrame = true;
      var FirstSW = new Stopwatch();
      NavigationSurface.ArrangeEvent += () =>
      {
        var MethodInfo = MethodList[MethodIndex];

        Debug.WriteLine(MethodInfo.Name + ": loading...");
        FirstFrame = true;
      };
      NavigationSurface.ComposeEvent += () =>
      {
        var MethodInfo = MethodList[MethodIndex];

        if (FirstFrame)
        {
          FirstFrame = false;
          FirstSW.Reset();
          FirstSW.Start();
        }
        else if (FirstSW.IsRunning)
        {
          // SecondFrame.
          FirstSW.Stop();
          Debug.WriteLine(MethodInfo.Name + ": " + FirstSW.ElapsedMilliseconds + " ms");
        }
      };

      foreach (var MethodInfo in MethodList)
      {
        var MethodItem = NavigationSurface.Drawer.AddItem(MethodInfo.Name.ReverseCamel());
        MethodItem.SingleTapEvent += () =>
        {
          var MethodPanel = (Inv.Panel)MethodInfo.Invoke(this, new object[] { ReflectionSurface });

          NavigationSurface.Toolbar.Title = MethodInfo.Name.ReverseCamel();
          NavigationSurface.SetClient(MethodPanel);
          NavigationSurface.Rearrange();

          foreach (var OtherItem in MethodItemList)
            OtherItem.IsHighlighted = OtherItem == MethodItem;

          MethodIndex = MethodItemList.IndexOf(MethodItem);
        };

        if (Default != null && Default.GetReflectionInfo() == MethodInfo)
          MethodIndex = MethodItemList.Count;

        MethodItemList.Add(MethodItem);
      }

      var DefaultItem = MethodItemList[MethodIndex];
      DefaultItem.SingleTap();

      Base.Window.Transition(NavigationSurface);
    }
    private Inv.Label NewLabel(Inv.Surface Surface, string Text, Inv.Colour Colour)
    {
      var Result = Surface.NewLabel();
      Result.Text = Text;
      Result.Font.Size = 20;
      Result.Background.Colour = Colour;
      return Result;
    }

    public static implicit operator Inv.Application(Application Application)
    {
      return Application != null ? Application.Base : null;
    }

    private Inv.Material.Application Base;
    private Inv.Image LogoImage;
  }

  internal sealed class ComplexButton
  {
    public ComplexButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Padding.Set(10);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.SingleTapEvent += () =>
      {
        Debug.WriteLine("test");
      };
      
      var TestDock = Surface.NewHorizontalDock();
      Base.Content = TestDock;

      var TestFrame = Surface.NewFrame();
      TestDock.AddHeader(TestFrame);
      TestFrame.Background.Colour = Inv.Colour.DarkOrange;
      TestFrame.CornerRadius.Set(10);
      TestFrame.Size.Set(44, 44);
      TestFrame.Margin.Set(10);

      var TypeLabel = Surface.NewLabel();
      TestFrame.Content = TypeLabel;
      TypeLabel.Font.Size = 25;
      TypeLabel.Font.Colour = Inv.Colour.White;
      TypeLabel.Alignment.Center();
      TypeLabel.Text = "A";

      var StaffLabel = Surface.NewLabel();
      TestDock.AddClient(StaffLabel);
      StaffLabel.Font.Size = 22;
      StaffLabel.Text = "Callan Hodgskin";
      StaffLabel.Alignment.CenterLeft();
    }

    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }

    public static implicit operator Inv.Button(ComplexButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Button Base;
  }

  internal sealed class CustomPanel
  {
    public CustomPanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.Black.Opacity(0.75F);

      var HeaderDock = Surface.NewHorizontalDock();
      Base.AddHeader(HeaderDock);

      this.CaptionLabel = Surface.NewLabel();
      HeaderDock.AddHeader(CaptionLabel);
      CaptionLabel.Visibility.Collapse();
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      CaptionLabel.Padding.Set(10, 10, 0, 0);
      CaptionLabel.Alignment.TopLeft();

      this.DescriptionLabel = Surface.NewLabel();
      HeaderDock.AddClient(DescriptionLabel);
      DescriptionLabel.Visibility.Collapse();
      DescriptionLabel.Font.Size = 24;
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Padding.Set(0, 10, 10, 0);
      DescriptionLabel.Alignment.TopRight();

      this.FooterLabel = Surface.NewLabel();
      Base.AddFooter(FooterLabel);
      FooterLabel.Margin.Set(10);
      FooterLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      FooterLabel.Font.Size = 14;
      FooterLabel.Visibility.Collapse();
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set
      {
        CaptionLabel.Text = value;
        CaptionLabel.Visibility.Set(value != null);
      }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set
      {
        DescriptionLabel.Text = value;
        DescriptionLabel.Visibility.Set(value != null);
      }
    }
    public string Footer
    {
      set
      {
        FooterLabel.Text = value;
        FooterLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }

    public static implicit operator Inv.Panel(CustomPanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    private Inv.Dock Base;
    private Inv.Label CaptionLabel;
    private Inv.Label DescriptionLabel;
    private Inv.Label FooterLabel;
    private Inv.Panel ContentPanel;
  }

  public sealed class CustomButton
  {
    public CustomButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.SingleTapEvent += () =>
      {
        if (IsClickable && SingleTapEvent != null)
          SingleTapEvent();
      };
      Base.ContextTapEvent += () =>
      {
        if (IsClickable && ContextTapEvent != null)
          ContextTapEvent();
      };

      this.IsClickable = true;
    }

    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsFocused
    {
      get { return Base.IsFocusable; }
      set { Base.IsFocusable = value; }
    }
    public bool IsClickable
    {
      get { return IsClickableField; }
      set
      {
        if (IsClickableField != value)
        {
          this.IsClickableField = value;

          Base.Background.Colour = IsClickableField ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public Inv.CornerRadius CornerRadius
    {
      get { return Base.CornerRadius; }
    }
    public Inv.Colour? BorderColour
    {
      get { return Base.Border.Colour; }
      set { Base.Border.Colour = value; }
    }
    public Inv.Edge BorderThickness
    {
      get { return Base.Border.Thickness; }
    }
    public Inv.Colour? BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public event Action SingleTapEvent;
    public event Action ContextTapEvent;

    public static implicit operator Inv.Panel(CustomButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Button Base;
    private bool IsClickableField;
  }

  internal sealed class ShadeButton
  {
    public ShadeButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewButton();
      Base.Alignment.Stretch();

      ShadeBrush = Inv.Colour.Transparent;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour ShadeBrush
    {
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(ShadeButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Button Base;
  }

  public sealed class RecordPanel
  {
    public RecordPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      //this.Base = Surface.NewVerticalScroll();

      //this.Base = Surface.NewTable();
      //Base.AddColumn().Length.Star();

      this.Base = Surface.NewVerticalStack();

      this.ButtonList = new Inv.DistinctList<RecordButton>();
    }

    public Inv.Colour? BackgroundBrush
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Edge Padding
    {
      get { return Base.Padding; }
    }
    public event Action ClickEvent;

    public bool HasButtons()
    {
      return ButtonList.Count > 0;
    }
    public void RemoveButtons()
    {
      ButtonList.Clear();
      Base.RemovePanels();
    }
    public RecordButton AddButton()
    {
      var Result = new RecordButton(this);

      //var Row = Base.AddRow();
      //Row.Length.Fixed(80);
      //Base.GetCell(Base.GetColumn(0), Row).Content = Result;

      Base.AddPanel(Result);
      ButtonList.Add(Result);

      return Result;
    }

    internal void ClickInvoke()
    {
      if (ClickEvent != null)
        ClickEvent();
    }

    public static implicit operator Inv.Panel(RecordPanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }

    //private Inv.Table Base;
    private Inv.Stack Base;
    private Inv.DistinctList<RecordButton> ButtonList;
  }

  public sealed class RecordButton
  {
    internal RecordButton(RecordPanel Panel)
    {
      this.Panel = Panel;

      this.Base = Panel.Surface.NewHorizontalDock();
      Base.Background.Colour = Inv.Colour.Black;
      Base.Margin.Set(0, 0, 0, 2);

      this.RankLabel = Panel.Surface.NewLabel();
      Base.AddHeader(RankLabel);
      RankLabel.Alignment.Center();
      RankLabel.Margin.Set(16, 0, 0, 0);
      RankLabel.CornerRadius.Set(25);
      RankLabel.Border.Thickness.Set(2);
      RankLabel.Border.Colour = Inv.Colour.LightGray;
      RankLabel.Size.Set(50, 50);
      RankLabel.Font.Size = 25;
      RankLabel.Font.Colour = Inv.Colour.LightGray;
      RankLabel.JustifyCenter();

      this.FameLabel = Panel.Surface.NewLabel();
      Base.AddFooter(FameLabel);
      FameLabel.Alignment.Center();
      FameLabel.Font.Size = 40;
      FameLabel.Font.Colour = Inv.Colour.White;

      var IdentityStack = Panel.Surface.NewVerticalStack();
      Base.AddClient(IdentityStack);
      IdentityStack.Alignment.CenterLeft();
      IdentityStack.Margin.Set(20, 0, 20, 0);
      IdentityStack.Background.Colour = Inv.Colour.Blue;

      this.IdentityLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(IdentityLabel);
      //IdentityLabel.Alignment.CenterStretch();
      IdentityLabel.LineWrapping = true;
      IdentityLabel.Font.Size = 16;
      IdentityLabel.Font.Colour = Inv.Colour.White;

      this.SummaryLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(SummaryLabel);
      //SummaryLabel.Alignment.TopStretch();
      SummaryLabel.LineWrapping = true;
      SummaryLabel.Font.Size = 10;
      SummaryLabel.Font.Colour = Inv.Colour.DarkGray;
    }

    public bool IsHighlighted
    {
      set
      {
        Base.Background.Colour = value ? Inv.Colour.Purple : Inv.Colour.Black;
      }
    }
    public int Rank
    {
      set { RankLabel.Text = value.ToString("N0"); }
    }
    public int Fame
    {
      set { FameLabel.Text = value.ToString("N0"); }
    }
    public string Identity
    {
      set { IdentityLabel.Text = value; }
    }
    public string Summary
    {
      set { SummaryLabel.Text = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }

    public void AddGlyph(Inv.Image Image)
    {
      var Graphic = Panel.Surface.NewGraphic();
      Base.AddFooter(Graphic);
      Graphic.Size.Set(64, 64);
      Graphic.Image = Image;
    }

    public static implicit operator Inv.Panel(RecordButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private Inv.Dock Base;
    private Inv.Label RankLabel;
    private Inv.Label IdentityLabel;
    private Inv.Label FameLabel;
    private Inv.Label SummaryLabel;
    private RecordPanel Panel;
  }

  public sealed class TilePanel
  {
    public TilePanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new CustomPanel(Surface);
      Base.Caption = "DEVICE NAME: " + Surface.Window.Application.Device.Name;
      Base.Footer = "DEVICE MODEL/SYSTEM: " + Surface.Window.Application.Device.Model + " ... " + Surface.Window.Application.Device.System;

      var Scroll = Surface.NewVerticalScroll();
      Base.Content = Scroll;

      this.LayoutStack = Surface.NewVerticalStack();
      Scroll.Content = LayoutStack;
      LayoutStack.Margin.Set(10, 0, 10, 10);

      this.GlyphSize = Surface.Window.Width < 1024 ? 32 : 48;
    }

    public int GlyphSize { get; set; }
    public Inv.Colour? BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }

    public void RemoveButtons()
    {
      LayoutStack.RemovePanels();
    }
    public TileButton AddButton()
    {
      var Result = new TileButton(this);

      LayoutStack.AddPanel(Result);

      return Result;
    }
    public void AddButton(TileButton Button)
    {
      LayoutStack.AddPanel(Button);
    }
    public void AddSeparator(string Title)
    {
      var Result = new TileSeparator(this);

      Result.Text = Title;
      LayoutStack.AddPanel(Result);
    }
    public void RemoveButton(TileButton Button)
    {
      LayoutStack.RemovePanel(Button);
    }

    public static implicit operator Inv.Panel(TilePanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }

    private CustomPanel Base;
    private Inv.Stack LayoutStack;
  }

  public interface TileItem
  {
    Inv.Panel Base { get; }
  }

  public sealed class TileSeparator : TileItem
  {
    public TileSeparator(TilePanel Panel)
    {
      this.Base = Panel.Surface.NewLabel();
      Base.Margin.Set(0, 0, 5, 0);
      Base.JustifyRight();
      Base.Font.Colour = Inv.Colour.White;
      Base.Font.Size = 12;
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    public static implicit operator Inv.Panel(TileSeparator Item)
    {
      return Item != null ? Item.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private Inv.Label Base;
  }

  public sealed class TileButton : TileItem
  {
    public TileButton(TilePanel Panel)
    {
      this.Base = new CustomButton(Panel.Surface);
      Base.Margin.Set(1);

      var HorizontalDock = Panel.Surface.NewHorizontalDock();
      Base.Content = HorizontalDock;

      this.Graphic = Panel.Surface.NewGraphic();
      HorizontalDock.AddHeader(Graphic);
      Graphic.Size.Set(Panel.GlyphSize, Panel.GlyphSize);
      Graphic.Visibility.Collapse();

      var VerticalStack = Panel.Surface.NewVerticalStack();
      HorizontalDock.AddClient(VerticalStack);
      VerticalStack.Alignment.CenterLeft();

      this.TitleLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(TitleLabel);
      TitleLabel.Padding.Set(5, 5, 0, 0);
      TitleLabel.Font.Size = 18;
      TitleLabel.Font.Colour = Inv.Colour.Yellow;
      TitleLabel.Visibility.Collapse();

      this.DescriptionLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Padding.Set(5, 0, 5, 0);
      DescriptionLabel.Font.Size = Panel.GlyphSize < 48 ? 14 : 16;
      DescriptionLabel.Font.Colour = Inv.Colour.White;
      DescriptionLabel.LineWrapping = true;
      DescriptionLabel.Visibility.Collapse();
    }

    public Inv.Image? Image
    {
      set
      {
        Graphic.Image = value;
        Graphic.Visibility.Set(value != null);
      }
    }
    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(value != null);
      }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set
      {
        DescriptionLabel.Text = value;
        DescriptionLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.BackgroundColour = IsCheckedField ? ColourField : Inv.Colour.Transparent;
        }
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        Base.BackgroundColour = value ? ColourField : Inv.Colour.Transparent;
      }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(TileButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private CustomButton Base;
    private Inv.Graphic Graphic;
    private Inv.Label TitleLabel;
    private Inv.Label DescriptionLabel;
    private bool IsCheckedField;
    private Inv.Colour ColourField;
  }

  public sealed class MenuPanel
  {
    public MenuPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = null;

      this.ButtonStack = Surface.NewHorizontalStack();
      Base.AddFooter(ButtonStack);
    }

    public Inv.Colour? BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }

    public bool HasButtons()
    {
      return ButtonStack.HasPanels();
    }
    public void RemoveButtons()
    {
      ButtonStack.RemovePanels();
    }
    public MenuButton AddButton()
    {
      var Result = new MenuButton(this);

      ButtonStack.AddPanel(Result);

      return Result;
    }

    public static implicit operator Inv.Panel(MenuPanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }

    private Inv.Dock Base;
    private Inv.Stack ButtonStack;
    private Inv.Panel ContentPanel;
  }

  public sealed class MenuButton
  {
    public MenuButton(MenuPanel Panel)
    {
      var ButtonSize = (Panel.Size.Width.Value / 4) - 10;
      var ImageSize = ButtonSize / 2;

      this.Base = new CustomButton(Panel.Surface);
      Base.Size.Set(ButtonSize, ButtonSize);
      Base.CornerRadius.Set(ImageSize);
      Base.BackgroundColour = Inv.Colour.DimGray;
      Base.Margin.Set(8, 8, 0, 8);

      this.Graphic = Panel.Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(ImageSize, ImageSize);
      Graphic.Alignment.Center();
      Graphic.Visibility.Collapse();
    }

    public Inv.Image? Image
    {
      set
      {
        Graphic.Image = value;
        Graphic.Visibility.Set(value != null);
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        if (IsCheckedField != value)
        {
          this.IsCheckedField = value;

          Base.BackgroundColour = value ? Inv.Colour.DarkGreen : Inv.Colour.DimGray;
        }
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }

    public void LinkPanel(Inv.Panel Panel)
    {
      Panel.Visibility.ChangeEvent += () =>
      {
        IsChecked = Panel.Visibility.Get();
      };
    }

    public static implicit operator Inv.Panel(MenuButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private CustomButton Base;
    private bool IsCheckedField;
    private Inv.Graphic Graphic;
  }

  public sealed class ActionPanel
  {
    public ActionPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = new CustomPanel(Surface);

      var LayoutDock = Surface.NewHorizontalDock();
      Base.Content = LayoutDock;
      LayoutDock.Margin.Set(20);

      var PortraitOverlay = Surface.NewOverlay();
      LayoutDock.AddHeader(PortraitOverlay);
      PortraitOverlay.Size.Set(160, 160);
      PortraitOverlay.Margin.Set(0, 0, 20, 0);

      this.Graphic = Surface.NewGraphic();
      PortraitOverlay.AddPanel(Graphic);
      Graphic.Size.Set(128, 128);
      Graphic.Visibility.Collapse();
      Graphic.Alignment.TopCenter();

      this.TitleLabel = Surface.NewLabel();
      PortraitOverlay.AddPanel(TitleLabel);
      TitleLabel.Alignment.BottomCenter();
      TitleLabel.Font.Size = 18;
      TitleLabel.LineWrapping = true;
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Visibility.Collapse();

      //var Scroll = Surface.NewVerticalScroll();
      //LayoutDock.AddClient(Scroll);
      //Scroll.Background.Colour = Inv.Colour.Red;

      this.StackDock = Surface.NewHorizontalDock();
      LayoutDock.AddClient(StackDock);
      StackDock.Background.Colour = Inv.Colour.Green;
    }

    public string Caption
    {
      get { return Base.Caption; }
      set { Base.Caption = value; }
    }
    public string Description
    {
      get { return Base.Description; }
      set { Base.Description = value; }
    }
    public string Footer
    {
      set { Base.Footer = value; }
    }
    public Inv.Colour? BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public string Title
    {
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Image? Image
    {
      set
      {
        Graphic.Image = value;
        Graphic.Visibility.Set(value != null);
      }
    }

    public bool HasButtons()
    {
      return StackDock.HasPanels();
    }
    public void RemoveButtons()
    {
      StackDock.RemovePanels();
    }
    public ActionButton AddLeftButton()
    {
      var Result = new ActionButton(this);

      StackDock.AddHeader(Result);

      return Result;
    }
    public ActionButton AddRightButton()
    {
      var Result = new ActionButton(this);

      StackDock.AddFooter(Result);

      return Result;
    }
    public void Focus()
    {
      if (StackDock.HasHeaders())
        Surface.SetFocus(StackDock.HeaderAt(0));
    }

    public static implicit operator Inv.Panel(ActionPanel Panel)
    {
      return Panel != null ? Panel.Base : null;
    }

    internal Inv.Surface Surface { get; private set; }

    private CustomPanel Base;
    private Inv.Dock StackDock;
    private Inv.Graphic Graphic;
    private Inv.Label TitleLabel;
  }

  public sealed class ActionButton
  {
    public ActionButton(ActionPanel Panel)
    {
      this.Base = new CustomButton(Panel.Surface);
      Base.IsFocused = true;
      Base.Margin.Set(0, 10, 10, 10);
      Base.Size.Set(128, 128);

      this.CaptionLabel = Panel.Surface.NewLabel();
      Base.Content = CaptionLabel;
      CaptionLabel.Alignment.Center();
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.White;
      CaptionLabel.LineWrapping = true;
      CaptionLabel.Visibility.Collapse();
    }

    public string Caption
    {
      set
      {
        CaptionLabel.Text = value;
        CaptionLabel.Visibility.Set(value != null);
      }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(ActionButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private CustomButton Base;
    private Inv.Label CaptionLabel;
  }

  public sealed class CaptionButton
  {
    public CaptionButton(Inv.Surface Surface)
    {
      this.Base = new CustomButton(Surface);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.JustifyCenter();
      Label.Font.Size = 18;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Edge Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Edge Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.BackgroundColour = value;
        }
      }
    }
    public string Text
    {
      set { Label.Text = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public static implicit operator Inv.Panel(CaptionButton Button)
    {
      return Button != null ? Button.Base : null;
    }

    private CustomButton Base;
    private Inv.Label Label;
    private Inv.Colour ColourField;
  }
}