/*! 7 !*/

namespace InvTest
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvTest.InvResourcePackage.rs");
    }

    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>74.1 KB</Summary>
    public readonly global::Inv.Image PhoenixLogo960x540;
    ///<Summary>2.8 KB</Summary>
    public readonly global::Inv.Image SoundOff256x256;
    ///<Summary>3.3 KB</Summary>
    public readonly global::Inv.Image SoundOn256x256;
    ///<Summary>193.5 KB</Summary>
    public readonly global::Inv.Image Waves;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>19.2 KB</Summary>
    public readonly global::Inv.Sound Clang;
  }
}