﻿/*! 16 !*/
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.OS;
using System.IO;
using System.Diagnostics;
using Inv.Support;

namespace InvTest
{
  [Activity(
    Label = "InvAndroidTest", 
    MainLauncher = true,
    //AlwaysRetainTaskState = true,
    //FinishOnTaskLaunch = false,
    LaunchMode = Android.Content.PM.LaunchMode.SingleTop,
    Theme = "@android:style/Theme.NoTitleBar", 
    Icon = "@drawable/icon", 
    ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    public override Inv.Application CreateApplication()
    {
      return new InvTest.Application();
    }
  }

  //[Activity(Label = "InvAndroidTest", MainLauncher = true, Theme = "@android:style/Theme.NoTitleBar", Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class OtherActivity : Android.App.Activity
  {
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as Exception;
        if (Exception != null)
          System.Diagnostics.Debug.WriteLine(Exception.Message);
      };

      // TODO: this is not re-entrant.
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += (Sender, Event) =>
      {
        var Exception = Event.Exception;
        if (Exception != null)
          System.Diagnostics.Debug.WriteLine(Exception.Message);
        Event.Handled = true;
      };
      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);

      try
      {
        // NOTE: this is a deliberate workaround, we need to use a different enum and cast it to get the desired behaviour.
        var UiFlags = Android.Views.SystemUiFlags.Fullscreen | Android.Views.SystemUiFlags.HideNavigation | Android.Views.SystemUiFlags.ImmersiveSticky;
        Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;

        base.OnCreate(bundle);

        var Container = new Inv.AndroidLayoutContainer(this);
        SetContentView(Container);
        Container.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
        Container.SetBackgroundColor(Android.Graphics.Color.Pink);
        Container.SetContentHorizontal(Inv.AndroidLayoutHorizontal.Center);
        Container.SetContentVertical(Inv.AndroidLayoutVertical.Center);

        var Button = new Inv.AndroidLayoutButton(this);
        Container.SetContentElement(Button);
        Button.SetPadding(20, 20, 20, 20);
        Button.Background.SetColor(Android.Graphics.Color.Green);
        Button.Background.SetBorderStroke(Android.Graphics.Color.Red, 10);
        Button.Click += (Sender, Event) => Button.RequestLayout();

        var Label = new Inv.AndroidLayoutLabel(this);
        Button.SetContentView(Label);
        //Container.SetContentElement(Label);
        Label.Text = "Hello World";
        Label.SetTextColor(Android.Graphics.Color.White);
        Label.SetBackgroundColor(Android.Graphics.Color.Purple);
        Label.SetTextSize(Android.Util.ComplexUnitType.Sp, 20);
      }
      catch (Exception Exception)
      {
        System.Diagnostics.Debug.WriteLine(Exception.Message);
      }
    }
  }
}