﻿/*! 13 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace InvTest
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        //Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.Nexus9;
        Inv.WpfShell.DeviceRotation = false;
        Inv.WpfShell.RespectPhysicalDimensions = false;
        //Inv.WpfShell.FullScreenMode = false;

        var Manifest = new Manifest();
        var Schematic = Manifest.CompileMapping();

        Inv.WpfShell.Run(new InvTest.Application());
      });
    }
  }

  public sealed class Manifest : Inv.Storage.Contract<Inv.Storage.Manifest>
  {
    public Manifest()
    {
      this.Base = new Inv.Storage.Manifest();
      Base.AddRoot<Profile>();
      Base.AddRoot<Record>();
      Base.AddRoot<Register>();
    }

    public Inv.Storage.Mapping CompileMapping()
    {
      return Base.CompileMapping();
    }

    Inv.Storage.Manifest Inv.Storage.Contract<Inv.Storage.Manifest>.Base
    {
      get { return Base; }
    }

    private Inv.Storage.Manifest Base;
  }

  public sealed class Profile
  {
    public string Name { get; set; }
    public string Email { get; set; }
    public int? Points { get; set; }
    public Inv.DistinctList<Register> Register { get; set; }
  }

  public sealed class Record
  {
    public string Identity { get; set; }
    public DateTimeOffset Timestamp { get; set; }
    public int Score { get; set; }
  }

  public sealed class Register
  {
    public string Identity { get; set; }
    public string Handle { get; set; }
  }
}
