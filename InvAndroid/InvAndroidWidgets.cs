/*! 28 !*/
using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Inv.Support;

namespace Inv
{
  internal sealed class AndroidLayoutView : Android.Widget.RelativeLayout
  {
    public AndroidLayoutView(Android.Content.Context context)
      : base(context)
    {
    }
  }

  internal sealed class AndroidSurfaceView : LinearLayout
  {
    public AndroidSurfaceView(Android.Content.Context context)
      : base(context)
    {
      this.ContentParams = new AndroidLinearLayout.LayoutParams(AndroidLinearLayout.LayoutParams.MatchParent, AndroidLinearLayout.LayoutParams.MatchParent);
    }

    public void SetContentView(Android.Views.View View)
    {
      if (ContentView != View)
      {
        if (ContentView != null)
          RemoveView(ContentView);

        this.ContentView = View;

        if (ContentView != null)
        {
          AddView(ContentView);
          ContentView.LayoutParameters = ContentParams;
        }
      }
    }

    private LayoutParams ContentParams;
    private View ContentView;
  }

  public sealed class AndroidBackground : Android.Graphics.Drawables.GradientDrawable
  {
    public AndroidBackground()
    {
    }

    public void SetCornerRadius(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      this.SetCornerRadii(new float[] { TopLeft, TopLeft, TopRight, TopRight, BottomRight, BottomRight, BottomLeft, BottomLeft });
    }
    public void SetBorderStroke(Android.Graphics.Color Color, int Width)
    {
      this.SetStroke(Width, Color);
    }
  }

  internal sealed class AndroidRenderView : Android.Views.View, Android.Views.GestureDetector.IOnGestureListener, ScaleGestureDetector.IOnScaleGestureListener
  {
    public AndroidRenderView(Android.Content.Context context)
      : base(context)
    {
      //this.Clickable = true; // NOTE: does not seem to be required when you are directly handling touch events.

      /*
      this.GestureDetector = new Android.Views.GestureDetector(this);

      GestureDetector.SingleTapConfirmed += (Sender, Event) =>
      {
        if (SingleTapEvent != null)
          SingleTapEvent((int)Event.Event.GetX(), (int)Event.Event.GetY());
      };
      GestureDetector.DoubleTapEvent += (Sender, Event) =>
      {
        if (DoubleTapEvent != null)
          DoubleTapEvent((int)Event.Event.GetX(), (int)Event.Event.GetY());
      };
      */
      this.ScaleDetector = new ScaleGestureDetector(context, this);

      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public event Action<int, int> PressEvent;
    public event Action<int, int> ReleaseEvent;
    public event Action<int, int> MoveEvent;
    public event Action<int, int> SingleTapEvent;
    public event Action<int, int> DoubleTapEvent;
    public event Action<int, int> LongPressEvent;
    public event Action<int> ZoomEvent;
    public event Action<Android.Graphics.Canvas> DrawEvent;

    public override void Draw(Android.Graphics.Canvas Canvas)
    {
      base.Draw(Canvas);

      if (DrawEvent != null)
        DrawEvent(Canvas);
    }
    public override bool OnTouchEvent(MotionEvent Event)
    {
      //Debug.WriteLine(Event.Action.ToString());

      ScaleDetector.OnTouchEvent(Event);
      //GestureDetector.OnTouchEvent(Event);

      switch (Event.Action & MotionEventActions.Mask)
      {
        case MotionEventActions.Down:
          if (Handler != null)
            Handler.RemoveCallbacks(LongPressedInvoke);

          this.LastX = (int)Event.GetX();
          this.LastY = (int)Event.GetY();

          var CurrentTime = Environment.TickCount;
          var IsDoubleTap = CurrentTime - LastTime < Android.Views.ViewConfiguration.DoubleTapTimeout;
          if (IsDoubleTap)
          {
            if (IsPressed)
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }

            if (DoubleTapEvent != null)
              DoubleTapEvent(LastX, LastY);
          }
          else
          {
            if (IsPressed && Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
            else
              this.IsPressed = true;

            if (Handler != null)
              Handler.PostDelayed(LongPressedInvoke, Android.Views.ViewConfiguration.LongPressTimeout);

            if (PressEvent != null)
              PressEvent(LastX, LastY);
          }
          this.LastTime = CurrentTime;
          break;

        case MotionEventActions.Move:
          var MoveX = Event.GetX();
          var MoveY = Event.GetY();

          if (MoveEvent != null)
            MoveEvent((int)MoveX, (int)MoveY);

          if (IsPressed)
          {
            var Distance = ((MoveY - LastY) * (MoveY - LastY)) + ((MoveX - LastX) * (MoveX - LastX));
            if (Distance > 400) // sqrt(400) == 20
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }
          }
          break;

        case MotionEventActions.Up:
          var ReleaseX = (int)Event.GetX();
          var ReleaseY = (int)Event.GetY();

          if (IsPressed)
          {
            this.IsPressed = false;

            if (SingleTapEvent != null)
              SingleTapEvent(ReleaseX, ReleaseY);

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }

          if (ReleaseEvent != null)
            ReleaseEvent(ReleaseX, ReleaseY);
          break;

        case MotionEventActions.Cancel:
          if (IsPressed)
          {
            this.IsPressed = false;

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }
          break;
      }

      return true;
    }

    public void Render()
    {
      Invalidate();
    }

    private void SingleTapInvoke()
    {
    }
    private void LongPressedInvoke()
    {
      this.IsPressed = false;

      if (LongPressEvent != null)
        LongPressEvent(LastX, LastY);
    }

    bool GestureDetector.IOnGestureListener.OnDown(MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      SingleTapInvoke();

      return true;
    }
    bool GestureDetector.IOnGestureListener.OnFling(MotionEvent Event1, MotionEvent Event2, float velocityX, float velocityY)
    {
      return true;
    }
    void GestureDetector.IOnGestureListener.OnLongPress(MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      LongPressedInvoke();
    }
    bool GestureDetector.IOnGestureListener.OnScroll(MotionEvent Event1, MotionEvent Event2, float distanceX, float distanceY)
    {
      return true;
    }
    void GestureDetector.IOnGestureListener.OnShowPress(MotionEvent Event)
    {
    }
    bool GestureDetector.IOnGestureListener.OnSingleTapUp(MotionEvent Event)
    {
      return true;
    }
    bool ScaleGestureDetector.IOnScaleGestureListener.OnScale(ScaleGestureDetector detector)
    {
      if (ZoomEvent != null)
        ZoomEvent((int)(detector.ScaleFactor) > 0 ? +1 : -1);

      return true;
    }
    bool ScaleGestureDetector.IOnScaleGestureListener.OnScaleBegin(ScaleGestureDetector detector)
    {
      return true;
    }
    void ScaleGestureDetector.IOnScaleGestureListener.OnScaleEnd(ScaleGestureDetector detector)
    {
    }

    private int LastTime;
    private int LastX;
    private int LastY;
    private bool IsPressed;
    private ScaleGestureDetector ScaleDetector;
    //private GestureDetector GestureDetector;
  }

  internal sealed class AndroidButtonView : LinearLayout
  {
    public AndroidButtonView(Android.Content.Context context)
      : base(context)
    {
      this.Clickable = true;
      base.Background = new AndroidBackground();

      this.ContentParams = new AndroidButtonView.LayoutParams(AndroidButtonView.LayoutParams.MatchParent, AndroidButtonView.LayoutParams.MatchParent);
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public void SetContentView(Android.Views.View View)
    {
      if (ContentView != View)
      {
        if (ContentView != null)
          RemoveView(ContentView);

        this.ContentView = View;

        if (ContentView != null)
        {
          AddView(ContentView);
          ContentView.LayoutParameters = ContentParams;
        }
      }
    }

    private LayoutParams ContentParams;
    private View ContentView;
  }

  internal sealed class AndroidCanvasView : Android.Widget.RelativeLayout
  {
    public AndroidCanvasView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }
  }

  internal sealed class AndroidEditView : Android.Widget.EditText
  {
    public AndroidEditView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      this.ImeOptions = Android.Views.InputMethods.ImeAction.Done;
      this.SetSingleLine(true);
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }
  }

  internal sealed class AndroidMemoView : Android.Widget.EditText
  {
    public AndroidMemoView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      Gravity = GravityFlags.Top;
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public bool IsReadOnly
    {
      get { return this.KeyListener == null; }
      set
      {
        if (IsReadOnly != value)
        {
          var Swap = this.ReadOnlyKeyListener;
          this.ReadOnlyKeyListener = this.KeyListener;
          this.KeyListener = Swap;
        }
      }
    }

    private Android.Text.Method.IKeyListener ReadOnlyKeyListener;
  }

  internal sealed class AndroidGraphicView : Android.Widget.ImageView
  {
    public AndroidGraphicView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }
  }

  internal sealed class AndroidLabelView : Android.Widget.TextView
  {
    public AndroidLabelView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      Ellipsize = Android.Text.TextUtils.TruncateAt.End;
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }
  }

  internal sealed class AndroidOverlayView : Android.Widget.RelativeLayout
  {
    public AndroidOverlayView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public override bool OnInterceptTouchEvent(MotionEvent ev)
    {
      return false;
    }
  }

  internal sealed class AndroidScrollView : Android.Widget.ScrollView
  {
    public AndroidScrollView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      this.ContentParams = new AndroidScrollView.LayoutParams(AndroidScrollView.LayoutParams.MatchParent, AndroidScrollView.LayoutParams.MatchParent);
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public void SetContentView(View View)
    {
      if (ContentView != View)
      {
        if (ContentView != null)
          RemoveView(ContentView);

        this.ContentView = View;

        if (ContentView != null)
        {
          AddView(ContentView);
          ContentView.LayoutParameters = ContentParams;
        }
      }
    }

    private View ContentView;
    private LayoutParams ContentParams;
  }

  public sealed class AndroidFrameView : LinearLayout
  {
    public AndroidFrameView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();

      this.ContentParams = new AndroidFrameView.LayoutParams(AndroidFrameView.LayoutParams.MatchParent, AndroidFrameView.LayoutParams.MatchParent);
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public void SetContentView(Android.Views.View View)
    {
      if (ContentView != View)
      {
        if (ContentView != null)
          RemoveView(ContentView);

        this.ContentView = View;

        if (ContentView != null)
        {
          AddView(ContentView);
          ContentView.LayoutParameters = ContentParams;
        }
      }
    }

    private LayoutParams ContentParams;
    private View ContentView;
  }

  public class AndroidLinearLayout : LinearLayout
  {
    public AndroidLinearLayout(Context Context)
      : base(Context)
    {
    }

    public override bool OnInterceptTouchEvent(MotionEvent ev)
    {
      return false;
    }
  }

  internal sealed class AndroidStackView : AndroidLinearLayout
  {
    public AndroidStackView(Android.Content.Context context)
      : base(context)
    {
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    internal void ComposeElements(IEnumerable<AndroidContainerLayout> Elements)
    {
      RemoveAllViews();
      foreach (var Element in ElementList)
      {
        switch (Orientation)
        {
          case Android.Widget.Orientation.Vertical:
            Element.LayoutParameters = new AndroidStackView.LayoutParams(AndroidLinearLayout.LayoutParams.MatchParent, AndroidLinearLayout.LayoutParams.WrapContent);
            break;

          case Android.Widget.Orientation.Horizontal:
            Element.LayoutParameters = new AndroidStackView.LayoutParams(AndroidLinearLayout.LayoutParams.WrapContent, AndroidLinearLayout.LayoutParams.MatchParent);
            break;

          default:
            throw new System.ApplicationException("Unknown orientation");
        }

        AddView(Element);
      }
    }
  }

  internal sealed class AndroidDockView : AndroidLinearLayout
  {
    public AndroidDockView(Context Context)
      : base(Context)
    {
      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    internal void ComposeElements(IEnumerable<AndroidContainerLayout> Headers, IEnumerable<AndroidContainerLayout> Clients, IEnumerable<AndroidContainerLayout> Footers)
    {
      RemoveAllViews();

      foreach (var Header in Headers)
      {
        switch (Orientation)
        {
          case Android.Widget.Orientation.Vertical:
            Header.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);
            break;

          case Android.Widget.Orientation.Horizontal:
            Header.LayoutParameters = new LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            break;

          default:
            throw new System.ApplicationException("Unknown orientation");
        }

        AddView(Header);
      }

      var ClientList = Clients.ToDistinctList();

      // TODO: if there's no clients, do we really need to add a dummy client?
      if (ClientList.Count == 0)
        ClientList.Add(new AndroidContainerLayout(Context));

      foreach (var Client in ClientList)
      {
        switch (Orientation)
        {
          case Android.Widget.Orientation.Vertical:
            Client.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent, 1);
            break;

          case Android.Widget.Orientation.Horizontal:
            Client.LayoutParameters = new LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent, 1);
            break;

          default:
            throw new System.ApplicationException("Unknown orientation");
        }

        AddView(Client);
      }

      foreach (var Footer in Footers)
      {
        switch (Orientation)
        {
          case Android.Widget.Orientation.Vertical:
            Footer.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);
            break;

          case Android.Widget.Orientation.Horizontal:
            Footer.LayoutParameters = new LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            break;

          default:
            throw new System.ApplicationException("Unknown orientation");
        }

        AddView(Footer);
      }
    }
  }

  internal sealed class AndroidTableView : GridLayout
  {
    public AndroidTableView(Android.Content.Context context)
      : base(context)
    {
      this.UseDefaultMargins = false;
      this.Orientation = GridOrientation.Vertical;

      base.Background = new AndroidBackground();
    }

    public new AndroidBackground Background
    {
      get { return (AndroidBackground)base.Background; }
    }

    public void RemoveElements()
    {
      RemoveAllViews();
    }
  }

  public class AndroidContainerLayout : AndroidLinearLayout
  {
    public AndroidContainerLayout(Context Context)
      : base(Context)
    {
      this.Orientation = Orientation.Vertical;

      this.HorizontalLayout = new AndroidLinearLayout(Context);
      HorizontalLayout.Orientation = Orientation.Horizontal;

      this.HorizontalParams = new AndroidLinearLayout.LayoutParams(AndroidLinearLayout.LayoutParams.MatchParent, AndroidLinearLayout.LayoutParams.MatchParent);
      this.VerticalParams = new AndroidLinearLayout.LayoutParams(AndroidLinearLayout.LayoutParams.MatchParent, AndroidLinearLayout.LayoutParams.MatchParent);
    }

    public void SetContentView(Android.Views.View View)
    {
      if (ContentView != View)
      {
        this.ContentView = View;

        ResetContent();
      }
    }
    public void SetContentVerticalCenter()
    {
      SetContentVertical(Android.Views.GravityFlags.CenterVertical);
    }
    public void SetContentHorizontalCenter()
    {
      SetContentHorizontal(Android.Views.GravityFlags.CenterHorizontal);
    }
    public void SetContentVerticalTop()
    {
      SetContentVertical(Android.Views.GravityFlags.Top);
    }
    public void SetContentVerticalBottom()
    {
      SetContentVertical(Android.Views.GravityFlags.Bottom);
    }
    public void SetContentHorizontalLeft()
    {
      SetContentHorizontal(Android.Views.GravityFlags.Left);
    }
    public void SetContentHorizontalRight()
    {
      SetContentHorizontal(Android.Views.GravityFlags.Right);
    }
    public void SetContentHorizontalStretch()
    {
      SetContentHorizontal(Android.Views.GravityFlags.NoGravity);
    }
    public void SetContentVerticalStretch()
    {
      SetContentVertical(Android.Views.GravityFlags.NoGravity);
    }
    public void SetContentWidth(int ContentWidth)
    {
      VerticalParams.Width = ContentWidth;

      if (HorizontalParams.Gravity == GravityFlags.NoGravity)
      {
        HorizontalParams.Width = AndroidLinearLayout.LayoutParams.WrapContent;
        HorizontalParams.Gravity = GravityFlags.CenterHorizontal;
      }
    }
    public void SetContentHeight(int ContentHeight)
    {
      VerticalParams.Height = ContentHeight;

      if (VerticalParams.Gravity == GravityFlags.NoGravity)
      {
        //VerticalParams.Height = AndroidLinearLayout.LayoutParams.WrapContent;
        VerticalParams.Gravity = GravityFlags.CenterVertical;
      }
    }
    public void SetContentMinimumWidth(int MinimumWidth)
    {
      ContentView.SetMinimumWidth(MinimumWidth);
    }
    public void SetContentMinimumHeight(int MinimumHeight)
    {
      ContentView.SetMinimumHeight(MinimumHeight);
    }
    public void SetContentMaximumWidth(int MaximumWidth)
    {
      this.ContentMaximumWidth = MaximumWidth;
      RequestLayout();
    }
    public void SetContentMaximumHeight(int MaximumHeight)
    {
      this.ContentMaximumHeight = MaximumHeight;
      RequestLayout();
    }
    public void SetContentMargins(int Left, int Top, int Right, int Bottom)
    {
      HorizontalParams.SetMargins(0, Top, 0, Bottom);
      VerticalParams.SetMargins(Left, 0, Right, 0);
      RequestLayout();
    }
    public void ClearContentWidth()
    {
      if (VerticalParams.Width >= 0)
        VerticalParams.Width = AndroidLinearLayout.LayoutParams.MatchParent;
    }
    public void ClearContentHeight()
    {
      if (VerticalParams.Height >= 0)
        VerticalParams.Height = AndroidLinearLayout.LayoutParams.MatchParent;
    }
    public void ClearContentMinimumWidth()
    {
      ContentView.SetMinimumWidth(0);
    }
    public void ClearContentMinimumHeight()
    {
      ContentView.SetMinimumHeight(0);
    }
    public void ClearContentMaximumWidth()
    {
      this.ContentMaximumWidth = 0;
      RequestLayout();
    }
    public void ClearContentMaximumHeight()
    {
      this.ContentMaximumHeight = 0;
      RequestLayout();
    }
    public void ResetContent()
    {
      if (ContentView == null)
      {
        if (ChildCount > 0)
        {
          HorizontalLayout.RemoveAllViews();
          this.RemoveAllViews();
        }
      }
      else
      {
        // NOTE: can't do this optimisation in reverse as vertical alignment does work without the horizontal layout parent.
        if (
          //HorizontalParams.Gravity == GravityFlags.NoGravity && 
          //HorizontalParams.Width == AndroidLinearLayout.LayoutParams.MatchParent && HorizontalParams.Height == AndroidLinearLayout.LayoutParams.MatchParent && 
          //HorizontalParams.TopMargin == 0 && HorizontalParams.BottomMargin == 0 && 
            VerticalParams.Gravity == GravityFlags.NoGravity &&
            VerticalParams.Width == AndroidLinearLayout.LayoutParams.MatchParent && VerticalParams.Height == AndroidLinearLayout.LayoutParams.MatchParent &&
            VerticalParams.LeftMargin == 0 && VerticalParams.RightMargin == 0
           )
        {
          if (ContentView.Parent != this)
          {
            HorizontalLayout.RemoveAllViews();
            this.RemoveAllViews();
            this.AddView(ContentView);
            ContentView.LayoutParameters = HorizontalParams;
          }
        }
        else
        {
          if (ContentView.Parent != HorizontalLayout)
          {
            HorizontalLayout.RemoveAllViews();
            this.RemoveAllViews();

            this.AddView(HorizontalLayout);
            HorizontalLayout.LayoutParameters = HorizontalParams;

            HorizontalLayout.AddView(ContentView);
            ContentView.LayoutParameters = VerticalParams;
          }
        }
      }
    }

    protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
      if (ContentMaximumWidth > 0)
        widthMeasureSpec = MeasureSpec.MakeMeasureSpec(ContentMaximumWidth, MeasureSpecMode.AtMost);

      if (ContentMaximumHeight > 0)
        heightMeasureSpec = MeasureSpec.MakeMeasureSpec(ContentMaximumHeight, MeasureSpecMode.AtMost);

      base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void SetContentVertical(Android.Views.GravityFlags GravityFlags)
    {
      VerticalParams.Width = AndroidLinearLayout.LayoutParams.MatchParent;
      
      if (GravityFlags == Android.Views.GravityFlags.NoGravity)
        VerticalParams.Height = AndroidLinearLayout.LayoutParams.MatchParent;
      else
        VerticalParams.Height = AndroidLinearLayout.LayoutParams.WrapContent;
      
      VerticalParams.Gravity = GravityFlags;
    }
    private void SetContentHorizontal(Android.Views.GravityFlags GravityFlags)
    {
      HorizontalParams.Height = AndroidLinearLayout.LayoutParams.MatchParent;

      if (GravityFlags == Android.Views.GravityFlags.NoGravity)
        HorizontalParams.Width = AndroidLinearLayout.LayoutParams.MatchParent;
      else
        HorizontalParams.Width = AndroidLinearLayout.LayoutParams.WrapContent;

      HorizontalParams.Gravity = GravityFlags;
    }

    private AndroidLinearLayout HorizontalLayout;
    private AndroidLinearLayout.LayoutParams HorizontalParams;
    private AndroidLinearLayout.LayoutParams VerticalParams;
    private Android.Views.View ContentView;
    private int ContentMaximumWidth;
    private int ContentMaximumHeight;
  }
}